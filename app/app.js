'use strict';

const express = require('./express');

module.exports.start = function start(callback) {
    var app = express.init();
    app.listen(3000, function () {
        console.log('listening on port : ' + 3000);
    });

};
