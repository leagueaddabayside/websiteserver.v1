/**
 * Created by adityagupta on 1/5/17.
 */
const seoMetaTitles={
    league:{
        metaTitle : 'LeagueAdda - Live Leagues',
            metaKeywords : 'LeagueAdda - Live Leagues',
            metaDescription : 'LeagueAdda - Live Leagues',
    },
    matchLeagues:{
        metaTitle : 'LeagueAdda - matchLeagues',
            metaKeywords : 'LeagueAdda - matchLeagues',
            metaDescription : 'LeagueAdda - matchLeagues',
    },
    browseLeague:{
        metaTitle : 'LeagueAdda - browseLeague',
            metaKeywords : 'LeagueAdda - browseLeague',
            metaDescription : 'LeagueAdda - browseLeague',
    },
    myJoinedLeague:{
        metaTitle : 'LeagueAdda - myJoinedLeague',
            metaKeywords : 'LeagueAdda - myJoinedLeague',
            metaDescription : 'LeagueAdda - myJoinedLeague',
    },
    myTeam:{
        metaTitle : 'LeagueAdda - My team',
            metaKeywords : 'LeagueAdda - My team',
            metaDescription : 'LeagueAdda - My team',
    },
    editteam:{
        metaTitle : 'LeagueAdda - Edit Team',
            metaKeywords : 'LeagueAdda - Edit Team',
            metaDescription : 'LeagueAdda - Edit Team',
    },
    page:{
        metaTitle : 'LeagueAdda - page Leagues',
            metaKeywords : 'LeagueAdda - page Leagues',
            metaDescription : 'LeagueAdda - page Leagues',
    },
    myprofile:{
        metaTitle : 'LeagueAdda - myprofile',
            metaKeywords : 'LeagueAdda - myprofile',
            metaDescription : 'LeagueAdda - myprofile',
    },
    myaccount:{
        metaTitle : 'LeagueAdda - myaccount',
            metaKeywords : 'LeagueAdda - myaccount',
            metaDescription : 'LeagueAdda - myaccount',
    },
    sal:{
        metaTitle : 'LeagueAdda - Select Caption & Vice Caption',
            metaKeywords : 'LeagueAdda - Select Caption & Vice Caption',
            metaDescription : 'LeagueAdda - Select Caption & Vice Caption',
    },
    createteam:{
        metaTitle : 'LeagueAdda - createteam',
            metaKeywords : 'LeagueAdda - createteam',
            metaDescription : 'LeagueAdda - createteam',
    },
    payments:{
        metaTitle : 'LeagueAdda - payments',
            metaKeywords : 'LeagueAdda - payments',
            metaDescription : 'LeagueAdda - payments',
    },
    promotions:{
        metaTitle : 'LeagueAdda - promotions',
            metaKeywords : 'LeagueAdda - promotions',
            metaDescription : 'LeagueAdda - promotions',
    },
    index:{
        metaTitle : 'LeagueAdda',
            metaKeywords : 'LeagueAdda',
            metaDescription : 'LeagueAdda',
    },
    verify:{
        metaTitle : 'LeagueAdda : verify',
            metaKeywords : 'LeagueAdda : verify',
            metaDescription : 'LeagueAdda : verify',
    },
    scorecard:{
        metaTitle : 'LeagueAdda : scorecard',
            metaKeywords : 'LeagueAdda : scorecard',
            metaDescription : 'LeagueAdda : scorecard',
    },
}
module.exports.seoMetaTitles=seoMetaTitles;
