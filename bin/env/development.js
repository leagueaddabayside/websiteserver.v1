'use strict';

module.exports = {
    db: {
        uri: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost:27017') + '/notifications',
        options: {
            user: '',
            pass: ''
        },
        // Enable mongoose debug mode
        debug: process.env.MONGODB_DEBUG || false
    },
    session: {
        maxAge: 1200000,
        SECRET: 'notify'
    },
     mysql: {
        host: '172.31.15.134',
        user: 'root',
        password: 'leagueadda52@123#',
        database: 'fantasy_cricket',
        logginng:false,
    },
    redis: {
        host: '172.31.15.134',
        port: '6379'
    },
    mongo: {
        host: '172.31.15.134',
        port: '27017'
    },
    log: {
        format: 'dev',
        options: {
            stream: {
                directoryPath: process.cwd(),
                fileName: 'access.log',
                rotatingLogs: { // for more info on rotating logs - https://github.com/holidayextras/file-stream-rotator#usage
                    active: false, // activate to use rotating logs
                    fileName: 'access-%DATE%.log', // if rotating logs are active, this fileName setting will be used
                    frequency: 'daily',
                    verbose: false
                }
            }
        }
    },
    livereload: false,
    uploadImagePath : '',
    isProduction : false,
    fileVersion: '1.0',
    cdnUrl : 'http://18.217.53.138:3000/fantasy_sports/app',
    serverUrl : 'http://18.217.53.138:3000', //http://www.thepokerbaazi.com   http://localhost:3000
    gameServerUrl : 'http://18.217.53.138:4000',
    bannerServerUrl : 'http://18.217.53.138:3336',
    secure: {
        ssl: false,
        privateKey: './server.key',
        certificate: './server.crt'
    },
    serverAuth: {
        accessKey: 'mn@zaq@213',
        ip: '::ffff:127.0.0.1'
    },
    auth:{
        google:{
            GOOGLE_CLIENT_ID:"408626758203-0lelg7kfv2c5868lmaaugr2gjhd711m9.apps.googleusercontent.com",
            GOOGLE_CLIENT_SECRET:"ClrsHL3N7lie7e3CKv9c6Uoj",
            CALLBACK_URL:"/auth/google/callback"

        }
    },
    facebookAuth : {
        clientID      : '249513412161509', // your App ID
        clientSecret  : 'd880c80e872482bb3615a01cc71ea0dd', // your App Secret
        callbackURL   : '/auth/facebook/callback'
    },
    smtpEmailHostAws:{
        host : 'email-smtp.us-west-2.amazonaws.com',
        port : 465,
        secure : true, // use SSL
        auth : {
            user : 'AKIAJZ5EPCBHWPEJ5NDQ',
            pass : 'AgMHw8F99wES/LsnJltaZZY0cWK29y0bXgF55cGUbEEC'
        }
    },
    imiService:{
        key:'a9a7fb4d-fa7e-48c6-9c82-85bc1a0cea79',
        senderAddress:'ADAOTP',
        host:'http://api-openhouse.imimobile.com',
        path:'/smsmessaging/1/outbound/tel%3A%2BOPNHSE/requests',
    },
    panCard:{
        prefix:'user_pan_card',
        status:['APPROVE','FAILED'],
        initStatus:['PROCESSING'],
        filtType:['pdf','png','jpeg','jpg']
    },
    txn:{
        status:['WONL','JOINEDL','DEPOSIT','CREDIT','BONUS','WITHDRAWL','REFUND']
    },
    bonus:{
        FIRST_DEPOSIT:{
            bonusId:1,
            bonusEvent:'FIRST_DEPOSIT',
            bonusAmt:25,
            MIN_CAP:50,
            MAX_CAP:500,
            status:'ACTIVE'
        },VERIFY_EMAIL:{
            bonusId:2,
            bonusEvent:'VERIFY_EMAIL',
            bonusAmt:0,
            status:'ACTIVE'
        },VERIFY_MOBILE:{
            bonusId:3,
            bonusEvent:'VERIFY_MOBILE',
            bonusAmt:10,
            status:'ACTIVE'
        },VERIFY_PANCARD:{
            bonusId:4,
            bonusEvent:'VERIFY_PANCARD',
            bonusAmt:0,
            status:'ACTIVE'
        }
    },
    remarks:{
      WITHDRAWL_CANCEL:'WITHDRAWL_CANCEL'
    },
    payU:{
        // key:'0pwswY',
        key:'gtKFFx',   // test credentials
        salt:'eCwWELxi',// test credentilas
        // salt:'qCDBwvdi',
        url:'https://test.payu.in/_payment',
        rurl:'/payments/payu/return',
        pinfo:'productInfo',
        html:`<html>
        <head>
        </head>
        <body>
        <form action='https://test.payu.in/_payment' id="pay" method='post'>
        <input type="hidden" name="firstname" value="{firstname}" />
        <input type="hidden" name="lastname" value="{lastname}" />
        <input type="hidden" name="surl" value="{surl}" />
        <input type="hidden" name="phone" value="{phone}" />
        <input type="hidden" name="key" value="{key}" />
        <input type="hidden" name="hash" value ="{hash}" />
        <input type="hidden" name="curl" value="{curl}" />
        <input type="hidden" name="furl" value="{furl}" />
        <input type="hidden" name="txnid" value="{txnid}" />
        <input type="hidden" name="productinfo" value="{productinfo}" />
        <input type="hidden" name="amount" value="{amount}" />
        <input type="hidden" name="email" value="{email}" />
        <!--<input type= "submit" value="submit">-->
        </form>
        </body></html>
        <script>document.getElementById('pay').submit()</script>`
    }


};
