
const express=require('express');
const routes=express.Router();
const otpCtrl=require('./controllers/index');
const middleware = require('../middleware/index');

routes.post('/users/verifyMobileNumber',middleware.auditTrailLog,middleware.getUserIdFromToken,middleware.checkMobileNumberVerified,otpCtrl.verifyMobileNumber);
routes.post('/users/verifyEmailId',middleware.auditTrailLog,middleware.getUserIdFromToken,otpCtrl.verifyEmailId);
routes.post('/users/userForgotPasswordInitiate',middleware.auditTrailLog,middleware.validateForgotInitiateRequest,otpCtrl.userForgotPasswordInitiate);

module.exports=routes;
