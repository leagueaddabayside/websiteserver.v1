require("../../utils/mongo_connection");
var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

var otpMasterSchema = new mongoose.Schema({
	otp: {type: String, required: true},
	otpFor : {type: String, enum: ['FORGOT_PASSWORD', 'VERIFY_EMAIL','VERIFY_MOBILE']},
	userData: {userId :Number, userName:String, mobileNo : String , emailId : String},	
	status: {type: String, enum: ['ACTIVE', 'INVALID','EXPIRED']},
	otpTime: {type: Date, default: Date.now},
	otpExpireTime: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model('OtpMaster', otpMasterSchema, "otp_master");

otpMasterSchema.plugin(autoIncrement.plugin, {
	model: 'OtpMaster',
	startAt: 1,
	incrementBy: 1
});