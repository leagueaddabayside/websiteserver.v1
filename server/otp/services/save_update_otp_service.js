var otpModel = require("../models/otp_master");
var responseCode = require("../../utils/response_code.js");
var logger = require('../../utils/logger.js').websiteLogs;
var utility = require('../../utils/utility.js');
var moment = require('moment');

/**
 *
 * @param requestData{userId,userDataType,otpFor}
 * @param callback
 */
function saveUpdateOtpService(requestData, callback) {
    console.log('saveUpdateOtpService requestData', requestData);
    var findClause = {};
    findClause['userData.userId'] = requestData.userId;
    findClause.otpFor = requestData.otpFor;

    var userData = {};
    userData.userId = requestData.userId;
    if (requestData.userDataType == "userName") {
        userData.userName = requestData.userName;
    } else if (requestData.userDataType == "emailId") {
        userData.emailId = requestData.emailId;
    } else if (requestData.userDataType == "mobileNo") {
        userData.mobileNo = requestData.mobileNo;
    }

    logger.info("saveUpdateOtpService findClause : ", findClause);

    var dataToInsertOrUpdate = {};

    let otp = utility.generateOtp();
    let otpTime = new Date();
    let otpExpireTime = moment(otpTime).add(10, 'minutes');

    //dataToInsertOrUpdate.userId 		= requestData.userId;
    dataToInsertOrUpdate.userData = userData;
    dataToInsertOrUpdate.otp = otp;
    dataToInsertOrUpdate.otpFor = requestData.otpFor;
    dataToInsertOrUpdate.status = 'ACTIVE';
    dataToInsertOrUpdate.otpTime = otpTime;
    dataToInsertOrUpdate.otpExpireTime = otpExpireTime;


    otpModel.findOneAndUpdate(findClause, dataToInsertOrUpdate, {upsert: true}, function (error, data) {
        var respData = {};
        if (error) {
            logger.error("saveUpdateOtpService findOneAndUpdate Error");
            respData.responseCode = responseCode.MONGO_ERROR;
            callback(true, respData);
            return;
        }
        logger.info("saveUpdateOtpService Data : ", data);

        respData.responseCode = responseCode.SUCCESS;
        respData.responseData = {otp : otp};
        callback(null, respData);

        return;
    });
}

module.exports = saveUpdateOtpService;

//Unit Test Case
if (require.main === module) {

    var findClause = {};
    findClause.userId = 19;
    findClause.userDataType = 'mobileNo';
    findClause.mobileNo = '1234567890'
    findClause.otpFor = 'FORGOT_PASSWORD'
    saveUpdateOtpService(findClause, function (error, responseData) {
        if (error) {
            console.log("error : ", error);
        }
        console.log("responseData : ", responseData);
    });
}