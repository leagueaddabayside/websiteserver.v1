var otpModel = require("../models/otp_master");
var logger = require("../../utils/logger").websiteLogs;
var responseCode = require("../../utils/response_code");
var moment = require('moment');

function findOtpByProperty(requestObject, callback) {
    var responseObject = new Object();
    var query = otpModel.findOne({});
    if (requestObject.userId)
        query.where("userData.userId").equals(requestObject.userId);

    if (requestObject.otpFor)
        query.where("otpFor").equals(requestObject.otpFor);

    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        if (data && data.otp) {
            if (data.otp == requestObject.otp) {
                var now = new Date();
                let currentTime = moment(now).unix();
                let otpExpireTime = moment(data.otpExpireTime).unix();

                console.log(currentTime, 'otpExpireTime', otpExpireTime);
                if (currentTime < otpExpireTime) {
                    responseObject.responseCode = responseCode.SUCCESS;
                    responseObject.responseData = data;
                } else {
                    responseObject.responseCode = responseCode.OTP_ALREADY_EXPIRED;

                }

            } else {
                responseObject.responseCode = responseCode.INVALID_OTP;

            }

        } else {
            responseObject.responseCode = responseCode.INVALID_OTP;
        }

        callback(null, responseObject);
    });
}

module.exports = findOtpByProperty;

//	Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.userId = 19;
    requestObject.otp = 986422;
    requestObject.otpFor = 'FORGOT_PASSWORD';

    findOtpByProperty(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}