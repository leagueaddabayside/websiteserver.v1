var saveUpdateOtpService = require("./save_update_otp_service.js");
var validateUserOtpService = require("./validate_user_otp");

module.exports = {
	saveUpdateOtpService : saveUpdateOtpService,
	validateUserOtpService : validateUserOtpService
}