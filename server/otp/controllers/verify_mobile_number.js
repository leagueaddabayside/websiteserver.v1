var logger = require("../../utils/logger").websiteLogs;
var otpService = require('../services/index');
const responseMessage = require('../../utils/response_message');
const responseCode = require('../../utils/response_code');
const smsService = require('../../utils/smsService');
const smsTemplate = require('../../utils/sms_template');
var format = require("string-template");

function verifyMobileNumber(request, response) {
    var requestData = request.body;
    var responseObject = new Object();

    var otpReqData = {};
    otpReqData.userDataType = 'mobileNo';
    otpReqData.mobileNo = requestData.mobileNumber;
    otpReqData.otpFor = 'VERIFY_MOBILE';
    otpReqData.userId = requestData.userId;

    otpService
        .saveUpdateOtpService(
        otpReqData,
        function (error, data) {
            if (error) {
                responseObject.respCode = data.responseCode;
            } else if (data.responseCode !== responseCode.SUCCESS) {
                responseObject.respCode = data.responseCode;
                responseObject.message = responseMessage[data.responseCode];
            } else {
                var responseData = data.responseData;

                var msg = format(smsTemplate.VERIFY_MOBILE_OTP, {otp: responseData.otp});
                var messageData = {to: requestData.mobileNumber, content: msg};
                smsService.sendOtp(messageData);

                responseObject.respCode = responseCode.SUCCESS;

                logger.info("verifyMobileNumber API :- Response - %j",
                    responseObject);
                response.json(responseObject);
            }

        });

}

module.exports = verifyMobileNumber;

if (require.main === module) {
    (function () {
        var request = {};

        var response = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();
        requestObject.otp = 123456,
            requestObject.userData = "9718225670",
            requestObject.password = "pageContent",

            console.log(requestObject);
        request.body = requestObject;
        verifyMobileNumber(request, response);
    })();
}
