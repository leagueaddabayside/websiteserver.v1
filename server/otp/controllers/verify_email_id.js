var logger = require("../../utils/logger").websiteLogs;
var otpService = require('../services/index');
const responseMessage = require('../../utils/response_message');
const responseCode = require('../../utils/response_code');
const nodeMailer = require('../../utils/node_mailer');

function verifyEmailId(request, response) {
    var requestData = request.body;
    if(!requestData.emailId){
        response.json({
            respCode: responseCode.INVALID_REQUEST_PARAMS,
            message: responseMessage[responseCode.INVALID_REQUEST_PARAMS]
        })
        return;
    }

    var responseObject = new Object();

    var otpReqData = {};
    otpReqData.userDataType = 'emailId';
    otpReqData.emailId = requestData.emailId;
    otpReqData.otpFor = 'VERIFY_EMAIL';
    otpReqData.userId = requestData.userId;

    otpService
        .saveUpdateOtpService(
        otpReqData,
        function (error, data) {
            if (error) {
                responseObject.respCode = data.responseCode;
            } else if (data.responseCode !== responseCode.SUCCESS) {
                responseObject.respCode = data.responseCode;
                responseObject.message = responseMessage[data.responseCode];
            } else {
                var responseData = data.responseData;

                var emailData = {};
                emailData.templateType = "VERIFY_EMAIL",
                emailData.to = requestData.emailId,
                emailData.templateData = {otp : responseData.otp};

                nodeMailer.sendMail(emailData,function(err, responseData) {
                   logger.info('send Email response',responseData);

                });

                responseObject.respCode = responseCode.SUCCESS;

                logger.info("verifyMobileNumber API :- Response - %j",
                    responseObject);
                response.json(responseObject);
            }

        });

}

module.exports = verifyEmailId;

if (require.main === module) {
    (function () {
        var request = {};

        var response = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();
        requestObject.otp = 123456,
            requestObject.userData = "9718225670",
            requestObject.password = "pageContent",

            console.log(requestObject);
        request.body = requestObject;
        verifyEmailId(request, response);
    })();
}
