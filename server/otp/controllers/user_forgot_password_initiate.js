var logger = require("../../utils/logger").websiteLogs;
var otpService = require('../services/index');
const responseMessage = require('../../utils/response_message');
const responseCode = require('../../utils/response_code');
const utility = require('../../utils/utility');
const nodeMailer = require('../../utils/node_mailer');
const smsService = require('../../utils/smsService');
const smsTemplate = require('../../utils/sms_template');
var format = require("string-template");

function userForgotPasswordInitiate(request, response) {
    var requestData = request.body;
    var responseObject = new Object();
    requestData.otpFor = 'FORGOT_PASSWORD';

    otpService
        .saveUpdateOtpService(
        requestData,
        function (error, data) {
            if (error) {
                responseObject.respCode = data.responseCode;
            } else if (data.responseCode !== responseCode.SUCCESS) {
                responseObject.respCode = data.responseCode;
                responseObject.message = responseMessage[data.responseCode];
            } else {
                var responseData = data.responseData;


                if(requestData.userDataType == 'mobileNo'){
                    var msg = format(smsTemplate.FORGOT_PASSWORD_OTP, {otp: responseData.otp});
                    var messageData = {to: requestData.mobileNo, content: msg};
                    smsService.sendOtp(messageData);
                }else{
                    var emailData = {};
                    emailData.templateType = "FORGOT_PASSWORD_OTP",
                        emailData.to = requestData.emailId,
                        emailData.templateData = {otp : responseData.otp};

                    nodeMailer.sendMail(emailData,function(err, responseData) {
                        logger.info('send Email response',responseData);

                    });
                }

                var respData = {};
                respData.userDataType = requestData.userDataType;
                respData.userId = requestData.userId;
                respData.emailId = utility.encryptEmailId(requestData.emailId);
                if (requestData.mobileNo) {
                    respData.mobileNo = utility.encryptMobileNumber(requestData.mobileNo);
                }
                responseObject.respCode = responseCode.SUCCESS;
                responseObject.responseData = respData;

                logger.info("verifyMobileNumber API :- Response - %j",
                    responseObject);
                response.json(responseObject);
            }

        });

}

module.exports = userForgotPasswordInitiate;

if (require.main === module) {
    (function () {
        var request = {};

        var response = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();
        requestObject.otp = 123456,
            requestObject.userData = "9718225670",
            requestObject.password = "pageContent",

            console.log(requestObject);
        request.body = requestObject;
        userForgotPasswordInitiate(request, response);
    })();
}
