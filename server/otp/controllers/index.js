const verifyMobileNumber=require('./verify_mobile_number');
const verifyEmailId = require('./verify_email_id');
const userForgotPasswordInitiate = require('./user_forgot_password_initiate');

module.exports.verifyMobileNumber = verifyMobileNumber;
module.exports.verifyEmailId = verifyEmailId;
module.exports.userForgotPasswordInitiate = userForgotPasswordInitiate;
