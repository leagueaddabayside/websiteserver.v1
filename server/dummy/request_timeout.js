/**
 * Created by sumit on 3/24/2017.
 */
let request = require('request');


console.log(new Date());


request.get('http://10.255.255.1', function(err) {
    console.log(new Date());

    console.log(err);
    console.log(err.code === 'ETIMEDOUT');
    // Set to `true` if the timeout was a connection timeout, `false` or
    // `undefined` otherwise.
    console.log(err.connect === true);
    process.exit(0);
});

/*

request.get('http://10.255.255.1', {timeout: 5000}, function(err) {
    console.log(new Date());

//    console.log(err);
    console.log(err.code === 'ETIMEDOUT');
    // Set to `true` if the timeout was a connection timeout, `false` or
    // `undefined` otherwise.
    console.log(err.connect === true);
    process.exit(0);
});*/
