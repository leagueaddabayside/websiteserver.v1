/**
 * Created by sumit on 1/18/2017.
 */
'use strict';

let express=require('express');
let route=express.Router();

     /* API CODE */
    route.get("/fetchLeagues", function (req, res) {
        var response = {};
        response.respCode = 100;
        response.respData = LEAGUES_ARRAY;
        setTimeout(function(){
            res.json(response);
        }, 1000);
    });

    route.get("/fetchMyDetails", function (req, res) {
        var response = {};
        response.responseCode = 100;
        response.responseData = MY_PROFILE;
        res.json(response);
    });

    route.get("/fetchTours", function (req, res) {
        var response = {};
        response.respCode = 100;
        response.respData = TOURS_ARRAY;
        setTimeout(function(){
            res.json(response);    
        }, 500);
    });

    route.get("/fetchMatchList", function (req, res) {
        var response = {};
        response.respCode = 100;
        response.respData = MATCHES_ARRAY;
        setTimeout(function(){
            res.json(response);    
        }, 500);
    });

    route.get("/fetchMatchPlayers", function (req, res) {
        var response = {};
        response.responseCode = 100;
        response.responseData = MATCH_PLAYER_ARRAY;
        setTimeout(function(){
            res.json(response);    
        }, 500);
    });

    module.exports=route;



    /* DUMMY JSON CODE */
    var MY_PROFILE = [
        {
            "name": "AshutoshGarg",
            "team_name": "AshuSapphire",
            "dob": "05/19/1990",
            "gender": "M",
            "email_id": "ashutoshgarg@gmail.com",
            "mobile": 9876543210,
            "address": "DLF PHASE 3",
            "city": "Gurgaon"
        }
    ];

    var MATCHES_ARRAY = [
        {
            matchId: 1,
            startTime: 'January 24, 2017 10:41:00',
            relatedName: '4th ODI Match',
            status: 'CLOSED',
            name: '',
            teams: [
                {
                    code: 'IND',
                    name: 'INDIA'
                },
                {
                    code: 'AUS',
                    name: 'AUSTRALIA'
                }
            ]
        },
        {
            matchId: 2,
            startTime: 'January 26, 2017 16:00:00',
            relatedName: '6th ODI Match',
            status: 'CLOSED',
            name: '',
            teams: [
                {
                    code: 'NZ',
                    name: 'NEW ZEALAND'
                },
                {
                    code: 'AUS',
                    name: 'AUSTRALIA'
                }
            ]
        },
        {
            matchId: 3,
            startTime: 'January 27, 2017 12:00:00',
            relatedName: '1st ODI Match',
            status: 'CLOSED',
            name: '',
            teams: [
                {
                    code: 'SL',
                    name: 'SRI LANKA'
                },
                {
                    code: 'AUS',
                    name: 'AUSTRALIA'
                }
            ]
        },     
        {
            matchId: 4,
            startTime: 'January 25, 2017 06:00:00',
            relatedName: '2nd TEST Match',
            status: 'CLOSED',
            name: '',
            teams: [
                {
                    code: 'IND',
                    name: 'INDIA'
                },
                {
                    code: 'AUS',
                    name: 'AUSTRALIA'
                }
            ]
        },
        {
            matchId: 5,
            startTime: 'January 24, 2017 12:00:00',
            relatedName: '3rd TEST Match',
            status: 'CLOSED',
            name: '',
            teams: [
                {
                    code: 'IND',
                    name: 'INDIA'
                },
                {
                    code: 'SL',
                    name: 'SRI LANKA'
                }
            ]
        },

    ];

// repersent players in final create team page
    var MATCH_PLAYER_ARRAY = [
        {
            playerName: "Hardik Pandya",
            "batter":true,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 8.0,
            scoredPoints:123 ,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1234
        },
        
         {
            playerName: "Yuvraj Singh",
            "batter":true,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 9.0,
            scoredPoints:123 ,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1235
        },
         {
            playerName: "Jos Buttler",
            "batter":false,
            "bowler":false,
            "wicketKeeper": true,
            playerCredit: 8,
            scoredPoints:23,
            teamName: "ENG",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1236
        },
        {
            playerName: "MS Dhoni",
            "batter":false,
            "bowler":false,
            "wicketKeeper": true,
            playerCredit: 9,
            scoredPoints:23,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1237
        },
         {
            playerName: "Rohit Sharma",
            "batter":true,
            "bowler":false,
            "wicketKeeper":false,
            playerCredit: 9.5,
            scoredPoints:34 ,
            teamName: "IND",
             isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1238
        },
        {
            playerName: "KL Rahul",
            "batter":true,
            "bowler":false,
            "wicketKeeper":false,
            playerCredit: 8.0,
            scoredPoints:34 ,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1239
        },
        
        {
            playerName: "Ajinkya Rahane",
            "batter":true,
            "bowler":false,
            "wicketKeeper":false,
            playerCredit: 8.5,
            scoredPoints:34 ,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1240
        },
        
        {
            playerName: "Alex hales",
            "batter":true,
            "bowler":false,
            "wicketKeeper":false,
            playerCredit: 9.0,
            scoredPoints:34 ,
            teamName: "ENG",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1241
        },
        
        {
            playerName: "Eion Morgan",
            "batter":true,
            "bowler":false,
            "wicketKeeper":false,
            playerCredit: 9.5,
            scoredPoints:34 ,
            teamName: "ENG",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1242
        },
        
        {
            playerName: "Suresh Raina",
            "batter":true,
            "bowler":false,
            "wicketKeeper":false,
            playerCredit: 8.5,
            scoredPoints:34 ,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1243
        },
        
        {
            playerName: "Joe Root",
            "batter":true,
            "bowler":false,
            "wicketKeeper":false,
            playerCredit: 10.0,
            scoredPoints:67 ,
            teamName: "ENG",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1244
        }
        ,
        
        {
            playerName: "Chris Jordan",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 8,
            scoredPoints:52,
            teamName: "ENG",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1245
        },
        
        {
            playerName: "Umesh Yadav",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 8,
            scoredPoints:26,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1246
        },
        
        {
            playerName: "Jaspreet Bumrah",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 8.5,
            scoredPoints:56,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1247
        },
        
        {
            playerName: "Ashish Nehra",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 8.5,
            scoredPoints:56,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1248
        },
        
        {
            playerName: "Yuzvendra Chahal",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 8.5,
            scoredPoints:56 ,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1249
        },
        
        {
            playerName: "Adil Rashid",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 7.5,
            scoredPoints:56 ,
            teamName: "ENG",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1250
        },
        
        {
            playerName: "Ishant Sharma",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 7.5,
            scoredPoints:562 ,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1251
        },
        
        {
            playerName: "Ravichandran Ashwin",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 8.5,
            scoredPoints:56 ,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1252
        },
        
        {
            playerName: "Liam Plunkett",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 7.5,
            scoredPoints:56 ,
            teamName: "ENG",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1253
        },
        
        
        {
            playerName: "Tyle Mills",
            "batter":false,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 9.0,
            scoredPoints:32 ,
            teamName: "ENG",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1254
        },
        
        {
            playerName: "Moen Ali",
            "batter":true,
            "bowler":true,
            "wicketKeeper":false,
            playerCredit: 8.5,
            scoredPoints:56 ,
            teamName: "ENG",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1255
        },
        
        {
            playerName: "Parveez Rasool",
            batter:true,
            bowler:true,
            wicketKeeper:false,
            playerCredit: 7.5,
            scoredPoints: 56 ,
            teamName: "IND",
            isSelected: false,
            isCaptain: false,
            isViceCaptain:false,
            isBonus: false,
            playerId: 1256
        }
    ]

    var TOURS_ARRAY = [
        {
            tourName: 'All Tours',
            tourId: 'ALL',
            status: 'ACTIVE'
        },
        {
            tourName: 'Aus Women T20 Bash',
            tourId: 613,
            status: 'ACTIVE'
        },
        {
            tourName: 'Aussie T20 Bash',
            tourId: 617,
            status: 'ACTIVE'
        },
        {
            tourName: 'NZ vs BAN Test',
            tourId: 626,
            status: 'ACTIVE'
        },
        {
            tourName: 'BAN vs SA Women ODI',
            tourId: 624,
            status: 'ACTIVE'
        },
        {
            tourName: 'AUS vs PAK ODI',
            tourId: 625,
            status: 'ACTIVE'
        },
        {
            tourName: 'SA vs SL T20I',
            tourId: 635,
            status: 'ACTIVE'
        }

    ];

    var LEAGUES_ARRAY = [
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 15,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 12,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 8,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 5,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 1,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 18,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 19,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 20,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
        {
            "leagueId": 9483066,
            "currentTeams": 10,
            "entryFee": 1200,
            "maxTeams": 20,
            "title": "Rs.20,000",
            "winnersCount": 5,
            "type": 'Multi Entry'
        },
    ];
