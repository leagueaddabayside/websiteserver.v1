/**
 * Created by sumit on 3/23/2017.
 */
var Promise = require('bluebird');

var items = [0,1,2,3,4,5,6,7,8,9];

Promise.each(items, function(item) {
    console.log('start processing item:',item);
    var randomExecutionTime = Math.random() * 10;
    return Promise.delay(randomExecutionTime)
        .then(function() {
            console.log('async Operation Finished. item:', item);
        });
}).then(function(resp,data) {
    console.log('All tasks are done now...',resp,data);
}).catch(function(err) {
    console.log('Got an error',err)
});