/**
 * Created by sumit on 4/24/2017.
 */
var bunyan  = require('bunyan');
var pathModule = require('path');
var commonLogger = new bunyan({
    src: true,
    name: 'common' ,
    streams: [
        {
            level: 'trace',
            stream: process.stdout            // log INFO and above to stdout
        },
        {
            type: 'rotating-file',
            level: 'trace',
            path: pathModule.join(__dirname, './logs/common-trace.log'),  // log trace and above to a file,
            period: '1d',   // daily rotation
            count: 3,        // keep 3 back copies
        },
        {
            type: 'rotating-file',
            level: 'error',
            path: pathModule.join(__dirname, './logs/common-error.log'),  // log ERROR and above to a file,
            period: '1d',   // daily rotation
            count: 3,        // keep 3 back copies
        }
    ]});

var requestLogger = new bunyan({
    src: true,
    name: 'request' ,
    streams: [
        {
            level: 'info',
            stream: process.stdout            // log INFO and above to stdout
        },
        {
            type: 'rotating-file',
            level: 'trace',
            path: pathModule.join(__dirname, './logs/request-trace.log'),  // log ERROR and above to a file,
            period: '1d',   // daily rotation
            count: 3,        // keep 3 back copies
        },
        {
            type: 'rotating-file',
            level: 'error',
            path: pathModule.join(__dirname, './logs/request-error.log'),  // log ERROR and above to a file,
            period: '1d',   // daily rotation
            count: 3,        // keep 3 back copies
        }
    ]});
module.exports.commonLogger = commonLogger;
module.exports.requestLogger = requestLogger;


//	Unit Test Case
if (require.main === module) {


    commonLogger.info('Hi');
    commonLogger.info({one:1,second:{third:'third inner data'}},'Hi');
    commonLogger.info({two : 2});
    commonLogger.info({one:{1 :2},two:2,three : 3},'one and two');
    try{
        commonLogger.info('hi %s', 'bob', 'foo',u);


    }catch(err){
        commonLogger.info(err);
        commonLogger.info(err, 'some msg about this error');
    }


}
