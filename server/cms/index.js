'use strict';

let express=require('express');
let route=express.Router();
var cms = require('./controllers/index');
const middleware = require('../middleware');

route.post("/addCMS",middleware.auditTrailLog,middleware.getAdminIdFromToken, cms.addCMS);
route.post("/updateCMS",middleware.auditTrailLog,middleware.getAdminIdFromToken, cms.updateCMS);
route.post("/findCMSByProperty",middleware.auditTrailLog,middleware.getAdminIdFromToken, cms.findCMSByProperty);
route.post("/findCMSList",middleware.auditTrailLog,middleware.getAdminIdFromToken, cms.findCMSList);

route.post("/addCMSPageType",middleware.getAdminIdFromToken, cms.addCMSPageType);
route.post("/updateCMSPageType",middleware.getAdminIdFromToken, cms.updateCMSPageType);
route.post("/findCMSPageTypeByProperty",middleware.getAdminIdFromToken, cms.findCMSPageTypeByProperty);
route.post("/findCMSPageTypeList",middleware.getAdminIdFromToken, cms.findCMSPageTypeList);


module.exports=route;