var cmsMaster = require("../models/cms_master");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function findCMSList(requestObject, callback) {
	var responseObject = new Object();

	var query = cmsMaster.find({});
	if(requestObject.status)
		query.where("status").equals(requestObject.status);

	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		var cmsListArr = data;
		var cmsList = [];
		for (var i=0, length=cmsListArr.length; i<length; i++) {
			var currentRow = cmsListArr[i];

			var currentObj = {};
			currentObj.pageId = currentRow.pageId;
			currentObj.typeId = currentRow.typeId;
			currentObj.pageDispName = currentRow.pageDispName;
			currentObj.deviceType = currentRow.pageData.deviceType;
			currentObj.pageContent = currentRow.pageData.pageContent;
			currentObj.pageTitle = currentRow.pageData.pageTitle;
			currentObj.urlName = currentRow.seoData.urlName;
			currentObj.metaTitle = currentRow.seoData.metaTitle;
			currentObj.metaKeywords = currentRow.seoData.metaKeywords;
			currentObj.metaDescription = currentRow.seoData.metaDescription;
			currentObj.displayOrder = currentRow.displayOrder;
			currentObj.status = currentRow.status;
			cmsList.push(currentObj);
		}
		responseObject.responseData = cmsList;
		responseObject.responseCode = responseCode.SUCCESS;
		console.log('findCMSList responseObject',responseObject);
		callback(null, responseObject);
    });
}

module.exports = findCMSList;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findCMSList(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}