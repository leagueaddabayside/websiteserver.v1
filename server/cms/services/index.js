var addCMSService = require('./add_cms');
var updateCMSService = require('./update_cms');
var findCMSByPropertyService = require('./find_cms_by_property');
var findCMSListService = require('./find_cms_list');

var addCMSPageTypeService = require('./add_cms_page_type');
var updateCMSPageTypeService = require('./update_cms_page_type');
var findCMSPageTypeByPropertyService = require('./find_cms_page_type_by_property');
var findCMSPageTypeListService = require('./find_cms_page_type_list');

module.exports = {
	addCMS : addCMSService,
	updateCMS : updateCMSService,
	findCMSByProperty : findCMSByPropertyService,
	findCMSList : findCMSListService,

	addCMSPageType : addCMSPageTypeService,
	updateCMSPageType : updateCMSPageTypeService,
	findCMSPageTypeByProperty : findCMSPageTypeByPropertyService,
	findCMSPageTypeList : findCMSPageTypeListService
};