var cmsPageTypeMaster = require("../models/cms_page_type_master");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function findCMSPageTypeList(requestObject, callback) {
	var responseObject = new Object();

	var query = cmsPageTypeMaster.find({});
	if(requestObject.status)
		query.where("status").equals(requestObject.status);
	query.sort("displayOrder");

	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
    });
}

module.exports = findCMSPageTypeList;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findCMSPageTypeList(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}