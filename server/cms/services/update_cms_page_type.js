var cmsPageTypeMaster = require("../models/cms_page_type_master");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function updateCMSPageType(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.pageDevName)
		updateObject.pageDevName = requestObject.pageDevName;

	if(requestObject.pageDispName)
		updateObject.pageDispName = requestObject.pageDispName;

	if(requestObject.pageTemplate)
		updateObject.pageTemplate = requestObject.pageTemplate;


	if(requestObject.displayOrder)
		updateObject.displayOrder = requestObject.displayOrder;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {typeId: requestObject.typeId};
	cmsPageTypeMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateCMSPageType;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.pageDevName = "pageDevName",
	requestObject.pageDispName = "pageDispNameUpdate",
	requestObject.displayOrder = 2;
	requestObject.status = "INACTIVE";
	console.log(requestObject);

	updateCMSPageType(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}