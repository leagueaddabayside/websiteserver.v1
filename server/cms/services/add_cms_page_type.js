var cmsPageTypeMaster = require("../models/cms_page_type_master");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function addCMSPageType(requestObject, callback) {
	var newCMSPageType = new cmsPageTypeMaster({
		pageDevName : requestObject.pageDevName,
		pageDispName : requestObject.pageDispName,
		pageTemplate:requestObject.pageTemplate,
		displayOrder : requestObject.displayOrder,
		status : requestObject.status,
		createBy : requestObject.createBy,
		createByName : requestObject.createByName
	});

	var responseObject = new Object();
	newCMSPageType.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addCMSPageType;

//	Unit Test Case
if (require.main === module) {

	let pageTypeArr = [{
		pageDevName : 'constant_page',
		pageDispName : 'Constant Page',
		displayOrder : 1,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'privacy_policy',
		pageDispName : 'Privacy Policy',
		displayOrder : 2,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'contact_us',
		pageDispName : 'Contact Us',
		displayOrder : 3,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'fantasy_points_system',
		pageDispName : 'Fantasy Point System',
		displayOrder : 4,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'how_to_play',
		pageDispName : 'How To Play',
		displayOrder : 1,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'faq',
		pageDispName : 'FAQ',
		displayOrder : 1,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'points-system',
		pageDispName : 'Points System',
		displayOrder : 1,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'managing-your-team',
		pageDispName : 'Manging Your Team',
		displayOrder : 1,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'playing-the-game',
		pageDispName : 'Playing The Game',
		displayOrder : 1,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'create-your-team',
		pageDispName : 'Create Your Team',
		displayOrder : 1,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	},{
		pageDevName : 'account-balance',
		pageDispName : 'Account Balance',
		displayOrder : 1,
		status : 'ACTIVE',
		createBy : 1,
		createByName : 'admin'
	}];


	for(var i=0;i<pageTypeArr.length;i++){
		requestObject = pageTypeArr[i];
		addCMSPageType(requestObject, function(error, responseObject) {
			console.log("Response Code - "+responseObject.responseCode);
			if(error)
				console.log("Error - "+error);
			else
				console.log("Response Data - "+responseObject.responseData);
		});
	}



}