var cmsMaster = require("../models/cms_master");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function updateCMS(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.typeId)
		updateObject.typeId = requestObject.typeId;
	if(requestObject.pageDispName)
		updateObject.pageDispName = requestObject.pageDispName;
	if(requestObject.deviceType)
		updateObject['pageData.deviceType'] = requestObject.deviceType;
	if(requestObject.pageContent)
		updateObject['pageData.pageContent'] = requestObject.pageContent;
	if(requestObject.pageTitle)
		updateObject['pageData.pageTitle'] = requestObject.pageTitle;
	if(requestObject.author)
		updateObject.author = requestObject.author;
	if(requestObject.websiteName)
		updateObject.websiteName = requestObject.websiteName;
	if(requestObject.imageArray)
		updateObject.imageArray = requestObject.imageArray;
	if(requestObject.urlName)
		updateObject['seoData.urlName'] = requestObject.urlName;
	if(requestObject.metaTitle)
		updateObject['seoData.metaTitle'] = requestObject.metaTitle;
	if(requestObject.metaKeywords)
		updateObject['seoData.metaKeywords'] = requestObject.metaKeywords;
	if(requestObject.metaDescription)
		updateObject['seoData.metaDescription'] = requestObject.metaDescription;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {pageId: requestObject.pageId};
	cmsMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateCMS;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.pageId = 1,
	requestObject.typeId = "typeId",
	requestObject.pageDispName = "pageDispNameUpdate",
	requestObject.deviceType = "deviceTypeUpdate",
	requestObject.pageContent = "pageContentUpdate",
	requestObject.pageTitle = "pageTitleUpdate",
	requestObject.author = "authorUpdate",
	requestObject.websiteName = "websiteNameUpdate",
	requestObject.imageArray = new Array(),
	requestObject.urlName = "urlNameUpdate";
	requestObject.metaTitle = "metaTitleUpdate";
	requestObject.metaKeywords = "metaKeywordsUpdate";
	requestObject.metaDescription = "metaDescriptionUpdate";
	requestObject.status = "INACTIVE";
	console.log(requestObject);

	updateCMS(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}