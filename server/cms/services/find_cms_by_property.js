var cmsMaster = require("../models/cms_master");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function findCMSByProperty(requestObject, callback) {
	var responseObject = new Object();

	var query = cmsMaster.findOne({});
	if(requestObject.pageId)
		query.where("pageId").equals(requestObject.pageId);

	if(requestObject.urlName)
		query.where("seoData.urlName").equals(requestObject.urlName);

	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		var currentObj = {};
		if(data){
			var currentRow = data;

			currentObj.pageId = currentRow.pageId;
			currentObj.typeId = currentRow.typeId;
			currentObj.pageDispName = currentRow.pageDispName;
			currentObj.deviceType = currentRow.pageData.deviceType;
			currentObj.pageContent = currentRow.pageData.pageContent;
			currentObj.pageTitle = currentRow.pageData.pageTitle;
			currentObj.urlName = currentRow.seoData.urlName;
			currentObj.metaTitle = currentRow.seoData.metaTitle;
			currentObj.metaKeywords = currentRow.seoData.metaKeywords;
			currentObj.metaDescription = currentRow.seoData.metaDescription;
			currentObj.displayOrder = currentRow.displayOrder;
			currentObj.status = currentRow.status;
		}


		responseObject.responseData = currentObj;
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
    });
}

module.exports = findCMSByProperty;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.pageId = 1;

	findCMSByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}