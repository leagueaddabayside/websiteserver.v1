var cmsMaster = require("../models/cms_master");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function addCMS(requestObject, callback) {
	var newCMS = new cmsMaster({
		typeId : requestObject.typeId,
		pageDispName : requestObject.pageDispName,
		pageData: {
			deviceType: requestObject.deviceType,
			pageContent: requestObject.pageContent,
			pageTitle: requestObject.pageTitle,
		},
		author: requestObject.author,
		websiteName: requestObject.websiteName,
		imageArray: requestObject.imageArray,
		seoData : {
			urlName : requestObject.urlName,
			metaTitle : requestObject.metaTitle,
			metaKeywords : requestObject.metaKeywords,
			metaDescription : requestObject.metaDescription,
		},
		status : 'ACTIVE',
		createBy : requestObject.createBy,
		createByName : requestObject.createByName
	});

	var responseObject = new Object();
	newCMS.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addCMS;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.typeId = 1,
	requestObject.pageDispName = "About Us",
	requestObject.deviceType = "deviceType",
	requestObject.pageContent = "pageContent",
	requestObject.pageTitle = "pageTitle",
	requestObject.author = "author",
	requestObject.websiteName = "websiteName",
	requestObject.imageArray = new Array(),
	requestObject.urlName = "urlName";
	requestObject.metaTitle = "metaTitle";
	requestObject.metaKeywords = "metaKeywords";
	requestObject.metaDescription = "metaDescription";
	requestObject.status = "ACTIVE";
	requestObject.createBy = 1;
	requestObject.createByName = "createByName";
	console.log(requestObject);

	addCMS(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}