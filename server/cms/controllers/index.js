var addCMSApi = require('./add_cms');
var updateCMSApi = require('./update_cms');
var findCMSByPropertyApi = require('./find_cms_by_property');
var findCMSListApi = require('./find_cms_list');

var addCMSPageTypeApi = require('./add_cms_page_type');
var updateCMSPageTypeApi = require('./update_cms_page_type');
var findCMSPageTypeByPropertyApi = require('./find_cms_page_type_by_property');
var findCMSPageTypeListApi = require('./find_cms_page_type_list');

module.exports = {
	addCMS : addCMSApi,
	updateCMS : updateCMSApi,
	findCMSByProperty : findCMSByPropertyApi,
	findCMSList : findCMSListApi,

	addCMSPageType : addCMSPageTypeApi,
	updateCMSPageType : updateCMSPageTypeApi,
	findCMSPageTypeByProperty : findCMSPageTypeByPropertyApi,
	findCMSPageTypeList : findCMSPageTypeListApi
};