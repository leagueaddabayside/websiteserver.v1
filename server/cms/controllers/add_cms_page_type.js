var logger = require("../../utils/logger").websiteLogs;;
var cmsService = require('../services/index');
var responseMessage = require("../../utils/response_message");

function addCMSPageType(request, response, next) {
	var requestObject = request.body;
	//console.log("addCMSPageType API :- Request - %j", requestObject);

	var responseObject = new Object();
	cmsService.addCMSPageType(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData =  data.responseData;
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData =  data.responseData;
			responseObject.responseData.message = responseMessage[data.responseCode];
		}

		logger.info("addCMSPageType API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addCMSPageType;