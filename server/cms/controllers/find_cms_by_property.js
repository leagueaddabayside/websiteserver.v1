var logger = require("../../utils/logger").websiteLogs;;
var cmsService = require('../services/index');
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findCMSByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findCMSByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	cmsService.findCMSByProperty(requestObject, function(error, data) {
		if(error === null) {
			responseObject.respCode = data.responseCode;
			responseObject.respData =  data.responseData;
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		}
		//logger.info("updateGroup API :- Response - %j",	responseObject);
		response.json(responseObject);
		

	});
}

module.exports = findCMSByProperty;