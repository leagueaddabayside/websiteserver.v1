var logger = require("../../utils/logger").websiteLogs;;
var cmsService = require('../services/index');
var responseMessage = require("../../utils/response_message");

function findCMSPageTypeList(request, response, next) {
	var requestObject = request.body;
	//console.log("findCMSPageTypeList API :- Request - %j", requestObject);

	var responseObject = new Object();
	cmsService.findCMSPageTypeList(requestObject, function(error, data) {
		if(error === null) {
			responseObject.respCode = data.responseCode;
			responseObject.respData =  data.responseData;
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		}

		//logger.info("findCMSPageTypeList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findCMSPageTypeList;