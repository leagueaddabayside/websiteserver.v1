/**
 * Created by aditya on 17/1/17.
 */
const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');
// const RoleActionMapping=require('./gnRoleActionMapping')
const RoleActionUserMapping= sequelize.define('gn_role_action_user_mapping', {
    id: {type: Sequelize.INTEGER, field: 'id',primaryKey:true},
    userId: {type: Sequelize.INTEGER, field: 'user_id'},
    status: {type: Sequelize.ENUM,values:['ACTIVE','INACTIVE','NA'], field: 'status'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

// RoleActionMapping.belongsTo(RoleActionUserMapping,{foreignKey:'action_id'});

module.exports=RoleActionUserMapping;
