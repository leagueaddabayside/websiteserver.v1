/**
 * Created by aditya on 17/1/17.
 */
const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require('../../../server/connection');
const RoleActionUserMapping=require('./gnRoleActionUserMapping');
const RoleActionMapping= sequelize.define('gn_role_action_mapping', {
    id: {type: Sequelize.INTEGER, field: 'role_action_id',primaryKey:true},
    roleId: {type: Sequelize.INTEGER, field: 'role_id'},
    moduleId: {type: Sequelize.INTEGER, field: 'module_id'},
    displayOrder: {type: Sequelize.INTEGER, field: 'display_order'},
    status: {type: Sequelize.ENUM,values:['ACTIVE','INACTIVE','NA'], field: 'status'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

RoleActionMapping.hasOne(RoleActionUserMapping,{foreignKey:'role_action_id'});
// RoleActionUserMapping.belongsTo(RoleActionMapping,{foreignKey:'action_id'});
RoleActionMapping.sync().then(function () {

});
module.exports=RoleActionMapping;
