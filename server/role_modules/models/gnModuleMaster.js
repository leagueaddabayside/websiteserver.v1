/**
 * Created by aditya on 17/1/17.
 */
const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');
const RoleAction=require('./gnRoleActionMapping');
const ModuleMaster= sequelize.define('gn_module_master', {
    moduleId: {type: Sequelize.STRING, field: 'module_id',primaryKey:true},
    moduleName: {type: Sequelize.STRING, field: 'module_name'},
    userType: {type: Sequelize.ENUM,values:['ADMIN'], field: 'user_type'},
    relatedTo: {type: Sequelize.ENUM,values:['BANNER_PANEL','WEBSITE'], field: 'related_to'},
    navigationUrl: {type: Sequelize.STRING, field: 'navigation_url'},
    navigationIcon: {type: Sequelize.STRING, field: 'navigation_icon'},
    parentModuleId: {type: Sequelize.INTEGER, field: 'parent_module_id'},
    description: {type: Sequelize.STRING, field: 'description'},
    isRoleHeadModule: {type: Sequelize.BOOLEAN, field: 'is_role_head_module'},
    isSubUserModule: {type: Sequelize.BOOLEAN, field: 'is_sub_user_module'},
    displayOrder: {type: Sequelize.INTEGER, field: 'display_order'},
    status: {type: Sequelize.ENUM,values:['ACTIVE','INACTIVE'], field: 'status'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});


ModuleMaster.hasOne(RoleAction,{ foreignKey: 'module_id' });
ModuleMaster.sync().then(function () {

})
module.exports=ModuleMaster;
