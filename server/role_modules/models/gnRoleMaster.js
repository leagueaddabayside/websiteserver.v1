/**
 * Created by aditya on 17/1/17.
 */
const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');
const RoleAction=require('./gnRoleActionMapping');
const RoleMaster= sequelize.define('gn_role_master', {
    id: {type: Sequelize.INTEGER, field: 'role_id',primaryKey:true},
    roleName: {type: Sequelize.STRING, field: 'role_name'},
    roleDescription: {type: Sequelize.STRING, field: 'role_description'},
    isMaster: {type: Sequelize.ENUM,values:['YES','NO'], field: 'is_master'},
    status: {type: Sequelize.ENUM,values:['ACTIVE','INACTIVE','NA'], field: 'status'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});


RoleMaster.hasOne(RoleAction,{ foreignKey: 'role_id' });
RoleMaster.sync().then(function () {

})
module.exports=RoleMaster;
