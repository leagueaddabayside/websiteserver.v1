/**
 * Created by adityagupta on 2/2/17.
 */
/**
 * Created by adityagupta on 20/1/17.
 */
const config=require('../../../bin/config')
const RoleMasterService= require('../services/roleMaster');
const Util=require(config.__base+'/server/utils/util')
const responseMessage =require('../../utils/response_message');

const RoleAttributes=(req,res)=>{
    let params=req.body;
    let reqParam = {userId : params.adminId,roleId : params.roleId,relatedTo : 'WEBSITE'};
    RoleMasterService.getRoleAttributes(reqParam)
        .then(function (attributes) {
            res.json({respCode:100,respData:attributes})
        })
        .catch(function (err) {
            console.log(err);
            res.json({respCode:102,message:responseMessage[102]})
        })
}

module.exports=RoleAttributes;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId: 20,
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        RoleAttributes(req,res);
    })()
}