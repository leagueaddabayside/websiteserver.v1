/**
 * Created by sumit on 2/2/2017.
 */
const express=require('express');
const routes=express.Router();
const Roles=require('./controllers');
const middleware = require('../middleware');



routes.post('/roles/attributes',middleware.getAdminIdFromToken,Roles.roleAttribute);

routes.get('/*',function (req,res) {
    res.json({code:404,error:'Invalid token'})
})

module.exports=routes;