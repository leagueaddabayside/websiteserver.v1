var logger = require("../conf/logger.js").log;
var dbPool = require("../conf/dbconnection.js");

module.exports = {
      get_admin_modules : function(reqData, callback){
            dbPool.websitePool.getConnection(function(err, connection) {
                  logger.info("err", err);
                  var param = [reqData.userId]; //, reqData.moduleName, reqData.navigationUrl, reqData.navigationIcon];

                  var strQuery = "SELECT aa.`module_id`,`module_name`,`navigation_url`,`navigation_icon` FROM `gn_module_master` aa INNER JOIN `gn_role_action_mapping` bb INNER JOIN `gn_role_action_user_mapping` cc ON aa.module_id=bb.module_id AND bb.`role_action_id` = cc.`role_action_id` WHERE cc.user_id=? AND aa.status='ACTIVE' AND bb.status='ACTIVE' AND cc.status='ACTIVE' ORDER BY aa.display_order";

                   connection.query(strQuery, param, function (err, rows, fields) {
                        logger.info("param,err, rows", param, err, rows);
                        callback(err, rows, fields);
                        connection.release();
                    });
                });
      }

};