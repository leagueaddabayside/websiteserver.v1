/**
 * Created by adityagupta on 2/2/17.
 */
const ModuleMaster=require('../models/gnModuleMaster');
const UserActionMapping=require('../models/gnRoleActionUserMapping');
const RoleAction=require('../models/gnRoleActionMapping');
const sequelize=require('../../connection');

const ModuleMasterClass=function () {

}

ModuleMasterClass.prototype.getRoleAttributes=function (reqParam) {
    return new Promise((resolve,reject)=>{
        ModuleMaster.findAll({raw:true,where:{status:'ACTIVE',relatedTo : reqParam.relatedTo},include:
            [{model:RoleAction,where:{status:'ACTIVE',roleId : reqParam.roleId},attributes:[]}],order: [ [ RoleAction,  'display_order', 'ASC' ] ]
            ,attributes:['moduleId',['module_name','name'],['navigation_url','uihref'],['navigation_icon','icon'],'description']})
            .then(resolve)
            .catch(reject)
    })
}
module.exports=new ModuleMasterClass;

/***********************************/
if(require.main==module){
    (function () {
        let params={}
        let ModuleMasterClassObj=new ModuleMasterClass();
         ModuleMasterClassObj.getRoleAttributes({roleId : 1,relatedTo : 'WEBSITE'});
    })();
}