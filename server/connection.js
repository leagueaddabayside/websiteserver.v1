/**
 * Created by aditya on 23/11/16.
 */
var Sequelize=require('sequelize');
var config = require('../bin/config');
var sequelizeConn = new Sequelize(config.mysql.database, config.mysql.user, config.mysql.password, {
    timezone: '+05:30', //here you can pass timezone
    host: config.mysql.host,
    dialect: 'mysql',
    logging: config.mysql.logginng,
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

module.exports=sequelizeConn; 