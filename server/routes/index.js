/**
 * Created by sumit on 1/16/2017.
 */
const config=require('../../bin/config');
const dummy = require(config.__base + 'server/dummy');
const Users= require(config.__base + 'server/users');
const adminUsers= require(config.__base + 'server/adminUsers');
const Roles= require(config.__base + 'server/role_modules');
const email_template= require(config.__base + 'server/email_template');
const cms= require(config.__base + 'server/cms');
const image= require(config.__base + 'server/image');
const website= require(config.__base + 'server/website');
const roleModules= require(config.__base + 'server/role_modules');
const Payments= require(config.__base + 'server/payment');
const AuthService = require('../users/services/Auth');
const otp = require('../otp/index');
const promotionService = require('../promotion/index');
const request=require('request');
const passport=require('passport');
const crypto=require('crypto');
const responseCode = require('../utils/response_code');


module.exports = function(app) {
    app.use('/',dummy);
    app.get('/admin/*',adminUsers);
    app.post('/admin/*',adminUsers);
    app.post('/users/*',Users);
    app.post('/roles/*',roleModules);
    app.get('/users/*',Users);
    app.get('/routes/*',Users);
    app.post('/payments/*',Payments);
    /*app.get('/payments/!*',Payments);*/
    app.use('/',email_template);
    app.use('/',cms);
    app.use('/',otp);
    app.use('/',image);
    app.use('/',website);
    app.use('/',promotionService);
    app.get("/server_time", function (req, res, next) {
        res.json({serverTime: Date.now()});
    });
    app.get('/auth/google', passport.authenticate('google', { scope: [
        'https://www.googleapis.com/auth/plus.login',
        'https://www.googleapis.com/auth/plus.profile.emails.read']
    }),function (req,res) {
        console.log('auth google middleware')
    });
    app.get('/auth/facebook', passport.authenticate('facebook',{display: 'popup' ,scope:['email']}));
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            failureRedirect: '/'
        }), function (req, res) {
            let respBodyData = req.body;
            if (respBodyData && respBodyData.responseData) {
            let responseData = respBodyData.responseData;
            if(responseData.respCode == 100){
                AuthService.UserAutharized(responseData.emailId)
                    .then(function (result) {
                        res.cookie('token', result.token, {maxAge: 1000000000, httpOnly: true});
                        res.redirect('/league');
                    })
                    .catch(function (errorCode) {
                        console.log('userLoginCtrl', errorCode);
                        res.redirect('/?status=false&code='+responseCode.EMAIL_ID_NOT_AUTHURISED);
                    })
            }else{
                res.redirect('/?status=false&code='+responseCode.EMAIL_ID_NOT_AUTHURISED);
            }

            }else{
                res.redirect('/?status=false&code='+responseCode.EMAIL_ID_NOT_AUTHURISED);
            }
        });
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            failureRedirect: '/'
        }), function (req, res) {

            let respBodyData = req.body;
            if (respBodyData && respBodyData.responseData) {
                let responseData = respBodyData.responseData;
                if(responseData.respCode == 100){
                    AuthService.UserAutharized(responseData.emailId)
                        .then(function (result) {
                            console.log('UserAutharized', result);
                            res.cookie('token', result.token, {maxAge: 1000000000, httpOnly: true});
                            res.redirect('/league');

                        })
                        .catch(function (errorCode) {
                            console.log('userLoginCtrl', errorCode);
                            res.redirect('/?status=false&code='+responseCode.EMAIL_ID_NOT_AUTHURISED);
                        })
                }else{
                    res.redirect('/?status=false&code='+responseCode.EMAIL_ID_NOT_AUTHURISED);
                }

            }else{
                res.redirect('/?status=false&code='+responseCode.EMAIL_ID_NOT_AUTHURISED);
            }
        });
    app.get('*', function(req, res){
        res.render('404error', {layout :'home'});
    });
};


