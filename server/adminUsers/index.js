/**
 * Created by adityagupta on 18/1/17.
 */
const express=require('express');
const routes=express.Router();
const UserCtrl=require('./controller');
const middleware = require('../middleware');


routes.post('/admin/login',middleware.auditTrailLog,middleware.checkIPAllowed,UserCtrl.adminLogin);
routes.post('/admin/logout',middleware.auditTrailLog,UserCtrl.adminLogout);
routes.post('/admin/validate/token',middleware.auditTrailLog,UserCtrl.validateToken);
routes.post('/admin/change/password',middleware.auditTrailLog,middleware.getAdminIdFromToken,UserCtrl.changePassword);

routes.post('/admin/adminLogin',middleware.auditTrailLog,UserCtrl.bannerAdminLogin);

routes.get('/*',function (req,res) {
    res.json({code:404,error:'invalid request'})
});

module.exports=routes;
