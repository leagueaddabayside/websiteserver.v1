/**
 * Created by adityagupta on 19/1/17.
 */
const config=require('../../../bin/config');
const gnUserMaster=require('../../users/models/gnUserMaster');
const Crypto=require(config.__base+'/server/utils/util');
const tokenizer=require('../../redis/redis');
const responseCode =require('../../utils/response_code');
const logger =require('../../utils/logger').websiteLogs;

const AuthClass=function () {
    
}
AuthClass.prototype.Autharized=(params)=>{
    let password = Crypto.CreateHash(params.password);
    var responseData = {};
    return new Promise((resolve,reject)=>{
        gnUserMaster.findOne({where:{userName:params.userName,userType : 'ADMIN',status : 'ACTIVE'}})
            .then((user)=>{
                if(!user) return reject(responseCode.INVALID_ADMIN_CREDENTIAL);
                if(user.password===password ){
                    responseData.userId = user.userId;
                    responseData.roleId = user.roleId;
                    responseData.screenName = user.screenName;
                    return tokenizer.setAdminToken(user,params.adminType);
                }
                reject(responseCode.INVALID_ADMIN_CREDENTIAL);
            })
            .then(function(tokenizerResponse){
                logger.info('tokenizerResponse',tokenizerResponse);
                responseData.token = tokenizerResponse.token;
                resolve(responseData);
            })
            .catch (reject);
    });
}
AuthClass.prototype.UpdatePassword=function (user) {
    return new Promise((resolve,reject)=>{
        gnUserMaster.findOne({where:{userId:user.userId,password:Crypto.CreateHash(user.password)}})
            .then((newUser)=>{
                if(!newUser) return reject('User match not found');
                newUser.password=Crypto.CreateHash(user.newPassword);
                newUser.save();
                resolve(newUser);
            }).catch(logger.info)

    })
}

AuthClass.prototype.authrizeGoogleUser=(profile)=>{
    return new Promise((resolve,reject)=>{
        gnUserMaster.findOrCreate({where:profile})
            .then(resolve)
            .catch(reject)
    });
}
AuthClass.prototype.validateToken=function (params) {
    logger.info('Auth js '+params.token);
    return new Promise((resolve,reject)=>{
        tokenizer.checkAdminToken(params)
            .then(resolve)
            .catch(reject);
    })
}

AuthClass.prototype.logout=function (params) {
    return new Promise((resolve,reject)=>{
        tokenizer.removeAdminToken(params)
            .then(resolve)
            .catch(reject);
    })
}
module.exports=new AuthClass();


/*************************************/
if(require.main==module){
    (function () {
        user={
            password:'cdf4a007e2b02a0c49fc9b7ccfbb8a10c644f635e1765dcf2a7ab794ddc7edac',
            newPassword:'sdcsdc',
            userId:6
        }
        module.exports.up(user).then(value => {
            logger.info(value);
        }).catch(err =>{
            logger.info(err);
        });

    })();
}