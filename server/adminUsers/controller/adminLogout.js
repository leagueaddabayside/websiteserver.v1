/**
 * Created by adityagupta on 19/1/17.
 */
const AuthService=require('../services/Auth');
const jwt = require('jsonwebtoken');
const responseMessage =require('../../utils/response_message');

const LogoutCtrl=(req,res)=>{
    let params=req.body;
    params.adminType = 'WEBSITE_ADMIN';
    if(params.token){
        AuthService.logout(params)
            .then(function (result) {
                return  res.json({respCode:100,respData:result})
            })
            .catch (function (err) {
            res.json({respCode:501,message:responseMessage[501]})
            })
    } else {
        res.json({respCode:501,message:responseMessage[501]})

    }
}
module.exports=LogoutCtrl;



/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                toksen:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjYsImlhdCI6MTQ4NjQxMDQ2MH0.xw8QwhMXcA-LlizFaIbxXYVEQHjmMl1kKVAWAVMWjD4",
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        LogoutCtrl(req,res);
    })()
}