/**
 * Created by adityagupta on 19/1/17.
 */
const AdminLoginCtrl=require('./adminLogin');
const ChangePasswordCtrl=require('./changePassword');
const ValidateTokenCtrl=require('./validateToken');
const AdminLogoutCtrl=require('./adminLogout');

const bannerAdminLoginCtrl=require('./bannerAdminLogin');


module.exports.adminLogin = AdminLoginCtrl;
module.exports.adminLogout = AdminLogoutCtrl;
module.exports.changePassword = ChangePasswordCtrl;
module.exports.validateToken = ValidateTokenCtrl;
module.exports.bannerAdminLogin = bannerAdminLoginCtrl;
