/**
 * Created by adityagupta on 19/1/17.
 */
const AuthService = require('../services/Auth')
const responseMessage = require('../../utils/response_message');
const RoleMasterService = require('../../role_modules/services/roleMaster');
const logger = require('../../utils/logger').websiteLogs;

const bannerAdminLoginCtrl = (req, res)=> {
    let params = req.body;
    params.adminType = 'BANNER_ADMIN';

    AuthService.Autharized(params)
        .then(function (result) {
            let userInfo = {userId : result.userId,userName : result.screenName , token : result.token,roleId : result.roleId};
            let reqParam = {userId : result.userId,relatedTo : 'BANNER_PANEL',roleId : result.roleId};
            RoleMasterService.getRoleAttributes(reqParam)
                .then(function (attributes) {
                    let modulesArr = [];
                    for(let i=0,length = attributes.length;i<attributes.length ; i++){
                        let currentModule = attributes[i];
                        let currentObject = {};
                        currentObject.name = currentModule.name;
                        currentObject.url = currentModule.uihref;
                        currentObject.module_name = currentModule.uihref;
                        currentObject.action_allowed = 'extended';
                        currentObject.description = currentModule.description;
                        modulesArr.push(currentObject);
                    }

                    res.json({status: true,message : 'Success', modules: modulesArr,userInfo : userInfo})
                    return;
                })
                .catch(function (err) {
                    logger.error(err);
                    res.json({status: false, message: responseMessage[102]})
                    return;
                })


        })
        .catch(function (errorCode) {
        res.json({status: false, message: responseMessage[errorCode]})
    })
}
module.exports = bannerAdminLoginCtrl;


/**********************/
if (require.main == module) {
    (function () {
        var req = {
            body: {
                userName: 'aditya',
                password: 'ssenthusiasm',
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        bannerAdminLoginCtrl(req, res);
    })()
}