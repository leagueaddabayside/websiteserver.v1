/**
 * Created by adityagupta on 19/1/17.
 */
const AuthService=require('../services/Auth')

const UpdatePassCtrl=(req,res)=>{
    let params=req.body;

    params.userId = params.adminId;
    AuthService.UpdatePassword(params)
        .then(function (result) {
            res.json({respCode:100,data:result,message:[]})
        })
        .catch (function (err) {
            res.json({respCode:400,data:[],message:'Error in updating'})
        })
}
module.exports=UpdatePassCtrl;
