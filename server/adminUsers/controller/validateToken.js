/**
 * Created by adityagupta on 3/2/17.
 */
/**
 * Created by adityagupta on 19/1/17.
 */
const AuthService=require('../services/Auth');
const Util=require('../../utils/util');
const responseMessage =require('../../utils/response_message');


const ValidateTokenCtrl=(req,res)=>{
    let params=req.body;
    let token=null;
    params.adminType = 'WEBSITE_ADMIN';
    if(!token)
        token=params.token;
    if(!!token) {
        AuthService.validateToken(params)
            .then((result) => {
                res.json({respCode: 100, respData: result})
            })
            .catch((errorCode) => {
                res.json({respCode: errorCode, message: responseMessage[errorCode]})
            });
    }
};
module.exports=ValidateTokenCtrl;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId: 6,
                token:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjYsImlhdCI6MTQ4NjA3NDYyNH0.it_AGc48VE3pxFwOcICNuy9jN4Venp6z8wB9CLLcSLc',
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        ValidateTokenCtrl(req,res);
    })()
}
