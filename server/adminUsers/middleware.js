/**
 * Created by adityagupta on 19/1/17.
 */
let UserMiddlwareClass=function () {

}

UserMiddlwareClass.prototype.validateEmail=()=>{
        if (email && email.indexOf('@') != -1) {
            var name = email.substring(0, email.indexOf('@'));
            var domain = email.substring(email.indexOf('@')+1, email.length);

            var nameRegex = /^[a-zA-Z0-9.+_-]+$/;
            var domainRegex = /^[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

            var retVal = false;

            if (name.match(nameRegex)) {
                retVal = true;

                //email name should not lead or trail with a '.'
                //there should not be multiple dots
                if (name.charAt(0) == '.' || name.charAt(name.length-1) == '.' ||
                    name.indexOf('..') != -1) {
                    retVal = false;
                }

                if (retVal) {
                    if (domain.match(domainRegex)) {
                        //domain name should not lead with a '.' or '-'
                        //there should not be multiple dots
                        //it should not end with 'web'
                        if (domain.charAt(0) == '.' || domain.charAt(0) == '-' ||
                            domain.indexOf('..') != -1 || domain.substring(domain.length-3, domain.length).toLowerCase() == 'web') {
                            retVal = false;
                        }
                    } else {
                        retVal = false;

                        //it should be a valid IP, may or may not be contained in []
                        if (domain.charAt(0) == '[' && domain.charAt(domain.length-1) == ']') {
                            domain = domain.substring(1, domain.length-1);
                        }

                        var ipRegex = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
                        if (domain.match(ipRegex)) {
                            retVal = true;
                        }
                    }//domain regex match check
                }//name matched regex, and didn't have other issues
            }//name matches regex

            return retVal;
        } else {
            errHelper.error('[ERR] Something went wrong in validating email ');
            errHelper.log('[ERR] Something went wrong in validating email');
            return false;
        }
}