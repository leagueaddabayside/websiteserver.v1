require("../../utils/mongo_connection");
var logger = require("../../utils/logger");
var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

var emailHistorySchema = new mongoose.Schema({
	emailFrom: {type: String},
	emailTo : String,
	emailSubject: String,	
	emailBody: {type: String},
	emailType : String,
	status: {type: String, enum: ['PENDING', 'SENT','FAILED']},
	createAt: {type: Date, default: Date.now}
});

module.exports = mongoose.model('EmailHistory', emailHistorySchema, "email_history");

emailHistorySchema.plugin(autoIncrement.plugin, {
	model: 'EmailHistory',
	startAt: 1,
	incrementBy: 1
});