require("../../utils/mongo_connection");
var logger = require("../../utils/logger");
var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

var emailTemplateSchema = new mongoose.Schema({
	templateId: {type: Number, unique: true},
	templateType: {type: String, required: true, unique: true},
	dummyTemplate : String,
	emailName : String,
	emailSubject: String,	
	emailBody: {type: String},
	status: {type: String, enum: ['ACTIVE', 'INACTIVE']},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model('EmailTemplate', emailTemplateSchema, "email_template");

emailTemplateSchema.plugin(autoIncrement.plugin, {
	model: 'EmailTemplate',
	field: 'templateId',
	startAt: 1,
	incrementBy: 1
});