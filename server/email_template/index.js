'use strict';

let express=require('express');
let route=express.Router();
const middleware = require('../middleware');

var email_template = require('./controllers/index');

route.post("/addEmailTemplate",middleware.auditTrailLog,middleware.getAdminIdFromToken, email_template.addEmailTemplateApi);
route.post("/updateEmailTemplate",middleware.auditTrailLog,middleware.getAdminIdFromToken, email_template.updateEmailTemplateApi);
route.post("/findEmailTemplateByProperty",middleware.auditTrailLog,middleware.getAdminIdFromToken,
			email_template.getEmailTemplateByPropertyApi);
route.post("/findEmailTemplateList",middleware.auditTrailLog,middleware.getAdminIdFromToken, email_template.getEmailTemplateListApi);


module.exports=route;