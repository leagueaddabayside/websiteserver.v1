var EmailTemplate = require("../models/email_template");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function updateEmailTemplate(requestObject, callback) {
	
	var updateObject = new Object();
	if(requestObject.templateType)
		updateObject.templateType = requestObject.templateType;

	if(requestObject.dummyTemplate)
		updateObject.dummyTemplate = requestObject.dummyTemplate;


	if(requestObject.emailName)
		updateObject.emailName = requestObject.emailName;

	if(requestObject.emailSubject)
		updateObject.emailSubject = requestObject.emailSubject;
	
	if(requestObject.emailBody)
		updateObject.emailBody = requestObject.emailBody;

	if(requestObject.status)
		updateObject.status = requestObject.status;

	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {templateId: requestObject.templateId};
	EmailTemplate.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateEmailTemplate;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.templateId = 1,
	requestObject.templateType = "blogTitle",
	requestObject.emailName = "imagePath",
	requestObject.emailSubject = "pageContent",
	requestObject.emailBody = "urlName";
	requestObject.status = "ACTIVE";
	console.log(requestObject);

	updateEmailTemplate(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}