var addEmailTemplateService = require('./add_email_template');
var updateEmailTemplateService = require('./update_email_template');
var findEmailTemplateByPropertyService = require('./find_email_template_by_property');
var findEmailTemplateListService = require('./find_email_template_list');
var addEmailHistoryService = require('./add_email_history');

module.exports = {
	addEmailTemplate : addEmailTemplateService,
	updateEmailTemplate : updateEmailTemplateService,
	findEmailTemplateByProperty : findEmailTemplateByPropertyService,
	findEmailTemplateList : findEmailTemplateListService,
	addEmailHistory : addEmailHistoryService
};