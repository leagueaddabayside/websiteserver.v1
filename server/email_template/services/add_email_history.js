var EmailHistory = require("../models/email_history");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function addEmailHistory(requestObject, callback) {
	var newEmailHsitory = new EmailHistory({
		emailFrom : requestObject.emailFrom,
		emailTo : requestObject.emailTo,
		emailSubject : requestObject.emailSubject,		
		emailBody : requestObject.emailBody,
		emailType : requestObject.emailType,
		status : requestObject.status,
	});

	var responseObject = new Object();
	newEmailHsitory.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addEmailHistory;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.emailFrom = "aa@info.com",
	requestObject.emailTo = "imagePath",
	requestObject.emailSubject = "Forgot password OTP",
	requestObject.emailBody = "your otp is {{otpToken}}";
	requestObject.emailType = "FORGOT_PASSWORD_OTP";
	requestObject.status = "PENDING";
	console.log(requestObject);

	addEmailHistory(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}