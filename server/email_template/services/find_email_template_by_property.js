var EmailTemplate = require("../models/email_template");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function findEmailTemplateByProperty(requestObject, callback) {
	console.log('findEmailTemplateByProperty:',requestObject);
	var responseObject = new Object();

	var query = EmailTemplate.findOne({});
	if(requestObject.templateId)
		query.where("templateId").equals(requestObject.templateId);

	if(requestObject.templateType)
		query.where("templateType").equals(requestObject.templateType);
	
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
    });
}

module.exports = findEmailTemplateByProperty;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.templateType = 'FORGOT_PASSWORD_OTP';

	findEmailTemplateByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}