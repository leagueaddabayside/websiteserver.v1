var EmailTemplate = require("../models/email_template");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function findEmailTemplateList(requestObject, callback) {
	var responseObject = new Object();

	var query = EmailTemplate.find({});
	if(requestObject.status)
		query.where("status").equals(requestObject.status);

	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
    });
}

module.exports = findEmailTemplateList;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findEmailTemplateList(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}