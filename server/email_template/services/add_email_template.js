var EmailTemplate = require("../models/email_template");
var logger = require("../../utils/logger").websiteLogs;;
var responseCode = require("../../utils/response_code");

function addEmailTemplate(requestObject, callback) {
	var newEmailTemplate = new EmailTemplate({
		templateType : requestObject.templateType,
		dummyTemplate : requestObject.dummyTemplate,
		emailName : requestObject.emailName,
		emailSubject : requestObject.emailSubject,		
		emailBody : requestObject.emailBody,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newEmailTemplate.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addEmailTemplate;

//	Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.templateType = "FORGOT_PASSWORD_OTP",
	requestObject.emailName = "imagePath",
	requestObject.emailSubject = "Forgot password OTP",
	requestObject.emailBody = "your otp is {otpToken}";
	requestObject.status = "ACTIVE";
	requestObject.createBy = 1;
	console.log(requestObject);

	addEmailTemplate(requestObject, function(error, responseObject) {
		console.log("Response Code - "+responseObject.responseCode);
		if(error)
			console.log("Error - "+error);
		else
			console.log("Response Data - "+responseObject.responseData);
	});
}