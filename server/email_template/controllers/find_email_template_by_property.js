var logger = require("../../utils/logger").websiteLogs;;
var emailTemplateService = require('../services/index');
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findEmailTemplateByProperty(request, response, next) {
	var requestData = request.body;
	// console.log("getGroupByProperty API :- Request - %j", requestData);

	var responseObject = new Object();
	emailTemplateService
			.findEmailTemplateByProperty(
					requestData,
					function(error, data) {
						if(error === null) {
							responseObject.respCode = data.responseCode;
							responseObject.respData =  data.responseData;
						} else {
							responseObject.respCode = data.responseCode;
							responseObject.message = responseMessage[data.responseCode];
						}
						//logger.info("findEmailTemplateByProperty API :- Response - %j",	responseObject);
						response.json(responseObject);
					});
}

module.exports = findEmailTemplateByProperty;

//Unit Test Case

if (require.main === module) {
	(function() {
		var request = {};

		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.templateId = 1;
		console.log(requestObject);
		request.body = requestObject;
		findEmailTemplateByProperty(request, response);
	})();
}