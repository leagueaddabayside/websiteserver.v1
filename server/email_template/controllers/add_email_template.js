var logger = require("../../utils/logger").websiteLogs;;
var emailTemplateService = require('../services/index');
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addEmailTemplate(request, response, next) {
	var requestData = request.body;
	// console.log("addGroup API :- Request - %j", requestData);

	var responseObject = new Object();
	emailTemplateService
			.addEmailTemplate(
					requestData,
					function(error, data) {
						if(error === null) {
							responseObject.respCode = data.responseCode;
							responseObject.respData =  data.responseData;
						} else {
							responseObject.respCode = data.responseCode;
							responseObject.message = responseMessage[data.responseCode];
						}

						logger.info("addEmailTemplate API :- Response -", responseObject);
						response.json(responseObject);

					});

}

module.exports = addEmailTemplate;

// Unit Test Case

if (require.main === module) {
	(function() {
		var request = {};

		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.templateType = "blogTitle2",
		requestObject.emailName = "imagePath",
		requestObject.emailSubject = "pageContent",
		requestObject.emailBody = "urlName";
		requestObject.status = "ACTIVE";
		requestObject.createBy = 1;
		console.log(requestObject);
		request.body = requestObject;
		addEmailTemplate(request, response);
	})();
}