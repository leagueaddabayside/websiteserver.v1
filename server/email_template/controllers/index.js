var addEmailTemplateApi = require('./add_email_template');
var updateEmailTemplateApi = require('./update_email_template');
var getEmailTemplateByPropertyApi = require('./find_email_template_by_property');
var getEmailTemplateListApi = require('./find_email_template_list');

module.exports = {
	addEmailTemplateApi : addEmailTemplateApi,
	updateEmailTemplateApi : updateEmailTemplateApi,
	getEmailTemplateByPropertyApi : getEmailTemplateByPropertyApi,
	getEmailTemplateListApi : getEmailTemplateListApi
}