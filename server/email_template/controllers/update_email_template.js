var logger = require("../../utils/logger").websiteLogs;;
var emailTemplateService = require('../services/index');
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateEmailTemplate(request, response, next) {
	var requestData = request.body;
	// console.log("updateGroup API :- Request - %j", requestData);

	var responseObject = new Object();
	emailTemplateService
			.updateEmailTemplate(
					requestData,
					function(error, data) {
						if(error === null) {
							responseObject.respCode = data.responseCode;
							responseObject.respData =  data.responseData;
						} else {
							responseObject.respCode = data.responseCode;
							responseObject.message = responseMessage[data.responseCode];
						}
						logger.info("updateEmailTemplate API :- Response - %j",
							responseObject);
						response.json(responseObject);
					});
}

module.exports = updateEmailTemplate;

//Unit Test Case

if (require.main === module) {
	(function() {
		var request = {};

		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.templateId = 1,
		requestObject.templateType = "blogTitle",
		requestObject.emailName = "imagePath",
		requestObject.emailSubject = "pageContent",
		requestObject.emailBody = "urlName";
		requestObject.status = "ACTIVE";
		console.log(requestObject);
		request.body = requestObject;
		updateEmailTemplate(request, response);
	})();
}