/**
 * Created by sumit on 1/18/2017.
 */
'use strict';

let express=require('express');
let route=express.Router();
var website = require('./controllers/index');
var handleLogin = require('./filters/handle_login');
var checkLogin = require('./filters/check_login');
var checkMobile = require('./filters/check_mobile');
var getMatchScoreCard = require('./filters/get_match_scorecard');

route.get('/',handleLogin, website.indexPage);
route.get('/league',handleLogin,checkLogin, website.leaguePage);
route.get('/match-leagues',handleLogin,checkLogin,checkMobile, website.tourMatches);
route.get('/match-leagues/:matchId',handleLogin,checkLogin,checkMobile, website.matchLeague);
route.get('/browse-league/:matchId/:leagueId',handleLogin,checkLogin,checkMobile, website.browseLeague);
route.get('/my-joined-league/:matchId',handleLogin,checkLogin,checkMobile, website.myJoinedLeague);
route.get('/my-teams/:matchId',handleLogin,checkLogin,checkMobile, website.myTeams);

route.get('/verify',handleLogin,checkLogin, website.verifyUserPage);
route.get('/createteam/:matchId/:teamId',handleLogin,checkLogin, website.createTeamPage);
route.get('/editteam/:matchId/:teamId',handleLogin,checkLogin, website.editTeamPage);
route.get('/page/:pageName',handleLogin, website.websiteConstantPage);
route.get('/myprofile',handleLogin,checkLogin,checkMobile, website.myProfilePage);
route.get('/myaccount',handleLogin,checkLogin, website.myAccountPage);
route.get('/sel-cap-vcap',handleLogin,checkLogin,checkMobile, website.selCapVcap);
route.get('/fantasy-scorecard/:matchId',handleLogin,getMatchScoreCard, website.showScorecardApi);
route.get('/payments/txn/history/download',handleLogin,checkLogin,website.downloadUserTxnHistory);
route.get('/promotions',handleLogin, website.promotionsApi);

module.exports=route;
