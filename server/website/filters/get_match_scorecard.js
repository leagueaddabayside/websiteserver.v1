/**
 * Created by sumit on 2/13/2017.
 */
var logger = require("../../utils/logger").websiteLogs;
var request = require('request');
var config = require("../../../bin/config");
var constant = require("../../utils/constant");

function getMatchScorecard(requestObj, response, next) {

    var requestParam = requestObj.params;
    request.post({
        url: config.gameServerUrl + '/fetchTourMatchScoreCard',
        form: {matchId : requestParam.matchId,  accessKey : constant.SERVER_REQUEST_ACCESS_KEY}
    }, function (error, responseData, body) {
        var respData = JSON.parse(body);
        logger.info('getMatchScorecard',body);
        if(respData.respCode == 100){
            requestObj.body.scoreCardData = respData.respData;
        }

        next();
    });

}

module.exports = getMatchScorecard;