/**
 * Created by sumit on 2/13/2017.
 */
var logger = require("../../utils/logger").websiteLogs;

function checkLogin(request, response, next) {

    if (response.locals.is_desktop) {
        if (!response.locals.isLogin) {
            response.redirect('/');
        } else {
            response.redirect('/league');
        }
    } else {
        next();
    }

}

module.exports = checkLogin;