var logger = require("../../utils/logger").websiteLogs;
var tokenizer = require('../../redis/redis');

function handleLogin(request, response, next) {

	var loginData = {};
	loginData.isLogin = false;
	loginData.serverTime = new Date().getTime();
	var isLogin = false;

	if (request.cookies.token) {
		logger.trace('handleLogin', request.cookies.token);

		tokenizer.checkToken(request.cookies.token)
			.then(function(tokenInfo){
				//console.log(tokenInfo);
				if (tokenInfo) {
					loginData.token = request.cookies.token;
					loginData.userId = tokenInfo.userId;
					loginData.isLogin = true;
					loginData.isProduction = response.locals.isProduction;

					if(tokenInfo.screenName && tokenInfo.screenName !== 'null'){
						loginData.screenName = tokenInfo.screenName;
						loginData.myName = tokenInfo.screenName;
					}else{
						loginData.myName = tokenInfo.emailId;
					}
					if(tokenInfo.firstName && tokenInfo.firstName !== 'null'){
						loginData.userName = tokenInfo.firstName;
						if(tokenInfo.lastName && tokenInfo.lastName !== 'null'){
							loginData.userName += ' '+tokenInfo.lastName;
						}
					}

					loginData.dob = tokenInfo.dob;
					loginData.state = tokenInfo.state;
					loginData.userStatus = tokenInfo.userStatus;

					let depositAmt = parseFloat(tokenInfo.depositAmt);
					let bonusAmt = parseFloat(tokenInfo.bonusAmt);
					let winningAmt = parseFloat(tokenInfo.winningAmt);
					let balance = depositAmt + bonusAmt + winningAmt;
					loginData.balance = parseFloat(balance).toFixed(2);

					isLogin = true;
					response.locals.loginData = loginData;
					response.locals.isLogin = isLogin;
					next();

				} else {
					response.clearCookie('token');
					response.locals.loginData = loginData;
					response.locals.isLogin = isLogin;
					logger.trace('response.locals', response.locals);
					next();
				}

			})
			.catch(function (err) {
				response.clearCookie('token');
				response.locals.loginData = loginData;
				response.locals.isLogin = isLogin;
				logger.info('response.locals', response.locals);
				next();
			});


	}else{
		response.locals.loginData = loginData;
		response.locals.isLogin = isLogin;
		logger.trace('response.locals', response.locals);
		next();
	}

}

module.exports = handleLogin;
