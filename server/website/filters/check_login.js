/**
 * Created by sumit on 2/13/2017.
 */
var logger = require("../../utils/logger").websiteLogs;

function checkLogin(request, response, next) {
    logger.info('response.locals.isLogin', response.locals.isLogin);

    if (!response.locals.isLogin) {
        response.redirect('/');
    } else {
        next();
    }
}

module.exports = checkLogin;