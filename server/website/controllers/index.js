/**
 * Created by sumit on 2/2/2017.
 */
var websiteConstantPageApi = require("./website_constant_page");
var indexPageApi = require("./index_page");
var leaguePageApi = require("./league_page");
var verifyUserPageApi = require("./verify_user_page");
var createTeamPageApi = require("./create_team_page");
var editTeamPageApi = require("./edit_team_page");
var myProfilePageApi = require("./my_profile");
var myAccountPageApi = require("./my_account");
var tourMatchesApi = require("./tour_match");
var matchLeagueApi = require("./match_leagues");
var browseLeagueApi = require("./browse_league");
var myJoinedLeagueApi = require("./my_joined_league");
var myTeamsApi = require("./my_teams");
var selCapVcapApi = require("./sel_cap_vcap");
var showScorecardApi = require("./showScorecard");
const downloadUserTxnHistory=require('./downloadUserTxnHistory');
var promotionsApi = require("./promotions");

module.exports.websiteConstantPage = websiteConstantPageApi;
module.exports.indexPage = indexPageApi;
module.exports.leaguePage = leaguePageApi;
module.exports.verifyUserPage = verifyUserPageApi;
module.exports.createTeamPage = createTeamPageApi;
module.exports.editTeamPage = editTeamPageApi;
module.exports.myProfilePage = myProfilePageApi;
module.exports.myAccountPage = myAccountPageApi;
module.exports.tourMatches = tourMatchesApi;
module.exports.matchLeague = matchLeagueApi;
module.exports.browseLeague = browseLeagueApi;
module.exports.myJoinedLeague = myJoinedLeagueApi;
module.exports.myTeams = myTeamsApi;
module.exports.selCapVcap = selCapVcapApi;
module.exports.showScorecardApi = showScorecardApi;
module.exports.downloadUserTxnHistory=downloadUserTxnHistory;
module.exports.promotionsApi = promotionsApi;
