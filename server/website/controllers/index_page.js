var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function indexPage(request, response, next) {

    let meta=config.seoMetaTitles['index'];
	if (response.locals.isLogin) {
        if (response.locals.is_desktop) {
            response.redirect('/league');
        }else{
            response.redirect('/match-leagues');
        }
		return;
	}

	var responsePageData = {};

	var seoData = {};
	seoData.metaTitle = 'LeagueAdda';
	seoData.metaKeywords = 'LeagueAdda';
	seoData.metaDescription = 'LeagueAdda';
	responsePageData.seoData = meta;

	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';


       var testimonialList = [
             {
                 name: "Ashutosh Garg",
                 img: "IMG_5592.jpg",
                 comment: "LeagueAdda is a great site because it gives users real time experience. I have received all my prizes till date and hence it's a trustable site too. I will continue to play at LeagueAdda."
             },

             {
                 name: "Manpreet Singh",
                 img: "IMG_5593.jpg",
                 comment: "LeagueAdda provides haslte free platform for cricket lovers to challange friends and win great cash prizes. Great fun playing here !"
             },

             {
                 name: "Bhavneesh Gupta",
                 img: "IMG_5594.jpg",
                 comment: "OMG... First Orkut, then FB... Now LeagueAdda!!! i mean im gettting addicted to LeagueAdda.. Spending hours here in LeagueAdda!!! and love to play here ! ;)"
             },
             {
                 name: "Shikher Sinha",
                 img: "IMG_5595.jpg",
                 comment: "I enjoy playing at ‘Leagueadda’ & won many Leagues...I could make money with my cricket knowledge and skill. WOW...!!! "
             },
             {
                 name: "Jagriti Saxena",
                 img: "IMG_5596.jpg",
                 comment:  "Feels good to be a part of LeagueAdda . Best part is the spontaneous update of points and i haven't noticed any fraud activity till now."
             },
             {
                 name: "Sumit Singla",
                 img: "IMG_5597.jpg",
                 comment:  "As I am a cricket lover, playing on LeagueAdda excites me even more. I have never faced any issue with withdrawal."
             },
             {
                 name: "Ankit Ghiya",
                 img: "IMG_5598.jpg",
                 comment:  "I enjoy playing in League Adda. I could make money with my cricket knowledge and skills..Won around 10k playing here!! "
             }
        ];

responsePageData.testimonialList = testimonialList;



	response.render('index', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = indexPage;

