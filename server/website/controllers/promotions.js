var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function promotionsApi(request, response, next) {
	var param = request.params;
   
	var responsePageData = {};
    let meta=config.seoMetaTitles['promotions'];
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Promotions';
	seoData.metaKeywords = 'LeagueAdda - Promotions';
	seoData.metaDescription = 'LeagueAdda - Promotions';
	responsePageData.seoData = meta;

	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('promotions', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = promotionsApi;
