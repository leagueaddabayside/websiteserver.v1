var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function leaguePage(request, response, next) {
	var responsePageData = {};
    let meta=config.seoMetaTitles['matchLeagues'];
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Matches';
	seoData.metaKeywords = 'LeagueAdda - Matches';
	seoData.metaDescription = 'LeagueAdda - Matches';
	responsePageData.seoData = meta;


	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('mobile/tour_matches', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = leaguePage;

