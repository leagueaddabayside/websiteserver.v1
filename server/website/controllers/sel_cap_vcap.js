var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function selCapVcap(request, response, next) {
	var param = request.params;

	var responsePageData = {};
    let meta=config.seoMetaTitles['sal'];
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Select Caption & Vice Caption';
	seoData.metaKeywords = 'LeagueAdda - Select Caption & Vice Caption';
	seoData.metaDescription = 'LeagueAdda - Select Caption & Vice Caption';
	responsePageData.seoData = meta;


	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('mobile/sel_c_vc', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = selCapVcap;

