/**
 * Created by adityagupta on 10/2/17.
 */
const config=require('../../../bin/config')
const userTxnMaster=require('../../payment/services/userRealTxnMaster');
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');
const Util=require('../../utils/util');
var Excel = require("exceljs");
var moment = require("moment");
const logger=require('../../utils/logger').websiteLogs;

const UserTxnsHistory=(req,res)=>{
    let params=req.body;
    let loginData = res.locals.loginData;

    params.userId= loginData.userId || false;
    if(params.userId){
        var workbook = new Excel.Workbook();
        var worksheet = workbook.addWorksheet("User Txn Report");

        worksheet.columns = [
            {header: "Sr No", key: "srNo", width: 10},
            {header: "Transaction Id", key: "txnId", width: 10},
            {header: "Transaction Date", key: "txnDate", width: 10},
            {header: "Transaction Type", key: "txnType", width: 10},
            {header: "Amount", key: "amount", width: 32},
            {header: "remarks", key: "remarks", width: 10}
        ];


        userTxnMaster.findByUserId(params)
            .then(function (txns) {


                for (var i=0, length=txns.length; i<length; i++) {
                    var currentRow = txns[i];
                    var currentObj = {};
                    currentObj.srNo = i+1;
                    currentObj.txnId = currentRow.txnId;
                    currentObj.txnType = currentRow.txnType;
                    currentObj.txnDate = moment.unix(currentRow.txnDate).format("YYYY-DD-MM HH:mm:ss");
                    currentObj.amount = currentRow.amount;
                    currentObj.remarks = currentRow.remarks;

                    worksheet.addRow(currentObj);
                }

                res.setHeader('Content-Type', 'application/vnd.openxmlformats');
                res.setHeader("Content-Disposition", "attachment; filename=" + "txnReport.xlsx");
                workbook.xlsx.write(res).then(function () {
                    logger.info("xls file is written.");

                    res.end();
                })

            }).catch(function (err) {
                logger.error(err);
            res.json(Util.response(400,'failed',err));
        })
    }else {
        res.json({respCode:responseCode.USER_NOT_EXIST,message:responseMessage[responseCode.USER_NOT_EXIST]})
    }
}

module.exports=UserTxnsHistory;

/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId: 2,
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        UserTxnsHistory(req,res);
    })()
}
