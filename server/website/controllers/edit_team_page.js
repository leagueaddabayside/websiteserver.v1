var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function editTeamPage(request, response, next) {
	var param = request.params;
	var initObject = {matchId : param.matchId};
    let meta=config.seoMetaTitles['editteam'];
	if(param.teamId){
		initObject.teamId = param.teamId;
	}
	initObject.isNewTeam = false;
	var responsePageData = {};
	responsePageData.initObject =initObject;

	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Edit Team';
	seoData.metaKeywords = 'LeagueAdda - Edit Team';
	seoData.metaDescription = 'LeagueAdda - Edit Team';
	responsePageData.seoData = meta;


	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('createteam', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = editTeamPage;

