var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function leaguePage(request, response, next) {
	var param = request.params;
	var initObject = {matchId : param.matchId};
    let meta=config.seoMetaTitles['myTeam'];
	var responsePageData = {};
	responsePageData.initObject =initObject;
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - My Teams';
	seoData.metaKeywords = 'LeagueAdda - My Teams';
	seoData.metaDescription = 'LeagueAdda - My Teams';
	responsePageData.seoData = meta;


	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('mobile/my_teams', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = leaguePage;

