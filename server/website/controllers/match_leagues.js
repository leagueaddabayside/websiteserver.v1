var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function leaguePage(request, response, next) {
	var param = request.params;
	var initObject = {matchId : param.matchId};
    let meta=config.seoMetaTitles['matchLeagues'];
	var responsePageData = {};
	responsePageData.initObject =initObject;

	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Live Leagues';
	seoData.metaKeywords = 'LeagueAdda - Live Leagues';
	seoData.metaDescription = 'LeagueAdda - Live Leagues';
	responsePageData.seoData = meta;


	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('mobile/match_leagues', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = leaguePage;

