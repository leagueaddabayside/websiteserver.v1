var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function myAccountPage(request, response, next) {
	var responsePageData = {};
    let meta=config.seoMetaTitles['myaccount'];
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - My Account';
	seoData.metaKeywords = 'LeagueAdda - My Account';
	seoData.metaDescription = 'LeagueAdda - My Account';
	responsePageData.seoData = meta;

	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('myaccount', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = myAccountPage;

