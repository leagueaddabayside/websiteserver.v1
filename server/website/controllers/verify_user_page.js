var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function verifyUserPage(request, response, next) {
	var responsePageData = {};
    let meta=config.seoMetaTitles['verify'];
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Verify';
	seoData.metaKeywords = 'LeagueAdda - Verify';
	seoData.metaDescription = 'LeagueAdda - Verify';
	responsePageData.seoData = meta;


	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('verify', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = verifyUserPage;

