var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var cmsService = require('../../cms/services/index');
var config=require('../../../bin/config');

function websiteConstantPage(request, response, next) {
	var pageName = request.params.pageName;

	cmsService.findCMSByProperty({urlName : pageName}, function(error,resp) {
		//console.log('findCMSByProperty',resp.responseData);

		var cmsResponseData = resp.responseData;

		if(!(cmsResponseData && cmsResponseData.metaTitle)){
			response.render('404error', {layout :'home'});
			return;
		}

		var cmsPageData = {};
		cmsPageData.pageData = cmsResponseData.pageData;
		cmsPageData.pageDispName = cmsResponseData.pageDispName;
		cmsPageData.pageContent = cmsResponseData.pageContent;
		cmsPageData.pageTitle = cmsResponseData.pageTitle;
		cmsPageData.typeId = cmsResponseData.typeId;


		var seoData = {};
		seoData.metaTitle = cmsResponseData.metaTitle;
		seoData.metaKeywords = cmsResponseData.metaKeywords;
		seoData.metaDescription = cmsResponseData.metaDescription;
		cmsPageData.seoData = seoData;
		var responsePageData = cmsPageData;


		responsePageData.layout = 'home';



		cmsService.findCMSPageTypeByProperty({typeId : responsePageData.typeId},function(error,respCmsType){
			logger.info('respCmsType',respCmsType.responseData.pageDevName);


			response.render('cms/'+respCmsType.responseData.pageDevName, responsePageData,function(error,html){
				if(error){
					response.render('404error', {layout :'home'});
					return;
				}
				response.send(html);
			});
		});

	});

}

module.exports = websiteConstantPage;

