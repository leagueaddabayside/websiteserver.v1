var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function showScorecardApi(request, response, next) {
	var param = request.params;
    var requestObj = request.body.scoreCardData;
    logger.info(requestObj);
	var responsePageData = {};
    let meta=config.seoMetaTitles['scorecard'];
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Scorecard';
	seoData.metaKeywords = 'LeagueAdda - Scorecard';
	seoData.metaDescription = 'LeagueAdda - Scorecard';
	responsePageData.seoData = meta;
    responsePageData.isPlayerArr = false;
    if(requestObj && requestObj.scoreCardPlayers && requestObj.scoreCardPlayers.length > 0){
        responsePageData.isPlayerArr = true;
        responsePageData.playerScoreData =requestObj.scoreCardPlayers;

    }

    responsePageData.matchDisplayName =requestObj.matchDisplayName;

	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('scorecard', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = showScorecardApi;

var PLAYER_SCORECARD = [
        {
            "startingPoints":2,
            "playerName": "Hardik Pandya",
            "runs": 8,
            "sixes":2,
            "fours":3,
            "strike_rate": 40.0,
            "centurty": 0,
            "maiden_overs":0 ,
            "wickets": 2,
            "economy": 1.97,
            "catches": 2,
            "runouts": 0,
            "bonus":0,
            "negative":0
        },

        {
            "startingPoints":2,
            "playerName": "Cheteshwar Pujara",
            "runs": 8,
            "sixes":2,
            "fours":3,
            "strike_rate": 40.0,
            "centurty": 0,
            "maiden_overs":0 ,
            "wickets": 2,
            "economy": 1.97,
            "catches": 2,
            "runouts": 0,
            "bonus":0,
            "negative":0
        },

        {
            "startingPoints":2,
            "playerName": "Virat Kohli",
            "runs": 8,
            "sixes":2,
            "fours":3,
            "strike_rate": 40.0,
            "centurty": 0,
            "maiden_overs":0 ,
            "wickets": 2,
            "economy": 1.97,
            "catches": 2,
            "runouts": 0,
            "bonus":0,
            "negative":0
        },

        {
            "startingPoints":2,
            "playerName": "Rohit Sharma",
            "runs": 8,
            "sixes":2,
            "fours":3,
            "strike_rate": 40.0,
            "centurty": 0,
            "maiden_overs":0 ,
            "wickets": 2,
            "economy": 0,
            "catches": 2,
            "runouts": 0,
            "bonus":0,
            "negative":0
        },

        {
            "startingPoints":2,
            "playerName": "Shaun Marsh",
            "runs": 8,
            "sixes":2,
            "fours":3,
            "strike_rate": 40.0,
            "centurty": 0,
            "maiden_overs":0 ,
            "wickets": 2,
            "economy": 1.97,
            "catches": 2,
            "runouts": 0,
            "bonus":0,
            "negative":0
        },

        {
            "startingPoints":2,
            "playerName": "Josh Hazlewood",
            "runs": 8,
            "sixes":2,
            "fours":3,
            "strike_rate": 40.0,
            "centurty": 0,
            "maiden_overs":0 ,
            "wickets": 2,
            "economy": 0,
            "catches": 2,
            "runouts": 0,
            "bonus":0,
            "negative":0
        },

        {
            "startingPoints":2,
            "playerName": "David Warner",
            "runs": 8,
            "sixes":2,
            "fours":3,
            "strike_rate": 40.0,
            "centurty": 0,
            "maiden_overs":0 ,
            "wickets": 2,
            "economy": 0,
            "catches": 2,
            "runouts": 0,
            "bonus":0,
            "negative":0
        },

		{
            "startingPoints":2,
            "playerName": "M. S. Dhoni",
            "runs": 8,
            "sixes":2,
            "fours":3,
            "strike_rate": 40.0,
            "centurty": 0,
            "maiden_overs":0 ,
            "wickets": 2,
            "economy": 0,
            "catches": 2,
            "runouts": 0,
            "bonus":0,
            "negative":0
        },
     ];
