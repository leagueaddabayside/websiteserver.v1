var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function leaguePage(request, response, next) {
	var responsePageData = {};
    let meta=config.seoMetaTitles['league'];
	if (!response.locals.is_desktop) {
			response.redirect('/match-leagues');
		return;
	}
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Live Leagues';
	seoData.metaKeywords = 'LeagueAdda - Live Leagues';
	seoData.metaDescription = 'LeagueAdda - Live Leagues';
	responsePageData.seoData = meta;
	console.log(meta);

	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('league', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = leaguePage;

