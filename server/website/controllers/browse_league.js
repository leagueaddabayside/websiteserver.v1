var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function leaguePage(request, response, next) {
	var param = request.params;
    let meta=config.seoMetaTitles['browseLeague'];
	var initObject = {leagueId : param.leagueId,matchId : param.matchId};

	var responsePageData = {};
	responsePageData.initObject =initObject;
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Browse League';
	seoData.metaKeywords = 'LeagueAdda - Browse League';
	seoData.metaDescription = 'LeagueAdda - Browse League';
	responsePageData.seoData = meta;


	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('mobile/browse_league', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = leaguePage;

//to test first commit

