var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function leaguePage(request, response, next) {
	var param = request.params;
	var initObject = {matchId : param.matchId};
    let meta=config.seoMetaTitles['myJoinedLeague'];
	var responsePageData = {};
	responsePageData.initObject =initObject;
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - Joined Leagues';
	seoData.metaKeywords = 'LeagueAdda - Joined Leagues';
	seoData.metaDescription = 'LeagueAdda - Joined Leagues';
	responsePageData.seoData = seoData;


	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('mobile/joined_leagues', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = leaguePage;

