var logger = require("../../utils/logger").websiteLogs;
var responseMessage = require("../../utils/response_message");
var config=require('../../../bin/config');

function myProfilePage(request, response, next) {

	var responsePageData = {};
    let meta=config.seoMetaTitles['myprofile'];
	var seoData = {};
	seoData.metaTitle = 'LeagueAdda - My Profile';
	seoData.metaKeywords = 'LeagueAdda - My Profile';
	seoData.metaDescription = 'LeagueAdda - My Profile';
	responsePageData.seoData = meta;

	if(!responsePageData){
		response.render('404error', {layout :'home'});
		return;
	}
	responsePageData.layout = 'home';
	response.render('myprofile', responsePageData,function(error,html){
		if(error){
			response.render('404error', {layout :'home'});
			return;
		}
		response.send(html);
	});

}

module.exports = myProfilePage;

