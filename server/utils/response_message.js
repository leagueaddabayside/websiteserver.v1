module.exports = {
	100 : 'Success',
	101 : 'Some unexpected error occurred',
	102 : 'Some unexpected error occurred',
	103 : 'Invalid email or password',
	104 : 'Invalid old password',
	107 : 'Invalid admin token',
	108 : 'Invalid user token',
	109 : "Please enter valid credentials",
	110 : "Invalid OTP",
	113 : "User does not exists",
	114 : "Team name already exists",
	115 : "Email id already exists",
	116 : "Email id can not be blank",
	117 : "token not exist",
	118 : "OTP has expired",
	119 : "Email template does not exists",
	120 : "Mobile number already exists",
	121 : "Mobile already verified",
	122 : "Error in transaction creation",
	123 : "Invalid request params",
	125:  "User wallet not updated",
	126:  "Invalid debit amount",
	124 : "Your withdrawl request successfully processed. Your amount will be credited into your bank account within 10 working days. ",
	127 : "Invalid Amount",
	128:  "user insufficient balance",
	129:  "Please update your profile first",
	130:  "withdrawl amount not valid",
	131:  "Deposit amount not valid",
	132:  "Deposit initialize txn not found",
	133:  "Deposit init txn allready done",
	134:  "jOIN lEAGUE TXN NOT FOUND",
	135:  'league txn already canceled',
	136:  'Email not Authorized',
	137:  'Account Already verified',
	138:  'Invalid hash calculated',
	139:  'Amount mismatch with payu',
	140:  'Invalid file type',
	141:  'Invalid admin credential',
	142 : 'Invalid Server authentication',
	143 : 'User not found',
    144 : 'Withdrawl Txn already cancelled',
    145 : 'Please verify your account first',
	501:  "Mysql error",
	404:  "Not found",
    305:  "Invalid amount"

}
