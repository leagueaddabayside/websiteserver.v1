/**
 * Created by adityagupta on 20/1/17.
 */
const Crypto=require('crypto');
const Emailer=require('./node_mailer');
const UserMaster=require('../users/services/UserMaster');
const Info=require('../users/models/gnUserInfo');
const UserMasterModel=require('../users/models/gnUserMaster');

let emailer=function () {
    this.userMaster=UserMaster;
    this.UserModel=UserMasterModel;
};

emailer.prototype.CreateHash=(str)=>{
    return Crypto.createHash('sha256').update(str).digest('hex');
};

emailer.prototype.response=(code,message,data)=>{
    return {respCode:code,message:message,respData:data}
};


emailer.prototype.sendEmailOnEvent=(userId,event,templateData)=>{
    let that=this;
    return new Promise((resolve,reject)=>{
        UserMasterModel.findOne({
            where:{userId:userId},
            attributes:['emailId'],
            raw:true,
            include:[{model:Info,as:'personalInfo',attributes:['firstName','lastName']}]
        }).then(function (user) {
                templateData.name='LeagueAdda Selector';
                if(user['personalInfo.firstName']) templateData.name=user['personalInfo.firstName'];
                if(user['personalInfo.lastName']) templateData.name=' '+user['personalInfo.lastName'];
                console.log(templateData);
                if(user && user.emailId){
                    Emailer.sendMail({
                        templateType:event,
                        to:user.emailId,
                        templateData:templateData
                    },function (err,ack) {
                        if(err) return console.error(err);
                        console.log(ack);
                    })
                }
            }).catch(reject);
    })
};


emailer.prototype.sendBonusEmailSms=(params)=>{
    if(params && params.emailType){
        UserMaster.getUserAttributes({userId:params.userId},['emailId'])
            .then(function (user) {
                if(user && user.emailId){
                    Emailer.sendMail({
                        templateType:params.emailType,
                        to:user.emailId,
                        templateData:{amount:params.amount}})
                }
            })
    }
}
emailer.prototype.sendBonusEmail=function (params) {
    let that =this;
    return new Promise((resolve,reject)=>{
        if(params && params.emailType){
            that.UserModel.findOne({where:{userId:params.userId},attributes:['emailId'],raw:true,include:[{model:Info,as:'personalInfo',attributes:['firstName','lastName']}]})
                .then(function (user) {
                    params.name='LeagueAdda Selector';
                    if(user['personalInfo.firstName']) params.name=user['personalInfo.firstName'];
                    if(user['personalInfo.lastName']) params.name=' '+user['personalInfo.lastName'];
                    if(user && user.emailId){
                        Emailer.sendMail({
                            templateType:params.emailType,
                            to:user.emailId,
                            templateData:params})
                    }
                });
        }
    })
};

emailer.prototype.beepCredential=function (str) {
    if(typeof str !=='string') str=str.toString();
    let leftPart=str.substring(0,Math.floor(str.length/2));
    for (i=Math.ceil(str.length/2);i<=str.length-1;i++){
         leftPart+='X';
    }
    return leftPart;
};

module.exports=new emailer();
