/**
 * Created by adityagupta on 4/4/17.
 */
var emailService = require("../email_template/services/index");
var logger = require("./logger").websiteLogs;
var nodemailer = require('nodemailer');
var format = require("string-template");
var responseCode = require("./response_code");
var smtpConfig = require('../../bin/config');
var pdf = require('html-pdf');
var options = { format: 'Letter' };
//

var transporter = nodemailer.createTransport(smtpConfig.smtpEmailHostAws);

function sendMailWithAttacment(requestObject, callback) {

    var responseObject = {};

    emailService.findEmailTemplateByProperty({
        templateType: requestObject.templateType
    }, function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        if (!(data && data.responseData)) {
            logger.error("email template not found", requestObject);
            responseObject.responseCode = responseCode.EMAIL_TEMPLATE_NOT_EXIST;
            callback(error, responseObject);
            return;
        }

        if (data.responseCode == responseCode.SUCCESS) {
            var responseData = data.responseData;
            pdf.create(format(responseData.emailBody, requestObject.templateData)).toBuffer(function(err, buffer){
                if (err) return callback(err,null);
            var attachments={filename:requestObject.filename,content:buffer,contentType:'application/pdf'};
            var mailOptions = {
                from: 'League Adda <fantasycricket@leagueadda.com>', // sender address
                to: requestObject.to,
                subject: format(responseData.emailSubject, responseData.templateData), // Subject line
                //text : 'Hello world ??', // plaintext body
                attachments:attachments
            };
            logger.info(' mailOptions', mailOptions);

            var emailHisObject = new Object();
            emailHisObject.emailFrom = mailOptions.from,
                emailHisObject.emailTo = mailOptions.to,
                emailHisObject.emailSubject = mailOptions.subject,
                emailHisObject.emailBody = mailOptions.html;
            emailHisObject.emailType = requestObject.templateType;
            emailHisObject.status = "PENDING";

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    logger.error(error);
                    emailHisObject.status = "FAILED";
                } else {
                    emailHisObject.status = "SENT";
                }
                emailService.addEmailHistory(emailHisObject, function (error,
                                                                       responseObject) {
                    if (error) {
                        console.log(error);
                    }
                    console.log('responseObject :',info, responseObject);
                });

                callback(error, responseObject);
                return;
            });
            });
        } else {
            logger.error("email template not found", requestObject);
            responseObject.responseCode = responseCode.EMAIL_TEMPLATE_NOT_EXIST;
            callback(error, responseObject);
            return;
        }

    });

}

module.exports.sendMailWithAttacment = sendMailWithAttacment;

//Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.templateType = "WITHDRAWL_RECEIPT_PDF",
        requestObject.to = 'aditya.gupta@gaussnetworks.com',
        requestObject.templateData = {name:'xyz'};
        requestObject.filename='withdrawl.pdf';

    console.log(requestObject);

    sendMailWithAttacment(requestObject, function (error, responseObject) {
        console.log("Response Code - ", responseObject);

    });
}

//template Type -
