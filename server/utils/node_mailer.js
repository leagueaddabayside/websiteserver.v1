var emailService = require("../email_template/services/index");
var logger = require("./logger").websiteLogs;
var nodemailer = require('nodemailer');
var format = require("string-template");
var responseCode = require("./response_code");
var smtpConfig = require('../../bin/config');

var transporter = nodemailer.createTransport(smtpConfig.smtpEmailHostAws);

function sendMail(requestObject, callback) {

    var responseObject = {};

    emailService.findEmailTemplateByProperty({
        templateType: requestObject.templateType
    }, function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        if (!(data && data.responseData)) {
            logger.error("email template not found", requestObject);
            responseObject.responseCode = responseCode.EMAIL_TEMPLATE_NOT_EXIST;
            callback(error, responseObject);
            return;
        }

        if (data.responseCode == responseCode.SUCCESS) {
            var responseData = data.responseData;

            var mailOptions = {
                from: 'League Adda <fantasycricket@leagueadda.com>', // sender address
                to: requestObject.to,
                subject: format(responseData.emailSubject, responseData.templateData), // Subject line
                //text : 'Hello world ??', // plaintext body
                html: format(responseData.emailBody, requestObject.templateData)
            };
            logger.info(' mailOptions', mailOptions);

            var emailHisObject = new Object();
            emailHisObject.emailFrom = mailOptions.from,
                emailHisObject.emailTo = mailOptions.to,
                emailHisObject.emailSubject = mailOptions.subject,
                emailHisObject.emailBody = mailOptions.html;
            emailHisObject.emailType = requestObject.templateType;
            emailHisObject.status = "PENDING";

            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    logger.error(error);
                    emailHisObject.status = "FAILED";
                } else {
                    emailHisObject.status = "SENT";
                }
                emailService.addEmailHistory(emailHisObject, function (error,
                                                                       responseObject) {
                    if (error) {
                        console.log(error);
                    }
                    console.log('responseObject :',info, responseObject);
                });

                callback(error, responseObject);
                return;
            });

        } else {
            logger.error("email template not found", requestObject);
            responseObject.responseCode = responseCode.EMAIL_TEMPLATE_NOT_EXIST;
            callback(error, responseObject);
            return;
        }

    });

}

module.exports.sendMail = sendMail;

//Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.templateType = "FORGOT_PASSWORD_OTP",
        requestObject.to = 'sumitsingla987@gmail.com',
        requestObject.templateData = {otp: 35343};

    console.log(requestObject);

    sendMail(requestObject, function (error, responseObject) {
        console.log("Response Code - ", responseObject);

    });
}

//template Type -