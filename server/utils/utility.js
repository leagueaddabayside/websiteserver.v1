var _ = require('lodash');
var generatePassword  	= require('password-generator');

function encryptMobileNumber(mobileNum) {
	var mobileNum = mobileNum + "";
	var length = mobileNum.length;
	var encryptMobileNum = _.repeat('*', length - 2);
	encryptMobileNum = encryptMobileNum + mobileNum.substr(length - 2);
	return encryptMobileNum;
}

function encryptEmailId(emailId) {
	var preEmailId = emailId.split('@')[0];
	var postEmailId = emailId.split('@')[1];
	var length = preEmailId.length;
	var encryptEmailId = _.repeat('*', length - 2);
	encryptEmailId = preEmailId.substr(0, 1) + encryptEmailId
			+ preEmailId.substr(length - 1) + '@' + postEmailId;
	return encryptEmailId;
}


function getUserStatus(user) {
	let isMobileVerified = user.isMobileVerified;
	let isEmailVerified = user.isEmailVerified;
	let isPanCardVerified = user.isPanCardVerified;
	let isBankDetailVerified = user.isBankDetailVerified;
	let userStatus = 'notverified';
	if(isMobileVerified === 'NO'){
		return userStatus;
	}
	if(isEmailVerified === 'NO'){
		return userStatus;
	}
	if(isPanCardVerified === 'NO'){
		return userStatus;
	}
	if(isBankDetailVerified === 'NO'){
		return userStatus;
	}


	if(isPanCardVerified === 'PROCESSING'){
		userStatus = 'underprocess';
		return userStatus;
	}
	if(isBankDetailVerified === 'PROCESSING'){
		userStatus = 'underprocess';
		return userStatus;
	}

	userStatus = 'verified';
	return userStatus;

}


function generateOtp(){
	let otp = generatePassword(6, false, /[\d]/);
	return otp;
}

module.exports.encryptMobileNumber = encryptMobileNumber;
module.exports.encryptEmailId = encryptEmailId;
module.exports.generateOtp = generateOtp;
module.exports.getUserStatus = getUserStatus;

// Unit Test Case
if (require.main === module) {
	console.log(encryptMobileNumber('23423424'));
	console.log(encryptEmailId('sumit.singla@gaussnetworks.com'));
	console.log(generateOtp());
}
