/**
 * Created by adityagupta on 20/1/17.
 */
const Crypto=require('crypto');
const Emailer=require('./node_mailer');
const UserMaster=require('../users/services/UserMaster');
let utilities=function () {
    
};

utilities.prototype.CreateHash=(str)=>{
    return Crypto.createHash('sha256').update(str).digest('hex');
};

utilities.prototype.response=(code,message,data)=>{
    return {respCode:code,message:message,respData:data}
};

utilities.prototype.sendEmailOnEvent=(userId,event,templateData)=>{
    console.log('preparing email for '+event,userId);
    return new Promise((resolve,reject)=>{
        UserMaster.getUserAttributes({userId:userId},['emailId'])
            .then(function (user) {
                templateData.name='LeagueAdda Selector';
                if(user['personalInfo.firstName']) templateData.name=user['personalInfo.firstName'];
                if(user['personalInfo.lastName']) templateData.name=' '+user['personalInfo.lastName'];
                if(user && user.emailId){
                    Emailer.sendMail({
                        templateType:event,
                        to:user.emailId,
                        templateData:templateData
                    },function (err,ack) {
                        if(err) return console.error(err);
                        console.log(ack);
                    })
                }
            }).catch(reject);
    })
};


utilities.prototype.sendBonusEmailSms=(params)=>{
    if(params && params.emailType){
        UserMaster.getUserAttributes({userId:params.userId},['emailId'])
            .then(function (user) {
                if(user && user.emailId){
                    Emailer.sendMail({
                        templateType:params.emailType,
                        to:user.emailId,
                        templateData:{amount:params.amount}})
                }
            })
    }
}
utilities.prototype.sendBonusEmail=function (params) {
    if(params && params.emailType){
        UserMaster.getUserAttributes({userId:params.userId},['emailId'])
            .then(function (user) {
                if(user && user.emailId){
                    Emailer.sendMail({
                        templateType:params.emailType,
                        to:user.emailId,
                        templateData:{amount:params.amount}})
                }
            });
    }
}

module.exports=new utilities();
