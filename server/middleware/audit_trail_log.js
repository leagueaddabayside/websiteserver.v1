var logger = require('../utils/logger.js').requestLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
const constant = require('../utils/constant');

function auditTrailLog(request, response, next) {
    var reqParams = request.body;
    var params = {};
    params.ip = request.ip;
    params.data = reqParams;
    params.url = request.url;
    params.userAgent = request.headers['user-agent'];

    logger.info(params);
    next();
}

module.exports = auditTrailLog;
