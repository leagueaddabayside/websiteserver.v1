var logger = require('../utils/logger.js').websiteLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
const constant = require('../utils/constant');

function checkIPAllowed(request, response, next) {
    var reqParams = request.body;
    console.log('checkIPAllowed request',request.ip);
    if (constant.adminLoginIPCheck) {
        let requestIP = request.ip;
        if(constant.adminLoginIPAllowed.indexOf(requestIP) > -1) {
            next();
            return;
        }else {
            response.json({
                respCode: responseCode.INVALID_SERVER_AUTHENTICATION,
                message: responseMessage[responseCode.INVALID_SERVER_AUTHENTICATION]
            })
        }

    } else {
        next();
        return;
    }

}

module.exports = checkIPAllowed;
