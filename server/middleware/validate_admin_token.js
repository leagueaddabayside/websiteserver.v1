var logger = require('../utils/logger.js').websiteLogs;
const tokenizer = require('../redis/redis');
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');


function getAdminIdFromToken(request, response, next) {
    var reqParams = request.body;

    if (typeof reqParams.token === "undefined" && reqParams.token === null) return response.json({
        respCode: responseCode.INVALID_ADMIN_TOKEN,
        message: responseMessage[responseCode.INVALID_ADMIN_TOKEN]
    });
    let params = {token : reqParams.token ,adminType : 'WEBSITE_ADMIN'};
    tokenizer.checkAdminToken(params)
        .then(resp => {
            request.body.adminId = resp.userId;
            request.body.roleId = resp.roleId;
            next();

        })
        .catch(function (err) {
            logger.error(err);
            response.json({
                respCode: responseCode.INVALID_ADMIN_TOKEN,
                message: responseMessage[responseCode.INVALID_ADMIN_TOKEN]
            })
        });

}

module.exports = getAdminIdFromToken;
