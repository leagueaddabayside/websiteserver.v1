var logger = require('../utils/logger.js').websiteLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
var validator = require('validator');
const gnUserMaster = require('../users/models/gnUserMaster');


function getUserInfoFromName(request, response, next) {
    var params = request.body;
    let userData = params.userData;
    var dataType = typeof(userData);

    var requestData = {};
    if ((dataType === "number") || (dataType === "string" && validator.isNumeric(userData))) {
        requestData.mobileNbr = userData;
    } else if (dataType === "string" && validator.isEmail(userData)) {
        requestData.emailId = userData;
    } else {
        requestData.screenName = userData;
    }
    console.log('getUserInfoFromName',requestData);
    gnUserMaster.findOne({where: requestData})
        .then((user)=> {
            if (!user) {
                response.json({
                    respCode: responseCode.USER_NOT_EXIST,
                    message: responseMessage[responseCode.USER_NOT_EXIST]
                })
                return;
            }
            request.body.userId = user.userId;
            request.body.screenName = user.screenName;

            next();
        })
        .catch(function (err) {
            logger.error(err);
            response.json({
                respCode: responseCode.USER_NOT_EXIST,
                message: responseMessage[responseCode.USER_NOT_EXIST]
            })
        });
}

module.exports = getUserInfoFromName;
