var logger = require('../utils/logger.js').websiteLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
var validator = require('validator');
const gnUserMaster = require('../users/models/gnUserMaster');


function checkMobileNumberVerified(request, response, next) {
    var params = request.body;

    if(!params.mobileNumber){
        response.json({
            respCode: responseCode.INVALID_REQUEST_PARAMS,
            message: responseMessage[responseCode.INVALID_REQUEST_PARAMS]
        })
        return;
    }




    var requestData = {};
    requestData.mobileNbr = params.mobileNumber;

    console.log('checkMobileNumberVerified',requestData);
    gnUserMaster.findOne({where: requestData})
        .then((user)=> {
            if (user) {
                response.json({
                    respCode: responseCode.MOBILE_ALREADY_VERIFIED,
                    message: responseMessage[responseCode.MOBILE_ALREADY_VERIFIED]
                })
                return;
            }else{
                next();
            }

        })
        .catch(function (err) {
            logger.error(err);
            response.json({
                respCode: responseCode.MOBILE_ALREADY_VERIFIED,
                message: responseMessage[responseCode.MOBILE_ALREADY_VERIFIED]
            })
        });
}

module.exports = checkMobileNumberVerified;
