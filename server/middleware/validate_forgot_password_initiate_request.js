var logger = require('../utils/logger.js').websiteLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
var validator = require('validator');
const gnUserMaster = require('../users/models/gnUserMaster');


function validateForgotInitiateRequest(request, response, next) {
    var params = request.body;
    let userData = params.userData;
    var dataType = typeof(userData);

    var requestData = {};
    var userDataType = '';
    if ((dataType === "number") || (dataType === "string" && validator.isNumeric(userData))) {
        requestData.mobileNbr = userData;
        userDataType = 'mobileNo';
    } else if (dataType === "string" && validator.isEmail(userData)) {
        requestData.emailId = userData;
        userDataType = 'emailId';
    }else{
        response.json({
            respCode: responseCode.INVALID_FORGOT_PASSWORD_PARAMS,
            message: responseMessage[responseCode.INVALID_FORGOT_PASSWORD_PARAMS]
        })
        return;
    }
    console.log('validateForgotInitiateRequest',requestData);
    gnUserMaster.findOne({where: requestData})
        .then((user)=> {
            if (!user) {
                response.json({
                    respCode: responseCode.USER_NOT_EXIST,
                    message: responseMessage[responseCode.USER_NOT_EXIST]
                })
                return;
            }

            request.body.userId = user.userId;
            request.body.emailId = user.emailId;
            request.body.mobileNo = user.mobileNbr;
            request.body.userDataType = userDataType;

            next();
        })
        .catch(function (err) {
            logger.error(err);
            response.json({
                respCode: responseCode.USER_NOT_EXIST,
                message: responseMessage[responseCode.USER_NOT_EXIST]
            })
        });
}

module.exports = validateForgotInitiateRequest;
