var logger = require('../utils/logger.js').websiteLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
var serverAuth = require('../../bin/config').serverAuth;
var config = require('../../bin/config');

function validateServerAuthentication(request, response, next) {
    var reqParams = request.body;
    console.log('validateServerAuthentication request',reqParams,request.ip);
    if(!config.isProduction){
        next();
        return;
    }


    if(serverAuth.ip == request.ip && reqParams.accessKey == serverAuth.accessKey){
        next();
    }else{
        console.log('validateServerAuthentication failed',reqParams,request.ip);
        response.json({
            respCode: responseCode.INVALID_SERVER_AUTHENTICATION,
            message: responseMessage[responseCode.INVALID_SERVER_AUTHENTICATION]
        })
    }

   }

module.exports = validateServerAuthentication;
