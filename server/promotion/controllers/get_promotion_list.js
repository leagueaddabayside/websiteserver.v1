/**
 * Created by sumit on 4/18/2017.
 */
var logger = require("../../utils/logger").websiteLogs;;
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var requestClient = require('request');
var config = require("../../../bin/config");

function findEmailTemplateList(request, resp, next) {
    var requestData = request.body;
    var responseObject = new Object();
    responseObject.respCode = 100;
    let promotionList = PROMOTION_LIST;
    //console.log('promotionList',promotionList);
    requestClient(config.bannerServerUrl + '/provide_banners?client=web&places=promotions_listing', function (error, response, body) {
       if(error){
           logger.error(error);
       }else{
           var respData = JSON.parse(body);

           if(respData.status == true){
               promotionList  = respData.data.promotions_listing;
           }
       }
        //console.log('promotionList',promotionList);
        responseObject.respData = promotionList;
        resp.json(responseObject);
        return;
    });

}

module.exports = findEmailTemplateList;

// Unit Test Case

if (require.main === module) {
    (function() {
        var request = {};

        var response = {
            json : function(result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();

        console.log(requestObject);
        request.body = requestObject;
        findEmailTemplateList(request, response);
    })();
}


var PROMOTION_LIST =  [];