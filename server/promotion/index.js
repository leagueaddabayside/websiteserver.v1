/**
 * Created by sumit on 4/18/2017.
 */
'use strict';

let express=require('express');
let routes=express.Router();
var promotionsApi = require('./controllers/index');

routes.post("/promotion",promotionsApi.getPromotionList);


module.exports=routes;
