/**
 * Created by aditya on 16/1/17.
 */
const Sequelize=require('sequelize');
const schema=require('../../connection');
const userInfo=require('./gnUserInfo');
const PanCard=require('./gnUserPanCard');
const BankDetails=require('./gnUserBankDetails');
const userWithdrawlTxn=require('../../payment/models/userRealWithdrawlTxn');
// const GnRoleActionUserMapping=require('../../role_modules/models/gnRoleActionUserMapping');
const UserMaster= schema.define('gn_user_master', {
    userId: {type: Sequelize.INTEGER, field: 'user_id',primaryKey: true,autoIncrement: true},
    domainId: {type: Sequelize.INTEGER, field: 'domain_id'},
    roleId: {type: Sequelize.INTEGER,field: 'role_id'},
    levelId: {type: Sequelize.INTEGER,field: 'level_id'},
    parentUserId: {type: Sequelize.INTEGER,field: 'parent_user_id'},
    isRoleHead: {type: Sequelize.ENUM,values:['YES','NO'],field: 'is_role_head'},
    userName: {type: Sequelize.STRING,field: 'user_name',default:null},
    screenName: {type: Sequelize.STRING,field: 'screen_name'},
    emailId: {type: Sequelize.STRING,field: 'email_id'},
    mobileNbr: {type: Sequelize.STRING,field: 'mobile_nbr'},
    userType: {type: Sequelize.ENUM,values:['PLAYER','ADMIN'],field: 'user_type'},
    password: {type: Sequelize.STRING,field: 'password'},
    firstLogin: {type: Sequelize.STRING,field: 'first_login'},
    activationToken: {type: Sequelize.STRING,field: 'activation_token',default:null},
    isMobileVerified: {type: Sequelize.ENUM,values:['YES','NO'],field: 'is_mobile_verified',default:'NO'},
    isEmailVerified: {type: Sequelize.ENUM,values:['YES','NO'],field: 'is_email_verified',default:'NO'},
    isPanCardVerified: {type: Sequelize.ENUM,values:['YES','NO','PROCESSING'],field: 'is_pan_card_verified',default:'NO'},
    isBankDetailVerified : {type: Sequelize.ENUM,values:['YES','NO','PROCESSING'],field: 'is_bank_detail_verified',default:'NO'},
    refferenceBy: {type: Sequelize.STRING,field: 'refference_by',default:null},
    status: {type: Sequelize.ENUM,values:['ACTIVE'],field: 'status'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});
UserMaster.belongsTo(userInfo,{ foreignKey: 'user_id',as: 'personalInfo' });
userWithdrawlTxn.belongsTo(UserMaster,{foreignKey: 'user_id' ,as:'user'});
PanCard.belongsTo(UserMaster,{foreignKey:'userId',as:'panCard'});
UserMaster.belongsTo(BankDetails,{foreignKey:'userId',as:'bankDetails'});
module.exports=UserMaster;




