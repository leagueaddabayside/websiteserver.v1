/**
 * Created by aditya on 17/1/17.
 */
const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');
const UserMaster=require('./gnUserMaster');
const userInfo= sequelize.define('gn_user_info', {
    userId: {type: Sequelize.STRING,primaryKey: true, field: 'user_id'},
    firstName: {type: Sequelize.STRING, field: 'first_name'},
    lastName: {type: Sequelize.STRING, field: 'last_name'},
    dob: {type: Sequelize.DATEONLY, field: 'dob'},
    gender: {type: Sequelize.STRING,field: 'gender'},
    alternateNumber: {type: Sequelize.STRING, field: 'alternate_number'},
    profilePic: {type: Sequelize.STRING, field: 'profile_pic'},
    country: {type: Sequelize.STRING, field: 'country'},
    state: {type: Sequelize.STRING, field: 'state'},
    city: {type: Sequelize.STRING, field: 'city'},
    pinCode: {type: Sequelize.STRING, field: 'pin_code'},
    address: {type: Sequelize.STRING, field: 'address'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports=userInfo;
