/**
 * Created by aditya on 16/1/17.
 */
const Sequelize=require('sequelize');
const schema=require('../../connection');
const UserMaster=require('../models/gnUserMaster');
const userWithdrawlTxn=require('../../payment/models/userRealWithdrawlTxn');
const UserPanCard= schema.define('gn_user_pan_card', {
    userId: {type: Sequelize.INTEGER, field: 'user_id',primaryKey:true},
    name: {type: Sequelize.STRING, field: 'name'},
    state: {type: Sequelize.STRING, field: 'state'},
    panNumber: {type: Sequelize.STRING, field: 'pan_number'},
    panCardimage: {type: Sequelize.STRING, field: 'image'},
    status: {type: Sequelize.ENUM,values:['PENDING','FAILED','SUCCESS','APPROVE'],field: 'status',default:'pending'},
    remarks: {type: Sequelize.STRING, field: 'remarks'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});
userWithdrawlTxn.belongsTo(UserPanCard,{foreignKey:'user_id',as:'panCard'});
module.exports=UserPanCard;




