/**
 * Created by aditya on 16/1/17.
 */
const Sequelize=require('sequelize');
const schema=require('../../connection');
const userWithdrawlTxn=require('../../payment/models/userRealWithdrawlTxn');
const UserBankDetails= schema.define('gn_user_bank_details', {
    userId: {type: Sequelize.INTEGER, field: 'user_id',primaryKey:true},
    bankName: {type: Sequelize.STRING, field: 'bank_name'},
    accountNumber: {type: Sequelize.STRING, field: 'account_number'},
    branch: {type: Sequelize.STRING, field: 'branch'},
    remarks: {type: Sequelize.STRING, field: 'remarks'},
    ifscCode: {type: Sequelize.STRING, field: 'ifsc_code'},
    status: {type: Sequelize.ENUM,values:['PENDING','FAILED','APPROVE'],field: 'status',default:'PENDING'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});
userWithdrawlTxn.belongsTo(UserBankDetails,{foreignKey:'user_id',as:'bank'});
module.exports=UserBankDetails;




