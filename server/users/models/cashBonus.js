/**
 * Created by aditya on 17/1/17.
 */
const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');
const cashBonus= sequelize.define('cash_bonus', {
    id: {type: Sequelize.STRING,primaryKey: true, field: 'id'},
    event: {type: Sequelize.STRING, field: 'first_name'},
    amount: {type: Sequelize.STRING, field: 'last_name'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});
module.exports=cashBonus;
