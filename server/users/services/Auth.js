/**
 * Created by adityagupta on 19/1/17.
 */
const config=require('../../../bin/config');
const gnUserMaster=require('../models/gnUserMaster');
const gnUserInfo=require('../models/gnUserInfo');
const gnUserWalletMaster=require('../../payment/models/userWalletMaster');

const Crypto=require(config.__base+'/server/utils/util');
const tokenizer=require('../../redis/redis');
const responseCode =require('../../utils/response_code');
const logger =require('../../utils/logger').websiteLogs;
const validator = require('validator');
const moment = require("moment");
const sequelize=require('../../connection');
const utility =require('../../utils/utility');

const AuthClass=function () {
    
}

function getUserPersonalInf(userId) {
    return new Promise((resolve,reject)=>{
        gnUserInfo.findOne({where:{userId:userId}})
            .then((userInfo)=>{
                if(!userInfo) return reject(responseCode.USER_NOT_EXIST);

                resolve(userInfo);
            }).catch(logger.info)

    })
}

function getUserWalletInfo(userId) {// TO do with this
    return new Promise((resolve,reject)=>{
        gnUserWalletMaster.findOne({where:{userId:userId}})
            .then((userWalletInfo)=>{
                if(!userWalletInfo) return reject(responseCode.USER_NOT_EXIST);

                resolve(userWalletInfo);
            }).catch(logger.info)

    })
}

AuthClass.prototype.Autharized=(userData,password)=>{
    //To-do
    var dataType = typeof(userData);
    var requestData = {};
    requestData.emailId = userData;
    requestData.status = 'ACTIVE';
    requestData.userType = 'PLAYER';
   /* if ((dataType==="number") || (dataType==="string" && validator.isNumeric(userData))) {
        requestData.mobileNbr = userData;
    } else if (dataType==="string" && validator.isEmail(userData)) {
        requestData.emailId = userData;
    } else {
        requestData.userName = userData;
    }*/
    var responseData = {};

    password = Crypto.CreateHash(password);
    return new Promise((resolve,reject)=>{
        gnUserMaster.findOne({where:requestData})
            .then((user)=>{
                if(!user) reject(responseCode.USER_NOT_FOUND);
                if(user.password===password ){
                    responseData.userId = user.userId;
                    responseData.screenName = user.screenName;
                    responseData.userStatus = utility.getUserStatus(user);
                    //logger.info('UserAutharized user',user);
                    return tokenizer.setToken(user);
                }
                reject(responseCode.INVALID_LOGIN_CREDENTIAL);
            })
            .then(function(tokenizerResponse){
                logger.info('tokenizerResponse',tokenizerResponse);


                responseData.token = tokenizerResponse.token;
                return getUserPersonalInf(tokenizerResponse.userId);
            })
            .then(function(userPersonalInfo){
               // logger.info('userPersonalInfo',userPersonalInfo);
                if(userPersonalInfo.dob){
                    responseData.dob =moment(userPersonalInfo.dob).format('MM/DD/YYYY');
                }
                if(userPersonalInfo.firstName){
                    responseData.firstName =userPersonalInfo.firstName;
                }
                if(userPersonalInfo.lastName){
                    responseData.lastName =userPersonalInfo.lastName;
                }
                responseData.state = userPersonalInfo.state;
                return getUserWalletInfo(userPersonalInfo.userId);
            })
            .then(function(userWalletInfo){
                //logger.info('userWalletInfo',userWalletInfo);
                if(userWalletInfo){
                    responseData.depositAmt = userWalletInfo.depositAmt;
                    responseData.bonusAmt =userWalletInfo.bonusAmt;
                    responseData.winningAmt =userWalletInfo.winningAmt;
                    tokenizer.updateUser(responseData);
                    resolve(responseData);
                    return;
                }
                reject(responseCode.USER_NOT_EXIST);
            })
            .catch (function(err){
            logger.error(err);
            reject(responseCode.SOME_INTERNAL_ERROR);
        });
    });
}


AuthClass.prototype.UserAutharized=(emailId)=>{
    var requestData = {};
    requestData.emailId = emailId;
    var responseData = {};
    return new Promise((resolve,reject)=>{
        gnUserMaster.findOne({where:requestData})
            .then((user)=>{
                if(!user) reject(responseCode.INVALID_LOGIN_CREDENTIAL);
                    responseData.userId = user.userId;
                    responseData.screenName = user.screenName;
                    responseData.userStatus = utility.getUserStatus(user);
                    return tokenizer.setToken(user);

            })
            .then(function(tokenizerResponse){
                logger.info('tokenizerResponse',tokenizerResponse);
                responseData.token = tokenizerResponse.token;
                return getUserPersonalInf(tokenizerResponse.userId);
            })
            .then(function(userPersonalInfo){
                //logger.info('userPersonalInfo',userPersonalInfo);
                if(userPersonalInfo.dob){
                    responseData.dob =moment(userPersonalInfo.dob).format('MM/DD/YYYY');
                }
                if(userPersonalInfo.firstName){
                    responseData.firstName =userPersonalInfo.firstName;
                }
                if(userPersonalInfo.lastName){
                    responseData.lastName =userPersonalInfo.lastName;
                }
                responseData.state = userPersonalInfo.state;
                return getUserWalletInfo(userPersonalInfo.userId);
            })
            .then(function(userWalletInfo){
                //logger.info('userWalletInfo',userWalletInfo);
                if(userWalletInfo){
                    responseData.depositAmt = userWalletInfo.depositAmt;
                    responseData.bonusAmt =userWalletInfo.bonusAmt;
                    responseData.winningAmt =userWalletInfo.winningAmt;
                    tokenizer.updateUser(responseData);
                    resolve(responseData);
                    return;
                }
                reject(responseCode.USER_NOT_EXIST);
            })
            .catch (function(err){
            logger.error(err);
            reject(responseCode.SOME_INTERNAL_ERROR);
        });
    });
}



AuthClass.prototype.UpdatePassword=function (user) {
    return new Promise((resolve,reject)=>{
        gnUserMaster.findOne({where:{userId:user.userId,password:Crypto.CreateHash(user.password)}})
            .then((newUser)=>{
                if(!newUser) return reject(responseCode.INVALID_OLD_PASSWORD);
                newUser.password=Crypto.CreateHash(user.newPassword);
                newUser.save();
                resolve(newUser);
            }).catch(function (er) {
            logger.info(er);
            reject(er);
        })

    })
}

AuthClass.prototype.userForgotPassword=function (params) {
    logger.info('update userForgotPassword params',params);
    var userInfo = {};

    if(params.password){
        userInfo.password = Crypto.CreateHash(params.password);
    }

    return new Promise((resolve,reject)=>{
        gnUserMaster.update(userInfo,{where:{userId:params.userId}})
            .then(function(result){
                resolve(result);
            })
            .catch(reject);

    })

}


AuthClass.prototype.authrizeGoogleUser=(params)=>{
    var user = {};
    user.emailId = params.emailId;
    user.status = 'ACTIVE';
    user.userType = 'PLAYER';
    user.activationToken = params.activationToken;
    user.refferenceBy = params.refferenceBy;

    let dob = null;

    if (typeof params.dob !== "undefined" && params.dob !== null)
    {
        var userDOB = new Date(params.dob);
        dob = moment(userDOB).format('YYYY-MM-DD');
    }

    return new Promise((resolve,reject)=>{

        return sequelize.transaction(function (t) {

            // chain all your queries here. make sure you return them.
            return gnUserMaster.create(user, {transaction: t}).then(function (user) {
                return gnUserInfo.create({
                    userId: user.userId,
                    firstName : params.firstName,
                    dob: dob
                }, {transaction: t})
                    .then(function (userInfo) {
                        return gnUserWalletMaster.create({
                            userId: userInfo.userId
                        }, {transaction: t});
                    });
            });

        }).then(function (result) {
            // Transaction has been committed
            resolve(result);
        }).catch(function (err) {
            // Transaction has been rolled back
            logger.error('user registration error',err);
            reject(responseCode.MYSQL_ERROR);
        });
    })

}
AuthClass.prototype.validateToken=function (token) {
    return new Promise((resolve,reject)=>{
        tokenizer.checkToken(token)
            .then(resolve)
            .catch(function(){
              reject(responseCode.INVALID_USER_TOKEN);
            });
    })
}

AuthClass.prototype.logout=function (token) {
    logger.info('AuthClass logout token '+token);
    return new Promise((resolve,reject)=>{
        tokenizer.removeToken(token)
            .then(resolve)
            .catch(function(err){
                logger.info(err);
                reject(responseCode.SOME_INTERNAL_ERROR);
            });
    })
}
module.exports=new AuthClass();


/*************************************/
if(require.main==module){
    (function () {
        user={
            password:'cdf4a007e2b02a0c49fc9b7ccfbb8a10c644f635e1765dcf2a7ab794ddc7edac',
            newPassword:'sdcsdc',
            userId:6
        };
        module.exports.up(user).then(value => {
            logger.info(value);
        }).catch(err =>{
            logger.info(err);
        });

    })();
}
