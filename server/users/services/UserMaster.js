/**
 * Created by adityagupta on 20/1/17.
 */
const config=require('../../../bin/config');
const UserMaster=require('../models/gnUserMaster');
const Info=require('../models/gnUserInfo');
const panCard=require('../models/gnUserPanCard');
const bankDetails=require('../models/gnUserBankDetails');
const gnUserWalletMaster=require('../../payment/models/userWalletMaster');
const util=require('../../utils/util');
const responseCode =require('../../utils/response_code');
const sequelize=require('../../connection');
const logger = require('../../utils/logger').websiteLogs;
const moment = require("moment");
const tokenizer=require('../../redis/redis');
const utility =require('../../utils/utility');
const bonusService=require('../../payment/services/userRealBonus');
const VERIFY_PANCARD=config.bonus.VERIFY_PANCARD;
const Emailer=require('../../utils/emailers');
let UserMasterClass=function () {
};
UserMasterClass.prototype.create=function (params) {

    var user = {};
    user.emailId = params.emailId;
    user.password=Emailer.CreateHash(params.password);
    logger.info(user.password)
    logger.info(util,'create hasj=')
    user.status = 'ACTIVE';
    user.userType = 'PLAYER';

    var userDOB = new Date(params.dob);
    var dob = moment(userDOB).format('YYYY-MM-DD');

    return new Promise((resolve,reject)=>{

            return sequelize.transaction(function (t) {

                // chain all your queries here. make sure you return them.
                return UserMaster.create(user, {transaction: t}).then(function (user) {
                    return Info.create({
                        userId: user.userId,
                        dob: dob
                    }, {transaction: t})
                        .then(function (userInfo) {
                        return gnUserWalletMaster.create({
                            userId: userInfo.userId
                        }, {transaction: t});
                    });
                });

            }).then(function (result) {
                // Transaction has been committed
                resolve(result);
            }).catch(function (err) {
                // Transaction has been rolled back
                logger.error('user registration error',err);
                reject(responseCode.MYSQL_ERROR);
            });
        })
}

UserMasterClass.prototype.getById=(id)=>{
    return new Promise((resolve,reject)=>{
        UserMaster.findById(id,{include:[{model:Info,as:'personalInfo'}],attributes:['userId','userName','screenName','emailId','mobileNbr','isMobileVerified','isEmailVerified','isPanCardVerified','isBankDetailVerified']}).then((user)=>{
           // logger.info(user);
            if(!user) return reject(responseCode.USER_NOT_EXIST);

            let responseData = {};
            responseData.screenName = user.screenName;
            responseData.userId = user.userId;
            responseData.emailId = user.emailId;
            responseData.mobileNbr = user.mobileNbr;
            responseData.isMobileVerified = user.isMobileVerified;
            responseData.isEmailVerified = user.isEmailVerified;
            responseData.isPanCardVerified = user.isPanCardVerified;
            responseData.isBankDetailVerified = user.isBankDetailVerified;
            if(user.personalInfo){
                let personalInfo = user.personalInfo;
                responseData.firstName = personalInfo.firstName;
                if(personalInfo.dob){
                    responseData.dob =moment(personalInfo.dob).format('MM/DD/YYYY');
                }
                responseData.country = personalInfo.country;
                responseData.gender = personalInfo.gender;
                responseData.state = personalInfo.state;
                responseData.city = personalInfo.city;
                responseData.pinCode = personalInfo.pinCode;
                responseData.address = personalInfo.address;
            }
            resolve(responseData)
        }).catch(function (err) {
            console.error(err);
            reject(err);
        });
    })
}

UserMasterClass.prototype.update1=function (params) {
    return new Promise((resolve,reject)=>{
        if('id' in params){ var userId=params.id;}
        delete params['id'];
        UserMaster.findOne({raw:true,where:{userId:userId},attributes:['id']})
            .then(UserMaster.update(params,{where:{userId:userId}}))
            .then(resolve(params))
            .catch(reject)
    })
}

UserMasterClass.prototype.update=function (params) {
    logger.info('update user params',params);
    var personalInfo = {};
    var updateData = {};
    updateData.userId = params.userId;
    if(params.firstName){
        personalInfo.firstName = params.firstName;
        updateData.firstName = params.firstName;
    }
    if(params.dob){
        let userDOB = new Date(params.dob);
        let dob = moment(userDOB).format('YYYY-MM-DD');
        personalInfo.dob = dob;
        updateData.dob = dob;

    }
    if(params.gender){
        personalInfo.gender = params.gender;
    }

    if(params.country){
        personalInfo.country = params.country;
    }
    if(params.state){
        personalInfo.state = params.state;
        updateData.state =  params.state;
    }
    if(params.city){
        personalInfo.city = params.city;
    }
    if(params.address){
        personalInfo.address = params.address;
    }
    if(params.pinCode){
        personalInfo.pinCode = params.pinCode;
    }
    return new Promise((resolve,reject)=>{
        Info.update(personalInfo,{where:{userId:params.userId}})
        .then(function(result){
                tokenizer.updateUser(updateData);
                resolve(result);
            })
        .catch(reject);

    })
}


UserMasterClass.prototype.updateUserData=function (params) {
    logger.info('update user data params',params);
    return new Promise((resolve,reject)=>{
        UserMaster.findOne({where:{userId:params.userId}})
            .then(function(user){
                if(!user)return reject(responseCode.USER_NOT_EXIST);

                if(params.mobileNbr){
                    user.mobileNbr = params.mobileNbr;
                    user.isMobileVerified = 'YES';
                }

                if(params.emailId){
                    user.emailId = params.emailId;
                    user.isEmailVerified = 'YES';
                }

                if(params.isBankVerified){
                    user.isBankDetailVerified = params.isBankVerified;
                }

                if(params.isPanCardVerified){
                    if(user.isPanCardVerified==params.isPanCardVerified && params.isPanCardVerified=='YES') return reject(responseCode.ACCOUNT_ALREADY_VERIFIED)
                    user.isPanCardVerified = params.isPanCardVerified;
                }
                return user.save().then(function(result){
                    if(!result) return reject(responseCode.USER_NOT_EXIST);
                    let redisUpdateData = {};
                    redisUpdateData.userStatus = utility.getUserStatus(result);
                    if(redisUpdateData.userStatus=='verified') {
                        Emailer.sendEmailOnEvent(params.userId,'ACCOUNT_VERIFY',{});
                    }
                    redisUpdateData.userId = result.userId;
                    tokenizer.updateUser(redisUpdateData);
                    resolve(result);

                }).catch(reject)
            })
            .catch(reject);
    })
}


UserMasterClass.prototype.updateTeam=function (params) {
    logger.info('update user params',params);
    var personalInfo = {};
    var updateData = {};
    updateData.userId = params.userId;


    if(params.state){
        personalInfo.state = params.state;
        updateData.state = params.state;
    }
    updateData.screenName = params.screenName;
    return new Promise((resolve,reject)=>{
        if(params.dob){
            if(!Date.parse(params.dob)) return reject(responseCode.INVALID_REQUEST_PARAMS);
            let userDOB = new Date(params.dob);
            let dob = moment(userDOB).format('YYYY-MM-DD');
            personalInfo.dob = dob;
            updateData.dob = dob;
        }
        return sequelize.transaction(function (t) {
            // chain all your queries here. make sure you return them.
            return UserMaster.update({screenName : params.screenName}, {where:{userId:params.userId},transaction: t}).then(function (user) {
                return Info.update(personalInfo, {where:{userId:params.userId},transaction: t});

            });

        }).then(function (result) {
            // Transaction has been committed
            tokenizer.updateUser(updateData);
            resolve(result);
        }).catch(function (err) {
            // Transaction has been rolled back
            logger.error('user registration error',err);
            reject(responseCode.MYSQL_ERROR);
        });
    })


}


UserMasterClass.prototype.checkDuplicateScreenName=function (params) {
    return new Promise((resolve,reject)=>{
        UserMaster.find({raw:true,where:{screenName:params.screenName}})
            .then(users=>(users)?reject(responseCode.DUPLICATE_SCREEN_NAME):resolve(params))
            .catch(function(err){
                logger.info(err);
                reject(responseCode.SOME_INTERNAL_ERROR);
            });
    })
}

UserMasterClass.prototype.checkDuplicateEmailId=function (params) {
    return new Promise((resolve,reject)=>{
        UserMaster.find({raw:true,where:{emailId:params.emailId}})
            .then(users=>(users)?reject(responseCode.DUPLICATE_EMAIL_ID):resolve(params))
            .catch(function(err){
                logger.info(err);
                reject(responseCode.SOME_INTERNAL_ERROR);
            });
    })
}

UserMasterClass.prototype.checkDuplicateMobileNumber=function (params) {
    logger.info(params);
    return new Promise((resolve,reject)=>{
        UserMaster.find({raw:true,where:{mobileNbr:params.mobileNbr,userId:params.userId}})
            .then(users=>(users)?reject(responseCode.DUPLICATE_MOBILE_NUMBER):resolve(params))
            .catch(function(err){
                logger.info(err);
                reject(responseCode.SOME_INTERNAL_ERROR);
            });
    })
}


UserMasterClass.prototype.checkUserEmailId=function (params) {
    return new Promise((resolve,reject)=>{
        UserMaster.find({raw:true,where:{emailId:params.emailId}})
            .then(resolve)
            .catch(function(err){
                logger.info(err);
                reject(responseCode.SOME_INTERNAL_ERROR);
            });
    })
}


UserMasterClass.prototype.updateInfo=function (info) {
    logger.info('params ',info);
   return new Promise((resolve,reject)=>{
       if('id' in info){ var userId=info.id;}
       delete params['id'];
       Info.update(info,{where:{userId:userId}})
           .then(resolve)
           .catch(reject)
   })
}

UserMasterClass.prototype.updateUserStatusRedis=function (userId) {
    logger.info('params ',userId);

    return new Promise((resolve,reject)=>{
        UserMaster.findOne({raw:true,where:{userId:userId}})
            .then(resolve)
            .catch(function(err){
                logger.info(err);
                reject(responseCode.SOME_INTERNAL_ERROR);
            });
    })

}

UserMasterClass.prototype.getUserAttributes=function (where,attr) {
    return new Promise((resolve,reject)=>{
        UserMaster.findOne({raw:true,where:where,attributes:attr,include:[{model:Info,as:'personalInfo',attributes:['firstName','lastName']}]})
            .then(function (user) {
                if(!user) return reject('user not found');
                resolve(user);
            })
            .catch(reject)
    });
};
UserMasterClass.prototype.getUserPanBankDetails=function (where,attr,amt) {
    return new Promise((resolve,reject)=>{
        let includes=[
                {model:UserMaster,as:'panCard',attributes:attr,include:[
                {model:bankDetails,as:'bankDetails',attributes:['accountNumber','ifscCode','bankName','branch']}]},
            ];
        panCard.findOne({raw:true,where:where,include:includes})
            .then(function (user) {
                if(!user) return reject(responseCode.USER_NOT_EXIST);
                user.amt=amt;
                logger.info(user,'??????????????//');
                resolve(user);
            })
            .catch(function (err) {
                logger.info(new Error(err));
                reject(responseCode.MYSQL_ERROR);
            })
    });
};
module.exports=new UserMasterClass;

/*************************************/
if(require.main==module){
    (function () {
        user={
            userId:18,
        }
        module.exports.getUserPanBankDetails(user,['email']);
    })();
}
