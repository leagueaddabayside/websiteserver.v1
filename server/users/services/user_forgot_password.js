var dbPool = require("../../conf/dbconnection.js");
var gnUserMaster = require("../../models/gnUserMaster");
var gnUserInfoHistory = require("../../models/gnUserInfoHistory");
var logger = require("../../conf/logger").log;
var responseCode = require("../../conf/response_code");
var moment = require(NODE_PATH + 'moment');
var async = require(NODE_PATH + 'async');
var utility = require("utility");
var crypto = require('crypto');

function userForgotPassword(requestObject, callback) {
	dbPool.websitePool.getConnection(function(error, connection) {
		if (error) {
			return;
		}
        
		var password = crypto.createHash('sha256').update(requestObject.password).digest("hex");

		var requestData = {
			userId : requestObject.userId,
			password : password,
			changeTime :utility.getMysqlDateTimeFormat(utility.currentTimeStamp()),
			changeType : 'PASSWORD',
			changeValue : password
		};
		var responseObject = new Object();

		async.waterfall([
			function(ascb) {
				gnUserInfoHistory.insertUserInfoHistory(requestData, connection,
						function(err, responseData) {
							if (error) {
								ascb(error, responseData);
								return;
							}
							ascb(null, responseData);
						});

			},
			function(passedData,ascb) {
				gnUserMaster.updateUserMasterInfo(requestData, connection,
						function(err, responseData) {
							if (error) {
								ascb(error, responseData);
								return;
							}
							ascb(null, responseData);
						});
			}
		], function(err, results) {
			logger.info("results", results);
			if (err) {
				logger.error(error);
				responseObject.responseCode = responseCode.MYSQL_ERROR;
				connection.release();
				callback(error, responseObject);
				return;
			} else {
				responseObject.responseCode = responseCode.SUCCESS;
				responseObject.responseData = results;
				callback(null, responseObject);
			}
			connection && connection.release();
		});

	});
	
	

}


module.exports = userForgotPassword;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.userId = 49;
	requestObject.password = 'dfdfdf';
	userForgotPassword(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - ", responseObject.responseData);
	});
}
