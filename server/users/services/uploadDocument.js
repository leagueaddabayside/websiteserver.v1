/**
 * Created by adityagupta on 2/3/17.
 */
const fs=require('fs');
const UserPanCard=require('../models/gnUserPanCard');
const UserBankDetails=require('../models/gnUserBankDetails');
const UserMaster=require('../services/UserMaster');
const User=require('../models/gnUserMaster');
const Info=require('../models/gnUserInfo');
const config=require('../../../bin/config');
const responseCode=require('../../utils/response_code');
function UserDocumentClass() {
    this.panCard=UserPanCard;
    this.userMaster=UserMaster;
    this.bank=UserBankDetails;
}
UserDocumentClass.prototype.uploadPanCard=function(params,file) {
    let that=this;
    return new Promise((resolve,reject)=>{
        that.uploadPanImage(params,file)
            .then((result)=>that.panCard.upsert(result,{userId:params.userId}))
            .then(that.userMaster.updateUserData({isPanCardVerified:'PROCESSING',userId:params.userId}))
            .then(resolve)
            .catch(function (err) {
                console.error(new Error(err));
                reject(responseCode.SOME_INTERNAL_ERROR);
            })
    })
};

UserDocumentClass.prototype.uploadPanImage=function (params,file) {
    return new Promise((resolve,reject)=>{
        let uploadDir=config.panCard.uploadDir;
        if(!file.panCardImage.path) return reject('image path not found');
        if(!fs.existsSync(uploadDir)) fs.mkdirSync(uploadDir);
        let extractedFile=file.panCardImage.path.split('.');
        let fileTail=extractedFile[extractedFile.length-1];
        fileTail=fileTail.toLowerCase();
        if(!config.panCard.filtType.includes(fileTail)) return reject(responseCode.INVALID_REQUEST_PARAMS);
        let imageName=config.panCard.prefix+'_'+new Date().getTime()+'.'+fileTail;
        fs.readFile(file.panCardImage.path,function (err,data) {
            fs.writeFile(uploadDir+'/'+imageName, data, 'utf8',function (err,result) {
                if(err) return reject(err);
                params.panCardimage=imageName;
                resolve(params);
            });
        })
    })
};
UserDocumentClass.prototype.getPenCardList=function () {
    let that=this;
    return new Promise((resolve,reject)=>{
        that.panCard.findAll({raw:true,where:{status:'PENDING'},include:[{model:User,as:'panCard',attributes:
            ['screenName'],include:[{model:Info,as:'personalInfo',attributes:['dob']}]}], order: '"userId" DESC'})
            .then(function (panCard) {
                 return resolve(panCard);
            }).catch(function (err) {
                console.error(new Error(err))
            reject(responseCode.SOME_INTERNAL_ERROR)
        })
    })
};
UserDocumentClass.prototype.updatePanStatus=function (where,params) {
    return new Promise((resolve,reject)=>{
        UserPanCard.findOne({where:where})
            .then(function (panCardDocument) {
                if(!panCardDocument) return reject(responseCode.NOT_FOUND);
                panCardDocument.status=params.status;
                panCardDocument.panNumber=params.panNumber;
                panCardDocument.remarks=params.remarks;
                let isPanVerified='YES';
                panCardDocument.save()
                    .then(function (doc) {
                        if(params.status.toLowerCase()=='failed') isPanVerified='NO';
                       return UserMaster.updateUserData({isPanCardVerified:isPanVerified,userId:doc.userId})
                            .then(resolve).catch(reject)
                    }).catch(reject)
            })
    })
};
UserDocumentClass.prototype.addBankDetails=function (params) {
    let that=this;
    return new Promise((resolve,reject)=>{
        UserBankDetails.upsert(params,{userId:params.userId})
            .then((result)=>that.userMaster.updateUserData({isBankVerified:'PROCESSING',userId:params.userId}))
            .then((data)=>resolve(params))
            .catch(reject);
    })
};
UserDocumentClass.prototype.approveBankAccountStatus=function (params) {
    return new Promise((resolve,reject)=>{
        UserBankDetails.findOne({where:{userId:params.userId}})
            .then(function (bankDocument) {
                if(!bankDocument) return reject(responseCode.NOT_FOUND);
                bankDocument.status=params.status.toUpperCase();
                if(params.remarks) bankDocument.remarks=params.remarks;
                let isBankVerified='YES';
                if(params.status.toLowerCase()=='failed') isBankVerified='NO';
                bankDocument.save()
                    .then(function (doc) {
                        return UserMaster.updateUserData({isBankVerified:isBankVerified,userId:doc.userId})
                            // .then(resolve).catch(reject)
                    }).then(function (userInfo) {
                    resolve(userInfo);
                }).catch(function (err) {
                    reject(responseCode.MYSQL_ERROR)
                })
            })
    })
};

UserDocumentClass.prototype.getBankAccountList=function () {
    return new Promise((resolve,reject)=>{
        UserBankDetails.findAll({where:{status:'pending'},order:'"id" DESC'})
            .then(function (bankList) {
                return resolve(bankList);
            }).catch(function (err) {
                console.error(err);
            reject(responseCode.SOME_INTERNAL_ERROR);
        });
    })
};
UserDocumentClass.prototype.getUserPanCard=function (id) {
    let that=this;
    return new Promise((resolve,reject)=>{
        that.panCard.findOne({where:{userId:id,status:{$in:['PENDING','APPROVE']}},raw:true})
            .then(function (panCard) {
                if(!panCard) return reject(responseCode.NOT_FOUND);
                resolve(panCard);
            }).catch(function (err) {
            console.error(new Error(err));
            reject(responseCode.SOME_INTERNAL_ERROR);
        });
    })
};
UserDocumentClass.prototype.getUserBankDetails=function (id) {
    let that=this;
    return new Promise((resolve,reject)=>{
        that.bank.findOne({where:{userId:id,status:{$in:['PENDING','APPROVE']}},raw:true})
            .then(function (panCard) {
                if(!panCard) return reject(responseCode.NOT_FOUND);
                resolve(panCard);
            }).catch(function (err) {
            console.error(err);
            reject(responseCode.SOME_INTERNAL_ERROR);
        });
    })
};
module.exports=new UserDocumentClass();
