var logger = require('../../utils/logger.js').websiteLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var otpService = require("../../otp/services/index");
const logger= require('../../utils/logger').websiteLogs;

function validateForgotUserOtp(request, response, next) {
	var reqParams = request.body;
	logger.info('validateUserOtp reqParams =', reqParams);
	reqParams.otpFor = 'FORGOT_PASSWORD';
	var responseData = new Object();

	if (!(reqParams.otp && reqParams.userId)) {
		responseData.respCode = responseCode.INVALID_OTP;
		responseData.message = responseMessage[responseCode.INVALID_OTP];
		logger.info('validateUserOtp responseData =', responseData);
		response.json(responseData);
		return;
	}
	
	otpService.findOtpByPropertyService(reqParams,function(error,data){
		if (error) {
			responseData.respCode = responseCode.MONGO_ERROR;
			response.json(responseData);
			return;
		}
		if(data.responseCode == responseCode.SUCCESS && data.responseData){
			next();
		}else{
			responseData.respCode = responseCode.INVALID_OTP;
			responseData.message = responseMessage[responseCode.INVALID_OTP];
			response.json(responseData);
			return;
		}
	});
	

}

module.exports.validateForgotUserOtp = validateForgotUserOtp;
