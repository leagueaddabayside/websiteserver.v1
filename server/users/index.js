/**
 * Created by adityagupta on 18/1/17.
 */
const express=require('express');
const routes=express.Router();
const UserCtrl=require('./controller');
const middleware = require('../middleware');
const userMw=require('./middleware');
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();


routes.post('/users/create',middleware.auditTrailLog,UserCtrl.create);
routes.post('/users/update',middleware.auditTrailLog,middleware.getUserIdFromToken,UserCtrl.update);
routes.post('/users/updateTeam',middleware.auditTrailLog,middleware.getUserIdFromToken,UserCtrl.updateTeam);

routes.post('/users/login',middleware.auditTrailLog,UserCtrl.userLogin);
routes.post('/users/logout',middleware.auditTrailLog,UserCtrl.userLogout);
routes.post('/users/getbyid',middleware.getUserIdFromToken,UserCtrl.get);
routes.post('/users/validate/token',UserCtrl.validateUserToken);
routes.post('/users/changePassword',middleware.auditTrailLog,middleware.getUserIdFromToken,UserCtrl.changePassword);
routes.post('/users/validate/mobileotp',middleware.auditTrailLog,middleware.getUserIdFromToken,UserCtrl.validateUserMobileOtp);
routes.post('/users/validate/emailotp',middleware.auditTrailLog,middleware.getUserIdFromToken,UserCtrl.validateUserEmailOtp);
routes.post('/users/userForgotPassword',middleware.auditTrailLog,UserCtrl.userForgotPassword);
routes.post('/users/pancard/upload',multipartMiddleware,middleware.getUserIdFromToken,UserCtrl.uploadPanCard);
routes.post('/users/pancard/list',middleware.getAdminIdFromToken,UserCtrl.GetPendingPenCardList);
routes.post('/users/pancard/approve/',middleware.auditTrailLog,middleware.getAdminIdFromToken,UserCtrl.updatePenCardStatus);
routes.post('/users/bank/add',middleware.getUserIdFromToken,UserCtrl.addBankDetails);
routes.post('/users/bank/approve',middleware.auditTrailLog,middleware.getAdminIdFromToken,UserCtrl.updateBankDetails);
routes.post('/users/bank/list',middleware.getAdminIdFromToken,UserCtrl.getBankDetails);
routes.post('/users/pancard',middleware.getUserIdFromToken,UserCtrl.getUserPanCard);
routes.post('/users/bankac',middleware.getUserIdFromToken,UserCtrl.getUserBankDetails);



routes.get('/*',function (req,res) {
    res.json({code:404,error:'Iinvalid token'})
})

module.exports=routes
