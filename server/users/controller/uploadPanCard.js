/**
 * Created by adityagupta on 19/1/17.
 */
const UploadDocument=require('../services/uploadDocument');
const Util=require('../../utils/util');
const config=require('../../../bin/config');
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');
const UploadPanCard=(req,res)=>{
    let params=req.body;
    params.status='PENDING';
    let code=responseCode.SUCCESS;
    if(Object.keys(req.files).length===0) code=responseCode.NOT_FOUND;
    //console.log(req.files);
    let extractedFile=req.files.panCardImage.name.split('.');
    let fileTail=extractedFile[extractedFile.length-1];
    fileTail=fileTail.toLowerCase();
    if(!config.panCard.filtType.includes(fileTail)) code=responseCode.INVALID_FILE_TYPE;
    let regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
    if(!regpan.test(params.panNumber))  code=responseCode.INVALID_REQUEST_PARAMS;
    if(!params.name || params.name.length===0) code=responseCode.INVALID_REQUEST_PARAMS;
    if(!params.userId)  code=responseCode.INVALID_REQUEST_PARAMS;
    if(!params.state)  code=responseCode.INVALID_REQUEST_PARAMS;
    if(code!==100) return  res.json(Util.response(code,responseMessage[code],[]));
    UploadDocument.uploadPanCard(params,req.files)
        .then(function (result) {
            let message='success';
            res.json(Util.response(100,message,result[0]))
        })
        .catch (function (errorCode) {
            res.json(Util.response(errorCode,responseMessage[errorCode],[]));
        })
}
module.exports=UploadPanCard;

/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                id:14
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        FindCtrl(req,res);
    })()
}
