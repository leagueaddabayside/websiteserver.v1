/**
 * Created by adityagupta on 19/1/17.
 */
const UserMasterService=require('../services/UserMaster');
const Util=require('../../utils/util');
const logger= require('../../utils/logger').websiteLogs;

const FindCtrl=(req,res)=>{
    let params=req.body;
    UserMasterService.getById(params.userId)
        .then(function (result) {
            logger.info(Util,'>>>>>>>>>>>>>>>>>>>>>>>>>');
            res.json({respCode:100,respData:result})

        })
        .catch (function (err) {
            logger.info(err);
            res.json(Util.response(400,err,[]));
        })
}
module.exports=FindCtrl;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                id:18
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        FindCtrl(req,res);
    })()
}
