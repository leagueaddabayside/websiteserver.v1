/**
 * Created by adityagupta on 3/2/17.
 */
const UserMasterService=require('../services/UserMaster');
const Util=require('../../utils/util');
const config=require('../../../bin/config');
const responseCode =require('../../utils/response_code');
const responseMessage =require('../../utils/response_message');
const otpService = require('../../otp/services/index');
const bonusService=require('../../payment/services/userRealBonus');
const VERIFY_MOBILE=config.bonus.VERIFY_MOBILE;
const logger= require('../../utils/logger').websiteLogs;

const ValidateUserMobileOtpCtrl=(req,res)=>{
    let params=req.body;
    logger.info('ValidateUserMobileOtpCtrl params',params);
    params.otpFor = 'VERIFY_MOBILE';
    var responseObject = {};

    otpService.validateUserOtpService(params,function(err,otpResponse){
        logger.info('otpResponse',otpResponse);
        if (err) {
            responseObject.respCode = otpResponse.responseCode;
            responseObject.message = responseMessage[otpResponse.responseCode];
            res.json(responseObject);
            return;
        } else if (otpResponse.responseCode !== responseCode.SUCCESS) {
            responseObject.respCode = otpResponse.responseCode;
            responseObject.message = responseMessage[otpResponse.responseCode];
            res.json(responseObject);
            return;
        } else {
            var userUpdateObj = {mobileNbr :  otpResponse.responseData.userData.mobileNo};
            userUpdateObj.userId = params.userId;

            UserMasterService.checkDuplicateMobileNumber(userUpdateObj)
                .then(UserMasterService.updateUserData)
                .then(function (result) {
                    if(result) {
                        if(VERIFY_MOBILE.status=='ACTIVE') bonusService.addBonus({bonusId:VERIFY_MOBILE.bonusId,remarks:'Mobile Verified',userId:params.userId,amount:VERIFY_MOBILE.bonusAmt,emailType:'BONUS_ON_MOBILE_VERIFY'});
                    }
                    res.json({respCode:100,respData:result})
                })
                .catch(function (errorCode) {
                    res.json({respCode:errorCode,message:responseMessage[errorCode]})
                })
        }
    });


}
module.exports=ValidateUserMobileOtpCtrl;


/*****************************************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTQ4NjUzMzk5OH0.l5uWmiZE_uwRXhfaiDAcvL4gnfcb1nV5vRRcSPJaGDw',
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        ValidateUserMobileOtpCtrl(req,res);
    })()
}
