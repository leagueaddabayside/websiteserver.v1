/**
 * Created by adityagupta on 2/3/17.
 */
const UploadDocument=require('../services/uploadDocument');
const Util=require('../../utils/util');
const config=require('../../../bin/config');
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');
const bonusService=require('../../payment/services/userRealBonus');
const VERIFY_PANCARD=config.bonus.VERIFY_PANCARD;
const SendEmail=require('../../utils/emailers');
const logger= require('../../utils/logger').websiteLogs;

const updateStatusCtrl=(req,res)=>{
    let params=req.body;
    let code=responseCode.SUCCESS;
    if(params.status && !config.panCard.status.includes(params.status.toUpperCase())) code=responseCode.INVALID_REQUEST_PARAMS;
    if(!params.userId || !params.status) code=responseCode.INVALID_REQUEST_PARAMS;
    if(code!==100) return res.json(Util.response(code,responseMessage[code],[]))
    UploadDocument.updatePanStatus({userId:params.userId},params)
        .then((result) => {
            logger.info(result)
            if(result && result.isPanCardVerified==='YES') {
                if(VERIFY_PANCARD.status=='ACTIVE') bonusService.addBonus({bonusId:VERIFY_PANCARD.bonusId,remarks:'Pan Verified',userId:params.userId,amount:VERIFY_PANCARD.bonusAmt,emailType:'BONUS_ON_PAN_VERFY'});
            }
            if(result && result.isPanCardVerified==='NO' && result.userId) {
                SendEmail.sendEmailOnEvent(result.userId,config.emailTemplates.PAN_REJECTED,{})
                    .then(logger.info).catch(logger.info)
            }
            res.json(Util.response(100,'success',result))
        })
        .catch ((errorCode)=> {
        logger.info(errorCode);
            res.json(Util.response(errorCode,responseMessage[errorCode],[]))
        })
}
module.exports=updateStatusCtrl;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                id:1
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        updateStatusCtrl(req,res);
    })()
}
