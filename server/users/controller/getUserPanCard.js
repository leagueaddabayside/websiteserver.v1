/**
 * Created by adityagupta on 2/3/17.
 */
const UploadDocument=require('../services/uploadDocument');
const Util=require('../../utils/util');
const responseMessage =require('../../utils/response_message');
const getUserPanCardCtrl=(req,res)=>{
    let params=req.body;
        UploadDocument.getUserPanCard(params.userId)
            .then((result) => {
                res.json(Util.response(100,'success',result))
            })
            .catch ((errorCode)=> {
                res.json(Util.response(errorCode,responseMessage[errorCode],[]))
            })

}
module.exports=getUserPanCardCtrl;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {

            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        getBankDetailsCtrl(req,res);
    })()
}
