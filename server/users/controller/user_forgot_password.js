var logger = require("../../conf/logger").log;
var userService = require('../../services/user/index');
var responseMessage = require("../../conf/response_message");
var responseCode = require("../../conf/response_code");
function userForgotPassword(request, response) {
	var requestData = request.body;
	var responseObject = new Object();
	userService
	.userForgotPassword(
			requestData,
			function(error, data) {
				if (error) {
					responseObject.responseCode = data.responseCode;
					responseObject.responseData = data.responseData;
				} else if (data.responseCode !== responseCode.SUCCESS) {
					responseObject.responseCode = data.responseCode;
					responseObject.responseData = {};
					responseObject.responseData.message = responseMessage[data.responseCode];
				} else {
					
					var serviceData = data.responseData;
					var responseData = {};
					
					responseObject.responseData =responseData;
					;
					responseObject.responseCode = responseCode.SUCCESS;

					logger.info("userForgotPassword API :- Response - %j",
							responseObject);
					response.json(responseObject);
				}
				
			});

}

module.exports = userForgotPassword;

if (require.main === module) {
	(function() {
		var request = {};

		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.otp = 123456,
		requestObject.userData = "9718225670",
		requestObject.password = "pageContent",
		
		console.log(requestObject);
		request.body = requestObject;
		userForgotPassword(request, response);
	})();
}
