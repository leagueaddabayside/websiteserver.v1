/**
 * Created by adityagupta on 3/2/17.
 */
const AuthService=require('../services/Auth');
const Util=require('../../utils/util')
const responseCode =require('../../utils/response_code');
const responseMessage =require('../../utils/response_message');
const otpService = require('../../otp/services/index');
const logger= require('../../utils/logger').websiteLogs;

const userForgotPasswordCtrl=(req,res)=>{
    let params=req.body;
    logger.info('ValidateUserMobileOtpCtrl params',params);
    params.otpFor = 'FORGOT_PASSWORD';
    var responseObject = {};

    otpService.validateUserOtpService(params,function(err,otpResponse){
        logger.info('otpResponse',otpResponse);
        if (err) {
            responseObject.respCode = otpResponse.responseCode;
            responseObject.message = responseMessage[otpResponse.responseCode];
            res.json(responseObject);
            return;
        } else if (otpResponse.responseCode !== responseCode.SUCCESS) {
            responseObject.respCode = otpResponse.responseCode;
            responseObject.message = responseMessage[otpResponse.responseCode];
            res.json(responseObject);
            return;
        } else {
            var userUpdateObj = {password :  params.password};
            userUpdateObj.userId = params.userId;

            AuthService.userForgotPassword(userUpdateObj)
                .then(function (result) {
                    res.json({respCode:100,respData:result})
                })
                .catch(function (errorCode) {
                    res.json({respCode:errorCode,message:responseMessage[errorCode]})
                })
        }
    });


}
module.exports=userForgotPasswordCtrl;


/*****************************************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTQ4NjUzMzk5OH0.l5uWmiZE_uwRXhfaiDAcvL4gnfcb1nV5vRRcSPJaGDw',
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        userForgotPasswordCtrl(req,res);
    })()
}