/**
 * Created by adityagupta on 19/1/17.
 */
const UserLoginCtrl=require('./login');
const UserLogoutCtrl=require('./userLogout');
const CreateCtrl=require('./create');
const ChangePasswordCtrl=require('./changePassword');
const UpdateCtrl=require('./update');
const UpdateTeamCtrl=require('./update_team');
const FindCtrl=require('./get');
const ValidateUserToken=require('./validateUserToken');
const validateUserMobileOtp=require('./validateUserMobileOtp');
const validateUserEmailOtp=require('./validateUserEmailOtp');
const userForgotPassword=require('./userForgotPassword');
const uploadPanCard=require('./uploadPanCard');
const GetPendingPenCardList=require('./getPendingPenCardList');
const updatePenCardStatus=require('./updatePenCardStatus');
const addBankDetails=require('./addBankDetails');
const updateBankDetails=require('./updateBankDetails');
const getBankDetails=require('./getBankDetailsList');
const getUserPanCard=require('./getUserPanCard');
const getUserBankDetails=require('./getUserBankDetails');


module.exports.userLogin = UserLoginCtrl;
module.exports.userLogout = UserLogoutCtrl;
module.exports.create = CreateCtrl;
module.exports.changePassword = ChangePasswordCtrl;
module.exports.update = UpdateCtrl;
module.exports.updateTeam = UpdateTeamCtrl;
module.exports.get = FindCtrl;
module.exports.validateUserToken = ValidateUserToken;
module.exports.validateUserMobileOtp = validateUserMobileOtp;
module.exports.validateUserEmailOtp = validateUserEmailOtp;
module.exports.userForgotPassword = userForgotPassword;
module.exports.uploadPanCard = uploadPanCard;
module.exports.GetPendingPenCardList = GetPendingPenCardList;
module.exports.updatePenCardStatus = updatePenCardStatus;
module.exports.addBankDetails = addBankDetails;
module.exports.updateBankDetails = updateBankDetails;
module.exports.getBankDetails = getBankDetails;
module.exports.getUserPanCard = getUserPanCard;
module.exports.getUserBankDetails = getUserBankDetails;
