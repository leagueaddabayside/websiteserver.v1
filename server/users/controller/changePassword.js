/**
 * Created by adityagupta on 19/1/17.
 */
const AuthService = require('../services/Auth')
const responseMessage = require('../../utils/response_message');
const responseCode= require('../../utils/response_code');
const logger= require('../../utils/logger').websiteLogs;

const UpdatePassCtrl = (req, res)=> {
    let params = req.body;
    let code=responseCode.SUCCESS;
    if(params.password && params.newPassword && params.password===params.newPassword ){
        logger.info('password is same with old one');
        code=responseCode.INVALID_REQUEST_PARAMS;
    }

    if(code!==100) return  res.json({respCode: responseCode.INVALID_REQUEST_PARAMS, message: responseMessage[responseCode.INVALID_REQUEST_PARAMS]})
    AuthService.UpdatePassword(params)
        .then(function (result) {
            res.json({respCode: 100})
        })
        .catch(function (errorCode) {
            logger.info(errorCode);
        res.json({respCode: errorCode, message: responseMessage[errorCode]})
    })
}
module.exports = UpdatePassCtrl;
