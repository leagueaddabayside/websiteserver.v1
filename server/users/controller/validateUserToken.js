/**
 * Created by adityagupta on 3/2/17.
 */
const AuthService=require('../services/Auth');
const Util=require('../../utils/util')
const responseMessage =require('../../utils/response_message');


const ValidateUserTokenCtrl=(req,res)=>{
    let params=req.body;

        AuthService.validateToken(params.token)
            .then((result) => {
                res.json({respCode:100,respData:result})
            })
            .catch ((errorCode)=> {
            res.json({respCode:errorCode,message:responseMessage[errorCode]})
        });

}
module.exports=ValidateUserTokenCtrl;


/*****************************************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTQ4NjUzMzk5OH0.l5uWmiZE_uwRXhfaiDAcvL4gnfcb1nV5vRRcSPJaGDw',
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        ValidateUserTokenCtrl(req,res);
    })()
}