/**
 * Created by adityagupta on 19/1/17.
 */
const AuthService = require('../services/Auth');
const jwt = require('jsonwebtoken');
const responseMessage = require('../../utils/response_message');
const responseCode = require('../../utils/response_code');

const LogoutCtrl = (req, res)=> {
    let params = req.body;
    let userId = '';
    let token = null;
    if (req.cookies && req.cookies.token) {
        token = req.cookies.token;
        res.clearCookie('token');
    }

    if (!token)
        token = params.token;

    if (token) {
        AuthService.logout(token)
            .then(function (result) {

                return res.json({respCode: 100, respData: result})
            })
            .catch(function (errorCode) {
            return res.json({
                respCode: responseCode.TOKEN_ALREADY_EXPIRED,
                message: responseMessage[responseCode.TOKEN_ALREADY_EXPIRED]
            })
        })
    } else {
        res.json({
            respCode: responseCode.TOKEN_ALREADY_EXPIRED,
            message: responseMessage[responseCode.TOKEN_ALREADY_EXPIRED]
        })
    }
}
module.exports = LogoutCtrl;


/**********************/
if (require.main == module) {
    (function () {
        var req = {
            body: {
                token: "adda52_admin_token_eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTQ4NjUzMzk5OH0.l5uWmiZE_uwRXhfaiDAcvL4gnfcb1nV5vRRcSPJaGDw",
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        LogoutCtrl(req, res);
    })()
}