/**
 * Created by adityagupta on 20/1/17.
 */
const UserMasterService=require('../services/UserMaster');
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');
const Emailer=require('../../utils/node_mailer');

const Create=(req,res)=>{
    let params=req.body;
    let code=responseCode.SUCCESS;
    let validate=params.emailId  && params.password && params.dob;
    /*if(Date.parse(params.dob)){
        let year=new Date(params.dob).getFullYear();
        let currentYear=new Date().getFullYear();
        console.log(currentYear,'ddddddd',year)
        if(currentYear-year<=18) code=responseCode.INVALID_REQUEST_PARAMS;
    }*/
    if(!validate) code=responseCode.INVALID_REQUEST_PARAMS;
    if(code!==100) return res.json({respCode:responseCode.INVALID_REQUEST_PARAMS,message:responseMessage[responseCode.INVALID_REQUEST_PARAMS]})
    UserMasterService.checkDuplicateEmailId(params)
        .then(UserMasterService.create)
        .then(function (result) {
            if(result) Emailer.sendMail({templateType:'WELCOME_EMAIL',to:params.emailId,templateData:{}},function () {
            });
            res.json({respCode:100,respData:result})
        })
        .catch(function (errorCode) {
            console.log(errorCode)
            res.json({respCode:errorCode,message:responseMessage[errorCode]})
        })
}

module.exports=Create;


/*************************************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                domainId: 11,
                roleId: 122,
                levelId: 12,
                parentUserId: 111,
                isRoleHead: 'YES',
                userName: 'asas',
                screenName: 'screen1ww',
                emailId: 'aditya.gupsta@gaussnetworks.com',
                mobileNbr: '928919828992',
                aliasName: 'alias',
                userType: 'HOME',
                password: 'password',
                dob:new Date()
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        Create(req,res);
    })()
}
