/**
 * Created by adityagupta on 2/3/17.
 */
const UploadDocument=require('../services/uploadDocument');
const Util=require('../../utils/util');
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');

const BankDetailsCtrl=(req,res)=>{
    let params=req.body;
    let checkParams=params.userId && params.ifscCode && params.accountNumber && params.branch;
    if(!checkParams) return  res.json(Util.response(responseCode.INVALID_REQUEST_PARAMS,responseMessage[responseCode.INVALID_REQUEST_PARAMS],[]));
    params.status='PENDING';
    UploadDocument.addBankDetails(params)
        .then((result) => {
            res.json(Util.response(responseCode.SUCCESS,'success',result))
        })
        .catch ((errorCode)=> {
            res.json(Util.response(responseCode.MYSQL_ERROR,responseMessage[responseCode.MYSQL_ERROR],[]))
        })
}
module.exports=BankDetailsCtrl;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:2,
                ifscCode:'AZYPd1233',
                branch:'dlf-phase 3',
                accountNumber:'1221212123333',
                bankName:'icici bank',
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        BankDetailsCtrl(req,res);
    })()
}
