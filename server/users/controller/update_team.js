/**
 * Created by adityagupta on 19/1/17.
 */
const UserMasterService=require('../services/UserMaster');
const Util=require('../../utils/util');
const responseCode =require('../../utils/response_code');
const responseMessage =require('../../utils/response_message');
const validator=require('validator');


const UpdateTeamCtrl=(req,res)=>{
    let params=req.body;
    let isValid = true;

    if(!params.dob){
        isValid = false;
    }
    if(!params.state){
        isValid = false;
    }else if(params.state.length == 0){
        isValid = false;
    }
    if(!params.screenName){
        isValid = false;
    }else if(params.screenName.length < 2 || params.screenName.length > 20){
        isValid = false;
    }else if(!validator.isAlphanumeric(params.screenName)){
        isValid = false;
    }


    if(!isValid){
        res.json({
            respCode: responseCode.INVALID_REQUEST_PARAMS,
            message: responseMessage[responseCode.INVALID_REQUEST_PARAMS]
        });

    }

    UserMasterService.checkDuplicateScreenName(params)
        .then(UserMasterService.updateTeam)
        .then(function (result) {
            res.json({respCode:100,message:responseMessage[100]})
        })
        .catch(function (errorCode) {
            res.json({respCode:errorCode,message:responseMessage[errorCode]})
        })

}
module.exports=UpdateTeamCtrl;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId: 14,
                screenName:'username789',
                dob:'12/22/2018',
                state:'Punjab'
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        UpdateTeamCtrl(req,res);
    })()
}
