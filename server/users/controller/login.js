/**
 * Created by adityagupta on 7/2/17.
 */

const AuthService=require('../services/Auth');
const Util=require('../../utils/util')
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');
const logger= require('../../utils/logger').websiteLogs;

const userLoginCtrl=(req,res)=>{
    let params=req.body;
    let validate=params.userData && params.password;
    if(!validate) return res.json({respCode:responseCode.INVALID_REQUEST_PARAMS,message:responseMessage[responseCode.INVALID_REQUEST_PARAMS]})
    AuthService.Autharized(params.userData,params.password)
        .then(function (result) {
            logger.info('userLoginCtrl',result);
            res.cookie('token', result.token, { maxAge: 1000000000, httpOnly: true });
            res.json({respCode:100,respData:result})
        })
        .catch (function (errorCode) {
        logger.info('userLoginCtrl',errorCode);
        res.json({respCode:errorCode,message:responseMessage[errorCode]})
        })
}
module.exports=userLoginCtrl;



/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userName:'aditya',
                password:'enthusiasm',
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        userLoginCtrl(req,res);
    })()
}
