/**
 * Created by adityagupta on 2/3/17.
 */
const UploadDocument=require('../services/uploadDocument');
const Util=require('../../utils/util');
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');
const config=require('../../../bin/config');
const SendEmail=require('../../utils/emailers');
const logger= require('../../utils/logger').websiteLogs;

const UpdateBankDetailsCtrl=(req,res)=>{
    let params=req.body;
    let code=responseCode.SUCCESS;
    if(params.status && !config.panCard.status.includes(params.status.toUpperCase())) code=responseCode.INVALID_REQUEST_PARAMS;
    if(!params.userId || !params.status) code=responseCode.INVALID_REQUEST_PARAMS;
    if (code!==100) return res.json(Util.response(code,responseMessage[code],[]));
    UploadDocument.approveBankAccountStatus(params)
        .then((result) => {
            if(result && result.isBankDetailVerified==='NO' && result.userId) {
                SendEmail.sendEmailOnEvent(result.userId,config.emailTemplates.BANK_REJECTED,{})
                    .then(logger.info).catch(logger.info)
            }
            res.json(Util.response(100,'success',result))
        })
        .catch ((errorCode)=> {
            res.json(Util.response(errorCode,responseMessage[errorCode],[]))
        })
}
module.exports=UpdateBankDetailsCtrl;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                status:'success',
                id:1,
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        UpdateBankDetailsCtrl(req,res);
    })()
}
