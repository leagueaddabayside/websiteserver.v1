/**
 * Created by adityagupta on 19/1/17.
 */
const UserMasterService=require('../services/UserMaster');
const Util=require('../../utils/util');
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');


const UpdateCtrl=(req,res)=>{
    let params=req.body;
    if(params.firstName && params.firstName.length<=2 && params.firstName.length>=20){
      return  res.json({respCode:responseCode.INVALID_REQUEST_PARAMS,message:responseMessage[responseCode.INVALID_REQUEST_PARAMS]})
    }
    UserMasterService.update(params)
        .then((result) => {
            res.json({respCode:100})
        })
        .catch ((errorCode)=> {
        res.json({respCode:errorCode,message:responseMessage[errorCode]})
        })
}
module.exports=UpdateCtrl;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                id: 14,
                userName:'username',
                emailId:'adityagupta160@gauss123',
                screenName: 'screens 123',
                dob:'12/22/2018',
                firstName:'firsdjcnjkfsd fds fjs df sd fh sdhf hsd fj sdfffffffffff'
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        UpdateCtrl(req,res);
    })()
}
