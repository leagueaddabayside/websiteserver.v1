/**
 * Created by adityagupta on 19/1/17.
 */
const UserMaster=require('../users/models/gnUserMaster');
const logger=require('../utils/logger').websiteLogs;

class UserMiddlwareClass{
    constructor(){
        this.user=UserMaster;
    }
    findByUserId(req,res,next){
        let params=req.body;
        UserMaster.findOne({where:{userId:params.userId},attributes:['userId']})
            .then(function (user) {
                logger.info(user);
                if(!user) return res.json({respCode:400,message:'user not found',respData:[]});
                next();
            }).catch(function (err) {
            res.json({respCode:400,message:'not found',data:[]});
        })
    }
}

module.exports=new UserMiddlwareClass();
