"use strict";

const config = require('../../bin/config');
const redis = require('./redisConnection');
const Chance = require('chance');
const tokenAuthPrefix = 'la_';
const adminTokenAuthPrefix = 'la_admin_';
const adminBannerPaneltokenAuthPrefix = 'la_banner_admin_';

const jwt = require('jsonwebtoken');
const tokenValidity = 86400;   // in secods (1 day)
const adminTokenValidity = 1800; //in seconds

const responseCode =require('../utils/response_code');

const TokenizerClass = function() {};

TokenizerClass.prototype.checkToken = function (token) {
    return new Promise((resolve, reject)=> {
        let redisKey = tokenAuthPrefix + token;
        console.log('rediskey '+redisKey);
        redis.get(redisKey, function(err, savedData) {
            console.log('savedData',savedData);
            if (savedData){
                redis.hgetall(savedData, function(err, userData) {
                    if (userData){
                        userData.token = token;
                        redis.expire(redisKey, tokenValidity);
                        redis.expire(savedData, tokenValidity);
                        return resolve(userData);
                    }
                    reject(responseCode.INVALID_USER_TOKEN);
                });
                return;
            }
            reject(responseCode.INVALID_USER_TOKEN);
        });

    })
}
TokenizerClass.prototype.setToken=function(user) {
    let redisValue ={userId : user.userId};
    if(user.screenName){
        redisValue.screenName = user.screenName;
    }
    if(user.emailId){
        redisValue.emailId = user.emailId;
    }

    let tokenValue ={userId : user.userId};
    return new Promise((resolve,reject)=>{
        let token  = jwt.sign(tokenValue,config.session.SECRET);

        let redisKey = tokenAuthPrefix + token;
        console.log('redisvalue',redisValue);
        redis.set(redisKey, user.userId);
        redis.expire(redisKey, tokenValidity);
        redis.hmset(user.userId, redisValue);
        redis.expire(user.userId, tokenValidity);

        redisValue.token = token;
        resolve(redisValue);
    });
}

TokenizerClass.prototype.setAdminToken=function(user,adminType) {
    let redisValue ={userId : user.userId};
    if(user.screenName){
        redisValue.screenName = user.screenName;
    }
    if(user.emailId){
        redisValue.emailId = user.emailId;
    }
    if(user.roleId){
        redisValue.roleId = user.roleId;
    }
    let tokenValue ={userId : user.userId};
    return new Promise((resolve,reject)=>{
        let token  = jwt.sign(tokenValue,config.session.SECRET);
        let redisKey = adminTokenAuthPrefix + token;
        if(adminType == 'BANNER_ADMIN'){
            redisKey = adminBannerPaneltokenAuthPrefix + token;
        }

        console.log('redisvalue',redisValue);
        redis.set(redisKey, user.userId);
        redis.expire(redisKey, adminTokenValidity);
        redisValue.token = token;
        redis.hgetall(user.userId, function(err, userData) {
            console.log('?????????????????????????????get userData',userData);
            if (userData && userData.token){
                let prevToken = adminTokenAuthPrefix + userData.token;
                if(adminType == 'BANNER_ADMIN'){
                    prevToken = adminBannerPaneltokenAuthPrefix + token;
                }
                console.log('?????????????????????????????get prevToken',prevToken);
                redis.del(prevToken);
            }
            redis.hmset(user.userId, redisValue);
        });




        resolve(redisValue);
    });
}
TokenizerClass.prototype.checkAdminToken = function (params) {
    let token = params.token;
    let redisKey = adminTokenAuthPrefix + token;
    if(params.adminType == 'BANNER_ADMIN'){
        redisKey = adminBannerPaneltokenAuthPrefix + token;
    }

    return new Promise((resolve, reject)=> {

        console.log('rediskey '+redisKey);
        redis.get(redisKey, function(err, savedData) {
            console.log('savedData',savedData);
            if (savedData){
                redis.hgetall(savedData, function(err, userData) {
                    if (userData){
                        userData.token = token;
                        redis.expire(redisKey, adminTokenValidity);
                        return resolve(userData);
                    }
                    reject(responseCode.INVALID_ADMIN_TOKEN);
                });
                return;
            }
            reject(responseCode.INVALID_ADMIN_TOKEN);
        });

    })
}
TokenizerClass.prototype.updateUser = function(updateData) {

    let redisValue ={};

    if(typeof updateData.depositAmt !== "undefined" && updateData.depositAmt !== null){
        redisValue.depositAmt = parseFloat(updateData.depositAmt).toFixed(2);
    }
    if(typeof updateData.bonusAmt !== "undefined" && updateData.bonusAmt !== null){
        redisValue.bonusAmt = parseFloat(updateData.bonusAmt).toFixed(2);
    }
    if(typeof updateData.winningAmt !== "undefined" && updateData.winningAmt !== null){
        redisValue.winningAmt = parseFloat(updateData.winningAmt).toFixed(2);
    }
    if(typeof updateData.dob !== "undefined" && updateData.dob !== null){
        redisValue.dob = updateData.dob;
    }
    if(typeof updateData.state !== "undefined" && updateData.state !== null){
        redisValue.state = updateData.state;
    }
    if(typeof updateData.firstName !== "undefined" && updateData.firstName !== null){
        redisValue.firstName = updateData.firstName;
    }
    if(typeof updateData.lastName !== "undefined" && updateData.lastName !== null){
        redisValue.lastName = updateData.lastName;
    }
    if(typeof updateData.screenName !== "undefined" && updateData.screenName !== null){
        redisValue.screenName = updateData.screenName;
    }

    if(typeof updateData.userStatus !== "undefined" && updateData.userStatus !== null){
        redisValue.userStatus = updateData.userStatus;
    }

    let redisKey = updateData.userId;
    redis.hmset(redisKey, redisValue);
}

TokenizerClass.prototype.getUserData = function(requestData) {
    let redisKey = requestData.userId;
    redis.hgetall(redisKey, function(err, savedData) {
        if (savedData) return resolve(savedData);
        reject(responseCode.INVALID_USER_TOKEN);
    });

}


TokenizerClass.prototype.removeToken = function (token) {
    return new Promise((resolve, reject)=> {
        let redisKey = tokenAuthPrefix + token;
        console.log('redisKey ' + redisKey);
        redis.exists(redisKey, function (err, savedData) {
            console.log('saved data', savedData);
            if (savedData === 1) {
                redis.del(redisKey, function (err, saved_token) {
                    console.log(err || saved_token);
                    if (saved_token && !err)
                        return resolve(saved_token);
                    reject(err);
                });
            } else {
                reject(responseCode.TOKEN_ALREADY_EXPIRED);
            }

        })
    })
}

TokenizerClass.prototype.removeAdminToken = function (params) {
    let token = params.token;
    let redisKey = adminTokenAuthPrefix + token;
    if(params.adminType == 'BANNER_ADMIN'){
        redisKey = adminBannerPaneltokenAuthPrefix + token;
    }

    return new Promise((resolve, reject)=> {
        console.log('redisKey ' + redisKey);
        redis.exists(redisKey, function (err, savedData) {
            console.log('saved data', savedData);
            if (savedData === 1) {
                redis.del(redisKey, function (err, saved_token) {
                    console.log(err || saved_token);
                    if (saved_token && !err)
                        return resolve(saved_token);
                    reject(err);
                });
            } else {
                reject(responseCode.TOKEN_ALREADY_EXPIRED);
            }

        })
    })
}

module.exports = new TokenizerClass();
/*---------------------------------------------*/
if (require.main === module) {
    (function () {

    })();
}
