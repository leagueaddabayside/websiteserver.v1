var addImageApi = require("./add_image");
var findImageByPropertyApi = require("./find_image_by_property");
var findImageListApi = require("./find_image_list");
var updateImageApi = require("./update_image");
var removeImageApi = require("./remove_image");
// Require

module.exports = {
	addImage : addImageApi,
	findImageByProperty : findImageByPropertyApi,
	findImageList : findImageListApi,
	updateImage : updateImageApi,
	removeImage : removeImageApi
// Export
};