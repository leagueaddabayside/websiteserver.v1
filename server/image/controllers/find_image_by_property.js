var logger = require("../../utils/logger").websiteLogs;
var imageService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findImageByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findImageByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	imageService.findImageByProperty(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var currentRow = data.responseData;
			var currentObj = {};
			currentObj.imageName = currentRow.imageName;
			currentObj.imageUrl = currentRow.imageUrl;
			currentObj.imageSize = currentRow.imageSize;
			currentObj.imageType = currentRow.imageType;
			currentObj.status = currentRow.status;

			responseObject.responseData = currentObj;
		}

		logger.info("findImageByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findImageByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.imageName = "imageName";
		//requestObject.imageUrl = "imageUrl";
		//requestObject.imageSize = "imageSize";
		//requestObject.imageType = "imageType";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findImageByProperty(request, response);
	})();
}