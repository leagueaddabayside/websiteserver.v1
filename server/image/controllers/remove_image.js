var logger = require("../../utils/logger").websiteLogs;
var imageService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function removeImage(request, response, next) {
	var requestObject = request.body;
	//console.log("updateImage API :- Request - %j", requestObject);

	var responseObject = new Object();
	imageService.removeImage(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("removeImage API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = removeImage;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.imageName = "";
		requestObject.imageUrl = "";
		requestObject.imageSize = "";
		requestObject.imageType = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		removeImage(request, response);
	})();
}