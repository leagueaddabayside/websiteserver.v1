var logger = require("../../utils/logger").websiteLogs;
var imageService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findImageList(request, response, next) {
	var requestObject = request.body;
	//console.log("findImageList API :- Request - %j", requestObject);

	var responseObject = new Object();
	imageService.findImageList(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var imageArr = data.responseData;
			var imageList = [];
			for (var i=0, length=imageArr.length; i<length; i++) {
				var currentRow = imageArr[i];
				var currentObj = {};
				currentObj.imageName = currentRow.imageName;
				currentObj.imageUrl = currentRow.imageUrl;
				currentObj.imageSize = currentRow.imageSize;
				currentObj.imageType = currentRow.imageType;
				currentObj.status = currentRow.status;
				imageList.push(currentObj);
			}
			responseObject.responseData = imageList;
		}

		logger.info("findImageList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findImageList;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findImageList(request, response);
	})();
}