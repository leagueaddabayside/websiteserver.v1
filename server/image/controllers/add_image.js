var logger = require("../../utils/logger").websiteLogs;
var imageService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addImage(request, response, next) {
	var requestObject = request.body;
	console.log(request.file);
	var file = request.file;
	requestObject.imageName = file.originalname;
	requestObject.imageUrl = file.originalname;
	requestObject.imageSize = file.size;
	requestObject.status = 'ACTIVE';

	console.log("addImage API :- Request - %j", requestObject);
	var responseObject = new Object();
	imageService.addImage(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("addImage API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addImage;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.imageName = "";
		requestObject.imageUrl = "";
		requestObject.imageSize = "";
		requestObject.imageType = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		addImage(request, response);
	})();
}