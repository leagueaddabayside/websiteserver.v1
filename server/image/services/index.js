var addImageService = require("./add_image");
var findImageByPropertyService = require("./find_image_by_property");
var findImageListService = require("./find_image_list");
var updateImageService = require("./update_image");
var removeImageService = require("./remove_image");

// Require

module.exports = {
	addImage : addImageService,
	findImageByProperty : findImageByPropertyService,
	findImageList : findImageListService,
	updateImage : updateImageService,
	removeImage : removeImageService
// Export
};