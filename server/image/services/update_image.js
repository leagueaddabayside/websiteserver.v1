var imageMaster = require("../models/image_master");
var logger = require("../../utils/logger").websiteLogs;
var responseCode = require("../../utils/response_code");

function updateImage(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.imageName)
		updateObject.imageName = requestObject.imageName;
	if(requestObject.imageUrl)
		updateObject.imageUrl = requestObject.imageUrl;
	if(requestObject.imageSize)
		updateObject.imageSize = requestObject.imageSize;
	if(requestObject.imageType)
		updateObject.imageType = requestObject.imageType;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {imageId: requestObject.imageId};
	imageMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateImage;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.imageName = "";
	requestObject.imageUrl = "";
	requestObject.imageSize = "";
	requestObject.imageType = "";
	requestObject.status = "";
	console.log(requestObject);

	updateImage(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}