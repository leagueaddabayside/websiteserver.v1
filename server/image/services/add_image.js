var imageMaster = require("../models/image_master");
var logger = require("../../utils/logger").websiteLogs;
var responseCode = require("../../utils/response_code");

function addImage(requestObject, callback) {
	var newImage = new imageMaster({
		imageName : requestObject.imageName,
		imageUrl : requestObject.imageUrl,
		imageSize : requestObject.imageSize,
		imageType : requestObject.imageType,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newImage.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addImage;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.imageName = "";
	requestObject.imageUrl = "";
	requestObject.imageSize = "";
	requestObject.imageType = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addImage(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}