var imageMaster = require("../models/image_master");
var logger = require("../../utils/logger").websiteLogs;
var responseCode = require("../../utils/response_code");
var fs = require('fs');
var path = require('path');
var config=require('../../../bin/config')

function removeImage(requestObject, callback) {

	var responseObject = new Object();
	var query = {imageId: requestObject.imageId};

	imageMaster.findOneAndRemove(query, function(error,imageObj){
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		console.log(imageObj);

		path.join(config.uploadImagePath);
		if(imageObj){
			fs.unlink(path.join(config.uploadImagePath,imageObj.imageUrl),function(err){
				console.log(err);
			});
		}

		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});


}

module.exports = removeImage;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.imageId = 12;
			console.log(requestObject);
	console.log(path.join(config.uploadImagePath,'abc.jpg'));


	removeImage(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}