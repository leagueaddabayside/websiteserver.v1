var imageMaster = require("../models/image_master");
var logger = require("../../utils/logger").websiteLogs;
var responseCode = require("../../utils/response_code");

function findImageByProperty(requestObject, callback) {
	var query = imageMaster.findOne({});
	if (typeof requestObject.imageId !== "undefined" && requestObject.imageId !== null)
		query.where("imageId").equals(requestObject.imageId);
	if (typeof requestObject.imageName !== "undefined" && requestObject.imageName !== null)
		query.where("imageName").equals(requestObject.imageName);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = findImageByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.imageId = "imageId";
	//requestObject.imageName = "imageName";

	findImageByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}