var logger = require("../utils/logger").websiteLogs;

var config = require("../../bin/config")
var responseMessage = require("../utils/response_message");
var multer  = require('multer');
var mkdirp  = require('mkdirp');

var path = require('path');
var fs = require('fs');

var uploadPath = path.join(config.uploadImagePath);
mkdirp.sync(uploadPath)
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null,uploadPath)
	},
	filename: function (req, file, cb) {
		console.log(file.originalname);
		cb(null, file.originalname)
	}
})


var upload = multer({ storage: storage }).single('file');


function uploadExcel(request, response, next) {
	upload(request, response, function (err) {
		if (err) {
			// An error occurred when uploading
			console.log(err);
			response.json({responseCode : 112});
			return
		}

		next();
		// Everything went fine
	});


}

module.exports = uploadExcel;