require("../../utils/mongo_connection");
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var ImageMasterSchema = new mongoose.Schema({
	imageId: {type: Number, unique: true},
	imageName: {type: String, unique: true},
	imageUrl: {type: String},
	imageSize: {type: Number},
	imageType: {type: String, enum: ['CMS', 'BLOG']},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("ImageMaster", ImageMasterSchema, "image_master");

ImageMasterSchema.plugin(autoIncrement.plugin, {
	model: "ImageMaster",
	field: "imageId",
	startAt: 1,
	incrementBy: 1
});