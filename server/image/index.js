'use strict';

let express = require('express');
let route = express.Router();
var obj = require('./controllers/index');
var uploadImage = require('./upload_image');

route.post("/uploadImage", uploadImage, obj.addImage);

route.post("/addImage", obj.addImage);
route.post("/updateImage", obj.updateImage);
route.post("/findImageByProperty", obj.findImageByProperty);
route.post("/findImageList", obj.findImageList);
route.post("/removeImage", obj.removeImage);

module.exports = route;