/**
 * Created by adityagupta on 10/2/17.
 */
const userWallet = require('../models/userWalletMaster');
const responseCode = require('../../utils/response_code');
const logger = require('../../utils/logger').websiteLogs;
const tokenizer = require('../../redis/redis');
const constant = require("../../utils/constant");

var moment = require('moment');

let UserWalletClass = function () {

}
UserWalletClass.prototype.getByUserid = function (userId) {
    let responseData = {};
    return new Promise((resolve, reject)=> {
        userWallet.findOne({raw: true, where: {userId: userId}})
            .then(function (userWallet) {
                responseData.userId = userWallet.userId;
                responseData.depositAmt = userWallet.depositAmt;
                responseData.bonusAmt = userWallet.bonusAmt;
                responseData.winningAmt = userWallet.winningAmt;
                responseData.totalWinning = userWallet.totalWinning;
                responseData.leaguesWin = userWallet.leaguesWin;
                resolve(responseData);
            })
            .catch(function (err) {
                logger.error(err);
                reject(responseCode.SOME_INTERNAL_ERROR);
            })
    })
}

UserWalletClass.prototype.makeUserWalletValidateData = function (params) {
    return new Promise((resolve, reject)=> {
        userWallet.findOne({where: {userId: params.userId}})
            .then(function (userWallet) {
                if (!userWallet) return reject(responseCode.USER_NOT_EXIST);
                if (!params.userId) return reject(responseCode.USER_NOT_EXIST);
                let regex=/([0-9]+)[.][\d]+|[\d]+/;
                params.depositAmt = userWallet.depositAmt;
                params.bonusAmt = userWallet.bonusAmt;
                params.winningAmt = userWallet.winningAmt;
                params.version = userWallet.version;

                if(params.txnType==='WIN_L'){
                    let tdsAmt = 0;
                    params.amount = parseFloat(params.amount);
                    if(params.winningType == 'WINNING'){

                        let winningAmt = params.amount;
                        let amountForTds = winningAmt - params.entryAmount;
                        if (amountForTds > 10000) {
                            tdsAmt = amountForTds  * constant.tdsPercentage * .01;
                        }
                        let remainingWinningAmt = winningAmt - tdsAmt;
                        params.updateWinning=remainingWinningAmt;
                        params.updateBonus=0;
                        params.updateDeposit=0;
                        params.amount = remainingWinningAmt;
                    }
                    params.tdsAmount = tdsAmt;
                    params.totalWinning=userWallet.totalWinning;
                    params.leaguesWin=userWallet.leaguesWin;
                }
                if (params.txnType && params.txnType == 'CREDIT') {
                    if (typeof params.toDeposit !== "undefined" && params.toDeposit !== null) {
                        params.updateDeposit = params.toDeposit;
                        if(!regex.test(params.toDeposit)) return reject(responseCode.INVALID_AMOUNT);
                    }
                    if (typeof params.toWinning !== "undefined" && params.toWinning !== null) {
                        params.updateWinning = params.toWinning;
                        if(!regex.test(params.toWinning)) return reject(responseCode.INVALID_AMOUNT);
                    }
                    if (typeof params.toBonus !== "undefined" && params.toBonus !== null) {
                        params.updateBonus = params.toBonus;
                        if(!regex.test(params.toBonus)) return reject(responseCode.INVALID_AMOUNT);
                    }
                }
                if (params.txnType && params.txnType == 'DEBIT') {
                    if (typeof params.fromDeposit !== "undefined" && params.fromDeposit !== null) {
                        if (params.depositAmt < params.fromDeposit) return reject(responseCode.DEBIT_INVALID_AMOUNT_ERROR);
                        if(!regex.test(params.fromDeposit)) return reject(responseCode.INVALID_AMOUNT);
                        params.updateDeposit = params.fromDeposit;

                    }
                    if (typeof params.fromWinning !== "undefined" && params.fromWinning !== null) {
                        if (params.winningAmt < params.fromWinning) return reject(responseCode.DEBIT_INVALID_AMOUNT_ERROR);
                        if(!regex.test(params.fromWinning)) return reject(responseCode.INVALID_AMOUNT);
                        params.updateWinning = params.fromWinning;
                    }
                    if (typeof params.fromBonus !== "undefined" && params.fromBonus !== null) {
                        if (params.bonusAmt < params.fromBonus) return reject(responseCode.DEBIT_INVALID_AMOUNT_ERROR);
                        if(!regex.test(params.fromBonus)) return reject(responseCode.INVALID_AMOUNT);
                        params.updateBonus = params.fromBonus;
                    }
                }
                if (params.txnType && params.txnType == 'DEPOSIT') {
                    params.updateDeposit = params.amount;
                    params.lasDepositDate=userWallet.lastDepositDate;
                    params.refTxnId=params.initTxnId;
                }
                if (params.txnType && params.txnType == 'WITHDRAWL') {
                        if (params.winningAmt < params.amount) return reject(responseCode.WITHDRAWAL_INVALID_AMOUNT_ERROR);

                    params.updateWinning = params.amount;
                }
                if (params.txnType && params.txnType == 'WITHDRAWL_C') {
                    // if (params.winningAmt < params.amount) return reject(responseCode.WITHDRAWAL_INVALID_AMOUNT_ERROR);
                    params.updateWinning = params.amount;
                }
                if (params.txnType && params.txnType == 'BONUS') {
                    params.updateBonus = params.amount;
                }
                if (params.txnType && params.txnType == 'JOIN_L') {
                    let maxBonusUsed = parseFloat(params.maxBonusUsed);
                    let cashBonus = params.bonusAmt;
                    if (params.bonusAmt > maxBonusUsed) {
                        cashBonus = maxBonusUsed;
                    }
                    let validBalance = cashBonus + params.depositAmt + params.winningAmt;
                    let leagueAmount = params.amount;
                    if (validBalance >= leagueAmount) {
                        if (params.depositAmt < leagueAmount) {
                            params.updateDeposit = params.depositAmt;
                            let remainingAmt = leagueAmount - params.depositAmt;
                            if (cashBonus < remainingAmt) {
                                params.updateBonus = cashBonus;
                                params.updateWinning = remainingAmt - cashBonus;
                            } else {
                                params.updateBonus = remainingAmt;
                                params.updateWinning = 0;
                            }

                        } else {
                            params.updateDeposit = leagueAmount;
                            params.updateWinning = 0;
                            params.updateBonus = 0;
                        }

                    } else {
                        return reject(responseCode.USER_INSUFFICIENT_BALANCE);
                    }
                }
                resolve(params);
            })
            .catch(function (err) {
                logger.error(err);
                reject(responseCode.USER_NOT_EXIST);
            })
    })
}

UserWalletClass.prototype.update = function (params, where, t) {
    return new Promise((resolve, reject)=> {
        userWallet.update(params, {where: where, transaction: t})
            .then(function (user) {
                if (user && user[0] == 1) {
                    let responseData = {};
                    if (typeof params.depositAmt !== "undefined" && params.depositAmt !== null) {
                        responseData.depositAmt = params.depositAmt;
                    }
                    if (typeof params.winningAmt !== "undefined" && params.winningAmt !== null) {
                        responseData.winningAmt = params.winningAmt;
                    }
                    if (typeof params.bonusAmt !== "undefined" && params.bonusAmt !== null) {
                        responseData.bonusAmt = params.bonusAmt;
                    }

                    responseData.userId = where.userId;
                    tokenizer.updateUser(responseData);
                    return resolve(user);
                }
                reject(responseCode.USER_WALLET_NOT_UPDATE_ERRROR);
            }).catch(reject);
    })
}


module.exports = new UserWalletClass();
/*************************************/
if (require.main == module) {
    (function () {
        var req = {
            body: {
                userId: 11,
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        let walletObj = new UserWalletClass();
        walletObj.getByUserid(11)
    })()
}
