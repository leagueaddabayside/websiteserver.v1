/**
 * Created by adityagupta on 16/2/17.
 */
const depositInit=require('../models/gnDepositInit');
const request=require('request');
const responseCode = require("../../utils/response_code.js");
const paymentConf=require('../../../bin/paymentConfig');
const config=require('../../../bin/config');
const crypto=require('crypto');
const User=require('../../users/services/UserMaster');
const format = require("string-template");
const logger =require('../../utils/logger').websiteLogs;

function DepositInitClass() {
    
}

DepositInitClass.prototype.create=function (params) {
    return new Promise((resolve,reject)=>{
        depositInit.create(params)
            .then((txn)=>{
                params.txnInitId=txn.id;
                resolve(JSON.stringify(params));
            })
            .catch(reject)
    })
}
DepositInitClass.prototype.createTxnPayU=function (params) {
    logger.info('params at create txn',params);
    return new Promise((resolve,reject)=>{
        depositInit.create(params)
            .then((txn)=>{
                if(txn && txn.id){
                     User.getUserAttributes({userId:params.userId},['emailId','mobileNbr'])
                        .then(function (user) {
                            let payuParams={
                                firstname:user['personalInfo.firstName'],
                                lastname:'',
                                surl:config.serverUrl+config.payU.rurl,
                                phone:user.mobileNbr,
                                key:config.payU.key,
                                curl:config.serverUrl+config.payU.rurl,
                                furl:config.serverUrl+config.payU.rurl,
                                txnid:txn.id,
                                productinfo:config.payU.pinfo,
                                amount:txn.amount,
                                email:user.emailId,
                                salt:config.payU.salt
                            };
                            if(!user) return reject(responseCode.USER_NOT_EXIST);
                            logger.info('|||||||||||||||||||passing txn init|||||||||||||||||');
                            resolve(payuParams);
                        })
                        .catch(function (err) {
                            logger.info(new Error(err));
                            reject(responseCode.SOME_INTERNAL_ERROR);
                        })
                }
            })

    })
}
DepositInitClass.prototype.getToken=function (params) {
    return new Promise((resolve,reject)=>{
        params=JSON.parse(params)
        // paymentConf.transaction.amount=params.amount;
        if(paymentConf.transaction.amount){
            request({
                url: 'https://www.tekprocess.co.in/mobile/paynimoV2.req', //URL to hit
                method: 'POST',
                form: JSON.stringify(paymentConf)
            }, function(error, response, body){
                if(error) return reject(error);
                body=JSON.parse(body);
                params.merchantCode=body.merchantCode;
                params.merchantTransactionIdentifier=body.merchantTransactionIdentifier;
                params.token=body.paymentMethod.token;
                logger.info('final response',params)
                resolve(params);
            });
        }
    })
}
DepositInitClass.prototype.sendToPayu=function (params) {
    return new Promise((resolve,reject)=>{
        if(!params) reject (responseCode.INVALID_REQUEST_PARAMS);
            let hash=this.validatePayUparams(params);
            logger.info('hash ',hash);
            if(!hash) return reject(responseCode.INVALID_HASH);
            params.hash=hash;
            logger.info('|||||||||||||||||||passing hash matched|||||||||||||||||');
            resolve(format(config.payU.html,params));
    })
}
DepositInitClass.prototype.validatePayUparams=function (params) {
    let validParams=params.txnid && params.productinfo
        && params.amount && params.firstname && params.email;
    if(!validParams) return false;
    let createHash=config.payU.key+'|'+params.txnid+'|'+params.amount
    createHash+='|'+params.productinfo+'|'+params.firstname;
    createHash+='|'+params.email;
    createHash+=(params.udf1)?params.udf1+'|':'|';
    createHash+=(params.udf2)?params.udf2+'|':'|';
    createHash+=(params.udf3)?params.udf3+'|':'|';
    createHash+=(params.udf4)?params.udf4+'|':'|';
    createHash+=(params.udf5)?params.udf5+'|':'|';
    createHash+='||||||'+config.payU.salt;
    const hash = crypto.createHash('sha512')
        .update(createHash)
        .digest('hex');
    if(hash) return hash;
    return false;
}
DepositInitClass.prototype.findBy=function (by) {
    return new Promise((resolve,reject)=>{
       depositInit.findOne({where:params})
           .then(resolve)
           .catch(reject);
    })
}
DepositInitClass.prototype.update=function (params,where,t) {// update deposittxn
    return new Promise((resolve,reject)=>{
        depositInit.update(params,{where:where,transaction:t})
            .then(resolve)
            .catch(reject)
    })
};

DepositInitClass.prototype.updatePayuTxn=function (params,where,t) {// update deposittxn
    let newParams=
    {   mihpayid: params.mihpayid,
        status: 'success',
        bank_ref_num: params.bank_ref_num,
        bankcode: params.bankcode,
        name_on_card:params.name_on_card ,
        cardnum: params.cardnum,
        issuing_bank: params.issuing_bank,
        card_type: params.card_type,
    }
    return new Promise((resolve,reject)=>{
        depositInit.update(newParams,{where:where,transaction:t})
            .then(resolve)
            .catch(reject)
    })
};

module.exports=new DepositInitClass();

