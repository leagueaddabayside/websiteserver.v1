/**
 * Created by adityagupta on 15/2/17.
 */
const userRealDebitModel = require('../models/userRealDebitTxn');
const userRealTxnMaster = require('../services/userRealTxnMaster');
const userWalletService = require('../services/userWallet');
const userWalletMaster = require('../models/userWalletMaster')
const sequelize = require('../../connection');
let responseCode = require("../../utils/response_code.js");


const UserRealDebitClass = function () { //prototyping class
    this.userWallet = userWalletService;
}

UserRealDebitClass.prototype.debit = function (params) {
    let that = this;// local class scope
    return new Promise((resolve, reject)=> {
        return sequelize.transaction(function (t) {// initialize transaction
            return userWalletService.makeUserWalletValidateData(params)
                .then((userDebitParams)=>userRealTxnMaster.createTxn(params, t))// create real transaction
                .then((txnId)=>that.debitTransaction(params, t))// init transaction
                .then((debitParams)=>that.userWallet.update(debitParams, {
                    userId: params.userId,
                    version: params.version
                }, t))
                .then((txnId)=>resolve(params.txnId))
                .catch(reject)

        }).then(function (result) {
            resolve(result);
        }).catch(function (err) {
            reject(err);
        });
    })
}



UserRealDebitClass.prototype.debitTransaction = function (params, t) {
    return new Promise((resolve, reject)=> {
        let modifiedParams = {
            txnId: params.txnId,
            userId: params.userId,
            fromDeposit: params.updateDeposit || 0.000,
            fromWinning: params.updateWinning || 0.000,
            fromBonus: params.updateBonus || 0.000,
            adminId: parseInt(params.adminId) || null,
            remarks: params.remarks || null,
            txnDate: params.txnDate
        };
        let checkParams=(modifiedParams.fromDeposit ||
            modifiedParams.fromWinning || modifiedParams.fromBonus)
            && modifiedParams.txnId && modifiedParams.userId && modifiedParams.adminId;
        if(!checkParams) return reject('not valid params for credit')
        userRealDebitModel.create(modifiedParams,{transaction:t})
            .then(function (result) {
                let returnParams = {version: params.version + 1}
                if (params.updateDeposit) returnParams['depositAmt'] = parseFloat(params.depositAmt) - params.updateDeposit;
                if (params.updateWinning) returnParams['winningAmt'] = parseFloat(params.winningAmt) - params.updateWinning;
                if (params.updateBonus) returnParams['bonusAmt'] = parseFloat(params.bonusAmt) - params.updateBonus;
                resolve(returnParams);
            })
            .catch(reject)
    })
}

module.exports = new UserRealDebitClass();
