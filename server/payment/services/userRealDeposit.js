/**
 * Created by adityagupta on 17/2/17.
 */
const realDepositModel = require('../models/userRealTxnMaster');
const config=require('../../../bin/config');
const userWalletMaster = require('../services/userWallet');
const userRealTxn = require('./userRealTxnMaster');
const userRealDeposit = require('../models/userRealDepositTxn');
const sequelize = require('../../connection');
const initTxnService = require('../services/gnDepositInit');
const responseCode = require("../../utils/response_code.js");
const Bonus=require('../services/userRealBonus');
const FIRST_DEPOSIT=config.bonus.FIRST_DEPOSIT;
const logger =require('../../utils/logger').websiteLogs;

function RealDepositClass() {
    this.sequalize = sequelize;
    this.userWalletMaster = userWalletMaster;
    this.userRealTxn = userRealTxn;
    this.userRealDeposit = userRealDeposit;
    this.initTxnService = initTxnService;
}
RealDepositClass.prototype.addDeposit = function (params) {
    let that = this;
    return that.sequalize.transaction(function (t) {
        return new Promise((resolve, reject)=> {
            that.userWalletMaster.makeUserWalletValidateData(params)
                .then(that.initTxnService.updatePayuTxn(params, {id: params.initTxnId}, t))
                .then((txnParams)=>that.userRealTxn.createTxn(params, t))
                .then((txnId)=>that.create(params, t))
                .then((returnParams)=>that.userWalletMaster.update(returnParams, {
                    userId: params.userId,
                    version: params.version
                }, t))
                .then(function (result) {
                    if(params.isFirstDeposit && params.amount>=FIRST_DEPOSIT.MIN_CAP){
                        let bonusAmt=params.amount;
                        if(params.amount>FIRST_DEPOSIT.MAX_CAP) bonusAmt=FIRST_DEPOSIT.MAX_CAP;
                        if(FIRST_DEPOSIT.status=='ACTIVE') Bonus.addBonus({
                            remarks:'Deposit Cash Bonus',
                            userId:params.userId,
                            bonusId:FIRST_DEPOSIT.bonusId,
                            bonus:bonusAmt,
                            deposit:params.amount,
                            amount:bonusAmt,
                            emailType:'FIRST_DEPOSIT_BONUS'});
                    }
                    resolve(params.txnId);
                })
                .catch(function (err) {
                    logger.error(err);
                    reject(err)
                })

        })
    })
};

RealDepositClass.prototype.create = function (params, t) {
    let that = this;
    return new Promise((resolve, reject)=> {
        let modifiedParams = {
            txnId: params.txnId,
            userId: params.userId,
            amount: params.updateDeposit || 0.000,
            remarks: params.remarks || null,
            txnDate: params.txnDate
        };
        that.userRealDeposit.create(modifiedParams, {transaction: t})
            .then((data)=> {
                params.isFirstDeposit = false;
                if(!params.lasDepositDate)  params.isFirstDeposit = true;
                let returnParams = {version: params.version + 1,lastDepositDate:params.txnDate};
                if (params.updateDeposit){
                    returnParams['depositAmt'] = parseFloat(params.depositAmt)+ +params.updateDeposit;
                    returnParams['lasDepositDate'] = new Date();
                }
                resolve(returnParams)
            })
            .catch(function (err) {
                logger.error(err);
                reject(err);
            })
    })
};

module.exports = new RealDepositClass();
