/**
 * Created by adityagupta on 17/2/17.
 */
const realDepositModel = require('../models/userRealTxnMaster');
const winningTxnModel = require('../models/userRealWinningTxn');
const cancelLeagueTxnModel = require('../models/userCancelLeagueTxn');
const gnAdminRakeDebitTxn = require('../models/gnAdminRakeDebitTxn');
const gnAdminRakeCreditTxn = require('../models/gnAdminRakeCreditTxn');

const userWalletMaster = require('../services/userWallet');
const userRealTxn = require('./userRealTxnMaster');
const userLeague = require('../models/userJoinLeagueTxn');
const winLeague = require('../models/userRealWinningTxn');
const gnUserTDS = require('../models/gnUserTDS');
const sequelize = require('../../connection');
const initTxnService = require('../services/gnDepositInit');
const responseCode = require("../../utils/response_code.js");
const constant = require("../../utils/constant");
const playerMatchDataRedis = require("../../redis/player_match_data.js");
const async = require('async');
const Promise = require('bluebird');
const Util=require('../../utils/util');
const moment = require('moment');
const logger =require('../../utils/logger').websiteLogs;

function LeagueClass() {
    this.sequalize = sequelize;
    this.userWalletMaster = userWalletMaster;
    this.userRealTxn = userRealTxn;
    this.winning = winLeague;
    this.league = userLeague;
    this.initTxnService = initTxnService;
    this.cancelLeagueTxn = cancelLeagueTxnModel;
    this.gnUserTDS = gnUserTDS;
    this.gnAdminRakeDebitTxn = gnAdminRakeDebitTxn;
    this.gnAdminRakeCreditTxn = gnAdminRakeCreditTxn;
}
LeagueClass.prototype.joinLeague = function (params) {
    var that = this;
    return that.sequalize.transaction(function (t) {
        return new Promise((resolve, reject) => {
            that.userWalletMaster.makeUserWalletValidateData(params)
                .then((txnParams)=>that.userRealTxn.createTxn(params, t))
                .then((txnId)=>that.create(params, t))
                .then((userWallet)=>that.userWalletMaster.update(userWallet, {
                    userId: params.userId,
                    version: params.version
                }, t))
                .then(function (txnId) {
                    resolve(params.txnId);
                }).catch(function (err) {
                    logger.error(err);
                    reject(err);
                })
        })
    })
}

LeagueClass.prototype.create = function (params, t) {
    var that = this;
    return new Promise((resolve, reject)=> {
        logger.info('user join league', params);

        let leagueParams = {
            txnId: params.txnId,
            userId: params.userId,
            depositAmt: params.updateDeposit || 0.000,
            winningAmt: params.updateWinning || 0.000,
            bonusAmt: params.updateBonus || 0.000,
            amount: params.amount,
            matchId: params.matchId,
            leagueId: params.leagueId,
            gameTxnId: params.refTxnId,
            remarks: params.remarks || null,
            txnDate: params.txnDate,
            status : 'JOIN_LEAGUE'

        };

        //let {depositAmt,bonusAmt,winningAmt}=that.getAmount(params);

        that.league.create(leagueParams, {transaction: t})
            .then((data)=> {
                let totalBonusUsed = params.updateBonus + parseFloat(params.bonusUsed);
                playerMatchDataRedis.set(params.userId, params.matchId, {bonusUsed: totalBonusUsed});

                let returnParams = {version: params.version + 1};
                returnParams.depositAmt = params.depositAmt - params.updateDeposit;
                returnParams.winningAmt = params.winningAmt - params.updateWinning;
                returnParams.bonusAmt = params.bonusAmt - params.updateBonus;
                resolve(returnParams)
            })
            .catch(reject)
    })
}
LeagueClass.prototype.getAmount = function (params) {
    let depositAmt = 0, bonusAmt = 0, winningAmt = 0;
    if (params.depositBeforeTxn + params.bonusBeforeTxn + params.winningBeforeTxn >= params.amount) {
        if (params.depositBeforeTxn < params.amount) {
            depositAmt = params.depositBeforeTxn;
            params.amount = params.amount - params.depositBeforeTxn;
            if (params.amount > params.bonusBeforeTxn) {

                bonusAmt = params.bonusBeforeTxn;
                winningAmt = params.amount - bonusAmt;
            } else {
                bonusAmt = params.amount;
                winningAmt = 0;
            }
        } else {
            depositAmt = params.amount;
            bonusAmt = 0;
            winningAmt = 0;
        }
    } else {
        return {depositAmt, bonusAmt, winningAmt};
    }
    return {depositAmt, bonusAmt, winningAmt};
}

LeagueClass.prototype.cancelLeague = function (params, cb) {
    let that = this;

    Promise.each(params.leagues,
        function (league) {
            logger.info(league);
            league.txnType = 'REFUND_L';
            return that.sequalize.transaction(function (t) {
                return new Promise((resolve, reject) => {
                    that.userWalletMaster.makeUserWalletValidateData(league)
                        .then((txnParams)=>that.userRealTxn.createTxn(txnParams, t))
                        .then((txnId)=>that.createCancelLeague(league, t))
                        .then((wallet)=>that.userWalletMaster.update(wallet, {
                            userId: league.userId,
                            version: league.version
                        }, t))
                        .then(function (result) {
                            if(result && league.matchStatus && league.matchStatus.toLowerCase()=='abandoned') {
                                let EmailData={amount:league.amount,match:league.matchName};
                                if(league.startTime) {
                                    league.startTime=+league.startTime;
                                    EmailData.time=new Date(league.startTime*1000).toDateString();
                                }
                                Util.sendEmailOnEvent(league.userId,'LEAGUE_ABANDONED',EmailData);
                            }else{
                                if(result) Util.sendEmailOnEvent(league.userId,'LEAGUE_C',{amount:league.amount});
                            }
                            resolve(result);
                        }).catch(function (err) {
                            logger.error(err);
                            reject(err);
                        });
                })
            })

        }).then(function (winResp) {
            logger.info('All tasks are done now...', winResp);
            logger.info('update league refund final done');
            cb(null, winResp);
        }).catch(function (err) {
            logger.error('Got an error', err)
            logger.error('update league refund final done');
            cb(err);
        })


};


LeagueClass.prototype.createCancelLeague = function (params, t) {
    let that = this;
    return new Promise((resolve, reject)=> {
        that.league.findOne({where: {gameTxnId: params.joinTxnId}})
            .then(function (joinLeagueObj) {
                if (!joinLeagueObj)return reject(responseCode.JOIN_LEAGUE_TXN_NOT_EXIST);
                if(joinLeagueObj.status === 'JOIN_LEAGUE'){
                    params.updateBonus = joinLeagueObj.bonusAmt;
                    params.updateWinning = joinLeagueObj.winningAmt;
                    params.updateDeposit = joinLeagueObj.depositAmt;
                    joinLeagueObj.cancelId = params.txnId;
                    joinLeagueObj.cancelTime = params.txnDate;
                    joinLeagueObj.cancelRemarks = params.remarks;
                    joinLeagueObj.status = 'CANCEL_LEAGUE';

                    return joinLeagueObj.save().then(function (result) {
                        if (!result) return reject(responseCode.JOIN_LEAGUE_TXN_NOT_EXIST);
                        //TODO update redis
                        let returnParams = {version: params.version + 1};
                        returnParams.depositAmt = +params.updateDeposit + params.depositAmt;
                        returnParams.winningAmt = +params.updateWinning + params.winningAmt;
                        returnParams.bonusAmt = +params.updateBonus + params.bonusAmt;
                        resolve(returnParams)

                    })
                }else{
                    return reject(responseCode.LEAGUE_TXN_ALREADY_CANCELLED);
                }

            })
            .catch(reject);
    })

};

LeagueClass.prototype.getLeagueViaLeagueId = function (id) {
    let that = this;
    return new Promise((resolve, reject)=> {
        let attr = ['txnId', 'userId', 'txnDate', 'amount', 'depositAmt', 'winningAmt', 'bonusAmt', 'leagueId', 'matchId']
        userLeague.findAll({where: {leagueId: id}, attributes: attr, raw: true})
            .then(function (leagues) {
                if (leagues.length > 0) return resolve(leagues)
                reject('No leagues Found');
            }).catch(reject)
    })
};
LeagueClass.prototype.updateStatus = function (params, cancelTxnId, t) {
    let that = this;
    return new Promise((resolve, reject)=> {
        let cancelParams = {cancelId: cancelTxnId, cancelTime: new Date().getTime() / 1000}
        cancelParams.cancelRemarks = params.cancelRemarks || null;
        that.userRealWithdrawl.update(cancelParams, {
            where: {userId: params.userId, txnId: params.txnId},
            transaction: t
        })
            .then(function (transaction) {
                params.txnId = cancelTxnId;
                let returnParams = {winningAmt: params.winningAmt}
                if (params.version) returnParams['version'] = params.version + 1;
                resolve(returnParams);
            }).catch(reject)
    })
};

LeagueClass.prototype.winningLeague = function (params, cb) {
    let that = this;
    Promise.each(params.leagues,
        function (league) {
            logger.info(league);
            league.txnType = 'WIN_L';
            return that.sequalize.transaction(function (t) {
                return new Promise((resolve, reject) => {
                    that.getJoinTxnInfo(league,t)
                        .then((txnParams)=>that.userWalletMaster.makeUserWalletValidateData(league))
                        .then((txnParams)=>that.userRealTxn.createTxn(league, t))
                        .then((txnId)=>that.createWinningTxn(league, t))
                        .then((wallet)=>that.userWalletMaster.update(wallet, {
                            userId: league.userId,
                            version: league.version
                        }, t))
                        .then(function (result) {
                            if(result) {
                                let EmailData={amount:league.amount,match:league.matchName};
                                if(league.startTime) {
                                    league.startTime=+league.startTime;
                                    EmailData.time=new Date(league.startTime*1000).toDateString();
                                }
                                Util.sendEmailOnEvent(league.userId,'WIN_LEAGUE',EmailData);
                            }
                            resolve(result);
                        }).catch(function (err) {
                            logger.error(err);
                            reject(err);
                        });
                })
            })

        }).then(function (winResp) {
            logger.info('All tasks are done now...', winResp);
            logger.info('update league winnings final done');
            that.createAdminRakeTxn(params.rakeData)
            .then(function(){
                    cb(null, winResp);
                })
                .catch(function (err) {
                    logger.error('Got an error', err)
                    logger.error('update league winnings final done');
                    cb(null);
                })

        }).catch(function (err) {
            logger.error('Got an error', err)
            logger.error('update league winnings final done');
            cb(null);
        })


};
LeagueClass.prototype.createWinningTxn = function (league, t) {
    logger.info('createWinningTxn',league);
    let that = this;
    return new Promise((resolve, reject)=> {
        let winLeague = {
            txnId: league.txnId, userId: league.userId, txnDate: league.txnDate,
            gameTxnId: league.refTxnId, teamId: league.teamId,matchId : league.matchId, remarks: league.remarks
        };
        winLeague.amount = league.amount;
        winLeague.winningAmt = league.updateWinning;
        winLeague.bonusAmt = league.updateBonus;
        winLeague.depositAmt = league.updateDeposit;
        winLeague.entryAmt = league.entryAmount;
        winLeague.leagueId = league.leagueId;

        that.createTDSTxn(league, t)
            .then(function () {
                return that.winning.create(winLeague, {transaction: t})
                    .then(function (createdLeague) {
                        let returnParams = {version: league.version + 1};
                        returnParams.winningAmt = league.updateWinning + league.winningAmt;
                        returnParams.bonusAmt = league.updateBonus + league.bonusAmt;
                        returnParams.depositAmt = league.updateDeposit + league.depositAmt;
                        returnParams.totalWinning = league.amount + league.totalWinning;
                        returnParams.leaguesWin = league.leaguesWin + 1;
                        resolve(returnParams)
                    }).catch(function (err) {
                        reject(err);
                    })
            });

    })
};

LeagueClass.prototype.createTDSTxn = function (params, t) {
    let that = this;
    return new Promise((resolve, reject)=> {

        if(params.tdsAmount <= 0){
           return resolve({});
        }


        let tdsTxn = {
            userId: params.userId, adminId: 5, date: new Date(),
            winningTxnId: params.txnId, tdsAmount: params.tdsAmount, winningAmount: params.updateWinning
        };

        that.gnUserTDS.create(tdsTxn, {transaction: t})
            .then(function (tdsResp) {
                resolve({});
            }).catch(function (err) {
                reject(err);
            })
    })
};

LeagueClass.prototype.createAdminRakeTxn = function (params) {
    let that = this;
    return that.sequalize.transaction(function (t) {
        return new Promise((resolve, reject) => {
            let currentDate = moment(new Date()).unix();
            let adminRakeTxn = {
                txnDate: currentDate,
                tourId: params.tourId,
                matchId: params.matchId,
                leagueId: params.leagueId,
                configId: params.configId,
                rakeAmt: params.rakeAmt,
                winningAmt: params.winningAmt,
                leagueAmt: params.leagueAmt
            };

            if(params.rakeAmt > 0){
                adminRakeTxn.adminId = 6;
                that.gnAdminRakeCreditTxn.create(adminRakeTxn, {transaction: t})
                    .then(function (tdsResp) {
                        resolve({});
                    }).catch(function (err) {
                        reject(err);
                    })
            }else{
                adminRakeTxn.adminId = 7;
                that.gnAdminRakeDebitTxn.create(adminRakeTxn, {transaction: t})
                    .then(function (tdsResp) {
                        resolve({});
                    }).catch(function (err) {
                        reject(err);
                    })
            }

        })
    })
};

LeagueClass.prototype.getJoinTxnInfo = function (params, t) {
    let that = this;

    return new Promise((resolve, reject)=> {

        if(params.winningType == 'WINNING_REFUND'){
            that.league.findOne({where: {gameTxnId: params.joinTxnId}})
                .then(function (joinLeagueObj) {
                    if (!joinLeagueObj)return reject(responseCode.JOIN_LEAGUE_TXN_NOT_EXIST);
                    if(joinLeagueObj.status === 'JOIN_LEAGUE'){
                        params.updateBonus = joinLeagueObj.bonusAmt;
                        params.updateWinning = joinLeagueObj.winningAmt;
                        params.updateDeposit = joinLeagueObj.depositAmt;
                        resolve(params);

                    }else{
                        return reject(responseCode.LEAGUE_TXN_ALREADY_CANCELLED);
                    }

                })
                .catch(reject);
        }else{
            resolve(params)
        }
    })

};

module.exports = new LeagueClass();
