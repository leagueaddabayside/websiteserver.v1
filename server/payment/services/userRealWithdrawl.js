/**
 * Created by AdityaGupta on 17/2/17.
 */
const realDepositModel=require('../models/userRealTxnMaster');
const userWallet=require('../models/userWalletMaster');
const User=require('../../users/models/gnUserMaster');
const UserMaster=require('../../users/services/UserMaster');
const UserPanCard=require('../../users/models/gnUserPanCard');
const UserBankDetails=require('../../users/models/gnUserBankDetails');
const Info=require('../../users/models/gnUserInfo');
const userWalletMaster=require('../services/userWallet');
const userRealTxn=require('./userRealTxnMaster');
const userRealWithdrawl=require('../models/userRealWithdrawlTxn');
const sequelize=require('../../connection');
const initTxnService=require('../services/gnDepositInit');
const responseCode = require("../../utils/response_code.js");
const config=require('../../../bin/config');
const logger =require('../../utils/logger').websiteLogs;

function RealWithdrawlClass() {
    this.sequalize=sequelize;
    this.userWalletMaster=userWalletMaster;
    this.userRealTxn=userRealTxn;
    this.userRealWithdrawl=userRealWithdrawl;
    this.initTxnService=initTxnService;
    this.bankDetails=UserBankDetails;
}

RealWithdrawlClass.prototype.withdrawl=function (params) {
    var that = this;
    return that.sequalize.transaction(function (t) {
        return new Promise((resolve, reject) => {
                 that.userWalletMaster.makeUserWalletValidateData(params)
                .then((txnParams)=>that.userRealTxn.createTxn(params,t))
                .then((txnId)=>that.create(params,t))
                .then((userWallet)=>that.userWalletMaster.update(userWallet,{userId:params.userId,version:params.version},t))
                 .then(function (txnId) {
                    resolve(params.txnId);
                }).catch(function (err) {
                     logger.error(err);
                     reject(responseCode.SOME_INTERNAL_ERROR);
            })
        })
    })
}

RealWithdrawlClass.prototype.create=function (params,t) {
    var that=this;
    return new Promise((resolve,reject)=>{
        let modifiedParams = {
            txnId: params.txnId,
            userId: params.userId,
            fromDeposit: params.updateDeposit || 0.000,
            fromWinning: params.updateWinning || 0.000,
            fromBonus: params.updateBonus || 0.000,
            remarks: params.remarks || null,
            txnDate: params.txnDate,
            status : 'pending'
        };

        that.userRealWithdrawl.create(modifiedParams,{transaction:t})
            .then((data)=>{
                let returnParams = {version: params.version + 1}
                if (params.updateWinning) returnParams['winningAmt'] = parseFloat(params.winningAmt) - params.updateWinning;
                resolve(returnParams)

            })
            .catch(function (err) {
                logger.error(err);
                reject(responseCode.SOME_INTERNAL_ERROR);
            })
    })
};

RealWithdrawlClass.prototype.withdrawlCancel=function (params) {
    let that=this;
    return that.sequalize.transaction(function (t) {
        return new Promise((resolve, reject) => {
            params.txnType='WITHDRAWL_C';
            that.userWalletMaster.makeUserWalletValidateData(params)
                .then((txnParams)=>that.userRealTxn.createTxn(params,t))
                .then((txnId)=>that.updateCancelStatus(params,t))
                .then((userWallet)=>that.userWalletMaster.update(userWallet,{userId:params.userId,version:params.version},t))
                .then(function (txnId) {
                    resolve(params.txnId);
                }).catch(function (err) {
                logger.error(err);
                reject(responseCode.SOME_INTERNAL_ERROR);
            })
        })
    })
};

RealWithdrawlClass.prototype.updateCancelStatus=function (params,t) {
    let that =this;
    return new Promise((resolve,reject)=>{
        let cancelParams={cancelId:params.txnId,cancelTime:params.txnDate};
        cancelParams.cancelRemarks=params.cancelRemarks || null;
        cancelParams.status=params.status || null;
        that.userRealWithdrawl.update(cancelParams,{where:{userId:params.userId,txnId:params.cancelTxnId},transaction:t})
            .then(function (transaction) {
                let returnParams={winningAmt:params.winningAmt+(+params.amount)};
                logger.error(returnParams,'returnParams');
                if(params.version) returnParams['version']=params.version+1;
                resolve(returnParams);
            }).catch(function (err) {
            logger.error(err);
            reject(responseCode.SOME_INTERNAL_ERROR);
        })
    })
};
RealWithdrawlClass.prototype.withdrawlHistory=function () {
    let that=this;
    return new Promise((resolve,reject)=>{
        that.userRealWithdrawl.findAll({where:{status:'PENDING'},
            include:[{model:userWallet,as:'wallet',attributes:['depositAmt','bonusAmt','winningAmt']},
            {model:User,as:'user',attributes:['screenName','emailId','userId','isPanCardVerified']},
                {model:UserPanCard,as:'panCard',attributes:['panNumber']}]
            ,attributes:['txnId','fromWinning','status','createdAt','txnDate']})
            .then(function (withdrawlTxn) {
                resolve(withdrawlTxn);
            }).catch(function (err) {
            logger.error(err);
            reject(responseCode.SOME_INTERNAL_ERROR);
        })
    })
};
RealWithdrawlClass.prototype.crmApproveHistory=function () {
    let that=this;
    return new Promise((resolve,reject)=>{
        that.userRealWithdrawl.findAll({where:{status:'CRM_A'},
            include:[{model:userWallet,as:'wallet',attributes:['depositAmt','bonusAmt','winningAmt']},
                {model:User,as:'user',attributes:['screenName','emailId','userId']},
                {model:UserPanCard,as:'panCard',attributes:['panNumber','name']},
                {model:UserBankDetails,as:'bank',attributes:['bankName','ifscCode','accountNumber','branch']}]
            ,attributes:['txnId','fromWinning','status','createdAt','txnDate']})
            .then(function (withdrawlTxn) {
                if(withdrawlTxn) return resolve(withdrawlTxn);
                reject(withdrawlTxn);
            }).catch(function (err) {
            logger.error(err);
            reject(responseCode.SOME_INTERNAL_ERROR);
        })
    })
};
RealWithdrawlClass.prototype.updateStatus=function (params) {
    let that=this;
    return new Promise((resolve,reject)=>{
        if(!['CRM_A','CRM_C','ACC_A','ACC_C'].includes(params.status)) return reject(responseCode.INVALID_REQUEST_PARAMS);
        if(!params.userId || !params.txnId || !params.status) return reject(responseCode.INVALID_REQUEST_PARAMS);
        let updateParams={status:params.status};
        that.userRealWithdrawl.findOne({where:{userId:params.userId,txnId:params.txnId}})
            .then(function (transaction) {
                if(!transaction) return reject(responseCode.NOT_FOUND);
                let beforeUpdateStatus=transaction.status;
                if(params.status=='ACC_A' || params.status=='CRM_A'){
                    transaction.status=params.status;
                    if(params.remarks) transaction.remarks=params.remarks;
                    // if(params.remarks) transaction.cancelRemarks=params.remarks;
                    transaction.save();
                }
                    // .then(function (newTxn) {
                        if(['ACC_C','CRM_C'].indexOf(beforeUpdateStatus)===-1) {
                            if (params.status == 'ACC_C' || params.status == 'CRM_C') {
                                let wcp = {
                                    cancelTxnId: transaction.txnId,
                                    userId: transaction.userId,
                                    cancelRemarks: params.remarks,
                                    status: params.status,
                                    amount: transaction.fromWinning
                                };
                                that.withdrawlCancel(wcp)
                            }
                        }
                        // if(transaction.status=='ACC_A' || transaction.status=='CRM_C' || transaction.status=='ACC_C')  {
                            return UserMaster.getUserPanBankDetails({userId:params.userId},['emailId'],transaction.fromWinning);
                        // }
                    // })
            }).then(function (userInfo) {
                if(!userInfo) return reject(responseCode.NOT_FOUND);
                resolve(userInfo);
        }).catch(function (err) {
            logger.error(err);
            reject(responseCode.SOME_INTERNAL_ERROR);
        })
    })
};
RealWithdrawlClass.prototype.findById= function (id) {
    let that=this;
    return new Promise((resolve, reject)=> {
        that.userRealWithdrawl.findOne({raw: true, where: {txnId: id}})
            .then(function (txn) {
                if (!txn) reject('no txn found');
                resolve(txn)
            }).catch(reject);
    })
};
module.exports=new RealWithdrawlClass();
