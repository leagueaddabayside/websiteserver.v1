/**
 * Created by adityagupta on 15/2/17.
 */
const userRealCreditModel=require('../models/userRealCreditTxn');
const userRealTxnMaster=require('../services/userRealTxnMaster');
const userWalletMaster=require('../services/userWallet');
const sequelize=require('../../connection');
let responseCode = require("../../utils/response_code.js");


const UserRealCreditClass=function() { //prototyping class
    this.sequalize=sequelize;
    this.userWallet=userWalletMaster;
    this.userRealTxn=userRealTxnMaster;
}

UserRealCreditClass.prototype.addCredit=function (params) {
    let that = this;// local class scope
        return that.sequalize.transaction(function (t) {// initialize transaction
            return new Promise((resolve,reject)=> {
                 that.userWallet.makeUserWalletValidateData(params)
                     .then((data)=>that.userRealTxn.createTxn(params, t))// create real transaction
                     .then((txnId)=>that.create(params,t))// init transaction
                     .then((userWallet)=>that.userWallet.update(userWallet,{userId:params.userId,version:params.version},t))
                     .then((txnId)=>resolve(params.txnId))
                     .catch(reject)
        })
    })
}

UserRealCreditClass.prototype.create=function (params,t) {//create credit type transaction
    return new Promise((resolve,reject)=>{
        let modifiedParams={
            txnId:params.txnId,
            userId:params.userId,
            toDeposit:params.updateDeposit || 0.000,
            toWinning:params.updateWinning || 0.000,
            toBonus:params.updateBonus || 0.000,
            adminId:parseInt(params.adminId) || null,
            remarks:params.remarks || null,
            txnDate:params.txnDate
        }

        userRealCreditModel.create(modifiedParams,{transaction:t})
            .then(function (result) {
                let returnParams={version:params.version+1}
                if(params.updateDeposit) returnParams['depositAmt']= parseFloat(params.depositAmt)+params.updateDeposit;
                if(params.updateWinning) returnParams['winningAmt']= parseFloat(params.winningAmt)+params.updateWinning;
                if(params.updateBonus) returnParams['bonusAmt']=  parseFloat(params.bonusAmt)+params.updateBonus;
                resolve(returnParams);
            })
            .catch(reject)
    })
}

module.exports=new UserRealCreditClass();
