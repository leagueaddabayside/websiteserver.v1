/**
 * Created by adityagupta on 17/2/17.
 */
const realDepositModel = require('../models/userRealTxnMaster');
const userWalletMaster = require('../services/userWallet');
const userRealTxn = require('./userRealTxnMaster');
const userRealBonus = require('../models/userRealBonusTxn');
const sequelize = require('../../connection');
const responseCode = require("../../utils/response_code.js");
const UserMaster=require('../../users/services/UserMaster');
const Emailer=require('../../utils/emailers');
const logger =require('../../utils/logger').websiteLogs;

function RealBonusClass() {
    this.sequalize = sequelize;
    this.userWalletMaster = userWalletMaster;
    this.userRealTxn = userRealTxn;
    this.userRealBonus = userRealBonus;
}
RealBonusClass.prototype.addBonus = function (params) {
    const that = this;
    params.txnType = 'BONUS';
    logger.info(params,'>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
    return that.sequalize.transaction(function (t) {
        return new Promise((resolve, reject)=> {
            that.userWalletMaster.makeUserWalletValidateData(params)
                .then((txnParams)=>that.userRealTxn.createTxn(params, t))
                .then((txnId)=>that.create(params, t))
                .then((returnParams)=>that.userWalletMaster.update(returnParams, {
                    userId: params.userId,
                    version: params.version
                }, t))
                .then(function (result) {
                    if(result && params.emailType) Emailer.sendBonusEmail(params);
                    resolve(params.txnId);
                })
                .catch(function (err) {
                    logger.error(err)
                    reject(err)
                })
        })
    })
}

RealBonusClass.prototype.create = function (params, t) {
    var that = this;
    logger.info(params,'?????????????????????????????????????????')
    return new Promise((resolve, reject)=> {

        let bonusParams = {
            txnId: params.txnId,
            userId: params.userId,
            bonusId: params.bonusId,
            amount: params.updateBonus || 0.000,
            remarks: params.remarks || null,
            txnDate: params.txnDate
        };

        that.userRealBonus.create(bonusParams, {transaction: t})
            .then((data)=> {
                if(!params.bonusAmt) params.bonusAmt=0.00;
                let bonusAmt = +parseFloat(params.bonusAmt) + parseFloat(params.updateBonus);
                let returnParams = {bonusAmt: bonusAmt, version: params.version + 1}
                resolve(returnParams)
            })
            .catch(reject)
    })
}
RealBonusClass.prototype.sendBonusEmail=function (params) {

}


module.exports = new RealBonusClass();
