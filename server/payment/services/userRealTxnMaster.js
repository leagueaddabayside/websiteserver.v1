/**
 * Created by adityagupta on 15/2/17.
 */
const userRealTxnMaster = require('../models/userRealTxnMaster');
const responseCode = require("../../utils/response_code.js");
const moment = require('moment');
const logger =require('../../utils/logger').websiteLogs;

const UserRealTxnMasterClass = function () {
}

UserRealTxnMasterClass.prototype.createTxn = function (params, t) {
    let currentDate = moment(new Date()).unix();
    let userCreditParams = {
        userId: params.userId,
        amount: params.amount,
        remarks: params.remarks,
        depositBeforeTxn: params.depositAmt,
        bonusBeforeTxn: params.bonusAmt,
        winningBeforeTxn: params.winningAmt,
        txnType: params.txnType,
        gameTxnId: params.refTxnId || null,
        txnDate: currentDate
    }
    params.txnDate = currentDate;
    return new Promise((resolve, reject)=> {
        userRealTxnMaster.create(userCreditParams, {transaction: t})
            .then(function (transaction) {
                if (!transaction.txnId) return reject(responseCode.ERROR_IN_TXN_CREATION);
                logger.error(params.txnId+'?????????????????????????????????');
                params.txnId = transaction.txnId;
                resolve(transaction.txnId);
            }).catch(reject);
    })
}

UserRealTxnMasterClass.prototype.findByUserId = function (params) {
    return new Promise((resolve, reject)=> {
        let where={userId:params.userId};
        //params.fromDate=+Date.parse(new Date(params.fromDate))/1000;
       // params.toDate=+Date.parse(new Date(params.toDate))/1000;
        if(params.fromDate && params.toDate){
            where.txnDate={$between:[params.fromDate,params.toDate]};
        }
        let query = {
            raw: true,
            where: where,
            attributes: ['txnType', 'amount', 'createdAt', 'remarks', 'txnId', 'txnDate'],
            order: 'txnId DESC'
        };
        if(params.limit){
            query.limit = 50;
        }
        userRealTxnMaster.findAll(query)
            .then(function (txn) {
                if (!txn) reject('no txn found');
                let txnArr = [];
                for (let i = 0; i < txn.length; i++) {
                    let currentRow = txn[i];
                    let txnObj = {};


                    var displayTxnType = 'coke';
                    switch(currentRow.txnType) {

                        case 'CREDIT':
                            displayTxnType = 'Credit Account';
                            break;
                        case 'DEBIT':
                            displayTxnType = 'Debit Account';
                            break;
                        case 'DEPOSIT':
                            displayTxnType = 'Deposited Cash';
                            break;
                        case 'JOIN_L':
                            displayTxnType = 'Joined A League';
                            break;
                        case 'WITHDRAWL':
                            displayTxnType = 'Withdrew Cash';
                            break;
                        case 'WITHDRAWL_C':
                            displayTxnType = 'Withdrawal Cancelled';
                            break;
                        case 'REFUND_L':
                            displayTxnType = 'Refunded';
                            break;
                        case 'WIN_L':
                            displayTxnType = 'Won A League';
                            break;
                        case 'BONUS':
                            displayTxnType = 'Credited Cash Bonus';
                            break;

                        default:
                            displayTxnType = 'Unknown type!';
                    }

                    txnObj.txnType = displayTxnType;
                    txnObj.amount = currentRow.amount;
                    txnObj.createdAt = currentRow.createdAt;
                    txnObj.remarks = currentRow.remarks;
                    txnObj.txnId = currentRow.txnId;
                    txnObj.txnDate = currentRow.txnDate;
                    txnArr.push(txnObj);
                }

                resolve(txnArr);
            }).catch(reject);
    });
}

UserRealTxnMasterClass.prototype.findUserTransaction = function (params) {
    return new Promise((resolve, reject)=> {
        let where={userId:params.userId};
        //params.fromDate=+Date.parse(new Date(params.fromDate))/1000;
        //params.toDate=+Date.parse(new Date(params.toDate))/1000;
        if(params.fromDate && params.toDate){
            where.txnDate={$between:[params.fromDate,params.toDate]};
        }
        let query = {
            raw: true,
            where: where,
            attributes: ['txnType', 'amount', 'createdAt', 'remarks', 'txnId', 'txnDate','depositBeforeTxn','winningBeforeTxn','bonusBeforeTxn'],
            order: 'txnId DESC'
        };
        if(params.limit){
            query.limit = 50;
        }
        userRealTxnMaster.findAll(query)
            .then(function (txn) {
                if (!txn) reject('no txn found');
                let txnArr = [];
                for (let i = 0; i < txn.length; i++) {
                    let currentRow = txn[i];
                    let txnObj = {};


                    var displayTxnType = 'coke';
                    switch(currentRow.txnType) {

                        case 'CREDIT':
                            displayTxnType = 'Credit Account';
                            break;
                        case 'DEBIT':
                            displayTxnType = 'Debit Account';
                            break;
                        case 'DEPOSIT':
                            displayTxnType = 'Deposited Cash';
                            break;
                        case 'JOIN_L':
                            displayTxnType = 'Joined A League';
                            break;
                        case 'WITHDRAWL':
                            displayTxnType = 'Withdrew Cash';
                            break;
                        case 'WITHDRAWL_C':
                            displayTxnType = 'Withdrawal Cancelled';
                            break;
                        case 'REFUND_L':
                            displayTxnType = 'Refunded';
                            break;
                        case 'WIN_L':
                            displayTxnType = 'Won A League';
                            break;
                        case 'BONUS':
                            displayTxnType = 'Credited Cash Bonus';
                            break;

                        default:
                            displayTxnType = 'Unknown type!';
                    }

                    txnObj.txnType = displayTxnType;
                    txnObj.amount = currentRow.amount;
                    txnObj.createdAt = currentRow.createdAt;
                    txnObj.depositBeforeTxn = currentRow.depositBeforeTxn;
                    txnObj.winningBeforeTxn = currentRow.winningBeforeTxn;
                    txnObj.bonusBeforeTxn = currentRow.bonusBeforeTxn;
                    txnObj.remarks = currentRow.remarks;
                    txnObj.txnId = currentRow.txnId;
                    txnObj.txnDate = currentRow.txnDate;
                    txnArr.push(txnObj);
                }

                resolve(txnArr);
            }).catch(reject);
    });
}

UserRealTxnMasterClass.prototype.findByTxnId = function (id) {
    return new Promise((resolve, reject)=> {
        userRealTxnMaster.findOne({raw: true, where: {txnId: id}})
            .then(function (txn) {
                if (!txn) reject('no txn found');
                resolve(txn)
            }).catch(reject);
    })
}


module.exports = new UserRealTxnMasterClass();
