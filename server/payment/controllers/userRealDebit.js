/**
 * Created by adityagupta on 17/2/17.
 */
const userRealDebitService=require('../services/userRealDebit');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const logger =require('../../utils/logger').websiteLogs;

const userRealDebit=function (req,res) {
    let params=req.body;
    params.txnType = 'DEBIT';
    userRealDebitService.debit(params)
        .then(function (result) {
            res.json({respCode:100,respData:result})
        })
        .catch(function (errorCode) {
            logger.error(errorCode);
            res.json({respCode:errorCode,message:responseMessage[errorCode]});
        })
};

module.exports=userRealDebit;
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:2,
                fromWinning:1,
                txnType:'DEBIT',
                amount:'2',
                adminId:'1' || null,
                remarks:'www' || null
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        userRealDebit(req,res);
    })()
}
