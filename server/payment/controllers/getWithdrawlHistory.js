/**
 * Created by adityagupta on 8/3/17.
 */
/**
 * Created by adityagupta on 17/2/17.
 */
const userRealWithdrawlService=require('../services/userRealWithdrawl');
const Util=require('../../utils/util');
const logger =require('../../utils/logger').websiteLogs;

const withdrawlHistory=function (req,res) {
    userRealWithdrawlService.withdrawlHistory()
        .then(function (result) {
            res.json(Util.response(100,'success',result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(400,'failed',err));
        })
}


module.exports=withdrawlHistory;
if(require.main==module) {
    (function () {
        var req = {
            body: {

            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        withdrawlHistory(req,res);
    })()
}
