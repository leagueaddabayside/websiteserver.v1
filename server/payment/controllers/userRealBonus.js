/**
 * Created by adityagupta on 17/2/17.
 */
const userRealBonusService=require('../services/userRealBonus');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const UserMaster=require('../../users/services/UserMaster');
const Emailer=require('../../utils/node_mailer');
const logger =require('../../utils/logger').websiteLogs;

const userRealBonus=function (req,res) {
    let params=req.body;
    userRealBonusService.addBonus(params)
        .then(function (result) {
            res.json(Util.response(100,'success',result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(400,err,[]));
        })
}


module.exports=userRealBonus;
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:2,
                amount:2,
                txnType:'BONUS',
                initTxnId:12
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        userRealBonus(req,res);
    })()
}
