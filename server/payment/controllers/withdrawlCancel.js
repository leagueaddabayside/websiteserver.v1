/**
 * Created by adityagupta on 17/2/17.
 */
const userRealWithdrawlService=require('../services/userRealWithdrawl');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const logger =require('../../utils/logger').websiteLogs;

const withdrawl=function (req,res) {
    let params=req.body;
    params.txnType = 'WITHDRAWL_C';
    userRealWithdrawlService.withdrawlCancel(params)
        .then(function (result) {
            res.json(Util.response(100,'success',result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(400,'failed',err));
        })
};


module.exports=withdrawl;
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:2,
                txnId:1,
                adminId:'1',
                cancelRemarks:'www',
                winningAmt:2,
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        withdrawl(req,res);
    })()
}
