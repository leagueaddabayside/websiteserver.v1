/**
 * Created by adityagupta on 10/2/17.
 */
const config=require('../../../bin/config')
const userWalletModel=require('../services/userWallet');
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');
const logger =require('../../utils/logger').websiteLogs;

const UserWalletInfo=(req,res)=>{

    let params=req.body;
    params.userId= params.userId || false;
    if(params.userId){
        userWalletModel.getByUserid(params.userId)
            .then(function (userInfo) {
                userInfo.screenName = params.screenName;

                res.json({respCode:100,respData:userInfo})
            })
            .catch(function (errorCode) {
                logger.error(errorCode);
                res.json({respCode:errorCode,message:responseMessage[errorCode]})
            })
    }else {
        res.json({respCode:responseCode.USER_NOT_EXIST,message:responseMessage[responseCode.USER_NOT_EXIST]})
    }
}

module.exports=UserWalletInfo;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId: 11,
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        UserWallet(req,res);
    })()
}
