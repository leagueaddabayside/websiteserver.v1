/**
 * Created by adityagupta on 8/3/17.
 */
/**
 * Created by adityagupta on 17/2/17.
 */
const userRealWithdrawlService=require('../services/userRealWithdrawl');
const Util=require('../../utils/util');
const logger =require('../../utils/logger').websiteLogs;

const crmAppwithdrawlHistory=function (req,res) {
    userRealWithdrawlService.crmApproveHistory()
        .then(function (result) {
            res.json(Util.response(100,'success',result));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(400,'failed',err));
        })
}


module.exports=crmAppwithdrawlHistory;
if(require.main==module) {
    (function () {
        var req = {
            body: {

            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        crmAppwithdrawlHistory(req,res);
    })()
}
