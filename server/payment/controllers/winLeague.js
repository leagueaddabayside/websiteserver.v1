/**
 * Created by adityagupta on 17/2/17.
 */
const userLeague=require('../services/userLeague');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const logger =require('../../utils/logger').websiteLogs;

const winLeague=function (req,res) {
    let params=req.body;
    if(params.leagues){
        userLeague.winningLeague(params,function(err,winResp){
            if(err){
                res.json(Util.response(responseCode.SOME_INTERNAL_ERROR,'failed',responseMessage[responseCode.SOME_INTERNAL_ERROR]));
                return;
            }
            logger.info('results come in db',winResp)

            if(!winResp){
                res.json(Util.response(responseCode.SOME_INTERNAL_ERROR,'failed',responseMessage[responseCode.SOME_INTERNAL_ERROR]));
                return;
            }

            let winArr = [];
            winResp.forEach(function (value) {
                winArr.push({refTxnId : value.refTxnId , txnId : value.txnId});
            });


            res.json(Util.response(100,'success',winArr));
        });


    }else{
        res.json(Util.response(responseCode.INVALID_REQUEST_PARAMS,'failed',responseMessage[responseCode.INVALID_REQUEST_PARAMS]));
    }

}

module.exports=winLeague;
if(require.main==module) {
    (function () {

        let leagues = [{
            userId: 2,
            refTxnId: 6,
            amount: 12,
            leagueId : 22,
            remarks : 'win'

        },{
            userId: 2,
            refTxnId: 7,
            amount: 12,
            leagueId : 22,
            remarks : 'win'

        },{
            userId: 2,
            refTxnId: 8,
            amount: 12,
            leagueId : 22,
            remarks : 'win'

        }];



        var req = {
            body: {
                userId:2,
                WinningAmt:2,
                leagueId:12,
                matchId:12,
                txnType:'WINL',
                adminId:'1',
                remarks:'www',

            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        winLeague(req,res);
    })()
}
