/**
 * Created by adityagupta on 17/2/17.
 */
const userRealWithdrawlService=require('../services/userRealWithdrawl');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const UserMaster=require('../../users/services/UserMaster');
const Emailer=require('../../utils/node_mailer');
const logger =require('../../utils/logger').websiteLogs;

const withdrawl=function (req,res) {
    let params=req.body;
    params.txnType = 'WITHDRAWL';
    userRealWithdrawlService.withdrawl(params)
        .then(function (result) {
            if(result) Util.sendEmailOnEvent(params.userId,'WITHDRAWL_CASH',{amount:params.amount});
            res.json({respCode:responseCode.SUCCESS,respData:result,message:responseMessage[responseCode.WITHDRAWL_SUCCESS]})
        })
        .catch(function (errorCode) {
            logger.error(errorCode);
            res.json({respCode:errorCode,message:responseMessage[errorCode]});
        })
};


module.exports=withdrawl;
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:2,
                amount:2,
                txnType:'with',
                adminId:'1',
                remarks:'www',
                fromWinning:2
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        withdrawl(req,res);
    })()
}
