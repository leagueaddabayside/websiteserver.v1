/**
 * Created by adityagupta on 18/4/17.
 */
/**
 * Created by adityagupta on 17/2/17.
 */
const userRealDepositService=require('../services/userRealDeposit');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const Emailer=require('../../utils/node_mailer');
const UserMaster=require('../../users/services/UserMaster');
const logger =require('../../utils/logger').websiteLogs;

const userRealDepositPayu=function (req,res) {
    let params=req.body;
    params.txnType = 'DEPOSIT';
    userRealDepositService.addDeposit(params)
        .then(function (result) {
            console.info('||||||||||||||||||| deposit success|||||||||||||||||');
            if(result) {
                Util.sendEmailOnEvent(params.userId,'DEPOSIT_CASH',{amount:params.amount,orderId:result})
            }
            res.redirect('/myaccount?status='+params.status+'&txnid='+params.initTxnId+'&amount='+params.amount);
        })
        .catch(function (errorCode) {
            logger.error(errorCode);
            res.redirect('/myaccount?status=failed&txnid='+params.initTxnId+'&amount='+params.amount);
        })
};


module.exports=userRealDepositPayu;
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:2,
                amount:2,
                txnType:'DEPOSIT',
                initTxnId:12
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        userRealDepositPayu(req,res);
    })()
}
