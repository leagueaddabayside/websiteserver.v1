/**
 * Created by adityagupta on 17/2/17.
 */
const userLeague=require('../services/userLeague');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const UserMaster=require('../../users/services/UserMaster');
const Emailer=require('../../utils/node_mailer');
const logger =require('../../utils/logger').websiteLogs;

const JoinLeague=function (req,res) {
    let params=req.body;
    params.txnType = 'JOIN_L';
    userLeague.joinLeague(params)
        .then(function (result) {
            if(result) {
                logger.info('??????????????????????????????',params.startTime);
                let EmailData={amount:params.amount,match:params.matchName};
                if(params.startTime) {
                    params.startTime=+params.startTime;
                    EmailData.time=new Date(params.startTime*1000).toDateString();
                }
                Util.sendEmailOnEvent(params.userId,'JOINED_L',EmailData)
            }
            res.json({respCode:100,respData:result})
        })
        .catch(function (errorCode) {
            logger.error(errorCode);
            res.json({respCode:errorCode,message:responseMessage[errorCode]});
        })
};


module.exports=JoinLeague;
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:2,
                amount:0.2,
                leagueId:12,
                matchId:12,
                txnType:'JOINL',
                adminId:'1',
                remarks:'www',
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        JoinLeague(req,res);
    })()
}
