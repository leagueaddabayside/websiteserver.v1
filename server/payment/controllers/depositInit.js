/**
 * Created by adityagupta on 16/2/17.
 */
const depositInitService=require('../services/gnDepositInit');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const Util=require('../../utils/util');
const logger =require('../../utils/logger').websiteLogs;

const depositInit=function (req,res) {
    let params=req.body;
    if(params.amount && params.userId){
        depositInitService.create(params)
            .then(depositInitService.getToken)
            .then(function (token) {
                 res.json(Util.response(100,'success',token));
            }).catch(function (err) {
                logger.error(err);
            res.json(Util.response(400,'failed',err));
        })
    }
}
module.exports=depositInit;
/*************************************************/
if(require.main==module){
    (function () {
       var req={
           body:{
               userId:2,
               amount:222
           }
       },res={
           json:function (result) {
               console.log(JSON.stringify(result));
           }
       }
       depositInit(req,res);
    })()
}
