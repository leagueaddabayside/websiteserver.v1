/**
 * Created by adityagupta on 10/2/17.
 */
const config=require('../../../bin/config')
const userWalletModel=require('../services/userWallet');
const responseMessage =require('../../utils/response_message');
const logger =require('../../utils/logger').websiteLogs;
const responseCode =require('../../utils/response_code');

const UserWallet=(req,res)=>{
    let params=req.body;
    params.userId= params.userId || false;
    if(params.userId){
        userWalletModel.getByUserid(params.userId)
            .then(function (rows) {
                res.json({respCode:100,respData:rows})
            })
            .catch(function (errorCode) {
                logger.info(errorCode);
                res.json({respCode:errorCode,message:responseMessage[errorCode]})
            })
    }else {
        res.json({respCode:responseCode.USER_NOT_EXIST,message:responseMessage[responseCode.USER_NOT_EXIST]})
    }
}

module.exports=UserWallet;


/******************************************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId: 11,
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        UserWallet(req,res);
    })()
}
