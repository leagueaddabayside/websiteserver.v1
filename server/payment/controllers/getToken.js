/**
 * Created by adityagupta on 15/2/17.
 */
const request=require('request');
const paymentConf=require('../../../bin/paymentConfig');
const getToken=(req,res)=>{
    request({
        url: 'https://www.tekprocess.co.in/mobile/paynimoV2.req', //URL to hit
        method: 'POST',
        form: JSON.stringify(paymentConf)
    }, function(error, response, body){
        if(error) return res.json({respCode:400,error:error});

        try{
            var token=JSON.parse(body);
        }catch(e) {
            res.json({respCode:400,error:'error in parsing json'});
        }
        res.json(token);
    });

};

/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId: 11,
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        getToken(req,res);
    })()
}

