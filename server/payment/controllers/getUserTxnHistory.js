/**
 * Created by adityagupta on 10/2/17.
 */
const config=require('../../../bin/config')
const userTxnMaster=require('../services/userRealTxnMaster');
const responseMessage =require('../../utils/response_message');
const responseCode =require('../../utils/response_code');
const Util=require('../../utils/util');
const logger =require('../../utils/logger').websiteLogs;

const UserTxnsHistory=(req,res)=>{
    let params=req.body;
    params.userId= params.userId || false;
    if(params.userId){
        userTxnMaster.findUserTransaction(params)
            .then(function (txns) {
                res.json(Util.response(100,'success',txns));
            }).catch(function (err) {
                logger.error(err);
            res.json(Util.response(400,'failed',err));
        })
    }else {
        res.json({respCode:responseCode.USER_NOT_EXIST,message:responseMessage[responseCode.USER_NOT_EXIST]})
    }
}

module.exports=UserTxnsHistory;


/**********************/
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId: 2,
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };
        UserTxnsHistory(req,res);
    })()
}
