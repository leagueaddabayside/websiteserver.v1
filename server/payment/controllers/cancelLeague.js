/**
 * Created by adityagupta on 17/2/17.
 */
const userLeague=require('../services/userLeague');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const UserMaster=require('../../users/services/UserMaster');
const Emailer=require('../../utils/node_mailer');
const logger =require('../../utils/logger').websiteLogs;

const cancelLeague=function (req,res) {
    let params=req.body;
    logger.info('cancelLeague params',params);
    params.txnType = 'REFUND_L';
    if(params.leagues){
        userLeague.cancelLeague(params,function(err,cancelResp){
            if(err){
                res.json(Util.response(err,'failed',responseMessage[err]));
                return;
            }
            let cancelArr = [];
            cancelResp.forEach(function (value) {
                cancelArr.push({refTxnId : value.refTxnId , txnId : value.txnId});
            });
            res.json(Util.response(100,'success',cancelArr));
        });


    }else{
        res.json(Util.response(responseCode.INVALID_REQUEST_PARAMS,'failed',responseMessage[responseCode.INVALID_REQUEST_PARAMS]));
    }
}

module.exports=cancelLeague;
if(require.main==module) {
    (function () {
        let leagues = [{
            userId: 2,
            refTxnId: 6,
            joinTxnId: 244,
            amount: 20,
            remarks: 'REFUND_L'

        }, {
            userId: 2,
            refTxnId: 7,
            joinTxnId: 245,
            amount: 20,
            remarks: 'REFUND_L'

        }, {
            userId: 2,
            refTxnId: 8,
            joinTxnId: 246,
            amount: 20,
            remarks: 'REFUND_L'

        }];

        var req = {
            body: {
                userId:2,
                amount:2,
                leagueId:12,
                matchId:12,
                txnType:'JOINL',
                adminId:'1',
                remarks:'www',
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        cancelLeague(req,res);
    })()
}
