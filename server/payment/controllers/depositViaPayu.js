/**
 * Created by adityagupta on 16/2/17.
 */
const depositInitService=require('../services/gnDepositInit');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const Util=require('../../utils/util');
const logger =require('../../utils/logger').websiteLogs;

const depositInitViaPayu=function (req,res) {
    let params=req.body;
    params.status='failed';
    if(params.amount && params.userId){
        depositInitService.createTxnPayU(params)
            .then((result)=>depositInitService.sendToPayu(result))
            .then(function (form) {
                logger.info('sending form to user',form);
                if(!form) return  res.redirect('/myaccount?status=failed');
                console.info('|||||||||||||||||||passing sending to payu|||||||||||||||||');
                res.send(form);
            }).catch(function (err) {
                logger.error(err);
            res.redirect('/myaccount?status=failed');
        })
    }
}
module.exports=depositInitViaPayu;
/*************************************************/
if(require.main==module){
    (function () {
       var req={
           body:{
               userId:18,
               amount:222
           }
       },res={
           json:function (result) {
               console.log(JSON.stringify(result));
           }
       }
        depositInitViaPayu(req,res);
    })()
}
