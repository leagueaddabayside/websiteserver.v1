/**
 * Created by adityagupta on 8/3/17.
 */
/**
 * Created by adityagupta on 17/2/17.
 */
const userRealWithdrawlService=require('../services/userRealWithdrawl');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const FileEmailer=require('../../utils/EmailPdfFIle');
const config=require('../../../bin/config');
const SendEmail=require('../../utils/emailers');
const logger =require('../../utils/logger').websiteLogs;

const withdrawlHistory=function (req,res) {
    let params=req.body;
    userRealWithdrawlService.updateStatus(params)
        .then(function (result) {
            if(params.status=='ACC_A' && result){
                let templateData={
                    name:result.name,
                    date:new Date().toLocaleDateString(),
                    amount:result.amt,
                    acnumber:SendEmail.beepCredential(result['panCard.bankDetails.accountNumber']),
                    ifscCode:SendEmail.beepCredential(result['panCard.bankDetails.ifscCode']),
                    branch:result['panCard.bankDetails.branch'],
                    bankName:SendEmail.beepCredential(result['panCard.bankDetails.bankName']),
                    via:'NEFT',
                    clientRefNo:params.txnId,
                    pan:SendEmail.beepCredential(result.panNumber)
                };
                FileEmailer.sendMailWithAttacment({
                    templateType:'WITHDRAWL_RECEIPT_PDF',
                    to:result['panCard.emailId'],
                    templateData:templateData
                },function (err,ack) {
                    logger.info(ack);
                })
            }
            if((params.status=='CRM_C' || params.status=='ACC_C') && result){
                    SendEmail.sendEmailOnEvent(params.userId,config.emailTemplates.WITHDRAWL_DECLINED,{amount:result.amt})

            }
            res.json(Util.response(100,'success',{}));
        })
        .catch(function (err) {
            logger.error(err);
            res.json(Util.response(400,'failed',err));
        })
}


module.exports=withdrawlHistory;
if(require.main==module) {
    (function () {
        var req = {
            body: {
                txnId:492,
                userId:2,
                status:'failed'
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        withdrawlHistory(req,res);
    })()
}
