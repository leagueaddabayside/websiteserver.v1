/**
 * Created by adityagupta on 15/2/17.
 */
const userRealCreditService=require('../services/userRealCredit');
const Util=require('../../utils/util');
const responseCode = require("../../utils/response_code.js");
const responseMessage = require("../../utils/response_message.js");
const logger =require('../../utils/logger').websiteLogs;

const userRealCredit=function (req,res) {// main middleware that handle response
    let params=req.body;
    params.txnType = 'CREDIT';
    userRealCreditService.addCredit(params)
        .then(function (result) {
            res.json({respCode:100,respData:result})

        })
        .catch(function (errorCode) {
            logger.error(errorCode);
            res.json({respCode:errorCode,message:responseMessage[errorCode]});

        })
}

module.exports=userRealCredit;
if(require.main==module) {
    (function () {
        var req = {
            body: {
                userId:2,
                toDeposit:4.5,
                toWinning:2.2,
                toBonus:34.4,
                txnType:'CREDIT',
                amount:'2',
                adminId:'1' || null,
                remarks:'www' || null
            }
        }, res = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        userRealCredit(req,res);
    })()
}
