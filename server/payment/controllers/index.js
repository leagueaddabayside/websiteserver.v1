/**
 * Created by adityagupta on 10/2/17.
 */
const UserWalletCtrl=require('./getUserWallet');
const UserWalletInfoCtrl=require('./getUserWalletInfo');
const depositInit=require('./depositInit');
const userRealDeposit=require('./userRealDeposit');
const userRealCredit=require('./userRealCredit');
const userRealDebit=require('./userRealDebit');
const userRealWithdrawl=require('./userRealWithdrawl');
const withdrawlCancel=require('./withdrawlCancel');
const getUserTxnHistory=require('./getUserTxnHistory');
const getWithdrawlHistory=require('./getWithdrawlHistory');
const getWithdrawlHistoryCrmApproved=require('./crmAppWithdrawlHistory');
const withdrawlUpdate=require('./withdrawlUpdate');
const userJoinLeagueTxn=require('./userJoinLeagueTxn');
const userCancelLeagueTxn=require('./cancelLeague');
const userWinLeagueTxn=require('./winLeague');
const userBonusTxn=require('./userRealBonus');
const depositViaPayu=require('./depositViaPayu');
const realDepositViaPayu=require('./realDepositViaPayu');

module.exports.UserWalletInfo = UserWalletInfoCtrl;
module.exports.UserWallet = UserWalletCtrl;
module.exports.depositInit=depositInit;
module.exports.realDeposit=userRealDeposit;
module.exports.userRealCredit=userRealCredit;
module.exports.userRealDebit=userRealDebit;
module.exports.userRealWithdrawl=userRealWithdrawl;
module.exports.withdrawlCancel=withdrawlCancel;
module.exports.getUserTxnHistory=getUserTxnHistory;
module.exports.getWithdrawlHistory=getWithdrawlHistory;
module.exports.withdrawlUpdate=withdrawlUpdate;
module.exports.userJoinLeagueTxn=userJoinLeagueTxn;
module.exports.userCancelLeagueTxn=userCancelLeagueTxn;
module.exports.userWinLeagueTxn=userWinLeagueTxn;
module.exports.getWithdrawlHistoryCrmApproved=getWithdrawlHistoryCrmApproved;
module.exports.userBonusTxn=userBonusTxn;
module.exports.depositViaPayu=depositViaPayu;
module.exports.realDepositViaPayu=realDepositViaPayu;


