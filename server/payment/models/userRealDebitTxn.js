/**
 * Created by adityagupta on 9/2/17.
 */

const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');

const userDebitTxn= sequelize.define('gn_user_real_debit_txn', {
    txnId: {type: Sequelize.BIGINT, field: 'txn_id',primaryKey:true},
    userId: {type: Sequelize.INTEGER, field: 'user_id'},
    txnDate: {type: Sequelize.INTEGER, field: 'txn_date'},
    amount: {type: Sequelize.DECIMAL(20,4), field: 'amount'},
    fromDeposit: {type: Sequelize.DECIMAL(20,4), field: 'from_deposit'},
    fromWinning: {type: Sequelize.DECIMAL(20,4), field: 'from_winning'},
    fromBonus: {type: Sequelize.DECIMAL(20,4), field: 'from_bonus'},
    adminId: {type: Sequelize.STRING, field: 'admin_id'},
    remarks: {type: Sequelize.STRING, field: 'remarks'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports=userDebitTxn;
