/**
 * Created by adityagupta on 9/2/17.
 */

const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');

const gnAdminRakeCreditTxn= sequelize.define('gn_admin_rake_credit_txn', {
    id: {type: Sequelize.BIGINT, field: 'id',primaryKey:true,autoIncrement: true},
    adminId: {type: Sequelize.INTEGER, field: 'admin_id'},
    txnDate: {type: Sequelize.INTEGER, field: 'txn_date'},
    leagueAmt: {type: Sequelize.DECIMAL(20,4), field: 'league_amt'},
    winningAmt: {type: Sequelize.DECIMAL(20,4), field: 'winning_amt'},
    rakeAmt: {type: Sequelize.DECIMAL(20,4), field: 'rake_amt'},
    tourId: {type: Sequelize.INTEGER, field: 'tour_id'},
    matchId: {type: Sequelize.INTEGER, field: 'match_id'},
    leagueId: {type: Sequelize.INTEGER, field: 'league_id'},
    configId: {type: Sequelize.INTEGER, field: 'config_id'}

}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports=gnAdminRakeCreditTxn;
