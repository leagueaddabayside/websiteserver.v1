/**
 * Created by adityagupta on 9/2/17.
 */

const Sequelize = require('sequelize');
const config = require('../../../bin/config')
const sequelize = require(config.__base + '/server/connection');

const gnUserTds = sequelize.define('gn_user_tds', {
    id: {type: Sequelize.INTEGER, field: 'id', primaryKey: true, autoIncrement: true},
    userId: {type: Sequelize.INTEGER, field: 'user_id'},
    adminId: {type: Sequelize.INTEGER, field: 'admin_id'},
    winningTxnId: {type: Sequelize.INTEGER, field: 'winning_txn_id'},
    date: {type: Sequelize.INTEGER, field: 'date'},
    tdsAmount: {type: Sequelize.DECIMAL(20, 4), field: 'tds_amount'},
    winningAmount: {type: Sequelize.DECIMAL(20, 4), field: 'winning_amount'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});
module.exports = gnUserTds;
