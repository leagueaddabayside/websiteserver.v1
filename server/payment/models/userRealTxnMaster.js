/**
 * Created by adityagupta on 9/2/17.
 */

const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');

const userRealTxnMaster= sequelize.define('gn_user_real_txn_master', {
    txnId: {type: Sequelize.BIGINT, field: 'txn_id',primaryKey:true,autoIncrement: true},
    userId: {type: Sequelize.INTEGER, field: 'user_id'},
    txnType: {type: Sequelize.STRING, field: 'txn_type'},
    txnDate: {type: Sequelize.INTEGER, field: 'txn_date'},
    amount: {type: Sequelize.DECIMAL(20,4), field: 'amount'},
    domainId: {type: Sequelize.STRING, field: 'domain_id'},
    depositBeforeTxn: {type: Sequelize.DECIMAL(20,4), field: 'deposit_before_txn'},
    winningBeforeTxn: {type: Sequelize.DECIMAL(20,4), field: 'winning_before_txn'},
    bonusBeforeTxn: {type: Sequelize.DECIMAL(20,4), field: 'bonus_before_txn'},
    gameTxnId: {type: Sequelize.STRING, field: 'game_txn_id'},
    remarks: {type: Sequelize.STRING, field: 'remarks'},
    createdAt: false,
    updatedAt: false
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports=userRealTxnMaster;
