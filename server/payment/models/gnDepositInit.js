/**
 * Created by adityagupta on 9/2/17.
 */
const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');

const gnDepositInit= sequelize.define('gn_deposit_init', {
    id: {type: Sequelize.INTEGER, field: 'id',primaryKey:true,autoIncrement:true},
    amount: {type: Sequelize.DOUBLE, field: 'deposit_amt',default:0.0},
    userId: {type: Sequelize.STRING, field: 'userId',default:null},
    mihpayid: {type: Sequelize.STRING, field: 'mihpayid',default:null},
    bank_ref_num: {type: Sequelize.STRING, field: 'bank_ref_num',default:null},
    bankcode: {type: Sequelize.STRING, field: 'bankcode',default:null},
    errorcode: {type: Sequelize.STRING, field: 'errorcode',default:null},
    errormessage: {type: Sequelize.STRING, field: 'errormessage',default:null},
    name_on_card: {type: Sequelize.STRING, field: 'name_on_card',default:null},
    cardnum: {type: Sequelize.STRING, field: 'cardnum',default:null},
    issuing_bank: {type: Sequelize.STRING, field: 'issuing_bank',default:null},
    card_type: {type: Sequelize.STRING, field: 'card_type',default:null},
    status: {type: Sequelize.ENUM,values:['pending,success'], field: 'status',required:true},

}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports=gnDepositInit;
