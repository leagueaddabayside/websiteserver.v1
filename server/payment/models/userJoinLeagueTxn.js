/**
 * Created by adityagupta on 9/2/17.
 */

const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');

const userLeagueTxn= sequelize.define('gn_user_real_legue_txn', {
    txnId: {type: Sequelize.BIGINT, field: 'txn_id',primaryKey:true},
    userId: {type: Sequelize.INTEGER, field: 'user_id'},
    txnDate: {type: Sequelize.INTEGER, field: 'txn_date'},
    amount: {type: Sequelize.DECIMAL(20,4), field: 'amount'},
    depositAmt: {type: Sequelize.DECIMAL(20,4), field: 'deposit_amt'},
    winningAmt: {type: Sequelize.DECIMAL(20,4), field: 'winning_amt'},
    bonusAmt: {type: Sequelize.DECIMAL(20,4), field: 'bonus_amt'},
    leagueId: {type: Sequelize.INTEGER, field: 'league_id'},
    matchId: {type: Sequelize.INTEGER, field: 'match_id'},
    remarks: {type: Sequelize.STRING, field: 'remarks'},
    gameTxnId : {type: Sequelize.BIGINT, field: 'game_txn_id'},
    status : {type: Sequelize.ENUM,values:['JOIN_LEAGUE','CANCEL_LEAGUE'], field: 'status'},
    cancelId: {type: Sequelize.BIGINT, field: 'cancel_id'},
    cancelTime: {type: Sequelize.INTEGER, field: 'cancel_time'},
    cancelRemarks: {type: Sequelize.STRING, field: 'cancel_remarks'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports=userLeagueTxn;
