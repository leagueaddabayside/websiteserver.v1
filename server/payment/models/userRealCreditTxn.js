/**
 * Created by adityagupta on 9/2/17.
 */

const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');

const userCreditTxn= sequelize.define('gn_user_real_credit_txn', {
    txnId: {type: Sequelize.BIGINT, field: 'txn_id',primaryKey:true},
    userId: {type: Sequelize.INTEGER, field: 'user_id'},
    txnDate: {type: Sequelize.INTEGER, field: 'txn_date'},
    amount: {type: Sequelize.DECIMAL(20,4), field: 'amount'},
    toDeposit: {type: Sequelize.DECIMAL(20,4), field: 'to_deposit'},
    toWinning: {type: Sequelize.DECIMAL(20,4), field: 'to_winning'},
    toBonus: {type: Sequelize.DECIMAL(20,4), field: 'to_bonus'},
    txnType: {type: Sequelize.STRING, field: 'txn_type'},
    adminId: {type: Sequelize.INTEGER, field: 'admin_id'},
    remarks: {type: Sequelize.STRING, field: 'remarks'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});


module.exports=userCreditTxn;
