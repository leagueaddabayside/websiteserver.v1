/**
 * Created by adityagupta on 9/2/17.
 */

const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');

const userCancelLeagueTxn= sequelize.define('gn_user_cancel_legue_txn', {
    txnId: {type: Sequelize.STRING, field: 'txn_id',primaryKey:true},
    userId: {type: Sequelize.STRING, field: 'user_id'},
    txnDate: {type: Sequelize.STRING, field: 'txn_date'},
    amount: {type: Sequelize.STRING, field: 'amount'},
    depositAmt: {type: Sequelize.FLOAT, field: 'deposit_amt'},
    winningAmt: {type: Sequelize.FLOAT, field: 'winning_amt'},
    bonusAmt: {type: Sequelize.FLOAT, field: 'bonus_amt'},
    leagueId: {type: Sequelize.INTEGER, field: 'league_id'},
    matchId: {type: Sequelize.STRING, field: 'match_id'},
    remarks: {type: Sequelize.STRING, field: 'remarks'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});
module.exports=userCancelLeagueTxn;
