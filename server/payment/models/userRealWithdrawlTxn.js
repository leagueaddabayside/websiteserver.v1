/**
 * Created by adityagupta on 9/2/17.
 */

const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');
const userWallet=require('../models/userWalletMaster')

const userWithdrawlTxn= sequelize.define('gn_user_real_wihdrawal_txn', {
    txnId: {type: Sequelize.BIGINT, field: 'txn_id',primaryKey:true},
    userId: {type: Sequelize.INTEGER, field: 'user_id'},
    txnDate: {type: Sequelize.INTEGER, field: 'txn_date'},
    fromDeposit: {type: Sequelize.DECIMAL(20,4), field: 'from_deposit'},
    fromWinning: {type: Sequelize.DECIMAL(20,4), field: 'from_winning'},
    fromBonus: {type: Sequelize.DECIMAL(20,4), field: 'from_bonus'},
    status: {type: Sequelize.ENUM,values:['CRM_A','CRM_C','ACC_A','ACC_C','PENDING'], field: 'status'},
    remarks: {type: Sequelize.STRING, field: 'remarks'},
    cancelId: {type: Sequelize.BIGINT, field: 'cancel_id'},
    cancelTime: {type: Sequelize.INTEGER, field: 'cancel_time'},
    cancelRemarks: {type: Sequelize.STRING, field: 'cancel_remarks'},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});
userWithdrawlTxn.belongsTo(userWallet,{foreignKey: 'userId' ,as:'wallet'});
module.exports=userWithdrawlTxn;
