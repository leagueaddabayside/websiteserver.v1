/**
 * Created by adityagupta on 9/2/17.
 */
const Sequelize=require('sequelize');
const config=require('../../../bin/config')
const sequelize=require(config.__base+'/server/connection');
const userWithdrawlTxn=require('../models/userRealWithdrawlTxn')

const userWalletMaster= sequelize.define('gn_user_wallet_master', {
    userId: {type: Sequelize.INTEGER, field: 'user_id',primaryKey:true},
    depositAmt: {type: Sequelize.DECIMAL(20,4), field: 'deposit_amt',default:0.0},
    bonusAmt: {type: Sequelize.DECIMAL(20,4), field: 'bonus_amt',default:0.0},
    winningAmt: {type: Sequelize.DECIMAL(20,4), field: 'winning_amt',default:0.0},
    totalWinning : {type: Sequelize.DECIMAL(20,4), field: 'total_winning',default:0.0},
    leaguesWin: {type: Sequelize.INTEGER, field: 'win_leagues',default:0},
    lastDepositDate: {type: Sequelize.INTEGER, field: 'last_deposit_date'},
    lastBonusDate: {type: Sequelize.DATE, field: 'last_bonus_date'},
    version: {type: Sequelize.INTEGER, field: 'version',default:0},
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});


module.exports=userWalletMaster;
