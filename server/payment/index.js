/**
 * Created by sumit on 2/8/2017.
 */
const express=require('express');
const routes=express.Router();
const PaymentCtrl=require('./controllers');
const middleware = require('../middleware');
const localMiddleware = require('./middleware');

routes.post('/payments/users/wallet',middleware.auditTrailLog,middleware.getUserIdFromToken,PaymentCtrl.UserWallet);
routes.post('/payments/users/walletInfo',middleware.getUserInfoFromName,PaymentCtrl.UserWalletInfo);
routes.post('/payments/init',localMiddleware.deposit,PaymentCtrl.depositInit);
routes.post('/payments/deposit/viapayu',localMiddleware.getInitTxnForPayu,PaymentCtrl.realDepositViaPayu);
routes.post('/payments/deposit',localMiddleware.getInitTxn,PaymentCtrl.realDeposit);
routes.post('/payments/credit',middleware.auditTrailLog,middleware.getAdminIdFromToken,localMiddleware.reformParams,PaymentCtrl.userRealCredit);
routes.post('/payments/debit',middleware.auditTrailLog,middleware.getAdminIdFromToken,localMiddleware.reformParams,PaymentCtrl.userRealDebit);
routes.post('/payments/bonus',middleware.auditTrailLog,PaymentCtrl.userBonusTxn);
routes.post('/payments/withdrawl',middleware.auditTrailLog,localMiddleware.withdrawl,PaymentCtrl.userRealWithdrawl);
routes.post('/payments/withdrawl/cancel',middleware.auditTrailLog,middleware.getAdminIdFromToken,localMiddleware.withdrawlCancel,PaymentCtrl.withdrawlCancel);
routes.post('/payments/withdrawl/update',middleware.auditTrailLog,PaymentCtrl.withdrawlUpdate);
routes.post('/payments/withdrawl/updateFinal',middleware.auditTrailLog,middleware.getAdminIdFromToken,PaymentCtrl.withdrawlUpdate);
routes.post('/payments/txn/history',PaymentCtrl.getUserTxnHistory);
routes.post('/payments/withdrawl/history',middleware.auditTrailLog,middleware.getAdminIdFromToken,PaymentCtrl.getWithdrawlHistory);
routes.post('/payments/withdrawl/history/crmapproved',middleware.auditTrailLog,middleware.getAdminIdFromToken,PaymentCtrl.getWithdrawlHistoryCrmApproved);
routes.post('/payments/joinleague',middleware.auditTrailLog,middleware.validateServerAuthentication,PaymentCtrl.userJoinLeagueTxn);
routes.post('/payments/cancelleague',middleware.auditTrailLog,middleware.validateServerAuthentication,PaymentCtrl.userCancelLeagueTxn);
routes.post('/payments/league/win',middleware.auditTrailLog,middleware.validateServerAuthentication,PaymentCtrl.userWinLeagueTxn);
routes.post('/payments/init/payu',middleware.auditTrailLog,localMiddleware.deposit,PaymentCtrl.depositViaPayu);
routes.post('/payments/payu/return',middleware.auditTrailLog,localMiddleware.checkReturnHash);


routes.get('/*',function (req,res) {
    res.json({code:404,error:'Invalid token'});
});

module.exports=routes;
