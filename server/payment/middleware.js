/**
 * Created by adityagupta on 17/2/17.
 */
const depositInitService = require('./services/gnDepositInit');
const depositInitModel = require('./models/gnDepositInit');
const userWallet = require('./services/userWallet');
const userTxn = require('./services/userRealTxnMaster');
const userMaster = require('../users/models/gnUserMaster');
const User = require('../users/services/UserMaster');
const responseCode = require("../utils/response_code.js");
const responseMessage = require("../utils/response_message.js");
const Util=require('../utils/utility');
const config=require('../../bin/config');
const crypto=require('crypto');
const logger =require('../utils/logger').websiteLogs;

function MiddlewareClass() {

}
MiddlewareClass.prototype.validateDeposit = function (req, res) {
    return new Promise((resolve, reject)=> {
        let params = req.body;
        if (params.txnInitId && params.userId) {
            depositInitService.findBy({userId: params.userId, id: params.txnInitId})
                .then(resolve)
                .catch(reject)
        }
    })
};

MiddlewareClass.prototype.reformParams = function (req, res, next) {
    let params = req.body;
    let path = req.path;
    let lastIndexOfDelimeter = path.lastIndexOf('/');
    let err = [];
    if (params.newDepositAmt && params.newDepositAmt < 0) err.push('Deposit amount can not negative');
    if (params.newWinningAmt && params.newWinningAmt < 0) err.push('Winning amount can not negative');
    if (params.newBonusAmt && params.newBonusAmt < 0) err.push('Bonus amount can not negative');
    params.amount = 0;
    if (params && err.length === 0) {
        switch (path.slice(lastIndexOfDelimeter + 1).toUpperCase()) {
            case 'CREDIT':
                if(params.newDepositAmt) params.amount+=req.body.toDeposit=parseFloat(params.newDepositAmt);
                if(params.newWinningAmt) params.amount+=req.body.toWinning=parseFloat(params.newWinningAmt);
                if(params.newBonusAmt) params.amount+=req.body.toBonus=parseFloat(params.newBonusAmt);
                break;
            case 'DEBIT':
                if (params.newDepositAmt) params.amount += req.body.fromDeposit = parseFloat(params.newDepositAmt);
                if (params.newWinningAmt) params.amount += req.body.fromWinning = parseFloat(params.newWinningAmt);
                if (params.newBonusAmt) params.amount += req.body.fromBonus = parseFloat(params.newBonusAmt);
                break;
            default:
                return res.json({respCode: 400, message: 'transaction type not valid', data: []})
        }

    } else {
        return res.json({respCode: 400, message: err, data: []})
    }
    delete params['newBonusAmt'];
    delete params['newWinningAmt'];
    delete params['newDepositAmt'];
    next();
};
MiddlewareClass.prototype.getInitTxn = function (req, res, next) {
    let params = req.body;
    // params.initTxnId=params.txnid.split('==')[1];
    params.initTxnId=params.txnid;

    depositInitModel.findOne({where: {id: params.initTxnId}})
        .then((txn)=> {
            if (!txn) return res.json({
                respCode: responseCode.DEPOSIT_INIT_TXN_NOT_FOUND,
                message: responseMessage[responseCode.DEPOSIT_INIT_TXN_NOT_FOUND]
            });
            if (txn && txn.status == 'success') return res.json({
                respCode: responseCode.DEPOSIT_INIT_TXN_ALREADY_DONE,
                message: responseMessage[responseCode.DEPOSIT_INIT_TXN_ALREADY_DONE]
            });

            if(+(txn.amount)!==+(params.amount)) return res.json({
                respCode: responseCode.AMOUNT_MISMATCH,
                message: responseMessage[responseCode.AMOUNT_MISMATCH]
            });
            Object.assign(req.body, {userId:txn.userId});
            next();
        })
        .catch((err)=>res.json({
            respCode: responseCode.DEPOSIT_INIT_TXN_NOT_FOUND,
            message: responseMessage[responseCode.DEPOSIT_INIT_TXN_NOT_FOUND]
        }))
};
MiddlewareClass.prototype.getInitTxnForPayu = function (req, res, next) {
    console.info('||||||||||||||||||| deposit middleware enter |||||||||||||||||');
    let params = req.body;
    // if(!params.initTxnId && params.txnid.includes('==')) {
    //     params.initTxnId=params.txnid.split('==')[1];
    // }else {
    //     return res.redirect('/myaccount?status=failed');
    // }
    params.initTxnId=params.txnid;
    depositInitModel.findOne({where: {id: params.initTxnId}})
        .then((txn)=> {
            let code=responseCode.SUCCESS;
            if (!txn) code=responseCode.DEPOSIT_INIT_TXN_NOT_FOUND;
            if (txn && txn.status == 'success')  code=responseCode.DEPOSIT_INIT_TXN_ALREADY_DONE;
            if(+(txn.amount)!==+(params.amount)) code=responseCode.AMOUNT_MISMATCH;
            if(params.status=='failure') {
                if(params.mihpayid) txn.mihpayid=params.mihpayid;
                if(params.bank_ref_num) txn.bank_ref_num=params.bank_ref_num;
                if(params.bankcode) txn.bankcode=params.bankcode;
                if(params.name_on_card) txn.name_on_card=params.name_on_card;
                if(params.cardnum) txn.cardnum=params.cardnum;
                if(params.issuing_bank) txn.issuing_bank=params.issuing_bank;
                if(params.card_type) txn.card_type=params.card_type;
                if(params.error) txn.errorcode=params.error;
                if(params.error_Message) txn.errormessage=params.error_Message;
                txn.save();
                return res.redirect('/myaccount?status=failed');
            }
            Object.assign(req.body, {userId:txn.userId});
            if(code!==100) return res.redirect('/myaccount?status=failed&code='+code);
            console.info('||||||||||||||||||| sending to final txn |||||||||||||||||');
            next();
        })
        .catch((err)=>{
            logger.error(err);
                res.redirect('/myaccount?status=failed');
        })
};
MiddlewareClass.prototype.withdrawl = function (req, res, next) {
    let params = req.body;
    let code = responseCode.SUCCESS;
    if (params.amount < 200 || params.amount > 200000) code = responseCode.WITHDRAWAL_AMOUNT_VALIDATION;

    if (code !== responseCode.SUCCESS) {
        res.json({respCode: code, message: responseMessage[code]});
        return;
    }
    userMaster.findById(params.userId)
        .then(function (user) {
            if (!user)  code = responseCode.USER_NOT_EXIST;
            if (code === responseCode.SUCCESS) {
                Util.getUserStatus(user)
                if (user && Util.getUserStatus(user)!=='verified') {
                    code = responseCode.WITHDRAWL_VERIFY;
                }
            }
            if(code===100)return next();
            res.json({respCode:code,message:responseMessage[code]});
            }).catch(function (err) {
        logger.error(err);
             res.json({respCode:responseCode.SOME_INTERNAL_ERROR,message:responseMessage[responseCode.SOME_INTERNAL_ERROR],resData:[]});
        })
};

MiddlewareClass.prototype.deposit = function (req, res, next) {
    let params = req.body;
    let code = responseCode.SUCCESS;
    if (params.amount < 1 || params.amount > 25000) code = responseCode.DEPOSIT_AMOUNT_VALIDATION;
    if (code !== responseCode.SUCCESS) {
        res.redirect('/myaccount?status=failed&code='+code);
        return;
    }
    User.getUserAttributes({userId:params.userId},['isMobileVerified','isEmailVerified'])
        .then(function (user) {
            if (!user)  code = responseCode.USER_NOT_EXIST;
            if (code === responseCode.SUCCESS) {
                if (!user['personalInfo.firstName'] || user.isMobileVerified!=='YES' || user.isEmailVerified!=='YES') {
                    code = responseCode.USER_NOT_VERIFIED;
                }
            }
            if (code === responseCode.SUCCESS) return next();
            console.info('|||||||||||||||||||passing from middleware|||||||||||||||||');
            res.redirect('/myaccount?status=failed&code='+code);
        }).catch(function (err) {
        logger.error(err);
            res.redirect('/myaccount?status=failed');
        })
};

MiddlewareClass.prototype.withdrawlCancel = function (req, res, next) {
    let params = req.body;
    return new Promise((resolve, reject)=> {
        userTxn.findByTxnId(params.txnId)
            .then(function (txn) {
                if (!txn) return res.json({respCode: 400, message: 'txn not found', resData: []});
                req.body.winningAmt = txn.winningBeforeTxn;
                req.body.userId = txn.userId;
                next();
            }).catch(function (err) {
                logger.error(err);
                res.json({respCode: 400, message: err, resData: []})
            })
    })
};
MiddlewareClass.prototype.checkReturnHash = function (req,res) {
    let params=req.body;
    let validParams=params.txnid && params.productinfo
        && params.amount && params.firstname && params.email && params.status;
    let failUrl='/myaccount?status=failed';
    // params.initTxnId=params.txnid.split('==')[1];
    params.initTxnId=params.txnid;
    if(params.initTxnId) failUrl+='&txnid'+params.initTxnId;
    if(params.amount) failUrl+='&amount'+params.amount;
    // params.initTxnId=params.txnid;

    if(!validParams) res.redirect(failUrl);
    let createHash=config.payU.salt+'|'+params.status+'||||||';
    createHash+=(params.udf1)?params.udf1+'|':'|';
    createHash+=(params.udf2)?params.udf2+'|':'|';
    createHash+=(params.udf3)?params.udf3+'|':'|';
    createHash+=(params.udf4)?params.udf4+'|':'|';
    createHash+=(params.udf5)?params.udf5+'|':'|';
    createHash+=params.email;
    createHash+='|'+params.firstname+'|'+params.productinfo;
    createHash+='|'+params.amount+'|'+params.txnid;
    createHash+='|'+params.key;
    const hash = crypto.createHash('sha512')
        .update(createHash)
        .digest('hex');
    console.info('|||||||||||||||||||passing checking hash in middleware|||||||||||||||||');
    if(hash==params.hash) return res.redirect(307,'/payments/deposit/viapayu');
    res.redirect('/myaccount?status=failed&txnid='+params.initTxnId+'&amount='+params.amount);
};



module.exports = new MiddlewareClass();
if(require.main===module){
    (function () {
        module.exports.checkReturnHash({
            status: 'success',
            key: 'gtKFFx',
            txnid: 'A--091',
            amount: '600.00',
            productinfo: 'SAU Admission 2014',
            firstname: 'Vikas Kumar',
            email: 'vikaskumarsre@gmail.com',
            phone: '9999999999',
        })
    })();
}
