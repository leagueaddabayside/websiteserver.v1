'use strict';

describe('Controller: deskPopupHandlerCtrl', function () {

  // load the controller's module
  beforeEach(module('fantasyCricketApp'));

  var deskPopupHandlerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    deskPopupHandlerCtrl = $controller('deskPopupHandlerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(deskPopupHandlerCtrl.awesomeThings.length).toBe(3);
  });
});
