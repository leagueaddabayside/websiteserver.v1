'use strict';

describe('Controller: WithdrawcashCtrl', function () {

  // load the controller's module
  beforeEach(module('fantasyCricketApp'));

  var WithdrawcashCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    WithdrawcashCtrl = $controller('WithdrawcashCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(WithdrawcashCtrl.awesomeThings.length).toBe(3);
  });
});
