'use strict';

describe('Controller: MyteamnameCtrl', function () {

  // load the controller's module
  beforeEach(module('fantasyCricketApp'));

  var MyteamnameCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MyteamnameCtrl = $controller('MyteamnameCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MyteamnameCtrl.awesomeThings.length).toBe(3);
  });
});
