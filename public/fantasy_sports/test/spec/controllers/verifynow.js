'use strict';

describe('Controller: VerifynowCtrl', function () {

  // load the controller's module
  beforeEach(module('fantasyCricketApp'));

  var VerifynowCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    VerifynowCtrl = $controller('VerifynowCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(VerifynowCtrl.awesomeThings.length).toBe(3);
  });
});
