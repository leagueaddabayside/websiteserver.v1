'use strict';

describe('Service: assignServicesToRootScope', function () {

  // load the service's module
  beforeEach(module('fantasyCricketApp'));

  // instantiate service
  var assignServicesToRootScope;
  beforeEach(inject(function (_assignServicesToRootScope_) {
    assignServicesToRootScope = _assignServicesToRootScope_;
  }));

  it('should do something', function () {
    expect(!!assignServicesToRootScope).toBe(true);
  });

});
