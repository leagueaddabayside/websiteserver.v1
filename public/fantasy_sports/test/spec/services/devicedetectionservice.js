'use strict';

describe('Service: devicedetectionservice', function () {

  // load the service's module
  beforeEach(module('fantasyCricketApp'));

  // instantiate service
  var devicedetectionservice;
  beforeEach(inject(function (_devicedetectionservice_) {
    devicedetectionservice = _devicedetectionservice_;
  }));

  it('should do something', function () {
    expect(!!devicedetectionservice).toBe(true);
  });

});
