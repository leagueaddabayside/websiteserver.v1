'use strict';

describe('Service: depositeservice', function () {

  // load the service's module
  beforeEach(module('fantasyCricketApp'));

  // instantiate service
  var depositeservice;
  beforeEach(inject(function (_depositeservice_) {
    depositeservice = _depositeservice_;
  }));

  it('should do something', function () {
    expect(!!depositeservice).toBe(true);
  });

});
