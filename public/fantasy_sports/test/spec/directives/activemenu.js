'use strict';

describe('Directive: activemenu', function () {

  // load the directive's module
  beforeEach(module('fantasyCricketApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<activemenu></activemenu>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the activemenu directive');
  }));
});
