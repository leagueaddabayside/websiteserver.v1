'use strict';

/**
 * @ngdoc directive
 * @name fantasyCricketApp.directive:countdown
 * @description
 * # countdown
 */
angular.module('fantasyCricketApp')
	.directive('countdown', ['$interval', '$rootScope', 'utilityservice', '$http', function ($interval, $rootScope, utilityservice, $http) {
		return {
				restrict: 'A',
				scope: false,
				link: function (scope, element, attrs) {
					var future;
					future = new Date(parseInt(attrs.date));

					$rootScope.$on('tickingEvent', function(event, args){
	                	future = new Date(parseInt(attrs.date));
	                	if(isNaN(future)){
							return element.text('Closed');
						}
	                	var diff;
	                	diff = Math.floor((future.getTime() - (new Date().getTime() + $rootScope.serverDiff)) / 1000);
						//diff = Math.floor((future.getTime() - $rootScope.serverTime) / 1000);
						if(diff < 0){
							return element.text('00 : 00 : 00');
						}
						return element.text(dhms(diff));
	                });

					var dhms = function (t) {
	                    var days, hours, minutes, seconds;
	                    // days = Math.floor(t / 86400);
	                    // t -= days * 86400;
	                    hours = Math.floor(t / 3600);
	                    t -= hours * 3600;
	                    minutes = Math.floor(t / 60) % 60;
	                    t -= minutes * 60;
	                    seconds = t % 60;
	                    return formatTime(days, hours, minutes, seconds);
	                }

	                var formatTime = function(days, hours, minutes, seconds){
	                	return [
	                		// days + ' :',
	                        //hours + ' :',    //('0' + hours).slice(-2)
	                        hours >= 10 ? hours : "0" + hours.toString(), ' :',
	                        ('0' + minutes).slice(-2)  + ' :',
	                        ('0' + seconds).slice(-2)
	                    ].join(' ');
	                }
				}
		};
	}]);
