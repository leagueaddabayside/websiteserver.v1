'use strict';

/**
 * @ngdoc directive
 * @name fantasyCricketApp.directive:clickAnywhereButHere
 * @description
 * # clickAnywhereButHere
 */
 angular.module('fantasyCricketApp')
 	.directive('clickAnywhereButHere', ['$document', function($document){
	return {
		restrict: 'A',
		link: function(scope, elem, attr, ctrl) {
			elem.bind('click', function(e) {
				// this part keeps it from firing the click on the document.
				e.stopPropagation();
			});
			$document.bind('click', function() {
				// magic here.
				scope.$apply(attr.clickAnywhereButHere);
			})
		}
	}
 }]);
