'use strict';

/**
 * @ngdoc directive
 * @name fantasyCricketApp.directive:activemenu
 * @description
 * # activemenu
 */

/*  This directives functionality need some below attributes in html dom:-
 obj currentrow =====> which gives the data of that row menu.

 if you are not using this currentrow data from html dom then you can send currentrow obj from js code as {id:val}
 By Ashutosh
 */
angular.module('fantasyCricketApp')
  .directive('activemenu', [function () {
    return {
      restrict: 'A',
      link: function (scope, element, attr) {
            element.bind('click', function () {
                var el;
                var arrlength = '';

                function handelActiveClass(el) {
                    arrlength = el.length;
                    for (var j = 0; j < arrlength ; j++) {
                        el[j].removeClassName('active');
                    }
                    if (attr != '' && attr != null) {
                        var val = attr.viewid;
                        if (document.querySelector('[viewid="' + val + '"]')) {
                            document.querySelector('[viewid="' + val + '"]').addClassName('active');
                        }
                    }
                }

                el = document.querySelectorAll('.menu-list li');
                handelActiveClass(el);
            });
        }
    };
  }]);
