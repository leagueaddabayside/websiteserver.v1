'use strict';

/**
 * @ngdoc directive
 * @name fantasyCricketApp.directive:deskpopupclick
 * @description
 * # deskpopupclick
 */
angular.module('fantasyCricketApp')
  .directive('deskpopupclick',['$rootScope' ,function ($rootScope) {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
          element.bind('click', function() {
               var menuId = attrs.menuId;
               var menuType = attrs.menuType;
               if(menuType !== 'popup') {
                  return false;
               }

               var popupOptions = {
                 template: '',
                 controller: '',
                 controllerAs: ''
               };
               switch(menuId) {
                  case 'myprofile':
                      popupOptions = {
                          template: '/fantasy_sports/app/views/desktop/d-myprofile.html',
                          popupSize: 'desk-sm',
                          controller: 'MyprofileCtrl',
		   			      controllerAs: 'myprofile',
                      };
                      break;
                  default:
				     break;
               }
               $rootScope.$broadcast('showDeskPopup', {options: popupOptions});
         });
      }
    };
  }]);
