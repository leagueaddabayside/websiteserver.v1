'use strict';

/**
 * @ngdoc overview
 * @name fantasyCricketApp
 * @description
 * # fantasyCricketApp
 *
 * Main module of the application.
 */
 angular
 .module('fantasyCricketApp', [
 	'ngAnimate',
 	// 'ngCookies',
 	// 'ngResource',
 	// 'ngRoute',
 	'ngDialog',
 	'ngSanitize',
 	'ngTouch',
 	'slick',
 	'ui.bootstrap',
    'ngFileUpload'
 	]);


 angular
 .module('fantasyCricketApp').run(['$rootScope', '$interval', 'devicedetectionservice', '$timeout', function ($rootScope, $interval, devicedetectionservice, $timeout) {

 		// Global cdnUrl to be used for static resources
		$rootScope.cdnUrl = "/fantasy_sports/app";

		$rootScope.mobHeaderTitle = "League Adda";

		$timeout(function(){
			$rootScope.$broadcast("tickingEvent");
		}, 100);

		$interval(function () {
			$rootScope.$broadcast("tickingEvent");
		}, 1000);

		removeUnusedCSS();

		function removeUnusedCSS() {
			var linkTagElem = document.querySelectorAll("link");
			var textToFind = devicedetectionservice.isDevice('DESKTOP') ? 'mobile' : 'desktop';
			for (var i = 0; i < linkTagElem.length; i++) {
				if (linkTagElem[i].href.indexOf(textToFind) > -1) {
					linkTagElem[i].href = "";
				}
			}
		}
        $rootScope.handleResponse = function (resp) {
            console.log("resp--->",resp);
        };
	}]);

Element.prototype.hasClassName = function (a) {
    return new RegExp("(?:^|\\s+)" + a + "(?:\\s+|$)").test(this.className);
};

Element.prototype.addClassName = function (a) {
    if (!this.hasClassName(a)) {
        this.className = [this.className, a].join(" ");
    }
};

Element.prototype.removeClassName = function (b) {
    if (this.hasClassName(b)) {
        var a = this.className;
        this.className = a.replace(new RegExp("(?:^|\\s+)" + b + "(?:\\s+|$)", "g"), " ");
    }
};
