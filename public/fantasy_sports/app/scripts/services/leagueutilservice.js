'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.leagueservice
 * @description
 * # leagueservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('leagueutilservice', ['$window', 'utilityservice', 'config', 'serviceName', '$rootScope','ngDialog', 'msgConstants', function ($window, utilityservice, config, serviceName, $rootScope,ngDialog, msgConstants) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var self = this;

        var checkLeagueResponse = null;

        self.setToLocalStorage = function(key, value){
            $window.localStorage[key] = angular.toJson(value);
        }

        self.getFromLocalStorage = function(key){
            if($window.localStorage[key] !== undefined && $window.localStorage[key] !== ""){
                return JSON.parse($window.localStorage[key]);    
            }
            else{
                return undefined;
            }            
        }

        self.getActiveMatch = function (matcharray) {
            var activeIndex = 0;
            var matchId = utilityservice.getFromLocalStorage('currentMatchId');
            // Check for matchId difference
            if(!!checkLeagueResponse && (checkLeagueResponse.matchId != matchId)){
                matchId = checkLeagueResponse.matchId;
            }

            var isMatchIdSaved = (matchId !== undefined && matchId !== "") ? true : false;
            
            var arrlen = matcharray.length;

            if(isMatchIdSaved){
                matchId = parseInt(matchId);
                activeIndex = self.getMatchIndexByKeyValue(matcharray, 'matchId', matchId);
                if(matcharray[activeIndex].status !== 'ACTIVE'){
                    activeIndex = self.getMatchIndexByKeyValue(matcharray, 'status', 'ACTIVE');
                }
            }
            else{
                activeIndex = self.getMatchIndexByKeyValue(matcharray, 'status', 'ACTIVE');
            }
            return activeIndex;
        }

        self.getMatchIndexByKeyValue = function(matcharray, key, value){
            var index = 0;
            for(var i = 0, len = matcharray.length; i < len; i++){
                if(matcharray[i][key] === value){
                    index = i;
                    break;
                }
            }
            return index;
        }

        self.getActiveMatchTeamId = function (matchId) {
            return 1;
        }

        self.updateTourTimer = function (tourArray, nextTimeTour) {
            //console.log('tourArray  nextTimeTour :: ', tourArray, nextTimeTour);
            for (var i = 0, length = tourArray.length; i < length; i++) {
                var currentTour = tourArray[i];
                currentTour.nextMatchTime = nextTimeTour[currentTour.tourId];
            }
        }

        self.updatePlayerPositionClass = function (playersArray) {
            var teamPreview = {
                bowlerCount: 0,
                batterCount: 0,
                alrdrCount: 0,
                wkCount: 0
            };

            for (var i = 0, length = playersArray.length; i < length; i++) {
                var player = playersArray[i];
                var playerClass = '';

                if (player.isCaptain === true) {
                    playerClass += ' sel-cap';
                }
                else if (player.isViceCaptain === true) {
                    playerClass += ' sel-v-cap';
                }

                if (player.isBonus === true) {
                    playerClass += ' bonus';
                }

                if (player.batter === true && player.bowler === false && player.wicketKeeper === false) {
                    teamPreview.batterCount = teamPreview.batterCount + 1;
                    playerClass += " bt" + teamPreview.batterCount;
                }
                else if (player.batter === false && player.bowler === true) {
                    teamPreview.bowlerCount = teamPreview.bowlerCount + 1;
                    playerClass += " bw" + teamPreview.bowlerCount;
                }
                else if (player.batter === true && player.bowler === true) {
                    teamPreview.alrdrCount = teamPreview.alrdrCount + 1;
                    playerClass += " alrdr" + teamPreview.alrdrCount;
                }
                else if (player.wicketKeeper === true) {
                    teamPreview.wkCount = teamPreview.wkCount + 1;
                    playerClass += " wk" + teamPreview.wkCount;
                }
                player.className = playerClass;
            }
            return playersArray;
        }

        self.getMatchStatus = function (status) {
            var matchstatus = '';
            switch (status) {
                case 'RUNNING':
                    matchstatus = 'In Progress';
                    break;
                case 'UNDER_REVIEW':
                    matchstatus = 'Under Review';
                    break;
                case 'CLOSED':
                    matchstatus = 'Completed';
                    break;
                case 'ABANDONED':
                    matchstatus = 'Abandoned';
                    break;
                default:
            }
            return matchstatus;
        }

        self.myTeamNamePopoup = function () {
            ngDialog.open({
                template: '/fantasy_sports/app/views/myteampopup.html',
                overlay: true,
                closeByDocument: false,
                closeByEscape: false,
                showClose: false,
                closeByNavigation: false,
                disableAnimation: true,
                controller: 'MyteamnameCtrl',
                controllerAs: 'myteamname',
                appendTo: '#mainWrapper',
                className: 'ngdialog-theme-popup desk-sm '
            });
        };

        self.prizeInfoPopup = function(data){
            ngDialog.open({
                template: '/fantasy_sports/app/views/prizeinfo.html',
                controller: "prizeInfoCtrl",
                controllerAs: "prizeinfo",
                data: data,
                overlay: true,
                closeByDocument: false,
                closeByEscape: false,
                closeByNavigation: true,
                disableAnimation: true,
                appendTo: '#mainWrapper',
                className: 'ngdialog-theme-popup prizeinfo-popup desk-sm',
                showClose: true
            });
        };

        self.checkJoinLeague = function (league, cb) {
            var params = {};
            params.leagueId = league.leagueId;
            utilityservice._postAjaxCall(config.gameNodeHost + '/checkJoinedLeague', params, function (response) {
                checkLeagueResponse = response.respData;
                cb(checkLeagueResponse);
                if (response.respCode == 100) {
                    leagueJoinConfirmPopup(checkLeagueResponse); 
                    return;
                }else {
                    showCheckLeagueValidation(response, checkLeagueResponse);
                }
            });
        }


        self.validateJoinLeague = function (league) {

            var params = {};
            params.leagueId = league.leagueId;
            params.matchId = league.matchId;
            // params.teamId = 1;
            utilityservice._postAjaxCall(config.gameNodeHost + '/checkJoinedLeague', params, function (response) {
                var checkResponse = response.respData;
                if (response.respCode == 100) {
                    leagueJoinConfirmPopup(checkResponse);
                    return;
                }else {
                    showCheckLeagueValidation(response, checkResponse);
                }
            });
        }

        function showCheckLeagueValidation(response, league) {
            if(response.respCode === 117){     //Check for Team not created
                createTeamPopup(league);
            }
            else if(response.respCode === 124){
                $rootScope.$broadcast("showPopup", {
                    type: "Info",
                    title: msgConstants.HEADER_INFO,
                    appendTo: '#mainWrapper',
                    msg: response.message,
                    popupSize: 'small',
                    showClose: false,
                    okButtonText: 'Ok',
                    okBtnCallback: function () {
                        var data = {
                            matchId: response.respData.matchId
                        }
                        $rootScope.$broadcast('fetchLeaguesEvent', data);
                    }
                });
            }
            else if(response.respCode === 129){     //Check for Insufficent balance
                var data = {
                    checkLeagueResponse: response.respData,
                    title: 'Oops! Not Enough Cash',
                    showCurrentBalance: true,
                }
                ngDialog.open({
                    template: '/fantasy_sports/app/views/addcash.html',
                    controller: "addCashCtrl",
                    controllerAs: "addcash",
                    overlay: true,
                    closeByDocument: false,
                    closeByEscape: false,
                    closeByNavigation: true,
                    disableAnimation: true,
                    appendTo: '#mainWrapper',
                    className: 'ngdialog-theme-popup addcash-popup desk-sm',
                    showClose: true,
                    resolve: {
                        leaguedata: function () {
                            return data;
                        }
                    }
                });
            }
            else{
                $rootScope.$broadcast("showPopup", {
                    type: "Info",
                    title: msgConstants.HEADER_INFO,
                    appendTo: '#mainWrapper',
                    msg: response.message,
                    popupSize: 'small',
                    showClose: false,
                    okButtonText: 'Ok',
                    okBtnCallback: function () {
                        var invCode = utilityservice.getUrlParameter("invCode");
                        if(invCode !== "" && invCode !== undefined){
                            // Reload page and removing the query string from url
                            //$window.location.href = utilityservice.getBaseUrl() + '/league';
                            $window.location.href = $window.location.href.split('?')[0];    

                        }
                    }
                });
            }
        }
        function createTeamPopup(league) {
            var msg = '<div>' +
                '<p class="fs18"><span class="clr2">Step 1. Create your Fantasy Team;</span></p>' +
                '<p class="fs18"><span class="clr2">Step 2. Join a league!</span></p>' +
                '</div>';
            var title = 'First things first';
            var okText = 'Create Team';

            $rootScope.$broadcast("showPopup", {
                type: "Info",
                title: title,
                appendTo: '#mainWrapper',
                msg: msg,
                popupSize: 'small',
                showClose: true,
                okButtonText: okText,
                okBtnCallback: function () {
                    window.location.href = utilityservice.getBaseUrl() + "/createteam/" + league.matchId + '/' + (league.totalTeams + 1) + '?invCode=' + league.leagueId;
                }
            });
        }

        function leagueJoinConfirmPopup(league) {

            var leaguedata = {
                league: league
            }

            ngDialog.open({
                template: '/fantasy_sports/app/views/leaguejoinconfirmpopup.html',
                overlay: true,
                closeByDocument: false,
                closeByEscape: false,
                showClose: true,
                closeByNavigation: false,
                disableAnimation: true,
                controller: 'leagueJoinConfirmCtrl',
                controllerAs: 'leaguejoin',
                appendTo: '#mainWrapper',
                className: 'ngdialog-theme-popup desk-sm ',
                resolve: {
                    leaguedata: function () {
                        return leaguedata;
                    }
                }
            });
        }
    }]);
