'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.session
 * @description
 * # session
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
  .service('session', ['localStorage', function (localStorage) {
    this.websiteAccessToken = localStorage.getItem('websiteAccessToken');
    this.depositeData = localStorage.getItem('depositeData');
    this.transactionID = localStorage.getItem('transID');
    this.transInitID = localStorage.getItem('transInitID');
    this._user = {};

    this.setScreenName = function(name){
        this._user.screenName = name;
    };

    this.getScreenName = function(){
        return this._user.screenName;
    };

    this.setTeamName = function(name){
        this._user.teamName = name;
    };

    this.getTeamName = function(){
        return this._user.teamName;
    };

    this.setUserDOB = function(name){
        this._user.userDob = name;
    };

    this.getUserDOB = function(){
        return this._user.userDob;
    };

    this.setUserStatus = function(status){
        this._user.userStatus = status;
    };

    this.getUserStatus = function(){
        return this._user.userStatus;
    };

    this.setUserBalance = function(balance){
        this._user.userBalance = balance;
    };

    this.getUserBalance = function(){
        return this._user.userBalance;
    };

    this.setUserID = function(id){
        this._user.userID = id;
    };

    this.getUserID = function(){
        return this._user.userID;
    };

    this.setUserName = function(name){
        this._user.userName = name;
    };

    this.getUserName = function(){
        return this._user.userName;
    };

    this.setWebsiteToken = function (token) {
        this.websiteAccessToken = token;
        try {
            localStorage.setItem("websiteAccessToken", token);
        } catch (e) {
            console.log(e);
        }
    };

    this.getWebsiteToken = function () {
        return this.websiteAccessToken;
    };


    this.setDepositeData = function(depData) {
        this.depositeData = depData;
        try {
            localStorage.setItem("depositeData", depData);
        } catch (e) {
            console.log(e);
        }
    };

    this.getDepositeData = function () {
        return this.depositeData;
    };

    this.setTransactionID = function(transID) {
        this.transactionID = transID;
        try {
            localStorage.setItem("transID", transID);
        } catch (e) {
            console.log(e);
        }
    };

    this.getTransactionID = function () {
        return this.transactionID;
    };

    this.setTransinitID = function(transID) {
        this.transInitID = transID;
        try {
            localStorage.setItem("transInitID", transID);
        } catch (e) {
            console.log(e);
        }
    };

    this.getTransinitID = function () {
        return this.transInitID;
    };
    /** Destroy session  **/
    this.destroy = function destroy() {
        this._user = {};
        this.setWebsiteToken(null);
        localStorage.removeItem("websiteAccessToken");
        this.removeDepositData();
    };

    this.removeDepositData = function() {
        this.setDepositeData(null);
        this.setTransactionID(null);
        this.setTransinitID(null);
        localStorage.removeItem("depositeData");
        localStorage.removeItem("transID");
        localStorage.removeItem("transInitID");
    };
  }]);
