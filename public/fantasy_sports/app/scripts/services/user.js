'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.user
 * @description
 * # user
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
  .service('user', ['msgConstants', function (msgConstants) {
        var that = this;
        that.numericPattern = /^[0-9]+$/;
        that.alphaNumericPattern = /^[a-zA-Z0-9_]+$/;
        that.aplphaNumericWithSpace = /^[a-zA-Z0-9]+[a-zA-Z0-9 ]+$/;
        that.restrictNumeric = /^([^0-9]*)$/;
        that.checkSpecialSymbol = /^([\w&\-]+)$/;
        that.emailPattern = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        that.checkPanCard = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
        that.mobilePattern = /^\d+$/;

        //Validations code---Ashu
        that.OnInputFieldValidation = function (fieldName,fieldValue, additionalInfo , callback) {
                var message = "";
                switch(fieldName){
                   case 'userName':{
                        if(fieldValue !== undefined && fieldValue !== null){
                             if(fieldValue.length === 0){
                                 message = msgConstants.REQUIRED_MESSAGE_1;
                             }else if(fieldValue.length < 2 || fieldValue.length > 20){
                                 message = msgConstants.ERR_USERNAME_1;
                             }else if (!that.aplphaNumericWithSpace.test(fieldValue)) {
                                 message = msgConstants.ERR_USERNAME_2;
                             }
                        }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                        }
                        break;
                   }

                    case 'userTeamName':{
                        if(fieldValue !== undefined && fieldValue !== null){
                             if(fieldValue.length === 0){
                                 message = msgConstants.REQUIRED_MESSAGE_1;
                             }else if(fieldValue.length < 2 || fieldValue.length > 20){
                                 message = msgConstants.ERR_TEAMNAME_1;
                             }else if (!that.alphaNumericPattern.test(fieldValue)) {
                                 message = msgConstants.ERR_TEAMNAME_2;
                             }
                        }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                        }
                        break;
                   }

                   case 'userEmail':
                   case 'loginEmail':
                   case 'userLoginEmail': {
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }else if(fieldValue.length > 40){
                                message = msgConstants.ERR_EMAIL_1;
                            }else if(!this.emailPattern.test(fieldValue)){
                                message = msgConstants.ERR_EMAIL_2;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'userMobile':{
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }else if(fieldValue.length !== 10){
                                message = msgConstants.ERR_MOBILE_1;
                            }else if (!this.mobilePattern.test(fieldValue)) {
                                message = msgConstants.ERR_MOBILE_2;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'userCity':{
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length < 3 || fieldValue.length >30){
                                message = msgConstants.ERR_CITY_1;
                            }else if (!this.restrictNumeric.test(fieldValue)) {
                                message = msgConstants.ERR_CITY_2;
                            }else if(!this.checkSpecialSymbol.test(fieldValue)){
                                message = msgConstants.ERR_SPECIAL_CHAR;
                            }
                       }
                       break;
                   }

                   case 'loginPassword':
                   case 'userLoginPassword':
                   case 'oldPass':  {
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'userPassword':
                   case 'newPass': {
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }else if(fieldValue.length < 5 || fieldValue.length > 15){
                                message = msgConstants.ERR_PASSWORD_1;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'confirmPass':  {
                       if(fieldValue !== undefined){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }else if(fieldValue !== additionalInfo){
                                message = msgConstants.ERR_PASSWORD_2;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'userPAN': {
                       if(fieldValue !== undefined){
                            if(fieldValue.length === 0){
                                message = msgConstants.ERR_PAN_1;
                            }else if(!this.checkPanCard.test(fieldValue)){
                                message = msgConstants.ERR_PAN_2;
                            }
                       }else{
                            message = msgConstants.ERR_PAN_1;
                       }
                       break;
                   }
               }
               if(message !== '') {
                     callback({resCode : 101,message:message});
               }else {
                     callback({resCode : 100});
               }
        };
  }]);
