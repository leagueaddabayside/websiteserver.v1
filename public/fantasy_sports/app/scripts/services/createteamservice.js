'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.createteamservice
 * @description
 * # createteamservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('createteamservice', ['utilityservice','config', 'serviceName', function (utilityservice,config, serviceName) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var self = this;

        self.getMatchPlayers = function(params, callback){
            utilityservice._postAjaxCall(config.gameNodeHost+serviceName.FETCH_TOUR_MATCH_PLAYERS, params, function (response) {
                callback(response);
            });
        };

        self.saveMyTeam = function(params, callback){
            utilityservice._postAjaxCall(config.gameNodeHost+serviceName.FETCH_SAVE_TEAM, params, function(response){
                callback(response);
            });
        };

        self.getMySavedPlayers = function(params, callback){
            utilityservice._postAjaxCall(config.gameNodeHost+serviceName.FETCH_TEAM_MATCH_PLAYERS, params, function(response){
                callback(response);
            });
        };
        
        self.updateUserTeam = function(params, callback){
            utilityservice._postAjaxCall(config.gameNodeHost+serviceName.UPDATE_USER_TEAM, params,function(response){
               callback(response); 
            });
        }

    }]);