'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.depositeservice
 * @description
 * # depositeservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
  .service('depositeservice', ['$window', '$rootScope', 'session', 'config', 'serviceName', 'utilityservice', '$timeout', 'msgConstants', function ($window, $rootScope, session, config, serviceName, utilityservice, $timeout, msgConstants) {
        var that = this;
        that.openCard = function(token, merchantCode, txnIdentifier, depositAmt) {
            //console.log(token, merchantCode, txnIdentifier);
            if(token==undefined || token=='' || merchantCode==undefined || merchantCode=='' || txnIdentifier==undefined || txnIdentifier==''){
              $rootScope.$broadcast("showPopup", {
                    type: "Info",
                    title: msgConstants.HEADER_INFO,
                    appendTo:'#mainWrapper',
                    msg: msgConstants.MSG_REQUEST_FAILED,
                    popupSize: 'small',
                    okButtonText: 'OK'
              });
              return;
            }

            var configJson = {
                'tarCall':false,
                'features':{
                  'showPGResponseMsg':true
                },
                'consumerData':{
                  'deviceId':'web',
                  'authKey':'123abc123',
                  'token':token,
                  'returnUrl': config.websiteNodeHost + "/myaccount",
                  'responseHandler': that.handleResponse,
                  'paymentMode': 'all',
                  'merchantId': merchantCode,
                  'txnId': txnIdentifier,
                  'items': [{ 'itemId' : 'test', 'amount' : depositAmt, 'comAmt':'0'}]
                }
              };
            new Card(configJson).init();
        };

        that.afterPayment = function() {
            var queryStr = window.location.search;
            if (queryStr.indexOf('txthdntpslmrctcd=') > -1 && queryStr.indexOf('txthdnMsg=') > -1) {
                var token = session.getDepositeData();
                new Card({
                    'tarCall': true,
                    'features': {
                        'showPGResponseMsg': true
                    },
                    'consumerData': {
                        'deviceId': 'web',
                        'authKey': '123abc123',
                        'token': token,
                        'returnUrl': config.websiteNodeHost + "/myaccount",
                        'responseHandler': that.handleResponse,
                        'paymentMode': 'all'
                    }
                }).init();
            }
        };

        that.handleResponse = function(res) {
            if (typeof res != 'undefined' && typeof res.paymentMethod != 'undefined' && typeof res.paymentMethod.paymentTransaction != 'undefined' && typeof res.paymentMethod.paymentTransaction.statusCode != 'undefined' && res.paymentMethod.paymentTransaction.statusCode == '0300') {
                //console.log('merchanttxnIdentifer--->', session.getTransactionID());
                //console.log("uid-->",session.getUserID());
                //console.log("initTxnId--->",session.getTransinitID());
                var params = {};
                params.userId = session.getUserID();
                params.merchanttxnIdentifer = session.getTransactionID();
                params.initTxnId = session.getTransinitID();
                params.txnType = "DEPOSIT";
                utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.DEPOSITE_FINISH, params, function (response) {
                    if(response.respCode == 100){
                        session.removeDepositData();
                        $timeout(function(){
                            $window.location.reload();
                            window.location.search = "";
                        },5000);
                    }else{
                        console.log("Server ERROR:", response.message);
                    }
                });
            }else{
                console.log('Failed response', res);
                //window.location.search = "";
            }

        };
  }]);
