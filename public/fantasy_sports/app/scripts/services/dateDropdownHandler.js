'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.createteamservice
 * @description
 * # createteamservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('dateDropdownHandler', ['utilityservice','config', 'constants', function (utilityservice,config, constants) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var that = this;
        var now = new Date();
        var startYr = 1920;
        var endYr = now.getFullYear()-18;
        var yearRange = [];
        var dayRange = [];

        that.initilizeYears = function(callback){
            yearRange = [];
            yearRange.push("YY");
            for (var i = startYr; i <= endYr; i++) {
                  yearRange.push(i);
            }
            callback(yearRange);
        };

        that.initilizeDays = function(callback){
            dayRange = [];
            dayRange.push('DD');
            for (var j = 1; j <= 31; j++) {
                if(j<10){
                      j = String('0'+j);
                }
                j = String(j);
                dayRange.push(j);
            }
            callback(dayRange);
        };

        that.getMonths = function(selectedYear, callback){
            var monthList = [];
            var monthArr = constants.MONTH_LIST;

            //Code to show month list upto current month of present year.
            if (selectedYear==now.getFullYear()) {
                for(var i=0; i<13; i++){
                      var key = that.getKey(monthArr[i]);
                      if(key != 'MM') {
                            if(key > now.getMonth()+1) {
                                  monthArr.splice(i,13-i);
                                  monthList = [];
                                  monthList = monthArr;
                                  break;
                            }
                      }
                }
            }else {
                monthList = constants.MONTH_LIST;
            }
            callback(monthList);
        };

        that.getKey = function(month) {
            return Object.keys(month)[0];
        };

        that.getDays = function(selectedYear, selectedMonth, callback){
            var dateRange = [];
            if(selectedYear==now.getFullYear() && selectedMonth ==(now.getMonth()+1)) {
                  dateRange.push('DD');
                  for(var i=1; i<now.getDate()+1; i++){
                        if(i<10) {
                            i = String('0'+i);
                        }
                        i = String(i);
                        dateRange.push(i);
                  }
            }else {
                  var days = 31;
                  var year = selectedYear;
                  var month = String(selectedMonth);
                  if(month==2) {
                        if((year % 4) === 0 && ((year % 100) !== 0 || (year % 400) === 0)) {
                              days = 29;//Leap Year
                        }else {
                              days = 28;
                        }
                  }else if (month==4 || month==6 || month==9 || month==11) {
                        days = 30;
                  }
                  dateRange = [];
                  dateRange.push('DD');
                  for(var j=1; j<days+1; j++){
                        if(j<10) {
                            j = String('0'+j);
                        }
                        j = String(j);
                        dateRange.push(j);
                  }
            }
            callback(dateRange);
        };

}]);