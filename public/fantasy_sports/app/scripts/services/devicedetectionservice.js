'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.devicedetectionservice
 * @description
 * # devicedetectionservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('devicedetectionservice', [function () {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var self = this;

        /**
         *
         * @param {type} deviceName //MOBILE/TABLET/DESKTOP
         * @returns {devicedetectionservice_L11.isDevice.result}
         */

        /* to blur elements used to open keyboard - when click on others #4565*/
        self.blurElement = function(elem){
            if(elem && elem.tagName){
                if (!((elem.tagName.toUpperCase() === 'INPUT' && ( elem.type.toUpperCase() === 'TEXT' || elem.type.toUpperCase() === 'PASSWORD' || elem.type.toUpperCase() === 'FILE' || elem.type.toUpperCase() === 'SEARCH' || elem.type.toUpperCase() === 'EMAIL' || elem.type.toUpperCase() === 'NUMBER' || elem.type.toUpperCase() === 'DATE' || elem.type.toUpperCase() === 'TEL' )) || elem.tagName.toUpperCase() === 'TEXTAREA' || elem.tagName.toUpperCase() === 'SELECT')) {
                    elem.blur();
                    if(document.activeElement){
                        document.activeElement.blur();
                    }
                }
            }
        };

        self.isDevice = function isDevice(deviceName) {
            if (currentDeviceName.toUpperCase() === deviceName.toUpperCase()) {
                return true;
            } else {
                return false;
            }
        };

        self.getPlatform = function () {
            return currentPlatform;
        };

        self.isIphone = function(){
            return !!navigator.userAgent.match(/iPhone/i);
        };

        self.getOrientation = function(){
            var angle = null;
            var orientation = null;
            if(typeof window.orientation !== "undefined")
                angle = window.orientation;
            else if(typeof window.screen.orientation !== "undefined")
                angle = window.screen.orientation.angle;
            else
                return ""; // return empty in case safari desktop where screen.angle is not available

            if (Math.abs(angle) === 90) {
                orientation = "landscape";
            } else {
                orientation = "portrait";
            }

            return orientation;
        };

        self.getOs = function () {
            var ua = navigator.userAgent.toLowerCase();
            var isAndroid = ua.indexOf("android") > -1;
            var isWindows = ua.indexOf("windows") > -1;
            var result = false;
            if (isAndroid && !isWindows) {
                result = 'android';
            } else {
                if (ua.match(/(iphone|ipod|ipad)/) && !isWindows) {
                    result = 'ios';
                }
            }
            if (currentPlatform === "NA") {
                if (navigator.appVersion.indexOf("Win") !== -1) {
                    result = 'winDesk';
                } else if (navigator.appVersion.indexOf("Linux") !== -1) {
                    result = 'linDesk';
                } else if (navigator.appVersion.indexOf("Mac") !== -1) {
                    result = 'macDesk';
                }
            }

            return result;
        };
        self.isIosChrome = function () {
            var is_chrome = navigator.userAgent.match('CriOS');
            if (is_chrome === null) {
                return false;
            } else {
                return true;
            }
        };
        self.getDevice = function () {
            var isiPad = navigator.userAgent.match(/iPad/i);
            if (isiPad != null) {
                result = 'tablet';
                return result;
            }

            var screenDim = {};
            if (screen.width > screen.height) {
                screenDim = {
                    'width': screen.width,
                    'height': screen.height
                };
            } else {
                screenDim = {
                    'width': screen.height,
                    'height': screen.width
                };
            }
            var result = '';
            var isFullScreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.documentElement.webkitRequestFullScreen;
            if ((!(isFullScreenEnabled)) && (self.getOs() === 'android')) {
                screenDim.width = screenDim.width / window.devicePixelRatio;
                screenDim.height = screenDim.height / window.devicePixelRatio;
            }
            if (screenDim.width >= 768 && !navigator.userAgent.match(/Windows Phone/i)) {
                result = 'tablet';
            } else {
                result = 'mobile';
            }
            if (getDevicePlatform() === 'NA') {
                result = 'desktop';
            }
            return result;

        };

        var getDevicePlatform = function () {
            var userAgent = navigator.userAgent;
            var platform = 'NA';
            if (userAgent.match(/Android/i)) {
                platform = 'Android';
            } else if (userAgent.match(/BlackBerry/i)) {
                platform = 'BlackBerry';
            } else if (userAgent.match(/iPhone|iPad|iPod/i)) {
                platform = 'iOS';
            } else if (userAgent.match(/Opera Mini/i)) {
                platform = 'Opera';
            } else if (userAgent.match(/IEMobile/i)) {
                platform = 'Windows';
            }
            return platform;
        };

        //function to full screen mobile devices in android small device
        self.launchFullscreen = function(element) {
            if(element.requestFullscreen) {
                element.requestFullscreen();
            } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if(element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            } else if(element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        };

        //function to full screen iphone devices.
        self.showSwipeToFullScreen = function(){
            var is_chrome = self.isIosChrome();
            if(!is_chrome){
                if((window.innerHeight <= 320)){
                    if(window.innerHeight < 320){
                        var adjustScreen = window.setTimeout(function(){
                            window.scrollTo(0,0);
                            window.clearTimeout(adjustScreen);
                            adjustScreen = null;
                        },500);
                        return true;
                    }
                }
            }
            window.scrollTo(0,0);
            return false;
        };

        var currentDeviceName = self.getDevice();
        var currentPlatform = getDevicePlatform();
        //console.log('currentDeviceName=',currentDeviceName,'=currentPlatform=',currentPlatform);

    }]);