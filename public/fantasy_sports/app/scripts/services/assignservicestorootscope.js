'use strict';

(function (angular) {

  function assignServicesToRootScope($rootScope, devicedetectionservice){
    $rootScope.deviceDetection = devicedetectionservice;
    //$rootScope.session = session;
    //$rootScope.auth = auth;
  }

  // Inject dependencies
  assignServicesToRootScope.$inject = ['$rootScope', 'devicedetectionservice'];

  // Export
  angular
    .module('fantasyCricketApp')
    .run(assignServicesToRootScope);

})(angular);
