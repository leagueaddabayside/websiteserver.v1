'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.promotionsservice
 * @description
 * # promotionsservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('promotionsservice', ['utilityservice','config', 'serviceName', function (utilityservice,config, serviceName) {

        var self = this;
        
        self.getPromotions = function(params, callback){
            utilityservice._postAjaxCall(config.websiteNodeHost+serviceName.PROMOTIONS, params, function (response) {
                //console.log(response,"promtions");
                if(response.respCode == 100){
                    callback(response.respData);
                }
                else{
                    // hide the loader
                }
                
            });
        };


            /*var response = {
                "status":true,
                "error":"",
                "data":{
                    "game_promotion":[
                        {
                            "id":"lobby-promo-1",
                            "name":"lobby promo 1",
                            "url":"",
                            "fields":{
                                "small_content":{"text":"zsfasff dfwgsdgfsd sdfafaf"},
                                "page_content":{"text":"This is a Sample Text for Lobby promotion 1"},
                                "image":{"url":"http://localhost:3001/fantasy_sports/app/assets/images/banner.png"},
                                "page_content":{
                                    "text": "<div class ='sample'>sample</div>"
                                },
                                "page_css":{
                                    "text": "<style>.sample{ background: green;}</style>"
                                }
                            }
                        },
                        {
                            "id":"lobby-promo-2",
                            "name":"lobby promo 2",
                            "url":"",
                            "fields":{
                                "small_content":{"text":"asdfsf asfaf swffafas"},
                                "page_content":{"text":"This is a Sample Text for Lobby promotion 1"},
                                "image":{"url":"http://localhost:3001/fantasy_sports/app/assets/images/banner.png"},
                                "page_content":{
                                    "text": "<div class ='sample'>sample</div>"
                                },
                                "page_css":{
                                    "text": "<style>.sample{ background: green;}</style>"
                                }
                            }
                        }
                    ]
                }
            }
            callback(response);
        };*/

    }]);