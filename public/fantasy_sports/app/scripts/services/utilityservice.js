'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.utilityservice
 * @description
 * # utilityservice
 * Service in the fantasyCricketApp.
 */
 angular.module('fantasyCricketApp')
 .service('utilityservice', ['$rootScope','$http', '$timeout', '$location', '$window', 'session', 'msgConstants',  function ($rootScope, $http, $timeout, $location, $window , session, msgConstants) {
	// AngularJS will instantiate a singleton by calling "new" on this function
	var self = this;

	var url = {
		protocol: $location.protocol(),
		host: $location.host(),
		port: $location.port()
	};

	var baseUrl = url.protocol + '://' + url.host + ':' + url.port;

	self._postAjaxCall = function(actionName,param,callback){
             self.hideLoader();
             self.showLoader();
			 var tS = new Date().getTime();
			 if (typeof param === "undefined") {
				param={};
			 }
			 param['ts'] = tS;
             var token = session.getWebsiteToken();
             if(token !== null && token !== undefined){
                   param['token'] = token;
             }

			$http({
				method: 'POST',
				dataType: 'json',
				data: param,
				async : false,
				url: actionName,
                timeout: 15000,
				crossDomain: true
			 }).then(function successCallback(result) {
				// this callback will be called asynchronously
                 self.hideLoader();
                 if(result.data.respCode == 108){
                       $window.location.href = baseUrl + "/";
                 }
                 else if(result.data.respCode == 134){	// Case handling if user send join league request after match has started
                       $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: result.data.message,
                        popupSize: 'small',
                        okButtonText: 'OK',
                        okBtnCallback: function () {
                           $window.location.href = baseUrl + "/league";
                        }
                    });
                 }
                 else{
                       callback(result.data);
                 }
			  }, function errorCallback(e) {
				// called asynchronously if an error occurs
                self.hideLoader();
				console.log(e.message);
			  });
		};


		self._getAjaxCall = function(actionName, param, callback){
             self.hideLoader();
             self.showLoader();
             var token = session.getWebsiteToken();
             if(token !== null && token !== undefined){
                   param['token'] = token;
             }
			 $http({
				method: 'GET',
				dataType: 'json',
				params: param,
				url: actionName,
                timeout: 5000,
				crossDomain: true
			 }).then(function successCallback(result) {
				// this callback will be called asynchronously
                 self.hideLoader();
                 if(result.data.respCode == 108){
                       $window.location.href = baseUrl + "/";
                 }else{
				       callback(result.data);
                 }
			  }, function errorCallback(e) {
				// called asynchronously if an error occurs
                self.hideLoader();
				console.log(e.message);
			  });
		};

		self.showLoader = function(opts){
			var defaults = {
				text: 'Loading...',
				delay: 0
			};

			var options = angular.extend({}, defaults, opts);
			var loader = document.querySelector('#loaderDom');
			if(loader !== undefined && loader !== null){
				document.querySelector('#loaderDom .loading-label').innerHTML = options.text;
				if(options.delay){
					$timeout(function(){
						loader.style.display = 'block';
					},options.delay);
				}else{
					loader.style.display = 'block';
				}
			}else{
				console.log('could not detect loader DOM');
			}
		};

		self.hideLoader = function(){
			var loader = document.querySelector('#loaderDom');

			if(loader !== undefined && loader !== null){
				loader.style.display = 'none';
			}else{
				console.log('could not detect loader DOM');
			}
		};

		self.getBaseUrl = function(){
			return baseUrl;
		};

		self.isEmptyObject = function(arg) {
			for (var item in arg) {
				return false;
			}
			return true;
		};

		self.getUrlParameter = function(param, dummyPath) {
		    var sPageURL = dummyPath || window.location.search.substring(1),
		        sURLVariables = sPageURL.split(/[&||?]/),
		        res;

		    for (var i = 0; i < sURLVariables.length; i += 1) {
		        var paramName = sURLVariables[i],
		            sParameterName = (paramName || '').split('=');

		        if (sParameterName[0] === param) {
		            res = sParameterName[1];
		        }
		    }
		    return res;
		}

		self.showToastError = function(dataObj){
			var defaults = {
				title: '',
				message: ''
			};
			if(dataObj !== undefined && dataObj !== null && !self.isEmptyObject(dataObj)){
				var options = angular.extend({}, defaults, dataObj);
				var elem =  document.querySelector(".mob-toast-error");
				var head =  document.querySelector(".mob-toast-error h3");
				var body =  document.querySelector(".mob-toast-error p");
				head.innerHTML = options.title;
				body.innerHTML = options.message;
				angular.element(elem).hide().show().delay(2000).fadeOut("slow");
			}
		};

        $rootScope.$on("setActiveHeaderMenu", function(){
             var a = window.location.href,
             b = a.lastIndexOf("/");
             self.setActiveMenu(a.substr(b + 1));
        });

        self.setActiveMenu = function(viewID){
                var el;
                var arrlength = '';

                function handelActiveClass(el) {
                    arrlength = el.length;
                    for (var j = 0; j < arrlength ; j++) {
                        el[j].removeClassName('active');
                    }
                    if (viewID != '' && viewID != null) {
                        if (document.querySelector('[viewid="' + viewID + '"]')) {
                            document.querySelector('[viewid="' + viewID + '"]').addClassName('active');
                        }
                    }
                }

                el = document.querySelectorAll('.menu-list li');
                handelActiveClass(el);
        };

        self.isPresentInURL = function(text){
        	var url = window.location.href;
        	if(url.indexOf(text) !== -1){
        		return true;
        	}
        	else{
        		return false;
        	}
        }

        self.updateBalance = function(amount){
        	var selectors = document.querySelectorAll('.userBalance');
        	for(var i = 0, len = selectors.length; i < len; i++){
        		selectors[i].innerHTML = amount;
        	}
        }

        self.setToLocalStorage = function(key, value){
            $window.localStorage[key] = angular.toJson(value);
        }

        self.getFromLocalStorage = function(key){
            if($window.localStorage[key] !== undefined && $window.localStorage[key] !== ""){
                return JSON.parse($window.localStorage[key]);    
            }
            else{
                return undefined;
            }            
        }

        self.removeFromLocalStorage = function(key){
        	localStorage.removeItem("currentMatchId");
        }

	}]);