'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.leagueservice
 * @description
 * # leagueservice
 * Service in the fantasyCricketApp.
 */
 angular.module('fantasyCricketApp')
 .service('leagueservice', ['$rootScope', 'utilityservice', 'config', 'serviceName', 'msgConstants', function ($rootScope, utilityservice, config, serviceName, msgConstants) {
	// AngularJS will instantiate a singleton by calling "new" on this function
	var self = this;

	self.getLeagues = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_LEAGUES, params, function (response) {
			callback(response);
		});
	};

	self.getTours = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_TOURS, params, function (response) {
			if(response.respCode === 100){
				var toursArray = response.respData;
				callback(toursArray);
			}else{
				console.log('Error in Fetching tours', response.message);
			}
		});
	};

	self.getMatches = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_MATCH_LIST, params, function (response) {
			if(response.respCode === 100) {
				var  matchesArray = response.respData.matchList;
				callback(matchesArray,response.respData.nextTimeTour);
			}else{

			}

		});
	};

	self.getLeaguePrizeInfo = function(params, callback){
		utilityservice.showLoader();
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_LEAGUE_PRIZE_INFO, params, function (response) {
			if (response.respCode === 100) {
                callback(response.respData);
            }else {
                console.log('Error in fetching League Prize INfo ::::');
            }
            utilityservice.hideLoader();
		});
	};

	self.getJoinedLeagues = function(params, callback){
		utilityservice.showLoader();

		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_USER_JOINED_LEAGUES, params, function (response) {
			if(response.respCode === 100){
				var joinedLeaguesArray = response.respData;
				callback(joinedLeaguesArray);
			}else{
				console.log('Error in Fetching tours', response.message);
			}
			utilityservice.hideLoader();
		});
	};

	self.getLeagueMembers = function(params, callback){
		utilityservice.showLoader();
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_JOINED_LEAGUE_TEAMS, params, function (response) {
			if(response.respCode === 100){
				var leagueMembersArray = response.respData;
				callback(leagueMembersArray);
			}else{
				console.log('Error in Fetching tours', response.message);
			}
			utilityservice.hideLoader();
		});
	};

	self.getTourMatchInfo = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_TOUR_MATCH_INFO, params, function (response) {
			callback(response);
		});
	};

	self.getLeagueInfo = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_LEAGUE_INFO, params, function (response) {
			callback(response);
		});
	};

	self.joinLeague = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_TOUR_MATCH_INFO, params, function (response) {
			callback(response);
		});
	};

	self.getTeamInfo = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_TEAM_INFO, params, function (response) {
			if(response.respCode === 100) {
				callback(response.respData);
			}else{

			}
		});
	};

	self.updateUserJoinedTeam = function(params, callback){
		utilityservice.showLoader();
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.UPDATE_USER_JOINED_TEAM, params, function (response) {
			if(response.respCode === 100) {
				callback(response);
			}else if(response.respCode === 118){
				$rootScope.$broadcast("showPopup", {
					type: "Info",
					title: msgConstants.HEADER_INFO,
					appendTo:'#mainWrapper',
					msg: response.message,
					popupSize: 'small',
					showClose: false,
					okButtonText: 'Ok'
				});
			}
			else{

			}
			utilityservice.hideLoader();
		});
	};

	self.getOpponentTeamInfo = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_OPPONENT_TEAM_INFO, params, function (response) {
			if(response.respCode === 100) {
				callback(response.respData);
			}else{

			}
		});
	};

}]);
