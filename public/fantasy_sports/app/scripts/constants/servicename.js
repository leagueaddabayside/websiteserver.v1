'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.serviceName
 * @description
 * # serviceName
 * Constant in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .constant('serviceName', {

    /* WEBSITE SERVICE */
    "USER_LOGIN" : "/users/login",
    "USER_LOGOUT" : "/users/logout",
    "USER_SIGNUP" : "/users/create",
    "CHANGE_PASSWORD" : "/users/changePassword",
    "MYACCOUNT_INFO" : "/payments/users/wallet",
    "GETUSER_PROFILE" : "/users/getbyid",
    "UPDATE_PROFILE" : "/users/update",
    "UPDATE_TEAMNAME" : "/users/updateTeam",
    "DEPOSITE_INIT_TEKPROCESS" : "/payments/init",
    "DEPOSITE_INIT_PAYU" : "/payments/init/payu",
    "DEPOSITE_FINISH" : "/payments/deposit",
    "WITHDRAW" : "/payments/withdrawl",
    "VERIFY_EMAIL" : "/users/verifyEmailId",
    "VERIFY_MOBILE" : "/users/verifyMobileNumber",
    "OTP_EMAIL" : "/users/validate/emailotp",
    "OTP_MOBILE" : "/users/validate/mobileotp",
    "FORGOT_PASSWORD_INIT" : "/users/userForgotPasswordInitiate",
    "FORGOT_PASSWORD" : "/users/userForgotPassword",
    "ADD_BANK_ACCOUNT" : "/users/bank/add",
    "PAN_UPLOAD" : "/users/pancard/upload",
    "TXN_HISTORY" : "/payments/txn/history",
    "PAN_DETAILS" : "/users/pancard",
    "BANK_DETAILS" : "/users/bankac",
    "REQUEST_TXN_HISTORY": "/payments/txn/history/download",

    /* GAME SERVICES */
    "FETCH_LEAGUES" : "/fetchLeagues",
    "FETCH_TOURS" : "/fetchTours",
    "FETCH_MATCH_LIST" : "/fetchMatchList",
    "FETCH_LEAGUE_PRIZE_INFO": "/fetchLeaguePrizeInfo",
    "FETCH_JOINED_LEAGUE" : "/fetchJoinedLeagues",
    "FETCH_LEAGUE_MEMBERS" : "/fetchLeagueMembers",
    "FETCH_TOUR_MATCH_INFO" : "/fetchTourMatchInfo",
    "FETCH_LEAGUE_INFO" : "/fetchLeagueInfo",
    "FETCH_TOUR_MATCH_PLAYERS" : "/fetchTourMatchPlayers",
    "FETCH_SAVE_TEAM":"/saveTeam",
    "FETCH_MY_SAVED_TEAM":"/getTeamInfo",
    "FETCH_TEAM_MATCH_PLAYERS":"/fetchTeamMatchPlayers",
    "UPDATE_USER_TEAM": "/updateUserTeam",
    "FETCH_USER_JOINED_LEAGUES" : "/fetchUserJoinedLeagues",
    "FETCH_JOINED_LEAGUE_TEAMS" : "/fetchJoinedLeagueTeams",
    "JOIN_LEAGUE" : "/userJoinedLeague",
    "FETCH_TEAM_INFO" : "/getTeamInfo",
    "UPDATE_USER_JOINED_TEAM" : "/updateUserJoined",
    "FETCH_OPPONENT_TEAM_INFO" : "/findOpponentTeamInfo",
    "PROMOTIONS": "/promotion"
});
