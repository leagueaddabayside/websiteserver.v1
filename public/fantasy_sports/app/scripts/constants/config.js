'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.config
 * @description
 * # config
 * Constant in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .constant('config',{
    "websiteNodeHost" : "http://18.217.53.138:3000",
    "gameNodeHost" : "http://18.217.53.138:4000"
});
