'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.msgConstants
 * @description
 * # msgConstants
 * Constant in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .constant('msgConstants', {

    /* Signup page validation and error messages */
    REQUIRED_MESSAGE : "Please fill out this field",
    REQUIRED_MESSAGE_1 :"This field is required",
    PASSWORD_CHANGE_SUCCESSFULLY: "Your password has been changed successfully",
    ERR_USERNAME_1: "Atleast 2-20 characters required",
    ERR_USERNAME_2: "Alphanumeric characters only",
    ERR_TEAMNAME_1: "Min. 2 & max. 20 characters allowed",
    ERR_TEAMNAME_2: "Alphanumeric characters only",
    ERR_PASSWORD_1: "Min. 5 & max. 15 characters allowed",
    ERR_PASSWORD_2: "Passwords do not match",
    ERR_PASSWORD_MATCH : "New password can't be same as the old one",
    ERR_EMAIL_1: "Max. 40 characters allowed",
    ERR_EMAIL_2: "Enter valid email id",
    ERR_MOBILE_1: "Enter 10 digits only",
    ERR_MOBILE_2: "Invalid mobile number",
    ERR_SPECIAL_CHAR: "No special symbols allowed except '-' and '&'",
    ERR_CITY_1: "Min. 2 & max. 30 characters allowed",
    ERR_CITY_2: "Alphabetic characters only",
    ERR_PAN_1: "Please fill your PAN details",
    ERR_PAN_2: "PAN details are not valid",
    ERR_PAN_3: "Please upload a PAN document",
    ERR_PAN_4: "PAN number is required",
    ERR_PAN_5: "Please upload either the image or PDF file",
    ERR_WITHDRAW_1: "Sorry! You can withdraw a min. of Rs.200",
    ERR_WITHDRAW_2: "Sorry! You can withdraw a max. of Rs.2,00,000",
    ERR_INSUFFICIENT_WINNING : "You don't have sufficient winnings",
    ERR_BANK_NAME: "Bank name is required",
    ERR_BRANCH_NAME: "Branch name is required",
    ERR_IFSC_CODE: "IFSC code is required",
    ERR_BANK_ACCOUNT: "Bank account number is required",
    ERR_STATE: "Please select your state",
    ERR_FULLNAME : "Full name is required",
    ERR_FULLNAME_2 : "Name should be an alphanumeric text",
    ERR_FULLNAME_3 : "Name should be 2-20 characters",
    ERR_STATE_CHECK : "You must confirm that you do not belong to Assam or Odisha",
    ERR_TNC : "You must accept the T&amp;C*",
    VERIFY_EMAIL_MOBILE : "Please verify your mobile / email id first",
    ERR_LARGE_FILE : "The file size can not exceed 4 MB",

    ERR_SEL_MAX_PLYR_SAME_TEAM : "Max 7 players allowed from one team",
    ERR_SEL_MAX_ALRDR :"Max 3 allrounders allowed (1 bonus opt.)",
    ERR_SEL_MAX_BATTER : "Max 5 batsman allowed (1 bonus opt.)",
    ERR_SEL_MAX_BOWLER : "Max 5 bowlers allowed (1 bonus opt.)",
    ERR_SEL_MAX_WK : "Max 1 wicket keeper allowed (1 bonus opt.)",
    ERR_SEL_MAX_CREDITS : "Insufficient Credits left",
    ERR_SEL_MIN_BATTER : "Min 3 batsman must be selected",
    ERR_SEL_MIN_BOWLER : "Min 3 bowlers must be selected",
    ERR_SEL_MIN_WK : "Min 1 wicket keeper must be selected",
    ERR_SEL_MIN_ALRDR : "Min 1 allrounder must be selected",
    ERR_SEL_SAME_C_VC : "Captain and vice captain can't be the same",
    ERR_SEL_CAP_ALRDY_TKN : "Captain is already selected",
    ERR_SEL_VCAP_ALRDY_TKN : "Vice captain is already selected",
    ERR_SEL_TEAM_PLAYER_EXCEED : "Max 11 players allowed",
    ERR_BONUS_SELECTED : "Please reset & reselect the player currently marked as bonus & try again",
    ERR_OTP : "Otp should be of 6 digits",
    DOWNLOAD_TXN_HISTORY : "Are you sure you want to download the transaction history?",      // Confirmation Popup
    CLEAR_TEAM: "Are you sure you want to clear your current selection?",
    TEAM_DETAILS_SAVED: "Team details saved",
    TRANSACTION_FAILURE_1 : "Deposit failed ! As your profile was not updated",
    TRANSACTION_FAILURE_2 : "Payment failed ! Please go ahead & try again. Contact us if the payment failed has been wrongly deducted from your bank account.",
    TRANSACTION_FAILURE_3 : "Max transaction limit: Rs.25000",
    TRANSACTION_FAILURE_4 : "You need to add a minimum of Rs.1",
    TRANSACTION_FAILURE_5 : "Please update your profile first",

    MSG_SIGNUP_SUCCESS: "Successfully signed up !",
    MSG_PROFILE_UPDATED_SUCCESS: "Your profile has been successfully updated",
    MSG_PANCARD_SAVED_SUCCESS: "Your PAN Card details have been saved successfully",
    MSG_BANK_DETAIL_SAVED_SUCCESS: "Your bank details have been saved successfully",
    MSG_REQUEST_FAILED: "Request failed please try again",
    ERR_FB_REGISTRATION : "Registration failed as you didn't allow us to access the required information of your Gmail/Facebook account.",

    HEADER_CONFIRMATION : "CONFIRMATION",
    HEADER_INFO : "INFO",
    HEADER_WITHDRAW_CASH : "WITHDRAW YOUR CASH",
});
