'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.constants
 * @description
 * # constants
 * Constant in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .constant('constants', {
     MONTH_LIST : [
                   {'MM':'MM'},
                   {'01':'Jan'},
                   {'02':'Feb'},
                   {'03':'Mar'},
                   {'04':'Apr'},
                   {'05':'May'},
                   {'06':'Jun'},
                   {'07':'Jul'},
                   {'08':'Aug'},
                   {'09':'Sep'},
                   {'10':'Oct'},
                   {'11':'Nov'},
                   {'12':'Dec'},
                  ],
     STATES_LIST : ["Select State",
                "Andaman and Nicobar Islands",
                "Arunachal Pradesh",
                "Bihar",
                "Chandigarh",
                "Chhattisgarh",
                "Dadra and Nagar Haveli",
                "Daman and Diu",
                "Delhi",
                "Goa",
                "Gujarat",
                "Haryana",
                "Himachal Pradesh",
                "Jammu and Kashmir",
                "Jharkhand",
                "Karnataka",
                "Kerala",
                "Lakshadweep",
                "Madhya Pradesh",
                "Maharashtra",
                "Manipur",
                "Meghalaya",
                "Mizoram",
                "Nagaland",
                "Puducherry",
                "Punjab",
                "Rajasthan",
                "Seemandhra",
                "Sikkim",
                "Tamil Nadu",
                "Tripura",
                "Uttar Pradesh",
                "Uttarakhand",
                "West Bengal"
               ]

});

