'use strict';

/**
 * @ngdoc overview
 * @name fantasyCricketApp
 * @description
 * # fantasyCricketApp
 *
 * Main module of the application.
 */
 angular
 .module('fantasyCricketApp', [
 	'ngAnimate',
 	// 'ngCookies',
 	// 'ngResource',
 	// 'ngRoute',
 	'ngDialog',
 	'ngSanitize',
 	'ngTouch',
 	'slick',
 	'ui.bootstrap',
    'ngFileUpload'
 	]);


 angular
 .module('fantasyCricketApp').run(['$rootScope', '$interval', 'devicedetectionservice', '$timeout', function ($rootScope, $interval, devicedetectionservice, $timeout) {

 		// Global cdnUrl to be used for static resources
		$rootScope.cdnUrl = "/fantasy_sports/app";

		$rootScope.mobHeaderTitle = "League Adda";

		$timeout(function(){
			$rootScope.$broadcast("tickingEvent");
		}, 100);

		$interval(function () {
			$rootScope.$broadcast("tickingEvent");
		}, 1000);

		removeUnusedCSS();

		function removeUnusedCSS() {
			var linkTagElem = document.querySelectorAll("link");
			var textToFind = devicedetectionservice.isDevice('DESKTOP') ? 'mobile' : 'desktop';
			for (var i = 0; i < linkTagElem.length; i++) {
				if (linkTagElem[i].href.indexOf(textToFind) > -1) {
					linkTagElem[i].href = "";
				}
			}
		}
        $rootScope.handleResponse = function (resp) {
            console.log("resp--->",resp);
        };
	}]);

Element.prototype.hasClassName = function (a) {
    return new RegExp("(?:^|\\s+)" + a + "(?:\\s+|$)").test(this.className);
};

Element.prototype.addClassName = function (a) {
    if (!this.hasClassName(a)) {
        this.className = [this.className, a].join(" ");
    }
};

Element.prototype.removeClassName = function (b) {
    if (this.hasClassName(b)) {
        var a = this.className;
        this.className = a.replace(new RegExp("(?:^|\\s+)" + b + "(?:\\s+|$)", "g"), " ");
    }
};

'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.config
 * @description
 * # config
 * Constant in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .constant('config',{
    "websiteNodeHost" : "http://18.217.53.138:3000/",
    "gameNodeHost" : "http://18.217.53.138:4000/"
});

'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.constants
 * @description
 * # constants
 * Constant in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .constant('constants', {
     MONTH_LIST : [
                   {'MM':'MM'},
                   {'01':'Jan'},
                   {'02':'Feb'},
                   {'03':'Mar'},
                   {'04':'Apr'},
                   {'05':'May'},
                   {'06':'Jun'},
                   {'07':'Jul'},
                   {'08':'Aug'},
                   {'09':'Sep'},
                   {'10':'Oct'},
                   {'11':'Nov'},
                   {'12':'Dec'},
                  ],
     STATES_LIST : ["Select State",
                "Andaman and Nicobar Islands",
                "Arunachal Pradesh",
                "Bihar",
                "Chandigarh",
                "Chhattisgarh",
                "Dadra and Nagar Haveli",
                "Daman and Diu",
                "Delhi",
                "Goa",
                "Gujarat",
                "Haryana",
                "Himachal Pradesh",
                "Jammu and Kashmir",
                "Jharkhand",
                "Karnataka",
                "Kerala",
                "Lakshadweep",
                "Madhya Pradesh",
                "Maharashtra",
                "Manipur",
                "Meghalaya",
                "Mizoram",
                "Nagaland",
                "Puducherry",
                "Punjab",
                "Rajasthan",
                "Seemandhra",
                "Sikkim",
                "Tamil Nadu",
                "Telangana",
                "Tripura",
                "Uttar Pradesh",
                "Uttarakhand",
                "West Bengal"
               ]

});


'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.msgConstants
 * @description
 * # msgConstants
 * Constant in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .constant('msgConstants', {

    /* Signup page validation and error messages */
    REQUIRED_MESSAGE : "Please fill out this field",
    REQUIRED_MESSAGE_1 :"This field is required",
    PASSWORD_CHANGE_SUCCESSFULLY: "Your password has been changed successfully",
    ERR_USERNAME_1: "Atleast 2-20 characters required",
    ERR_USERNAME_2: "Alphanumeric characters only",
    ERR_TEAMNAME_1: "Min. 2 & max. 20 characters allowed",
    ERR_TEAMNAME_2: "Alphanumeric characters only",
    ERR_PASSWORD_1: "Min. 5 & max. 15 characters allowed",
    ERR_PASSWORD_2: "Passwords do not match",
    ERR_PASSWORD_MATCH : "New password can't be same as the old one",
    ERR_EMAIL_1: "Max. 40 characters allowed",
    ERR_EMAIL_2: "Enter valid email id",
    ERR_MOBILE_1: "Enter 10 digits only",
    ERR_MOBILE_2: "Invalid mobile number",
    ERR_SPECIAL_CHAR: "No special symbols allowed except '-' and '&'",
    ERR_CITY_1: "Min. 2 & max. 30 characters allowed",
    ERR_CITY_2: "Alphabetic characters only",
    ERR_PAN_1: "Please fill your PAN details",
    ERR_PAN_2: "PAN details are not valid",
    ERR_PAN_3: "Please upload a PAN document",
    ERR_PAN_4: "PAN number is required",
    ERR_PAN_5: "Please upload either the image or PDF file",
    ERR_WITHDRAW_1: "Sorry! You can withdraw a min. of Rs.200",
    ERR_WITHDRAW_2: "Sorry! You can withdraw a max. of Rs.2,00,000",
    ERR_INSUFFICIENT_WINNING : "You don't have sufficient winnings",
    ERR_BANK_NAME: "Bank name is required",
    ERR_BRANCH_NAME: "Branch name is required",
    ERR_IFSC_CODE: "IFSC code is required",
    ERR_BANK_ACCOUNT: "Bank account number is required",
    ERR_STATE: "Please select your state",
    ERR_FULLNAME : "Full name is required",
    ERR_FULLNAME_2 : "Name should be an alphanumeric text",
    ERR_FULLNAME_3 : "Name should be 2-20 characters",
    ERR_STATE_CHECK : "You must confirm that you do not belong to Assam or Odisha",
    ERR_TNC : "You must accept the T&amp;C*",
    VERIFY_EMAIL_MOBILE : "Please verify your mobile / email id first",
    ERR_LARGE_FILE : "The file size can not exceed 4 MB",

    ERR_SEL_MAX_PLYR_SAME_TEAM : "Max 7 players allowed from one team",
    ERR_SEL_MAX_ALRDR :"Max 3 allrounders allowed (1 bonus opt.)",
    ERR_SEL_MAX_BATTER : "Max 5 batsman allowed (1 bonus opt.)",
    ERR_SEL_MAX_BOWLER : "Max 5 bowlers allowed (1 bonus opt.)",
    ERR_SEL_MAX_WK : "Max 1 wicket keeper allowed (1 bonus opt.)",
    ERR_SEL_MAX_CREDITS : "Insufficient Credits left",
    ERR_SEL_MIN_BATTER : "Min 3 batsman must be selected",
    ERR_SEL_MIN_BOWLER : "Min 3 bowlers must be selected",
    ERR_SEL_MIN_WK : "Min 1 wicket keeper must be selected",
    ERR_SEL_MIN_ALRDR : "Min 1 allrounder must be selected",
    ERR_SEL_SAME_C_VC : "Captain and vice captain can't be the same",
    ERR_SEL_CAP_ALRDY_TKN : "Captain is already selected",
    ERR_SEL_VCAP_ALRDY_TKN : "Vice captain is already selected",
    ERR_SEL_TEAM_PLAYER_EXCEED : "Max 11 players allowed",
    ERR_BONUS_SELECTED : "Please reset & reselect the player currently marked as bonus & try again",
    ERR_OTP : "Otp should be of 6 digits",
    DOWNLOAD_TXN_HISTORY : "Are you sure you want to download the transaction history?",      // Confirmation Popup
    CLEAR_TEAM: "Are you sure you want to clear your current selection?",
    TEAM_DETAILS_SAVED: "Team details saved",
    TRANSACTION_FAILURE_1 : "Deposit failed ! As your profile was not updated",
    TRANSACTION_FAILURE_2 : "Payment failed ! Please go ahead & try again. Contact us if the payment failed has been wrongly deducted from your bank account.",
    TRANSACTION_FAILURE_3 : "Max transaction limit: Rs.25000",
    TRANSACTION_FAILURE_4 : "You need to add a minimum of Rs.1",
    TRANSACTION_FAILURE_5 : "Please update your profile first",

    MSG_SIGNUP_SUCCESS: "Successfully signed up !",
    MSG_PROFILE_UPDATED_SUCCESS: "Your profile has been successfully updated",
    MSG_PANCARD_SAVED_SUCCESS: "Your PAN Card details have been saved successfully",
    MSG_BANK_DETAIL_SAVED_SUCCESS: "Your bank details have been saved successfully",
    MSG_REQUEST_FAILED: "Request failed please try again",
    ERR_FB_REGISTRATION : "Registration failed as you didn't allow us to access the required information of your Gmail/Facebook account.",

    HEADER_CONFIRMATION : "CONFIRMATION",
    HEADER_INFO : "INFO",
    HEADER_WITHDRAW_CASH : "WITHDRAW YOUR CASH",
});

'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.serviceName
 * @description
 * # serviceName
 * Constant in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .constant('serviceName', {

    /* WEBSITE SERVICE */
    "USER_LOGIN" : "/users/login",
    "USER_LOGOUT" : "/users/logout",
    "USER_SIGNUP" : "/users/create",
    "CHANGE_PASSWORD" : "/users/changePassword",
    "MYACCOUNT_INFO" : "/payments/users/wallet",
    "GETUSER_PROFILE" : "/users/getbyid",
    "UPDATE_PROFILE" : "/users/update",
    "UPDATE_TEAMNAME" : "/users/updateTeam",
    "DEPOSITE_INIT_TEKPROCESS" : "/payments/init",
    "DEPOSITE_INIT_PAYU" : "/payments/init/payu",
    "DEPOSITE_FINISH" : "/payments/deposit",
    "WITHDRAW" : "/payments/withdrawl",
    "VERIFY_EMAIL" : "/users/verifyEmailId",
    "VERIFY_MOBILE" : "/users/verifyMobileNumber",
    "OTP_EMAIL" : "/users/validate/emailotp",
    "OTP_MOBILE" : "/users/validate/mobileotp",
    "FORGOT_PASSWORD_INIT" : "/users/userForgotPasswordInitiate",
    "FORGOT_PASSWORD" : "/users/userForgotPassword",
    "ADD_BANK_ACCOUNT" : "/users/bank/add",
    "PAN_UPLOAD" : "/users/pancard/upload",
    "TXN_HISTORY" : "/payments/txn/history",
    "PAN_DETAILS" : "/users/pancard",
    "BANK_DETAILS" : "/users/bankac",
    "REQUEST_TXN_HISTORY": "/payments/txn/history/download",

    /* GAME SERVICES */
    "FETCH_LEAGUES" : "/fetchLeagues",
    "FETCH_TOURS" : "/fetchTours",
    "FETCH_MATCH_LIST" : "/fetchMatchList",
    "FETCH_LEAGUE_PRIZE_INFO": "/fetchLeaguePrizeInfo",
    "FETCH_JOINED_LEAGUE" : "/fetchJoinedLeagues",
    "FETCH_LEAGUE_MEMBERS" : "/fetchLeagueMembers",
    "FETCH_TOUR_MATCH_INFO" : "/fetchTourMatchInfo",
    "FETCH_LEAGUE_INFO" : "/fetchLeagueInfo",
    "FETCH_TOUR_MATCH_PLAYERS" : "/fetchTourMatchPlayers",
    "FETCH_SAVE_TEAM":"/saveTeam",
    "FETCH_MY_SAVED_TEAM":"/getTeamInfo",
    "FETCH_TEAM_MATCH_PLAYERS":"/fetchTeamMatchPlayers",
    "UPDATE_USER_TEAM": "/updateUserTeam",
    "FETCH_USER_JOINED_LEAGUES" : "/fetchUserJoinedLeagues",
    "FETCH_JOINED_LEAGUE_TEAMS" : "/fetchJoinedLeagueTeams",
    "JOIN_LEAGUE" : "/userJoinedLeague",
    "FETCH_TEAM_INFO" : "/getTeamInfo",
    "UPDATE_USER_JOINED_TEAM" : "/updateUserJoined",
    "FETCH_OPPONENT_TEAM_INFO" : "/findOpponentTeamInfo",
    "PROMOTIONS": "/promotion"
});

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:addCashCtrl
 * @description
 * # addCashCtrl
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('addCashCtrl',['$scope', 'ngDialog', 'utilityservice', 'session', 'config', 'serviceName', 'depositeservice', 'leaguedata', 'msgConstants', function ($scope, ngDialog,  utilityservice,session, config, serviceName, depositeservice, leaguedata, msgConstants) {
	var that = this;

	that.isAddCashDisable = false;

	that.title = leaguedata.title;
	that.showCurrBalance = leaguedata.showCurrentBalance;
	that.userID = session.getUserID();

	if(that.showCurrBalance) {		// Case for add cash popup in join league 
		that.userBalance = leaguedata.checkLeagueResponse.validBalance;
		that.entryFee = leaguedata.checkLeagueResponse.leagueCost;
		that.amount = leaguedata.checkLeagueResponse.remainingAmt;
	}else{
		that.amount = leaguedata.balance || '';
	}
	that.amountError = '';

	that.addAmount = function(amount){
		if(that.amount == undefined){
			that.amount = '';
		}
		that.amount =  that.amount === '' ? 0 : that.amount;
		if(parseInt(that.amount) + parseInt(amount) > 25000){
			that.amountError = msgConstants.TRANSACTION_FAILURE_3;
			return false;
		}
		that.amount = parseInt(that.amount) + parseInt(amount);
		that.amountError = '';
	};

	that.onAddCashClick = function(e){
		var isSuccess = true;
		var user = session.getUserName();
		if(session.getUserName() === null || session.getUserName() === undefined){
			that.amountError = msgConstants.TRANSACTION_FAILURE_5;
			isSuccess = false;
		}
		if(parseInt(that.amount) < 1 || that.amount === undefined || that.amount === ""){
			that.amountError = msgConstants.TRANSACTION_FAILURE_4;
			isSuccess = false;
		}

		if(parseInt(that.amount) > 25000){
			that.amountError = msgConstants.TRANSACTION_FAILURE_3;
			isSuccess = false;
		}

		if(isSuccess){
			that.isAddCashDisable = true;
			var paymentMethod = "PAYU";

			if(paymentMethod == "TEKPROCESS"){
				var amt = String(that.amount);
				var params = {};
				params.userId = session.getUserID();
				params.amount = amt;
				utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.DEPOSITE_INIT_TEKPROCESS, params, function (response) {
										//console.log("paymentAPI resp--",response);
					if(response.respCode == 100) {
						session.setDepositeData(response.respData.token);
						session.setTransactionID(response.respData.merchantTransactionIdentifier);
						session.setTransinitID(response.respData.txnInitId);
						depositeservice.openCard(response.respData.token, response.respData.merchantCode, response.respData.merchantTransactionIdentifier,amt);
						ngDialog.close();
					}else{
						that.isAddCashDisable = false;
						that.amountError = response.message;
					}
				});
			}else if(paymentMethod == "PAYU"){
				document.getElementById("addCash").submit();
			}
		}
	};

	that.checkAlphabet = function(event,fieldName){
		var keyCode = event.keyCode;
		if(keyCode<48 || keyCode>57 ){
			that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
						 //event.preventDefault();
		 }
	 };
}]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:ChangepasswordCtrl
 * @description
 * # ChangepasswordCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('ChangepasswordCtrl',['$rootScope', 'config', 'user', 'utilityservice', '$window', 'serviceName', 'ngDialog', 'msgConstants', function ($rootScope, config, user, utilityservice, $window, serviceName, ngDialog, msgConstants) {
        var that = this;

        that.onFieldChange = function(fieldName,fieldValue,additionalInfo){
          user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
            if(response.resCode !== 100){
                that[fieldName+'Msg'] = response.message;
            }else{
                console.log("resCode---->",response.resCode);
            }
          });
        };

        /* Change password api call- ashu*/
        that.changePassword = function() {
            that.isValidate = true;
            that.oldPassMsg = '';
            that.newPassMsg = '';
            that.confirmPassMsg = '';
            if(that.oldPass == that.newPass) {
                that.isValidate = false;
                that.newPassMsg = msgConstants.ERR_PASSWORD_MATCH;
            }

            user.OnInputFieldValidation('oldPass', that.oldPass,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.oldPassMsg = response.message;
                }
            });
            user.OnInputFieldValidation('newPass', that.newPass,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.newPassMsg = response.message;
                }
            });
            user.OnInputFieldValidation('confirmPass', that.confirmPass, that.newPass, function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.confirmPassMsg = response.message;
                }
            });

            if(that.isValidate) {
                var params = {};
                params.password = CryptoJS.MD5(that.oldPass)+'';
                params.newPassword = CryptoJS.MD5(that.newPass)+'';
                utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.CHANGE_PASSWORD, params, function (response) {
                    if(response.respCode == 100) {
                        $rootScope.$broadcast("showPopup", {
                              type: "Info",
                              title: msgConstants.HEADER_INFO,
                              appendTo:'#mainWrapper',
                              msg: msgConstants.PASSWORD_CHANGE_SUCCESSFULLY,
                              popupSize: 'small',
                              okButtonText: 'OK',
                              okBtnCallback: function () {
                                 $rootScope.$broadcast("setActiveHeaderMenu");
			 			      }
                        });
                    }else{
                        that.oldPassMsg = response.message;
                        that.isValidate = false;
                    }
                });
            }

        };

        that.close = function(){
            $rootScope.$broadcast("setActiveHeaderMenu");
            ngDialog.close();
        };
  }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:creatTeamCtrl
 * @description
 * # creatTeamCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
    .controller('createTeamCtrl', ['$window','$rootScope','$timeout', 'constants','msgConstants',  'createteamservice', 'utilityservice','devicedetectionservice','session','ngDialog', function ($window, $rootScope, $timeout, constants,msgConstants, createteamservice , utilityservice, devicedetectionservice, session, ngDialog) {
        var that = this;
        that.screenName = session.getScreenName();
        that.matchPlayersArray =[];
        that.allRounderArray =[];
        that.bowlerArray = [];
        that.batterArray = [];
        that.wkArray = [];
        that.bonusPlayersArray =[];
        that.selectedSquadArray = [];
        that.selectedCaptain = {};
        that.selectedViceCaptain = {};
        
        that.captainSelected = false;
        that.viceCaptainSelected = false;

        that.selectedWkArray = [];
        that.selectedBowlerArray = [];
        that.selectedBatterArray = [];
        that.selectedAllRounderArray = [];
        that.selectedBonusPlayerArray = [];

        var selectedPlayerCount = 0;
        var selectedAllRounderCount = 0;
        var selectedBatterCount = 0;
        var selectedBowlerCount = 0;
        var selectedWkCount = 0;
        var selectedBonusPlayerCount = 0;

        that.selectedSquadCredits = 0;
        that.selectionErr ="";
        that.teamComplete = false;

        that.matchId = '';
        that.teamId = '';
        that.isProcessing = false;


        that.mobileUI = {
            showTeamPreview : false,
            showPlayerList : true
        };

        that.showSelErr = false;

        var minPlayers = {
            wk: 1,
            batter : 3,
            bowler : 3,
            alrdr : 1,
            bonus : 0,
            total: 8

        };

        var maxPlayers = {
            wk: 1,
            batter : 5,
            bowler : 5,
            alrdr : 3,
            bonus : 1,
            total: 15

        };

        var selectedPlayers = {
            wk: 0,
            batter: 0,
            bowler: 0,
            alrdr:0,
            bonus: 0,
            total: 0
        };

        var teamPlyrCount = {
            team1: 0,
            team1Name: '',
            team2: 0,
            team2Name: ''
        };

        that.matchInfo ={};
        that.autoTeamAdd = false;


        // init function called when controller loads
        that.init = function(obj){
            // obj used to get match id
            //console.log('create team initialized:::',obj);
            that.matchId = obj.matchId;
            that.teamId = obj.teamId;
            that.isNewTeam = obj.isNewTeam;
            fetchMatchPlayers(obj.matchId);

        };


        function fetchMatchPlayers(matchId){
            var params = {};
            params.matchId = matchId;
            utilityservice.showLoader();
            createteamservice.getMatchPlayers(params, function(response){

                if(response.respCode === 100){
                    that.setMatchInfo(response.respData.matchInfo);
                    that.matchPlayersArray = response.respData.playersArr;
                    if(that.isNewTeam == false){
                        that.autoTeamAdd = true;
                        fetchMySavedPlayers(response.respData.playersArr);
                    }

                    else{
                        filterPlayersByRole(response.respData.playersArr);
                    }

                    utilityservice.hideLoader();
                }
            });
        }

        that.setMatchInfo = function(matchInfo){
            that.matchInfo = matchInfo;
            teamPlyrCount.team1Name = (matchInfo.teams.a.key).toUpperCase();
            teamPlyrCount.team2Name = (matchInfo.teams.b.key).toUpperCase();
        };

        function fetchMySavedPlayers(players){

            var params = {};
            params.matchId = that.matchId;
            params.teamId = that.teamId;
            params.token = session.getWebsiteToken();
            //console.log("get saved players", params);
            utilityservice.showLoader();

            createteamservice.getMySavedPlayers(params, function(response){

                if(response.respCode === 100){
                    //console.log("saved players :: ",response.respData);
                    that.matchPlayersArray = response.respData.playersArr;

                    var players = response.respData.playersArr;
                    var len = players.length;
                    for(var i=0; i<len ;i++){
                        if(players[i].isSelected){
                            //console.log("selected before",players[i].isCaptain);
                            if(players[i].isCaptain){
                                that.selectedCaptain = players[i];
                                that.captainSelected = true;
                            }
                            if(players[i].isViceCaptain){
                                that.selectedViceCaptain = players[i];
                                that.viceCaptainSelected = true;
                            }
                            that.addPlayer(players[i],true);
                        }
                    }
                    //if(that.matchId !== undefined || that.matchId !== ""){
                    filterPlayersByRole(response.respData.playersArr);
                    //}
                }

                utilityservice.hideLoader();
            });

        }


        that.getRole = function(player){

            if((player.batter === true) && (player.bowler === true)){
                return "alrdr-icon";
            }

            if((player.batter === true) && (player.bowler === false)){
                return "bt-icon";
            }

            if((player.batter === false) && (player.bowler === true)){
                return "bw-icon";
            }

            if((player.wicketKeeper === true)){
                return "wk-icon";
            } 

        };


        that.getPlayerRole = function(player){

            if((player.batter === true) && (player.bowler === true)){
                return "alrdr";
            }

            if((player.batter === true) && (player.bowler === false)){
                return "batter";
            }

            if((player.batter === false) && (player.bowler === true)){
                return "bowler";
            }

            if((player.wicketKeeper === true)){
                return "wk";
            } 

        };

        function filterPlayersByRole(players){
            //console.log(players);

            var length = players.length;

            for(var i=0;i<length;i++){

                if((players[i].batter === true) && (players[i].bowler === true)){
                    that.allRounderArray.push(players[i]);
                }

                if((players[i].batter === true) && (players[i].bowler === false)){
                    that.batterArray.push(players[i]);
                }

                if((players[i].batter === false) && (players[i].bowler === true)){
                    that.bowlerArray.push(players[i]);
                }

                if((players[i].wicketKeeper === true)){
                    that.wkArray.push(players[i]);
                    //console.log(players[i].isSelected);
                } 

            }
        }


        that.addPlayer = function(player, savedTeam){
            //console.log(that.selectedSquadArray);
            // clearing prev error message
            that.showSelErr = false;
            that.selectionErr = "";
            //console.log("processing :::: ",that.isProcessing);

            if(that.isProcessing == false){
                that.isProcessing = true;

                if(player.isSelected === false || savedTeam == true){

                    if(that.selectedSquadArray.length <11) {

                        updateTeamCount(player,"add");

                        if((teamPlyrCount.team1 >= 8) || (teamPlyrCount.team2 >= 8)){

                            if(teamPlyrCount.team1 >= 8){
                                updateTeamCount(player,"sub");
                            }

                            if(teamPlyrCount.team2 >= 8){
                                updateTeamCount(player,"sub");
                            }
                            //console.log(teamPlyrCount);
                            that.isProcessing = false;
                            that.showSelErr = true;
                            that.selectionErr = msgConstants.ERR_SEL_MAX_PLYR_SAME_TEAM;
                            if(devicedetectionservice.isDevice("MOBILE")){
                                showToast(that.selectionErr);
                            }
                            return false;
                        }   

                        that.selectedSquadCredits = that.selectedSquadCredits + player.playerCredit;

                        if(that.selectedSquadCredits <= 100){

                            if((player.batter === true) && (player.bowler === true)){
                                // check alrdr is grtr than 0 that we are left with min alrdrs we can select
                                if(minPlayers.alrdr > 0){
                                    minPlayers.alrdr--;
                                    minPlayers.total--;
                                    addPlayerInSquad(player,"alrdr");
                                    //console.log("after adding alrdr and less than one :: ",that.selectedSquadCredits);
                                }

                                else{
                                    if(minPlayers.total + selectedPlayers.total < 11){
                                        if(selectedPlayers.alrdr < maxPlayers.alrdr ){
                                            addPlayerInSquad(player,"alrdr");
                                            //console.log("after adding alrdr and greater than one :: ",that.selectedSquadCredits);
                                        }
                                        else{  
                                            if(selectedPlayers.bonus < maxPlayers.bonus){
                                                //checkMinPlayersCreteria(player);
                                                player.isBonus = true;
                                                addPlayerInSquad(player,"alrdr");
                                            }
                                            else{
                                                that.isProcessing = false;
                                                that.showSelErr = true;
                                                that.selectionErr = msgConstants.ERR_SEL_MAX_ALRDR;
                                                if(devicedetectionservice.isDevice("MOBILE")){
                                                    showToast(that.selectionErr);
                                                }
                                                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                                                updateTeamCount(player,"sub");
                                                return false;
                                            }

                                        }
                                    }
                                    else{
                                        updateTeamCount(player,"sub");
                                        checkMinPlayersCreteria(player);
                                    }

                                }

                            }

                            if((player.batter === true) && (player.bowler === false)){
                                //console.log("batter");
                                if(minPlayers.batter > 0){
                                    minPlayers.batter--;
                                    minPlayers.total--;
                                    addPlayerInSquad(player,"batter");
                                    //console.log("after adding batter and less than three :: ",that.selectedSquadCredits);
                                }

                                else{
                                    if(minPlayers.total + selectedPlayers.total < 11){
                                        if(selectedPlayers.batter < maxPlayers.batter ){
                                            addPlayerInSquad(player,"batter");
                                            //console.log("after adding batter and greater than three :: ",that.selectedSquadCredits);

                                        }
                                        else{  

                                            // add 6th as bonus player
                                            //console.log("adding 6 batter");

                                            if(selectedPlayers.bonus < maxPlayers.bonus){
                                                //checkMinPlayersCreteria(player);
                                                player.isBonus = true;
                                                addPlayerInSquad(player,"batter");

                                            }
                                            else{
                                                that.isProcessing = false;
                                                that.showSelErr = true;
                                                that.selectionErr = msgConstants.ERR_SEL_MAX_BATTER;
                                                if(devicedetectionservice.isDevice("MOBILE")){
                                                    showToast(that.selectionErr);
                                                }
                                                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                                                //console.log(teamPlyrCount);
                                                updateTeamCount(player,"sub");
                                                //console.log(teamPlyrCount);
                                                return false;
                                            }
                                        }
                                    }
                                    else{
                                        updateTeamCount(player,"sub");
                                        checkMinPlayersCreteria(player);
                                    }
                                }
                            }


                            if((player.batter === false) && (player.bowler === true)){
                                //console.log("bowler");
                                if(minPlayers.bowler > 0){
                                    minPlayers.bowler--;
                                    minPlayers.total--;
                                    addPlayerInSquad(player,"bowler");
                                    //console.log("after adding bowler and less than three :: ",that.selectedSquadCredits);
                                }

                                else{
                                    if(minPlayers.total + selectedPlayers.total < 11){
                                        if(selectedPlayers.bowler < maxPlayers.bowler ){


                                            addPlayerInSquad(player,"bowler");
                                            //console.log("after adding bowler and greater than three :: ",that.selectedSquadCredits);

                                        }
                                        else{  
                                            if(selectedPlayers.bonus < maxPlayers.bonus){
                                                //checkMinPlayersCreteria(player);
                                                player.isBonus = true;
                                                addPlayerInSquad(player,"bowler");

                                            }
                                            else{
                                                that.isProcessing = false;
                                                that.showSelErr = true;
                                                that.selectionErr = msgConstants.ERR_SEL_MAX_BOWLER;
                                                if(devicedetectionservice.isDevice("MOBILE")){
                                                    showToast(that.selectionErr);
                                                }
                                                updateTeamCount(player,"sub");
                                                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                                                return false;
                                            }

                                        }
                                    }
                                    else{
                                        updateTeamCount(player,"sub");
                                        checkMinPlayersCreteria(player);
                                    }

                                }
                            }

                            if(player.wicketKeeper === true){
                                //console.log("WK");
                                if(minPlayers.wk > 0){
                                    minPlayers.wk--;
                                    minPlayers.total--;
                                    addPlayerInSquad(player,"wk");
                                    //console.log("after adding wk and less than one :: ",that.selectedSquadCredits);
                                }

                                else{
                                    if(minPlayers.total + selectedPlayers.total < 11){
                                        if(selectedPlayers.wk < maxPlayers.wk ){

                                            addPlayerInSquad(player,"wk");
                                            //console.log("after adding wk and greater than one :: ",that.selectedSquadCredits);

                                        }
                                        else{  
                                            if(selectedPlayers.bonus < maxPlayers.bonus){
                                                // checkMinPlayersCreteria(player);
                                                player.isBonus = true;
                                                addPlayerInSquad(player,"wk");

                                            }
                                            else{
                                                that.isProcessing = false;
                                                that.showSelErr = true;
                                                that.selectionErr = msgConstants.ERR_SEL_MAX_WK;
                                                if(devicedetectionservice.isDevice("MOBILE")){
                                                    showToast(that.selectionErr);
                                                }
                                                updateTeamCount(player,"sub");
                                                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                                                return false;
                                            }

                                        }
                                    }
                                    else{
                                        updateTeamCount(player,"sub");
                                        checkMinPlayersCreteria(player);
                                    }

                                }
                            }
                        }

                        else{
                            // if(that.selectedSquadArray.length <11){
                            that.isProcessing = false;
                            that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                            updateTeamCount(player,"sub");
                            that.showSelErr = true;
                            that.selectionErr = msgConstants.ERR_SEL_MAX_CREDITS;
                            if(devicedetectionservice.isDevice("MOBILE")){
                                showToast(that.selectionErr);
                            }
                            //}
                            //else{
                            //alert("Team complete, now select captain and vice-captain");
                            // that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                            //}

                        }
                    }

                    else{
                        that.isProcessing = false;
                        that.showSelErr = true;
                        that.selectionErr = msgConstants.ERR_SEL_TEAM_PLAYER_EXCEED;

                        if(devicedetectionservice.isDevice("MOBILE")){
                            showToast(that.selectionErr);
                        }

                        //alert("Team complete, now select captain and vice-captain");
                        //that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                    }
                }
                else{
                    //console.log("already selected ");
                    deletePlayerInSquad(player);

                    //that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;

                }
            }
            else {
                return false;
            }
        };



        function addPlayerInSquad(player, playerRole){

            if(playerRole === "alrdr"){
                if(player.isBonus === false){
                    selectedPlayers.alrdr++;
                    that.selectedAllRounderArray.push(player);
                }

                else{
                    selectedPlayers.bonus++;
                    that.selectedBonusPlayerArray.push(player);
                }

                selectedPlayers.total++;
                player.isSelected = true;
                that.selectedSquadArray.push(player);
                // var count = selectedPlayers.alrdr + selectedPlayers.bonus;
                addPlayerUI(player,playerRole);
            }

            if(playerRole === "bowler"){
                if(player.isBonus === false){
                    selectedPlayers.bowler++;
                    that.selectedBowlerArray.push(player);
                }

                else{
                    selectedPlayers.bonus++;
                    that.selectedBonusPlayerArray.push(player);
                }

                selectedPlayers.total++;
                player.isSelected = true;
                that.selectedSquadArray.push(player);
                //var count = selectedPlayers.bowler + selectedPlayers.bonus;
                addPlayerUI(player,playerRole);

            }

            if(playerRole === "batter"){ 

                if(player.isBonus === false){
                    selectedPlayers.batter++;
                    that.selectedBatterArray.push(player);
                }

                else{
                    selectedPlayers.bonus++;
                    that.selectedBonusPlayerArray.push(player);
                }

                selectedPlayers.total++;
                player.isSelected = true;
                that.selectedSquadArray.push(player);
                //var count = selectedPlayers.batter + selectedPlayers.bonus;
                addPlayerUI(player,playerRole);

            }

            if(playerRole === "wk"){
                if(player.isBonus === false){
                    selectedPlayers.wk++;
                    that.selectedWkArray.push(player);
                }

                else{
                    selectedPlayers.bonus++;
                    that.selectedBonusPlayerArray.push(player);
                }

                selectedPlayers.total++;
                player.isSelected = true;
                that.selectedSquadArray.push(player);
                //var count = selectedPlayers.wk + selectedPlayers.bonus;
                addPlayerUI(player,playerRole);

            }

        }


        function deletePlayerInSquad(player){
            //console.log("deleting player");
            updateTeamCount(player,"sub");
            that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;

            if((player.batter === true) && (player.bowler === true)){    
                deletePlayerUI(player,"alrdr");
                player.isSelected = false;

                if(minPlayers.alrdr >=0 && selectedPlayers.alrdr <= 1){
                    if(player.isBonus == false){
                        minPlayers.alrdr++;
                        minPlayers.total++;
                    }
                }

                if(player.isBonus === false){
                    selectedPlayers.alrdr--;

                    //  that.selectedAllRounderArray.pop(player);
                    deletePlyerFrmArray(that.selectedAllRounderArray,player);
                }
                else{
                    selectedPlayers.bonus--;
                    player.isBonus = false;
                    // that.selectedBonusPlayerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBonusPlayerArray,player);
                }

                //that.selectedSquadArray.pop(player);
                deletePlyerFrmArray(that.selectedSquadArray, player);
                //console.log(that.selectedSquadArray);
                selectedPlayers.total--;



            }

            if((player.batter === false) && (player.bowler === true)){
                deletePlayerUI(player,"bowler");
                player.isSelected = false;

                if(minPlayers.bowler >=0 && selectedPlayers.bowler <= 3){
                    if(player.isBonus == false){
                        minPlayers.bowler++;
                        minPlayers.total++;
                    }
                }

                if(player.isBonus === false){
                    selectedPlayers.bowler--;
                    //that.selectedBowlerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBowlerArray,player);
                }
                else{
                    selectedPlayers.bonus--;
                    player.isBonus = false;
                    //that.selectedBonusPlayerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBonusPlayerArray,player);
                }

                // that.selectedSquadArray.pop(player);
                deletePlyerFrmArray( that.selectedSquadArray,player);
                selectedPlayers.total--;


            }

            if((player.batter === true) && (player.bowler === false)){

                deletePlayerUI(player,"batter");
                player.isSelected = false;
                if(minPlayers.batter >=0 && selectedPlayers.batter <= 3){
                    if(player.isBonus == false){
                        minPlayers.batter++;
                        minPlayers.total++;
                    }
                }

                if(player.isBonus === false){
                    selectedPlayers.batter--;
                    //that.selectedBatterArray.pop(player);
                    deletePlyerFrmArray(that.selectedBatterArray,player);
                }
                else{
                    selectedPlayers.bonus--;
                    player.isBonus = false;
                    //that.selectedBonusPlayerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBonusPlayerArray,player);
                }

                //  that.selectedSquadArray.pop(player);
                deletePlyerFrmArray(that.selectedSquadArray,player);
                selectedPlayers.total--;



            }

            if(player.wicketKeeper === true){
                deletePlayerUI(player,"wk");
                player.isSelected = false;

                if(minPlayers.wk >=0 && selectedPlayers.wk <= 1){
                    if(player.isBonus == false){
                        minPlayers.wk++;
                        minPlayers.total++;
                    }

                }

                if(player.isBonus === false){
                    selectedPlayers.wk--;
                    // that.selectedWkArray.pop(player);
                    deletePlyerFrmArray(that.selectedWkArray,player);
                }
                else{
                    selectedPlayers.bonus--;
                    player.isBonus = false;
                    // that.selectedBonusPlayerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBonusPlayerArray,player);
                }

                //that.selectedSquadArray.pop(player);
                deletePlyerFrmArray(that.selectedSquadArray,player);

                selectedPlayers.total--;


            }

        }


        function addPlayerUI(player,playerRole){

            var index;
            if(playerRole === "batter"){
                var count = maxPlayers.batter+selectedPlayers.bonus;
                for(var i=0;i< count;i++){
                    var j=i+1;
                    var dummyElem = document.getElementById("bt"+j);
                    if(dummyElem.innerHTML === ""){
                        //console.log("got the blank player");
                        index = i+1;
                        break;
                    }
                }
                //console.log("adding at position bt  ::: ",(index));
                var elem = document.getElementById("bt"+index);
            }

            if(playerRole === "bowler"){
                var count = maxPlayers.bowler+selectedPlayers.bonus;
                for(var i=0;i< count;i++){
                    var j=i+1;
                    var dummyElem = document.getElementById("bw"+j);
                    if(dummyElem.innerHTML === ""){
                        //console.log("got the blank player");
                        index = i+1;
                        break;
                    }
                }
                //console.log("adding at position bw ::: ",(index));
                var elem = document.getElementById("bw"+index);
            }

            if(playerRole === "wk"){
                var count = maxPlayers.wk+selectedPlayers.bonus;
                for(var i=0;i< count;i++){
                    var j=i+1;
                    var dummyElem = document.getElementById("wk"+j);
                    if(dummyElem.innerHTML === ""){
                        //console.log("got the blank player");
                        index = i+1;
                        break;
                    }
                }
                //console.log("adding at position  ::: wk ",(index));
                var elem = document.getElementById("wk"+index);
            }

            if(playerRole === "alrdr"){
                var count = maxPlayers.alrdr+selectedPlayers.bonus;
                for(var i=0;i< count;i++){
                    var j=i+1;
                    var dummyElem = document.getElementById("alrdr"+j);
                    if(dummyElem.innerHTML === ""){
                        //console.log("got the blank player");
                        index = i+1;
                        break;
                    }
                }
                //console.log("adding at position  ::: alrdr ",(index));
                var elem = document.getElementById("alrdr"+index);
            }

            elem.style.backgroundImage = "url('" + $rootScope.cdnUrl + "/assets/images/sports/cricket/jersey/"+player.teamName+".png')";
            if(player.isCaptain)
                elem.classList.add("sel-cap");
            if(player.isViceCaptain)
                elem.classList.add("sel-v-cap");
            elem.title = player.playerName;
            elem.setAttribute("datapid",player.playerId);
            if(devicedetectionservice.isDevice('DESKTOP')){
                if(player.isBonus){
                    elem.innerHTML="<div class='plyr-details bonus'><span class='plyr-name'>(B)"+player.shortName+"</span><span class='plyr-credits'>"+player.playerCredit+"</span></div>";
                }

                else{
                    elem.innerHTML="<div class='plyr-details'><span class='plyr-name'>"+player.shortName+"</span><span class='plyr-credits'>"+player.playerCredit+"</span></div>";
                }

            }

            if(devicedetectionservice.isDevice('MOBILE')){
                if(player.isBonus){
                    elem.innerHTML="<div class='plyr-details bonus'><div class='plyr-name'>(B)"+player.shortName+"</div></div>";
                }

                else{
                    elem.innerHTML="<div class='plyr-details'><div class='plyr-name'>"+player.shortName+"</div></div>";
                }

            }
            //console.log("adding player ui while processing before : ", that.isProcessing);
            that.isProcessing = false;
            //console.log("adding player ui while processing after : ", that.isProcessing);
            // team complete popup
            if(that.selectedSquadArray.length == 11){
                that.showCapVCapPopup();
            }

        }

        function deletePlayerUI(player,playerRole){
            var elem = document.querySelectorAll(".selected-squad .player");
            var elemLength = elem.length;
            for(var i=0;i<elemLength;i++){
                if(elem[i].getAttribute("datapid") !== null){
                    if(elem[i].getAttribute("datapid") == player.playerId){
                        //console.log("got one to delete");
                        var e = elem[i];
                        break;
                    }
                }

            }


            e.style.backgroundImage = "url('" + $rootScope.cdnUrl + "/assets/images/sports/cricket/placeholder.png')";
            //e.classList.remove(player.teamName);
            if(player.isCaptain){
                e.removeClassName("sel-cap"); 
            }
            if(player.isViceCaptain){
                e.removeClassName("sel-v-cap"); 
            }

            e.title = "";
            e.innerHTML="";


            if(player.isCaptain == true){
                player.isCaptain = false;
                that.selectedCaptain ={};
                that.captainSelected = false;
            }
            if(player.isViceCaptain == true){
                player.isViceCaptain = false;
                that.selectedViceCaptain ={};
                that.viceCaptainSelected = false;
            }
            /* if(player.isBonus = true){
                player.isBonus = false;
            }*/

            //console.log("deleting player ui while processing before : ", that.isProcessing);
            that.isProcessing = false;
            //console.log("deleteing player ui while processing after : ", that.isProcessing);


            //console.log(that.selectedSquadArray);

        }


        function deletePlyerFrmArray(players, player) {
            var length = players.length;

            for(var i=0; i< length; i++){
                if(players[i].playerId == player.playerId){
                    //console.log("got the player ::: "+players[i].playerName);
                    players.splice(i,1);
                    break;

                }
            }
            //console.log(players);
        }



        function checkMinPlayersCreteria(player){
            if(selectedPlayers.batter < 3){
                that.isProcessing = false;
                that.showSelErr = true;
                if(that.bonusPlayersArray.length>0){
                    that.selectionErr = msgConstants.ERR_BONUS_SELECTED;
                }
                else{
                    that.selectionErr = msgConstants.ERR_SEL_MIN_BATTER;    
                }


                if(devicedetectionservice.isDevice("MOBILE")){
                    showToast(that.selectionErr);
                }
                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                return false;
            }

            if(selectedPlayers.bowler < 3){
                that.isProcessing = false;
                that.showSelErr = true;
                if(that.bonusPlayersArray.length>0){
                    that.selectionErr = msgConstants.ERR_BONUS_SELECTED;
                }
                else{
                    that.selectionErr = msgConstants.ERR_SEL_MIN_BOWLER;
                }
                if(devicedetectionservice.isDevice("MOBILE")){
                    showToast(that.selectionErr);
                }
                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                return false;
            }

            if(selectedPlayers.wk < 1){
                //console.log("al least one keeperr reqiured");
                that.isProcessing = false;
                that.showSelErr = true;
                if(that.selectedBonusPlayerArray.length>0){
                    that.selectionErr = msgConstants.ERR_BONUS_SELECTED;
                }
                else{
                    that.selectionErr = msgConstants.ERR_SEL_MIN_WK;
                }
                if(devicedetectionservice.isDevice("MOBILE")){
                    showToast(that.selectionErr);
                }
                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                return false;
            }

            if(selectedPlayers.alrdr < 1){
                //console.log("3 peice for 1");
                that.isProcessing = false;
                that.showSelErr = true;
                if(that.bonusPlayersArray.length>0){
                    that.selectionErr = msgConstants.ERR_BONUS_SELECTED;
                }
                else{
                    that.selectionErr = msgConstants.ERR_SEL_MIN_ALRDR;
                }
                if(devicedetectionservice.isDevice("MOBILE")){
                    showToast(that.selectionErr);
                }
                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;    
                return false;
            }

        }


        function updateTeamCount(player,op){
            var playerTeamName = player.teamName.toUpperCase();

            if(op =="add"){

                if(playerTeamName === teamPlyrCount.team1Name){
                    teamPlyrCount.team1++;
                }
                else if(playerTeamName === teamPlyrCount.team2Name){
                    teamPlyrCount.team2++;
                }
                //console.log(teamPlyrCount,"adding");


            }

            if(op =="sub"){

                if(playerTeamName === teamPlyrCount.team1Name){
                    teamPlyrCount.team1--;
                }
                else if(playerTeamName === teamPlyrCount.team2Name){
                    teamPlyrCount.team2--;
                }

                //console.log(teamPlyrCount,"deleting");

            }


        }

        that.clearTeam = function(event){
            if(angular.element(event.currentTarget).hasClass("active")){
                $rootScope.$broadcast("showPopup", {
                    type: "Confirm",
                    title: msgConstants.HEADER_CONFIRMATION,
                    appendTo:'#mainWrapper',
                    msg: msgConstants.CLEAR_TEAM,
                    okButtonText: 'YES',
                    cancelButtonText: 'NO',
                    okBtnCallback: function () {
                        var elemArr = document.querySelectorAll('.selected-squad .player');
                        var length = elemArr.length;

                        for(var i=0;i <length; i++){

                            elemArr[i].style.backgroundImage = "";
                            //elemArr[i].classList.remove(player.teamName);
                            elemArr[i].setAttribute("datapid","");
                            elemArr[i].title = "";
                            elemArr[i].innerHTML="";
                            elemArr[i].removeClassName("sel-cap");
                            elemArr[i].removeClassName("sel-v-cap");
                        }


                        var count = that.matchPlayersArray.length;
                        for(var j=0;j<count;j++){
                            that.matchPlayersArray[j].isSelected = false;
                            that.matchPlayersArray[j].isCaptain = false;
                            that.matchPlayersArray[j].isViceCaptain = false;
                            that.matchPlayersArray[j].isBonus = false;
                        }

                        // cleararray

                        that.selectedAllRounderArray =[];
                        that.selectedBatterArray =[];
                        that.selectedBonusPlayerArray =[];
                        that.selectedBowlerArray = [];
                        that.selectedWkArray = [];
                        that.selectedSquadArray = [];
                        that.selectedCaptain = {};
                        that.captainSelected = false;
                        that.selectedViceCaptain = {};
                        that.viceCaptainSelected = false;
                        that.selectedSquadCredits = 0;

                        selectedPlayers = {
                            wk: 0,
                            batter: 0,
                            bowler: 0,
                            alrdr:0,
                            bonus: 0,
                            total: 0
                        };

                        minPlayers = {
                            wk: 1,
                            batter : 3,
                            bowler : 3,
                            alrdr : 1,
                            bonus : 0,
                            total: 8

                        };

                        teamPlyrCount.team1 = 0;
                        teamPlyrCount.team2 = 0;

                    }
                });
            }
            else{
                return false;
            }
        }



        // auto open

        that.showCapVCapPopup = function(event){
            if(that.autoTeamAdd == false){
                that.teamComplete = true;
            }

            that.autoTeamAdd = false;

        }

        that.closeCapVCapPopup = function(){

            that.capVcapErr ="";

            that.teamComplete = false;
            if(that.isNewTeam){
                that.viceCaptainSelected = false;
                that.captainSelected = false;
            }
            else{


            }
        }

        // ui click open
        that.showCapVCapPopupUI = function(event){
            if(angular.element(event.currentTarget).hasClass("active")){
                that.teamComplete = true;
            }

        }


        that.capSelClick = function(){
            var capWrapper = document.querySelector('.cap');
            var vcapWrapper = document.querySelector(' .vcap');
            vcapWrapper.classList.remove('open');
            if(capWrapper.classList.contains('open')){
                capWrapper.classList.remove('open');
            }   
            else{
                capWrapper.classList.add('open');
            }


        }

        that.viceCapSelClick = function(){

            var vcapWrapper = document.querySelector(' .vcap');
            if(vcapWrapper.classList.contains('open')){
                vcapWrapper.classList.remove('open');
            }
            else{
                vcapWrapper.classList.add('open');
            }
        }

        that.onCaptainSelect = function(player){
            
            that.captainSelected = true;
            that.selectedCaptain = player;
            clearCaptain();
            player.isCaptain = true;
            var playerWrapper = document.querySelector('.cap');
            playerWrapper.classList.remove('open');

        }

        that.onViceCaptainSelect = function(player){

            that.viceCaptainSelected = true;
            clearViceCaptain();
            that.selectedViceCaptain = player;
            player.isViceCaptain = true;
            var playerWrapper = document.querySelector('.vcap');
            playerWrapper.classList.remove('open');

        }


        function clearCaptain(){
            var length = that.selectedSquadArray.length;
            for(var i=0;i<length;i++){
                that.selectedSquadArray[i].isCaptain = false;
            }

        }

        function clearViceCaptain(){
            var length = that.selectedSquadArray.length;
            for(var i=0;i<length;i++){
                that.selectedSquadArray[i].isViceCaptain = false;
            }
        }


        function getPlayerFromUI(player){
            var elem = document.querySelectorAll(".selected-squad .player");
            var elemLength = elem.length;
            for(var i=0;i<elemLength;i++){
                if(elem[i].getAttribute("datapid") !== null){
                    if(elem[i].getAttribute("datapid") == player.playerId){
                        return elem[i];
                    }
                }

            }

        }

        that.closeErrorBox = function(){
            that.showSelErr = false;
        }


        // mobile specific funcitons

        that.showMyteam = function(){
            that.mobileUI = {
                showTeamPreview : true,
                showPlayerList : false
            }
        }

        that.hideMyTeam = function(){
            //console.log("hiding");
            that.mobileUI = {
                showTeamPreview : false,
                showPlayerList : true
            }
        }

        that.saveMyTeam =function(event){
            //console.log("called");
            // for desktop 
            if(devicedetectionservice.isDevice('DESKTOP') == true){
                if(angular.element(event.currentTarget).hasClass("active")){


                    if(that.selectedCaptain.playerId == that.selectedViceCaptain.playerId){
                        that.capVcapErr = msgConstants.ERR_SEL_SAME_C_VC;
                        return false;
                    }

                    // icons on head UI
                    addCapVCapUi();
                    // close the dialog
                    var elem = document.querySelectorAll(".cap-vice-cap-sel-container");
                    //elem.();
                    // call api for save team
                    var params = {};
                    params.players = that.selectedSquadArray;
                    params.matchId = that.matchId;
                    params.teamId = that.teamId;

                    //console.log(params);

                    if(that.isNewTeam === true){
                        //console.log("saving first time");
                        createteamservice.saveMyTeam(params,function(response){
                            if(response.respCode == 100){
                                //console.log("team saved");
                                var invCode = utilityservice.getUrlParameter("invCode");
                                console.log(invCode);
                                // show popup with ok button

                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.TEAM_DETAILS_SAVED,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        if(invCode !== undefined && invCode !==  ""){
                                            $window.location.href = utilityservice.getBaseUrl()+"/league?invCode="+invCode;    
                                        }
                                        else{
                                            $window.location.href = utilityservice.getBaseUrl()+"/league";
                                        }

                                    }
                                });
                            }

                            else {
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: response.message,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        ngDialog.close();
                                    }
                                });
                            }

                        });
                    }
                    else{


                        //console.log("saving next time");
                        //console.log(params);
                        createteamservice.updateUserTeam(params,function(response){

                            if(response.respCode == 100){
                                //console.log("upadted team saved");
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.TEAM_DETAILS_SAVED,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        $window.location.href = utilityservice.getBaseUrl()+"/league";
                                    }
                                });
                            }

                            else {
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: response.message,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        ngDialog.close();
                                    }
                                });
                            }

                        });
                    }

                    that.teamComplete =false;
                    that.capVcapErr ="";

                }

            }


            // only in mobile
            if(devicedetectionservice.isDevice('MOBILE') == true){

                if(angular.element(event.currentTarget).hasClass("active")){

                    // call api for save team
                    var params = {};
                    params.players = that.selectedTeam;
                    params.matchId = that.matchInfoLocal.matchId;

                    //console.log(params);

                    if(that.isNewTeamLocal === true){
                        //console.log("saving first time");
                        createteamservice.saveMyTeam(params,function(response){
                            if(response.respCode == 100){
                                //console.log("team saved");
                                var invCode = utilityservice.getUrlParameter("invCode");
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.TEAM_DETAILS_SAVED,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        if(invCode !== undefined && invCode !==  ""){
                                            $window.location.href = utilityservice.getBaseUrl()+"/match-leagues/"+that.matchInfoLocal.matchId+"?invCode="+invCode;    
                                        }
                                        else{
                                            $window.location.href = utilityservice.getBaseUrl()+"/match-leagues/"+that.matchInfoLocal.matchId;
                                        }
                                    }
                                });
                            }

                            else {
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: response.message,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        ngDialog.close();
                                    }
                                });
                            }
                        });
                    }
                    else{
                        params.teamId = that.teamIdLocal;
                        //console.log("saving next time");
                        createteamservice.updateUserTeam(params,function(response){
                            if(response.respCode == 100){
                                //console.log("upadted team saved");
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.TEAM_DETAILS_SAVED,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        $window.location.href = utilityservice.getBaseUrl()+"/match-leagues/"+that.matchInfoLocal.matchId;
                                    }
                                });
                            }

                            else {
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: response.message,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        ngDialog.close();
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }


        that.saveMyTeamInLocal = function(event){
            if(angular.element(event.currentTarget).hasClass("active")){

                $window.localStorage['selectedTeam'] = angular.toJson(that.selectedSquadArray);
                //console.log(that.matchInfo);
                $window.localStorage['matchInfo'] = angular.toJson(that.matchInfo);
                $window.localStorage['selectedCaptain'] = angular.toJson(that.selectedCaptain);
                $window.localStorage['selectedViceCaptain'] = angular.toJson(that.selectedViceCaptain);
                $window.localStorage['captainSelected'] = angular.toJson(that.captainSelected);
                $window.localStorage['viceCaptainSelected'] = angular.toJson(that.viceCaptainSelected);
                $window.localStorage['teamId'] = angular.toJson(that.teamId);
                $window.localStorage['isNewTeam'] = angular.toJson(that.isNewTeam);
                var invCode = utilityservice.getUrlParameter("invCode");
                if(invCode !== undefined && invCode !==  ""){
                    $window.location.href = utilityservice.getBaseUrl()+"/sel-cap-vcap?invCode="+invCode;
                }
                else{
                    $window.location.href = utilityservice.getBaseUrl()+"/sel-cap-vcap";
                }



            }
        }

        function addCapVCapUi(){
            //console.log("called fn");

            var elem = document.querySelectorAll(".selected-squad .player");
            var elemLength = elem.length;
            for(var i=0;i<elemLength;i++){
                if(elem[i].getAttribute("datapid") !== null){
                    if(elem[i].getAttribute("datapid") == that.selectedCaptain.playerId){
                        //console.log("captain ::",elem[i].attributes,elem[i]);
                        //elem[i].classList.addClass("sel-cap");
                        elem[i].className += " sel-cap";
                    }
                    else {
                        elem[i].classList.remove("sel-cap");
                    }

                    if(elem[i].getAttribute("datapid") == that.selectedViceCaptain.playerId){
                        //console.log("vice captain ::",elem[i].innerHTML);
                        elem[i].className += " sel-v-cap";
                    }

                    else{
                        elem[i].classList.remove("sel-v-cap");
                    }


                }

            }


        }



        that.selectedTeam = [];
        that.matchInfoLocal = {};
        that.getTeamFromLocalstorage = function(){

            that.selectedTeam =      
                JSON.parse($window.localStorage['selectedTeam']);
            that.matchInfoLocal = JSON.parse($window.localStorage['matchInfo']);
            that.selectedCaptain = JSON.parse($window.localStorage['selectedCaptain']);
            that.selectedViceCaptain = JSON.parse($window.localStorage['selectedViceCaptain']);
            that.captainSelected = JSON.parse($window.localStorage['captainSelected']);
            that.viceCaptainSelected = JSON.parse($window.localStorage['viceCaptainSelected']);
            that.teamIdLocal = JSON.parse($window.localStorage['teamId']);
            that.isNewTeamLocal = JSON.parse($window.localStorage['isNewTeam']);

            //console.log(JSON.parse($window.localStorage['selectedTeam']));
        }


        that.mobileSelCapClick = function(player){

            if(that.selectedViceCaptain.playerId == player.playerId){
                that.selectionErr = msgConstants.ERR_SEL_SAME_C_VC;
                showToast(that.selectionErr);
                return false;
            }

            if(that.selectedCaptain.playerId !== undefined){

                if(that.captainSelected){

                    if(that.selectedCaptain.playerId == player.playerId){
                        player.isCaptain = false;
                        that.selectedCaptain = {};
                        that.captainSelected = false;
                    }

                    else{

                        that.selectionErr = msgConstants.ERR_SEL_CAP_ALRDY_TKN;
                        showToast(that.selectionErr);
                    }

                }

            }

            else{
                player.isCaptain = true;
                that.selectedCaptain = player;
                that.captainSelected = true;
            }

        }

        that.mobileSelVCapClick = function(player){

            if(that.selectedCaptain.playerId == player.playerId){
                that.selectionErr = msgConstants.ERR_SEL_SAME_C_VC;
                showToast(that.selectionErr);
                return false;
            }


            if(that.selectedViceCaptain.playerId !== undefined){

                if(that.viceCaptainSelected){

                    if(that.selectedViceCaptain.playerId == player.playerId){
                        player.isViceCaptain = false;
                        that.selectedViceCaptain = {};
                        that.viceCaptainSelected = false;
                    }

                    else{
                        that.selectionErr = msgConstants.ERR_SEL_VCAP_ALRDY_TKN;
                        showToast(that.selectionErr);
                    }

                }

            }

            else{




                player.isViceCaptain = true;
                that.selectedViceCaptain = player;
                that.viceCaptainSelected = true;
            }

        }



        function showToast(msg){
            var elem =  document.querySelector(".m-selection-err");
            angular.element(elem).hide().show().delay(2000).fadeOut("slow");
        }


        // get save team button class UI

        that.getSaveTeamClass = function(){

            if((that.captainSelected && that.viceCaptainSelected) && (that.selectedSquadArray.length == 11)){
                return 'active';
            }
            else{
                return 'unactive';
            }

        }


        /*   function checkSelectedTeam(players){
            var selectedPlayers = {
                bttr:0,
                bwlr:0,
                alrdr:0,
                wk:0,
                bns:0
            }

            var length  = team.length;
            for(var j=0;j<length;j++){
                if(that.getPlayerRole(players[i]) == "alrdr"){
                    selectedPlayers.alrdr++;
                }

                if(that.getPlayerRole(players[i]) == "batter"){
                    selectedPlayers.bttr++;
                }


                if(that.getPlayerRole(players[i]) == "bowler"){
                    selectedPlayers.bwlr++;
                }

                if(that.getPlayerRole(players[i]) == "wk"){
                    selectedPlayers.wk++;
                }

                if(player[i].isBonus){
                    selectedPlayers.bns++;
                }

            }

            if(selectedPlayers.bns > 1){
                //console.log("max one bonus player allowed");
                return false;
            }


            if(selectedPlayers.bttr<3){
                //console.log("min 3 btr req");
                return false;
            }

             if(selectedPlayers.bwlr<3){
                //console.log("min 3 bwlr req");
                 return false;
            }

             if(selectedPlayers.wk == 0){
                //console.log("min 1 wk req");
                 return false;
            }

             if(selectedPlayers.alrdr == 0){
                //console.log("min 1 alrdr req");
                 return false;
            }



            if(selectedPlayers.bttr>5){
                //console.log("max 5 bttr select");
                return false;
            }

            if(selectedPlayers.bwlr>5){
                //console.log("max 5 bwlr select");
                return false;
            }

             if(selectedPlayers.alrdr>3){
                //console.log("max 3 alrdr req");
                 return false;
            }

             if(selectedPlayers.wk > 1){
                //console.log("max 1 wk sel");
                 return false;
            }

            return true;


        }*/


    }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:deskPopupHandlerCtrl
 * @description
 * # deskPopupHandlerCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('deskPopupHandlerCtrl',['$scope','$rootScope','ngDialog', function ($scope, $rootScope, ngDialog) {
    var that = this;
    that.showDeskPopup = function(options){

        var className = getClassName(options.popupSize)+" "+options.className;

        // Return false if template is not given
        if(options.template === '' || options.template === null){
            return false;
        }
        ngDialog.open({
            template: options.template,
            controller: options.controller,
            controllerAs: options.controllerAs,
            scope: options.scope,
            data: options.data,
            showClose: true/*options.showClose*/,
            closeByDocument: options.closeByDocument,
            closeByEscape: options.closeByEscape,
            overlay: options.overlay,
            preCloseCallback: options.preCloseCallback,
            /*appendTo: options.appendTo,*/
            className: className
        });
    };

    $scope.$on("showDeskPopup", function(event, data){
        var defaultOptions = {
            template: null,
            className: '',
            controller: null,
            controllerAs: null,
            scope: null,
            data: null,
            showClose: false,
            closeByDocument: false,
            closeByEscape: false,
            overlay: true,
            preCloseCallback: null,
            closeByNavigation: true,
            disableAnimation: true,
            appendTo: '#lobbycontainer',
            id: '',
            name: '',
            popupSize: 'desk-sm'
        };

        // Overriding default options with the given options
        var options = angular.extend({}, defaultOptions, data.options);
        ngDialog.close();

        that.showDeskPopup(options);
    });

    var getClassName = function(size){
        var ngDialogClass = '';
       switch(size){
            case 'desk-lg':
               ngDialogClass = 'ngdialog-theme-popup desk-lg';
                break;
            case 'desk-md':
                ngDialogClass = 'ngdialog-theme-popup desk-md';
                break;
            case 'desk-sm':
                ngDialogClass = 'ngdialog-theme-popup desk-sm';
                break;
            default:
                ngDialogClass = 'ngdialog-theme-popup';
        }
        return ngDialogClass;
    };

  }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:forgotPasswordCtrl
 * @description
 * # forgotPasswordCtrl
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('forgotPasswordCtrl', ['$rootScope', 'msgConstants', '$interval', 'utilityservice', 'config', 'ngDialog', 'serviceName', 'user', function ($rootScope, msgConstants, $interval, utilityservice, config, ngDialog, serviceName, user) {

 	var that = this;
 	that.isPhase = "PRE_PHASE";
 	that.popupTitle = "Forgot Password";
 	that.isResendDisabled = false;
 	that.userDataType = '';
 	that.userId = '';
 	that.userEmailId = '';
	that.userMobile = '';
 	that.otpChannel = '';

 	that.userInput = '';
 	that.otpInput = '';
 	that.userPassword = '';
 	that.errmsg_userInput = '';
 	that.errmsg_otpInput = '';
 	that.userPasswordMsg = '';

 	that.otpTimer = '';

 	that.checkAlphabet = function(event,fieldName){
		that.userMobileMsg='';
		var keyCode = event.keyCode;
		if(keyCode<48 || keyCode>57 ){
		   that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
		   //event.preventDefault();
		}
    };

	that.validatePrephase = function(){
		var isSuccess = true;
		if(that.userInput == ''){
			that.errmsg_userInput = msgConstants.REQUIRED_MESSAGE;
			isSuccess = false
		}

		if(isSuccess){
			var params = {
				userData: that.userInput
			};
			utilityservice._postAjaxCall(config.websiteNodeHost+ serviceName.FORGOT_PASSWORD_INIT, params, function (response) {
			 	if(response.respCode == 100){
			 		that.userId = response.responseData.userId;
			 		that.userEmailId = response.responseData.emailId;
					that.userMobile = response.responseData.mobileNo;
			 		that.setPlaceholder(response.responseData.userDataType);
			 		that.isPhase = "POST_PHASE";
			 	}else{
			 		that.errmsg_userInput = response.message;
			 	}
			});
		}
	}

	that.validatePostphase = function(){
		var isSuccess = true;
		if(that.otpInput.length !== 6){
			isSuccess = false;
			that.errmsg_otpInput = msgConstants.ERR_OTP;
		}
		user.OnInputFieldValidation('userPassword', that.userPassword,'', function(response) {
			if(response.resCode !== 100){
				isSuccess = false;
				that.userPasswordMsg = response.message;
			}
		});

		if(isSuccess){
			var md5Password  = CryptoJS.MD5(that.userPassword) + "";
			var params = {
				userId: that.userId,
				otp: that.otpInput,
				password: md5Password
			};
			utilityservice._postAjaxCall(config.websiteNodeHost+ serviceName.FORGOT_PASSWORD, params, function (response) {
			 	if(response.respCode == 100){
					ngDialog.close();
			 		$rootScope.$broadcast("showPopup", {
			 			type: "INFO",
			 			title: msgConstants.HEADER_INFO,
			 			appendTo:'#mainWrapper',
			 			msg: msgConstants.PASSWORD_CHANGE_SUCCESSFULLY,
			 			okButtonText: 'OK',
			 			okBtnCallback: function () {
			 			}
			 		});
			 	}else{
			 		that.errmsg_otpInput = response.message;
			 	}
			 });
		}
	};

	that.resendOtp = function(){
		that.isResendDisabled = true;
		var params = {
			userData: that.userInput
		};
		utilityservice._postAjaxCall(config.websiteNodeHost+ serviceName.FORGOT_PASSWORD_INIT, params, function (response) {
			if(response.responseCode == 100){
				//console.log('OTP resend successfully');
			}else{
				console.log("server error : ",response.message);
			}
		});
		setTimeout(function(){
			that.isResendDisabled = false;
		}, 5000);
	};

	that.setPlaceholder = function(type){
		switch(type){
			case 'userName':
			that.userDataType = 'Username';
			that.otpChannel = that.userEmailId;
			break;
			case 'mobileNo':
			that.userDataType = 'Mobile No.';
			that.otpChannel = that.userMobile;
			break;
			case 'emailId':
			that.userDataType = 'Email ID';
			that.otpChannel = that.userEmailId;
			break;
			default: 
			that.userDataType = "";
		}
	};

	that.resetForgotPasswordForm = function(){
		that.isPhase = "PRE_PHASE";
		that.popupTitle = "Forgot Password";
		that.userInput = '';
		that.otpInput = '';
		that.userPassword = '';
		that.errmsg_userInput = '';
		that.errmsg_otpInput = '';
		that.userPasswordMsg = '';
	};

}]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:headerCtrl
 * @description
 * # headerCtrl
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('headerCtrl', ['$rootScope','$scope', 'user','ngDialog', 'msgConstants', 'devicedetectionservice', 'utilityservice', 'config', 'session', '$window', '$location', 'serviceName','$http','$timeout', function ($rootScope,$scope, user, ngDialog, msgConstants, devicedetectionservice, utilityservice, config, session, $window, $location, serviceName,$http,$timeout) {
	var that = this;
    var isDesktop = devicedetectionservice.isDevice('DESKTOP');
	if(window.opener){
        // const url = '/private';
        // window.opener.open(url, '_self');
        // window.opener.focus();
        // window.close();
	}
	if(window.location.hash && window.location.hash){
		window.location.hash=''
	}

	 that.init = function (loginData) {
	 	if(loginData && loginData.token){
	 		session.setWebsiteToken(loginData.token);
	 		session.setScreenName(loginData.myName);
	 		session.setUserBalance(loginData.balance);
			session.setUserDOB(loginData.dob);
            session.setUserID(loginData.userId);
            session.setUserStatus(loginData.userStatus);
            if(loginData.userName !== undefined){
                session.setUserName(loginData.userName);
            }
			if(loginData.screenName !== undefined) {
				session.setTeamName(loginData.screenName);
			}
			if(loginData.isProduction){
				disableDevTools();	//Disabling right click and developer tools in browser
			}
	 	}
         /* Hightight mobile menu item ---Ashutosh */
         var a = window.location.href,
         b = a.lastIndexOf("/");
         utilityservice.setActiveMenu(a.substr(b + 1));

         //Add cash success failure response handling---Ashutosh
         $timeout(function(){
             var txnStatus = utilityservice.getUrlParameter("status");
             var txnID = utilityservice.getUrlParameter("txnid");
             var txnAmt = utilityservice.getUrlParameter("amount");
             var errCode = utilityservice.getUrlParameter("code");
             if(txnStatus !== undefined){
                 if(txnStatus == "success"){
                     var successMsg = "Cash amount of Rs "+txnAmt+" has been added to your LeagueAdda account, Now its time to win some real cash !!";
                     $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: successMsg,
                        popupSize: 'small',
                        okButtonText: 'OK',
                        okBtnCallback: function () {
                           $window.location.href = utilityservice.getBaseUrl() + "/myaccount";
                        }
                    });
                 }else {
                     var failureMsg = "";
                     if(errCode == 129){
                         failureMsg = msgConstants.TRANSACTION_FAILURE_1;
                     }else if(errCode == 131){
                         failureMsg = msgConstants.TRANSACTION_FAILURE_3;
                     }else if(errCode == 136){
                         failureMsg = msgConstants.ERR_FB_REGISTRATION;
                     }else {
                         failureMsg = msgConstants.TRANSACTION_FAILURE_2;
                     }
                     $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: failureMsg,
                        popupSize: 'small',
                        okButtonText: 'OK',
                        okBtnCallback: function () {
                           $window.location.href = utilityservice.getBaseUrl() + "/myaccount";
                        }
                    });
                 }
             }
         },100);

		$rootScope.serverTime = loginData.serverTime;
		$rootScope.serverDiff = loginData.serverTime - new Date().getTime();
	 }


	that.loginEmail = '';
	that.loginPassword = '';
	that.loginEmailError = '';
	that.loginPasswordError = '';

	that.onLoginClick = function(){

		var isSuccess = true;

		user.OnInputFieldValidation('loginEmail', that.loginEmail,'', function(response){
			if(response.resCode !== 100){
				isSuccess = false;
				that.loginEmailError = response.message;
			}
		});

		user.OnInputFieldValidation('loginPassword', that.loginPassword,'', function(response){
			if(response.resCode !== 100){
				isSuccess = false;
				that.loginPasswordError = response.message;
			}
		});

		if(isSuccess){
			var params = {};
			params.userData = that.loginEmail;
			params.password = CryptoJS.MD5(that.loginPassword)+'';
			utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.USER_LOGIN, params, function (response) {
				if(response.respCode === 100) {
					session.setWebsiteToken(response.respData.token);
					 if(isDesktop){
                        $window.location.href = utilityservice.getBaseUrl() + "/league";
                     } else{
                        $window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
                     }
				}else{
					that.loginEmailError = response.message;
				}
			});
		}
	}

	that.onLogoutClick = function(){
		var params = {};
		utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.USER_LOGOUT, params, function (response) {
				if(response.respCode === 100) {
					utilityservice.removeFromLocalStorage('currentMatchId');
					session.destroy();
				 	$window.location.href = utilityservice.getBaseUrl() + "/";
				}else{
					session.destroy();
					$window.location.href = utilityservice.getBaseUrl() + "/";
				}
			});
	}

	that.onForgotPasswordClick = function(){
		ngDialog.open({
			template: '/fantasy_sports/app/views/forgotpassword.html',
			overlay: true,
			closeByDocument: false,
			closeByEscape: false,
			showClose: true,
			closeByNavigation: false,
			disableAnimation: true,
			controller: 'forgotPasswordCtrl',
			controllerAs: 'forgot',
			appendTo: '#mainWrapper',
			className: 'ngdialog-theme-popup forgot-password-popup desk-sm'
		});
	}

	that.onFieldChange = function(fieldName, fieldValue, additionalInfo){
		user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
			if(response.resCode !== 100){
				that[fieldName+'Error'] = response.message;
			}else{
				console.log("resCode---->",response.resCode);
			}
		});
	};

	that.toggleMenu = function(){
		var mobMenuWrapper = document.querySelector('#mobMenuWrapper');
		if(mobMenuWrapper.classList.contains('open')){
			mobMenuWrapper.classList.remove('open');
		}
		else{
			mobMenuWrapper.classList.add('open');
		}
	}

	that.closeMenu = function(){
		if(document.querySelector('#mobMenuWrapper') != null) {
			var mobMenuWrapper = document.querySelector('#mobMenuWrapper');
			mobMenuWrapper.classList.remove('open');
		}
	}

	that.onChangePassword = function(){
		that.closeMenu();
		ngDialog.open({
			template: '/fantasy_sports/app/views/changepassword.html',
			overlay: true,
			closeByDocument: false,
			closeByEscape: false,
			showClose: false,
			closeByNavigation: false,
			disableAnimation: true,
			controller: 'ChangepasswordCtrl',
			controllerAs: 'changePassword',
			appendTo: '#mainWrapper',
			className: 'ngdialog-theme-popup forgot-password-popup desk-sm'
		});
	};

	that.addCash = function() {
		var data = {
			balance: 0,
			entryFee: 0,
			title: 'ADD MONEY TO YOUR ACCOUNT',
			showCurrentBalance: false,
		};
		ngDialog.open({
			template: '/fantasy_sports/app/views/addcash.html',
			controller:"addCashCtrl",
			controllerAs: "addcash",
			overlay: true,
			closeByDocument: false,
			closeByEscape: false,
			closeByNavigation: true,
			disableAnimation: true,
			appendTo: '#mainWrapper',
			className: 'ngdialog-theme-popup addcash-popup desk-sm',
			showClose: true,
			resolve: {
				leaguedata: function() {
					return data;
				}
			}
		});
	};

	that.onLogoClick = function(){
		if(utilityservice.isPresentInURL('createteam') || utilityservice.isPresentInURL('editteam')){
			return false;
		}
		$window.location.href = utilityservice.getBaseUrl() + "/";
	}

	function disableDevTools(){
		//disabling right click
		document.oncontextmenu = function(event) {
	        event.preventDefault();
	    }

	    window.addEventListener("keydown", function(event){
	    	if(event.keyCode == 123){
	    		event.preventDefault();
			    return false;
			   }
			else if(event.ctrlKey && event.shiftKey && event.keyCode == 73){        
			      event.preventDefault();
			      return false;  //Prevent from ctrl+shift+i
			   }
	    });
	}

 }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:leaguesCtrl
 * @description
 * # leaguesCtrl as leagues
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
    .controller('leaguesCtrl', ['$window', '$location', '$scope', 'constants', 'leagueservice', 'utilityservice', '$timeout', '$rootScope', 'devicedetectionservice', 'ngDialog', 'session', 'createteamservice', 'config', 'serviceName', 'leagueutilservice', function ($window, $location, $scope, constants, leagueservice, utilityservice, $timeout, $rootScope, devicedetectionservice, ngDialog, session, createteamservice, config, serviceName, leagueutilservice) {
        var that = this;

        that.baseUrl = utilityservice.getBaseUrl();
        that.screenName = session.getScreenName();
        that.selectedTour = null;
        that.initSlick = false;
        that.carouselIndex = 0;
        that.leagueDisplayType = 'ACTIVE';
        that.selectedMatch = null;
        that.nextMatchTime = [];
        that.isJoinBtnDisabled = false;
        that.isShowLeagueTab = true;
        that.currentLeagueTab = "";
        that.myTeamsArray = [];
        that.currentMatchTeamCount = 0;
        that.showTeamDropdown = false;
        that.noLeaguesError = false;
        that.noMyLeaguesError = false;

        var checkLeagueResponse = null;

        var pageSize = 200;

        that.pagination = {
            currentPage: pageSize,
            pageSize: pageSize
        };

        // init function called when controller loads
        that.init = function () {

            var invCode = utilityservice.getUrlParameter('invCode');
            if (session.getTeamName() === '' || session.getTeamName() === null || session.getTeamName() === undefined) {
                leagueutilservice.myTeamNamePopoup();
            }

            if(invCode !== undefined && invCode !== ""){
                var league = {
                    leagueId: invCode
                };
                leagueutilservice.checkJoinLeague(league, function(checkLeagueResponse){
                    fetchTours();
                });
            }
            else{
                fetchTours();
            }    
        };

        that.resetPagination = function(){
            that.pagination = {
                currentPage: pageSize,
                pageSize: pageSize
            }
        };

        that.updatePagination = function(league, type){
            //var limit = that.pagination.pageSize;
            switch(type){
                case 'SHOW_MORE':
                    that.pagination.currentPage = that.pagination.currentPage + that.pagination.pageSize;
                    break;
                case 'SHOW_TOP':
                    that.pagination.currentPage = that.pagination.pageSize;
                    break;
                case 'SHOW_PREVIOUS':
                    that.pagination.currentPage = that.pagination.currentPage - that.pagination.pageSize;
                    break;
            }
            
            var params = {
                matchId: that.selectedMatch.matchId,
                leagueId: league.leagueId,
                limit: that.pagination.currentPage,
                limitSize: pageSize
            };
            that.leagueMembersArray = [];
            utilityservice.showLoader();
            leagueservice.getLeagueMembers(params, function (leagueMembersArray) {
               that.leagueMembersArray = leagueMembersArray;
                utilityservice.hideLoader();
            });
        };

        that.leagueTabHandler = function (tabType) {
            that.currentLeagueTab = tabType;
            switch (tabType) {
                case 'PUBLIC_LEAGUE':
                    that.fetchLeagues(that.selectedMatch.matchId);
                    break;
                case 'MY_LEAGUE':
                    fetchJoinedLeagues();
                    break;
                default:
            }
        };

        that.teamTabHandler = function (currentTeam, index) {
            that.currentTeamId = currentTeam.teamId;
            that.fetchTeamInfo(currentTeam.teamId);
            resetLeagueAccordions();
        };

        that.browseLeagueHandler = function (joinedLeague, index) {
            if(joinedLeague.isOpen){
                return false;
            }
            that.resetPagination(pageSize);
            fetchLeagueMembers(joinedLeague.leagueId, index);
        };

        that.onTourClick = function () {
            var tourWrapper = document.querySelector('.cstm-tour-select');
            if (tourWrapper.classList.contains('open')) {
                tourWrapper.classList.remove('open');
            }
            else {
                tourWrapper.classList.add('open');
            }
        };

        that.closeTourDropdown = function(){
            var tourWrapper = document.querySelector('.cstm-tour-select');
            tourWrapper.classList.remove('open');
        };

        that.validateJoinLeague = function (league) {
            return leagueutilservice.validateJoinLeague(league);
        };

        that.reJoinLeague = function (league, event) {
            event.preventDefault();
            event.stopPropagation();
            return leagueutilservice.validateJoinLeague(league);
        };

        that.onTourSelect = function (tour) {
            utilityservice.removeFromLocalStorage('currentMatchId');
            that.selectedTour = tour;
            fetchMatches(tour.tourId);
            var tourWrapper = document.querySelector('.cstm-tour-select');
            tourWrapper.classList.remove('open');
        };

        that.onMatchClick = function (match, index) {
            that.selectedMatch.isSelectedMatch = false;
            getActiveMatchData(index);
        };

        that.fetchLeaguePrizeInfo = function (league) {
            var params = {};
            params.leagueId = league.leagueId;
            leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
                leagueutilservice.prizeInfoPopup(prizeinfodata);
            });
        };

        // get league prize info popup from joined league title click
        that.getLeaguePrizeInfo = function (league, event) {
            if(league.chipType.toUpperCase() == 'SKILL'){
                return false;
            }
            event.preventDefault();
            event.stopPropagation();
            var params = {};
            params.leagueId = league.leagueId;
            leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
                leagueutilservice.prizeInfoPopup(prizeinfodata);
            });
        };

        that.fetchLeagues = function (matchid) {
            var params = {};
            params.matchId = matchid;
            utilityservice.showLoader();
            that.noLeaguesError = false;
            leagueservice.getLeagues(params, function (response) {
                that.noLeaguesError = true;
                if (response.respCode === 100) {
                    that.leaguesArray = response.respData;
                }
                else {
                    console.log('Error in Fetching leagues', response.message);
                }
                utilityservice.hideLoader();
            });
        };


        that.onCreateTeamClick = function () {
            if(that.selectedMatch.totalTeams === undefined || that.selectedMatch.totalTeams === 0){
                that.selectedMatch.totalTeams = 0;
            }
            $window.location.href = that.baseUrl + "/createteam/" + that.selectedMatch.matchId + '/' + (that.selectedMatch.totalTeams + 1);
        };

        that.onEditTeamClick = function () {
            $window.location.href = that.baseUrl + "/editteam/" + that.selectedMatch.matchId + '/' + that.currentTeamId;
        };

        that.fetchTeamInfo = function (teamId, event) {
            if(event !== undefined){
                var currentLi = event.currentTarget;
            }
            var params = {};
            params.matchId = that.selectedMatch.matchId;
            params.teamId = teamId;
            utilityservice.showLoader();

            leagueservice.getTeamInfo(params, function (responseData) {

                that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.currentTeam.players);
                that.myTeamInfo = responseData;
                that.myTeamsArray = responseData.teamsArr;
                that.currentTeamInfo = responseData.currentTeam;	//to be reviewed
                that.currentTeamInfo.teamName = responseData.screenName;
                that.currentTeamId = teamId;
                utilityservice.hideLoader();
                if(event !== undefined){
                    highlightList('joined-leagues-list', currentLi);
                }
            });
        };

        that.fetchOpponentTeamInfo = function (data, event) {
            if(event !== undefined){
                var currentLi = event.currentTarget;
            }
            var params = {};
            params.matchId = that.selectedMatch.matchId;
            params.teamId = data.teamId;
            params.opponentId = data.userId;
            utilityservice.showLoader();
            leagueservice.getOpponentTeamInfo(params, function (responseData) {
                that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.opponentTeam.players);
                that.currentTeamInfo = responseData.opponentTeam;
                that.currentTeamInfo.teamName = responseData.screenName;
                that.currentTeamId = -1;    // resetting teamId to remove highlight from team tabs
                if(event !== undefined){
                    highlightList('joined-leagues-list', currentLi);
                }
                utilityservice.hideLoader();
            });
        };

        that.onTeamUpdateClick = function (data) {
            data.showTeamDropdown = true;
            data.newTeamId = data.teamId.toString();
            //console.log('teams array ::', that.myTeamsArray);
        };

        that.updateMyTeam = function (data) {
            var params = {
                teamId: data.newTeamId,
                leagueJoinedId: data.leagueJoinedId,
                leagueId: data.leagueId,
                matchId: that.selectedMatch.matchId
            };

            leagueservice.updateUserJoinedTeam(params, function (response) {
                data.showTeamDropdown = false;
                data.teamId = data.newTeamId;
            });
        };

        that.getMatchStatus = function (status) {
            return leagueutilservice.getMatchStatus(status);
        };

        that.onScorecardClick = function(){
            $window.open(that.baseUrl + "/fantasy-scorecard/" + that.selectedMatch.matchId, '_blank');
        };

        that.onRefreshClick = function(){
            fetchJoinedLeagues();
            that.fetchTeamInfo(that.currentTeamId);
        };

        // Private Functions
        function fetchTours() {
            var params = {};
            leagueservice.getTours(params, function (toursArray) {
                var alltour = {shortName: 'All', status: 'ACTIVE', tourId: -1, tourName: 'All Series'};
                toursArray.unshift(alltour);
                that.toursArray = toursArray;
                that.selectedTour = that.toursArray[0];
                fetchMatches(-1);
            });
        }

        function fetchMatches(tourid) {
            var params = {};
            if (tourid !== -1) {
                params.tourId = tourid;
            }
            that.initSlick = false;
            leagueservice.getMatches(params, function (matchesArray, nextTimeTour) {
                that.matchesArray = matchesArray;
                if(that.matchesArray.length == 0){
                    return false;
                }
                var activeMatchIndex = leagueutilservice.getActiveMatch(that.matchesArray);
                getActiveMatchData(activeMatchIndex);
                // Timeout to show matches carousel
                $timeout(function () {
                    that.initSlick = true;
                    // Timeout to slide matches carousel
                    that.carouselIndex = 0;
                    $timeout(function () {
                        that.carouselIndex = activeMatchIndex;
                    }, 400);
                },100);

                
                
                if (tourid === -1) {
                    leagueutilservice.updateTourTimer(that.toursArray, nextTimeTour);
                }
            });
        }

        function getActiveMatchData(matchIndex) {
            var currentMatch = that.matchesArray[matchIndex];
            that.selectedMatch = currentMatch;
            that.matchesArray[matchIndex].isSelectedMatch = true;
            that.leaguesArray = [];
            that.matchPlayersArray = [];
            that.joinedLeaguesArray = [];
            that.myTeamsArray = [];
            that.currentTeamInfo = {};
            that.myTeamInfo = {};
            that.currentMatchTeamCount = that.selectedMatch.totalTeams;

            var teamId = leagueutilservice.getActiveMatchTeamId(that.selectedMatch.matchId);
            that.currentTeamId = teamId;

            //Save matchId to local storage
            utilityservice.setToLocalStorage('currentMatchId', that.selectedMatch.matchId);

            if (currentMatch.status === 'LOCKED') {
                that.currentLeagueTab = '';
                that.leagueDisplayType = 'LOCKED';
                return false;
            } else if (currentMatch.status === 'ACTIVE') {
                that.leagueDisplayType = 'ACTIVE';
                that.currentLeagueTab = 'PUBLIC_LEAGUE';
                if (that.currentMatchTeamCount !== 0) {
                    that.fetchTeamInfo(teamId);
                }
                that.fetchLeagues(that.selectedMatch.matchId);
            } else {
                that.leagueDisplayType = 'CLOSED';
                that.currentLeagueTab = 'MY_LEAGUE';
                if (that.currentMatchTeamCount !== 0) {
                    that.fetchTeamInfo(teamId);
                }
                fetchJoinedLeagues();
            }
        }
        
        function fetchJoinedLeagues() {
            that.joinedLeaguesArray = [];
            var params = {
                matchId: that.selectedMatch.matchId
            };
            that.noMyLeaguesError = false;
            leagueservice.getJoinedLeagues(params, function (joinedLeaguesArray) {
                for(var i = 0, len = joinedLeaguesArray.length; i < len; i++){
                    joinedLeaguesArray[i].isOpen = false;
                }
                that.noMyLeaguesError = true;
                that.joinedLeaguesArray = joinedLeaguesArray;
                
            });
        }

        function fetchLeagueMembers(leagueid, index) {
            var params = {
                matchId: that.selectedMatch.matchId,
                leagueId: leagueid,
                limit: pageSize,
                limitSize: pageSize
            };
            that.leagueMembersArray = [];
            leagueservice.getLeagueMembers(params, function (leagueMembersArray) {
                that.leagueMembersArray = leagueMembersArray;

                var userTeam = leagueMembersArray.userTeam;
                that.currentTeamId = userTeam.currentTeam.teamId;
                // data to show in team status in ground section
                that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(userTeam.currentTeam.players);
                that.currentTeamInfo.teamId = userTeam.currentTeam.teamId;
                that.currentTeamInfo.teamName = userTeam.screenName;
                that.currentTeamInfo.teamPoints = userTeam.currentTeam.teamPoints;

                that.myTeamsArray = userTeam.teamsArr;
                var currentTeamCount = that.currentMatchTeamCount;
                var teamArr = [];
                for (var i = 1; i <= currentTeamCount; i++) {
                    teamArr.push({teamId: i});
                }
                that.currentTeamArr = teamArr;
                // Updating toprank and teamid on opening of my leagues accordion
                that.joinedLeaguesArray[index].rank = that.leagueMembersArray.leagueUsers[0].rank;
                that.joinedLeaguesArray[index].teamId = that.leagueMembersArray.leagueUsers[0].teamId;

            });
        }

        function checkAutoLeagueJoinFlow(){
            var invCode = utilityservice.getUrlParameter('invCode');
            var isAutoJoinFlow = (invCode !== undefined && invCode !== "");
            if(isAutoJoinFlow){
                var league = {
                    matchId: 1,
                    leagueId: invCode
                }
                leagueutilservice.validateJoinLeague(league);
            }
        }

        function highlightList(ulSelector, currentLi){
            if(currentLi === undefined){
                currentLi = document.querySelectorAll('.' + ulSelector + ' li:eq(0)');
            }
            var liList = document.querySelectorAll('.' + ulSelector + ' li');
            if(liList === undefined){
                return false;
            }
            for(var i = 0, len = liList.length; i < len; i++){
                liList[i].classList.remove('active');
            }
            currentLi.classList.add('active');
        }

        function resetLeagueAccordions(){
            for(var i = 0, len = that.joinedLeaguesArray.length; i < len; i++){
                that.joinedLeaguesArray[i].isOpen = false;
            }
        }

        $rootScope.$on('fetchLeaguesEvent', function(event, args){
            // Update league count for the current Match
            if(args.leagueCount !== undefined){
                that.selectedMatch.leagueCount = args.leagueCount;    
            }

            var invCode = utilityservice.getUrlParameter('invCode');
            if(invCode !== undefined && invCode !== ""){
                $window.location.href = $window.location.href.split('?')[0];    // Removing query string after successful league join if invCode is present
            }
            else{
                that.fetchLeagues(args.matchId);
                fetchJoinedLeagues();
            }
        });
}]);
'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('MainCtrl',['$rootScope', 'constants', 'ngDialog', 'user', 'config', 'utilityservice', 'session', '$window', 'devicedetectionservice', 'serviceName','dateDropdownHandler', 'msgConstants', function ($rootScope, constants, ngDialog, user, config, utilityservice, session, $window, devicedetectionservice, serviceName,dateDropdownHandler, msgConstants) {
		var that = this;
		that.isSignup = true;
		that.testiArray = constants.TESTIMONIAL_ARRAY;
		that.initSlick = true;
		var isDesktop = devicedetectionservice.isDevice('DESKTOP');

  	      //Calender Module functionality Starts here---Ashutosh
      that.selectedMonth = "MM";
      that.selectedYear = "YY";
      that.selectedDate = "DD";
      that.dobRange = [];
      that.dobRange.push("YY");
      that.monthName = constants.MONTH_LIST;
      that.dateRange = [];
      dateDropdownHandler.initilizeYears(function(yearRange){
            that.dobRange = yearRange;
      });
      dateDropdownHandler.initilizeDays(function(dayRange){
            that.dateRange = dayRange;
      });

      that.getKey = function(month) {
            return Object.keys(month)[0];
      };
      // On year change function--Ashu.
      that.changeYear = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getMonths(that.selectedYear, function(monthList){
                  that.monthName = monthList;
            });
            that.checkDays();
      };
      // On day change function
      that.dayChange = function(){
            that.dateEmptyvalidator();
      };

      // On month change calucate days to show in dropdown--Ashu
      that.checkDays = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getDays(that.selectedYear, that.selectedMonth, function(dayList){
                  that.dateRange = dayList;
                  if(that.selectedDate > dayList.length-1) {
					  that.selectedDate = "DD";
				  }
            });
      };

      that.dateEmptyvalidator = function(){
            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = "Please fill all date fields.";
            }else {
                that.userDateMsg = '';
            }
      };
      //Calender Module functionality Ends

     that.showSignupLogin = function(homeTabType, event){
			var homeTabs = document.querySelectorAll('#homeTabs li');
			var currentTab = event.target;
			for(var i = 0; i < homeTabs.length; i++){
				homeTabs[i].classList.remove('active');
			}
			currentTab.classList.add('active');
			switch(homeTabType){
				case 'REGISTER':
					that.isSignup = true;
					that.userLoginEmail = '';
					that.userLoginEmailMsg = '';
					that.userLoginPassword = '';
					that.userLoginPasswordMsg = '';
					break;
				case 'LOGIN':
					that.isSignup = false;
					that.userEmail = '';
					that.userEmailMsg = '';
					that.userPassword = '';
					that.userPasswordMsg = '';
					that.selectedYear = 'YY';
					that.selectedMonth = 'MM';
					that.selectedDate = 'DD';
					that.userDateMsg = '';
					that.userTerms = false;
					that.userTermsMsg = '';
					break;
				default:
			}
		};

		that.onForgotPasswordClick = function(){
				ngDialog.open({
					template: '/fantasy_sports/app/views/forgotpassword.html',
					overlay: true,
					closeByDocument: false,
					closeByEscape: false,
					showClose: true,
					closeByNavigation: false,
					disableAnimation: true,
					controller: 'forgotPasswordCtrl',
					controllerAs: 'forgot',
					appendTo: '#mainWrapper',
					className: 'ngdialog-theme-popup forgot-password-popup'
				});
		};

		that.onFieldChange = function(fieldName,fieldValue,additionalInfo){
		  user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
			if(response.resCode !== 100){
				that[fieldName+'Msg'] = response.message;
			}else{
				//console.log("resCodeInputField---->",response.resCode);
			}
		  });
		};

		that.checkTNC = function() {
				if(that.userTerms) {
						that.userTermsMsg = '';
				}else {
						that.userTermsMsg = msgConstants.ERR_TNC;
				}
		};

		/* Signup Api call & validation--Ashu*/
		that.signup = function(){
				that.isValidate = true;
				that.userEmailMsg = '';
				that.userPasswordMsg = '';

				user.OnInputFieldValidation('userEmail', that.userEmail,'', function(response) {
						if(response.resCode !== 100){
							that.isValidate = false;
							that.userEmailMsg = response.message;
						}
				});

				user.OnInputFieldValidation('userPassword', that.userPassword,'', function(response) {
						if(response.resCode !== 100){
							that.isValidate = false;
							that.userPasswordMsg = response.message;
						}
				});

				if(that.selectedYear=="YY" || that.selectedYear=="" || that.selectedMonth=="MM" || that.selectedMonth=="" || that.selectedDate=="DD" || that.selectedDate=="") {
						that.isValidate = false;
						that.userDateMsg = msgConstants.REQUIRED_MESSAGE_1;
				}

				if(!that.userTerms){
					   that.isValidate = false;
					   that.userTermsMsg = msgConstants.ERR_TNC;
				}

				//Call Api if all validations are pass.
				if(that.isValidate) {
						var params = {};
						params.emailId = that.userEmail;
						params.password = CryptoJS.MD5(that.userPassword)+'';
						params.dob = that.selectedMonth+"/"+that.selectedDate+"/"+that.selectedYear;
						utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.USER_SIGNUP, params, function (response) {
								if(response.respCode === 100) {
										$rootScope.$broadcast("showPopup", {
											  type: "Info",
											  title: msgConstants.HEADER_INFO,
											  appendTo:'#mainWrapper',
											  msg: msgConstants.MSG_SIGNUP_SUCCESS,
											  popupSize: 'small',
											  okButtonText: 'OK',
											  okBtnCallback: function () {
												ngDialog.close();
												var params = {};
												params.userData = that.userEmail;
												params.password = CryptoJS.MD5(that.userPassword)+'';
												that.loginServiceCall(params);
											  }
										});
								}else if (response.respCode == 115){
										that.userEmailMsg = response.message;
								}else{
										console.log("some error occured",response);
								}
						});

				}
		};

		/* Login Api call and validations */
		that.login = function() {
				that.isValidate = true;
				that.userLoginEmailMsg = '';
				that.userLoginPasswordMsg = '';

				user.OnInputFieldValidation('userLoginEmail', that.userLoginEmail,'', function(response) {
						if(response.resCode !== 100){
							that.isValidate = false;
							that.userLoginEmailMsg = response.message;
						}
				});

				user.OnInputFieldValidation('userLoginPassword', that.userLoginPassword,'', function(response) {
						if(response.resCode !== 100){
							that.isValidate = false;
							that.userLoginPasswordMsg = response.message;
						}
				});
				if(that.isValidate) {
						var params = {};
						params.userData = that.userLoginEmail;
						params.password = CryptoJS.MD5(that.userLoginPassword)+'';
						that.loginServiceCall(params);
				}
		   };

		   that.loginServiceCall = function(params) {
				utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.USER_LOGIN, params, function (response) {
						if(response.respCode === 100) {
								session.setWebsiteToken(response.respData.token);
								if(isDesktop){
									$window.location.href = utilityservice.getBaseUrl() + "/league";
								}
								else{
									$window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
								}
						}else{
								console.log("some error occured",response);
								that.userLoginPasswordMsg = response.message;
						}
				});
		   };

		  //  that.onGoogleLoign = function(){
				// console.log('google login');
				// var winOpts = {
				// 	wUrl: "auth/google",
				// 	wHeight: 600,
				// 	wWidth: 450,
				// 	wName: 'Google Login'
				// }
				// var newWindow = window.open(winOpts.wUrl, winOpts.wName, 'height=' + winOpts.wHeight + ',width=' + winOpts.wWidth + ', top=' + ((window.innerHeight - winOpts.wHeight) / 2) + ', left=' + ((window.innerWidth - winOpts.wWidth) / 2));
				// if (window.focus) {
				// 	newWindow.focus();
				// }
		  //  	};

		 //   that.onFbLoign = function(event){
			// 	console.log('ONFB Login');
			// 	if(event.target.href !== ''){
			// 		window.location.href = event.target.href;
			// 		event.target.removeAttribute('href');
			// 	}
				
			// 	event.preventDefault();
			// };

			that.clickAndDisable = function(event) {
				if(event.target.href !== ''){
					window.location.href = event.target.href;
					event.target.removeAttribute('href');
				}
			};
  }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:MyaccountCtrl
 * @description
 * # MyaccountCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('MyaccountCtrl',['session','$rootScope','$window','utilityservice','msgConstants','config','ngDialog','serviceName','depositeservice','devicedetectionservice', function (session, $rootScope, $window, utilityservice , msgConstants, config, ngDialog, serviceName, depositeservice, devicedetectionservice) {
        var that = this;
        var isDesktop = devicedetectionservice.isDevice('DESKTOP');
        that.userStatus = session.getUserStatus();
        that.downloadTXNhistory = config.websiteNodeHost+serviceName.REQUEST_TXN_HISTORY;
        angular.element(document).ready(function () {
              utilityservice.showLoader();
              var params = {};
              utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.MYACCOUNT_INFO, params, function (response) {
                    utilityservice.hideLoader();
                    if(response.respCode == 100) {
                      var respData = response.respData;
                      that.totalAmt = (respData.depositAmt + respData.bonusAmt + respData.winningAmt).toFixed(2);

                      that.depositeAmt = respData.depositAmt.toFixed(2);
                      that.bonusAmt = respData.bonusAmt.toFixed(2);
                      that.winningAmt = respData.winningAmt.toFixed(2);

                      that.totalWinning = respData.totalWinning.toFixed(2);
                      that.leaguesWin = respData.leaguesWin;
                    }
              });

              var params1 = {userId:session.getUserID(), limit : 50};
              utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.TXN_HISTORY, params1, function (response) {
                    if(response.respCode == 100) {
                        that.TranactionArr = response.respData;
                        for(var i=0; i < that.TranactionArr.length; i++){
                            var date = new Date(that.TranactionArr[i].createdAt);
                            that.TranactionArr[i].createdAt = String(date.getMonth() + 1)+"/"+date.getDate()+"/"+date.getFullYear();
                        }
                    }
              });
              depositeservice.afterPayment();
        });

        that.showLeaguepage = function() {
            if(isDesktop){
                  $window.location.href = utilityservice.getBaseUrl() + "/league";
            } else{
                  $window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
            }
        };
        that.showVerifyNow = function() {
            $window.location.href = utilityservice.getBaseUrl() + "/verify";
        };

        that.downloadHistory = function(){
            $rootScope.$broadcast("showPopup", {
              type: "Confirm",
              title: msgConstants.HEADER_CONFIRMATION,
              appendTo:'#mainWrapper',
              msg: msgConstants.DOWNLOAD_TXN_HISTORY,
              okButtonText: 'YES',
              cancelButtonText: 'NO',
              okBtnCallback: function () {
                    $window.location.href = that.downloadTXNhistory;
              }
            });
        };

        that.addCash = function() {
            var data = {
				balance: 0,
				entryFee: 0,
                title: 'ADD MONEY TO YOUR ACCOUNT',
                showCurrentBalance: false,
			};
			ngDialog.open({
				template: '/fantasy_sports/app/views/addcash.html',
				controller:"addCashCtrl",
				controllerAs: "addcash",
				overlay: true,
				closeByDocument: false,
				closeByEscape: false,
				closeByNavigation: true,
				disableAnimation: true,
				appendTo: '#mainWrapper',
				className: 'ngdialog-theme-popup addcash-popup desk-sm',
				showClose: true,
				resolve: {
			        leaguedata: function() {
			            return data;
			        }
		        }
			});

        };

        that.withdrawCash = function() {
            var data = {
				amt: that.winningAmt,
			};
            ngDialog.open({
				template: '/fantasy_sports/app/views/withdrawcash.html',
                controller:"WithdrawcashCtrl",
				controllerAs: "withdrawcash",
				overlay: true,
				closeByDocument: false,
				closeByEscape: false,
				closeByNavigation: true,
				disableAnimation: true,
				appendTo: '#mainWrapper',
				className: 'ngdialog-theme-popup addcash-popup desk-sm',
				showClose: true,
                resolve: {
			        winningAmt: function() {
			            return data;
			        }
		        }
			});

        };
  }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:MyprofileCtrl
 * @description
 * # MyprofileCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('MyprofileCtrl',['constants','$rootScope','utilityservice','config','devicedetectionservice','user','ngDialog','$window','serviceName','dateDropdownHandler', 'msgConstants', function (constants, $rootScope, utilityservice, config, devicedetectionservice, user, ngDialog, $window, serviceName, dateDropdownHandler, msgConstants) {
      var that = this;
      that.userName = '';
      that.userTeamName = '';
      that.userEmail = '';
      that.userMobile = '';
      that.User_DOB = '';
      that.isUserNameDisable = false;
      that.isTeamNameDisable = false;
      that.isEmailDisable = false;
      that.isMobileDisable = false;
      that.selectedState = "Select State";
      that.statesArray = constants.STATES_LIST;
      that.submitDisable = true;
      that.errMsg = "";
      var isDesktop = devicedetectionservice.isDevice('DESKTOP');

      //Calender Module functionality Starts here---Ashutosh
      that.selectedMonth = "MM";
      that.selectedYear = "YY";
      that.selectedDate = "DD";
      that.dobRange = [];
      that.dobRange.push("YY");
      that.monthName = constants.MONTH_LIST;
      that.dateRange = [];
      dateDropdownHandler.initilizeYears(function(yearRange){
            that.dobRange = yearRange;
      });
      dateDropdownHandler.initilizeDays(function(dayRange){
            that.dateRange = dayRange;
      });

      that.getKey = function(month) {
            return Object.keys(month)[0];
      };
      // On year change function--Ashu.
      that.changeYear = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getMonths(that.selectedYear, function(monthList){
                  that.monthName = monthList;
            });
            that.checkDays();
      };
      // On day change function
      that.dayChange = function(){
            that.dateEmptyvalidator();
      };

      // On month change calucate days to show in dropdown--Ashu
      that.checkDays = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getDays(that.selectedYear, that.selectedMonth, function(dayList){
                  that.dateRange = dayList;
                  if(that.selectedDate > dayList.length-1) {
					  that.selectedDate = "DD";
				  }
            });
      };

      that.dateEmptyvalidator = function(){
            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = "Please fill all date fields.";
            }else {
                that.userDateMsg = '';
            }
      };
      //Calender Module functionality Ends


      angular.element(document).ready(function () {
            var params = {};
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.GETUSER_PROFILE, params, function (response) {
                if(response.respCode === 100){
                    if(response.respData.isEmailVerified == "NO"  || response.respData.isMobileVerified == "NO"){
                          that.submitDisable = true;
                          that.errMsg = msgConstants.VERIFY_EMAIL_MOBILE;
                    }else{
                          that.submitDisable = false;
                          that.errMsg = "";
                    }
                    that.userName = response.respData.firstName;
                    that.userTeamName = response.respData.screenName;
                    if(response.respData.dob != null) {
                          that.User_DOB = new Date(response.respData.dob);
                          that.selectedMonth = String(that.User_DOB.getMonth() + 1);
                          if(that.selectedMonth < 9) {
                              that.selectedMonth = '0'+that.selectedMonth;
                          }
                          that.selectedDate = String(that.User_DOB.getDate());
                          if(that.selectedDate < 9) {
                              that.selectedDate = '0'+that.selectedDate;
                          }
                          that.selectedYear = that.User_DOB.getFullYear();
                    }
                    that.userGender = response.respData.gender;
                    that.userEmail = response.respData.emailId;
                    that.userMobile = response.respData.mobileNbr;
                    that.userAddress = response.respData.address;
                    that.userCity = response.respData.city;
                    that.userPincode = response.respData.pinCode;
                    if(response.respData.state!= null && response.respData.state!= ''){
                          that.selectedState = response.respData.state;
                    }

                    //that.setDisableEnableInput();
                }else {
                    $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: response.message,
                        popupSize: 'small',
                        okButtonText: 'OK'
                    });
                }
            });
      });

      /* Update profile on form submit */
      that.updateProfile = function() {
          that.isValidate = true;
          that.userNameMsg = '';
          that.userTeamNameMsg = '';
          that.userEmailMsg = '';
          that.userMobileMsg = '';

          user.OnInputFieldValidation('userName', that.userName,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userNameMsg = response.message;
                }
          });

          /*user.OnInputFieldValidation('userTeamName', that.userTeamName,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userTeamNameMsg = response.message;
                }
          });

          user.OnInputFieldValidation('userEmail', that.userEmail,'', function(response){
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userEmailMsg = response.message;
                }
          });

          user.OnInputFieldValidation('userMobile', that.userMobile,'', function(response){
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userMobileMsg = response.message;
                }
          });*/


          //Call Api if all validations are pass.
          if(that.isValidate) {
              var params = {};
              var updateDob = '';
              if(that.selectedYear=="YY" || that.selectedYear=="" || that.selectedMonth=="MM" || that.selectedMonth=="" || that.selectedDate=="DD" || that.selectedDate=="") {
                  updateDob = '';
              }else {
                  updateDob = (that.selectedMonth + '/' + that.selectedDate + '/' +  that.selectedYear);
              }
              params.firstName = that.userName;
              //params.screen_name = that.userTeamName;
              //params.emailId = that.userEmail;
              //params.mobileNbr = that.userMobile;
              params.dob = updateDob;
              params.gender = that.userGender;
              params.address = that.userAddress;
              params.city = that.userCity;
              params.pinCode = that.userPincode;
              params.state = that.selectedState;
              params.country = "India";

              utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.UPDATE_PROFILE, params, function (response) {
                  if(response.respCode == 100) {
                        ngDialog.close();
                        $rootScope.$broadcast("showPopup", {
                              type: "Info",
                              title: msgConstants.HEADER_INFO,
                              appendTo:'#mainWrapper',
                              msg: msgConstants.MSG_PROFILE_UPDATED_SUCCESS,
                              popupSize: 'small',
                              okButtonText: 'OK',
                              okBtnCallback: function () {
                                 if(isDesktop){
                                    $window.location.reload();
                                 } else{
                                    $window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
                                 }
			 			      }
                        });
                  }else {
                        $rootScope.$broadcast("showPopup", {
                              type: "Info",
                              title: msgConstants.HEADER_INFO,
                              appendTo:'#mainWrapper',
                              msg: response.message,
                              popupSize: 'small',
                              okButtonText: 'OK'
                        });
                  }
              });
          }
      };

      that.setDisableEnableInput = function() {
            if(that.userName != '' && that.userName != null) {
                that.isUserNameDisable = true;
            }
            if(that.userTeamName != '' && that.userTeamName != null) {
                that.isTeamNameDisable = true;
            }
            if(that.userEmail != '' && that.userEmail != null) {
                that.isEmailDisable = true;
            }
            if(that.userMobile != '' && that.userMobile != null) {
                that.isMobileDisable = true;
            }
      };

      that.checkAlphabet = function(event,fieldName){
            that.userMobileMsg='';
            var keyCode = event.keyCode;
            if(keyCode<48 || keyCode>57 ){
               that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
               //event.preventDefault();
            }
      };

      that.onFieldChange = function(fieldName,fieldValue,additionalInfo){
          user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
            if(response.resCode !== 100){
                that[fieldName+'Msg'] = response.message;
            }else{
                console.log("resCode---->",response.message);
            }
          });
      };

  }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:MyteamnameCtrl
 * @description
 * # MyteamnameCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('MyteamnameCtrl',['$rootScope','$window','constants','user','msgConstants','session','config','utilityservice','serviceName','dateDropdownHandler','$timeout', function ($rootScope, $window, constants, user, msgConstants, session, config, utilityservice,  serviceName, dateDropdownHandler,$timeout) {
      var that = this;
      that.userTerms = false;
      that.selectedState = "Select State";
      that.statesArray = constants.STATES_LIST;

      //Calender Module functionality Starts here---Ashutosh
      that.selectedMonth = "MM";
      that.selectedYear = "YY";
      that.selectedDate = "DD";
      that.dobRange = [];
      that.dobRange.push("YY");
      that.monthName = constants.MONTH_LIST;
      that.dateRange = [];
      dateDropdownHandler.initilizeYears(function(yearRange){
            that.dobRange = yearRange;
      });
      dateDropdownHandler.initilizeDays(function(dayRange){
            that.dateRange = dayRange;
      });

      that.getKey = function(month) {
            return Object.keys(month)[0];
      };
      // On year change function--Ashu.
      that.changeYear = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getMonths(that.selectedYear, function(monthList){
                  that.monthName = monthList;
            });
            that.checkDays();
      };
      // On day change function
      that.dayChange = function(){
            that.dateEmptyvalidator();
      };

      // On month change calucate days to show in dropdown--Ashu
      that.checkDays = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getDays(that.selectedYear, that.selectedMonth, function(dayList){
                  that.dateRange = dayList;
                  if(that.selectedDate > dayList.length-1) {
					  that.selectedDate = "DD";
				  }
            });
      };

      that.dateEmptyvalidator = function(){
            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = "Please fill all date fields.";
            }else {
                that.userDateMsg = '';
            }
      };
      //Calender Module functionality Ends

      that.onFieldChange = function(fieldName,fieldValue,additionalInfo){
          user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
            if(response.resCode !== 100){
                that[fieldName+'Msg'] = response.message;
            }else{
                console.log("resCode---->",response.resCode);
            }
          });
      };

       that.init = function() {
             if(session.getUserDOB() != undefined && session.getUserDOB() != null && session.getUserDOB() != '') {
                    that.User_DOB = new Date(session.getUserDOB());
                    that.userTeamName = session.getTeamName();
                    //console.log("that.userDOB---",that.User_DOB+"    that.screenName--",that.userTeamName);
                    that.selectedMonth = String(that.User_DOB.getMonth() + 1);
                    if(that.selectedMonth < 9) {
                        that.selectedMonth = '0'+that.selectedMonth;
                    }
                    that.selectedDate = String(that.User_DOB.getDate());
                    if(that.selectedDate < 9) {
                        that.selectedDate = '0'+that.selectedDate;
                    }
                    that.selectedYear = that.User_DOB.getFullYear();
              }
       };

       that.changeState = function() {
            if(that.selectedState=="Select State" || that.selectedState=="") {
                  that.userStateMsg = msgConstants.REQUIRED_MESSAGE;
            }else {
                  that.userStateMsg = '';
            }
       };

       that.checkTNC = function() {
				if(that.userTerms) {
						that.userTermsMsg = '';
				}else {
						that.userTermsMsg = msgConstants.ERR_TNC;
				}
		};

       that.updateProfile = function() {
            that.isValidate = true;
            that.userTeamNameMsg = '';
            that.userDateMsg = '';
            that.userStateMsg = '';

            user.OnInputFieldValidation('userTeamName', that.userTeamName,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userTeamNameMsg = response.message;
                }
            });

            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = msgConstants.REQUIRED_MESSAGE;
                that.isValidate = false;
            }else {
                that.userDateMsg = '';
            }

            if(that.selectedYear=="YY" || that.selectedYear==null || that.selectedMonth=="MM" || that.selectedMonth==null || that.selectedDate=="DD" || that.selectedDate==null) {
                that.userDateMsg = msgConstants.REQUIRED_MESSAGE;
                that.isValidate = false;
            }

            if(that.selectedState=="Select State" || that.selectedState=="" ) {
                that.userStateMsg = msgConstants.REQUIRED_MESSAGE;
                that.isValidate = false;
            }

            if(!that.userTerms){
                 that.isValidate = false;
                 that.userTermsMsg = msgConstants.ERR_TNC;
            }
            if(that.isValidate) {
                  var params = {};
                  var updateDob = '';
                  updateDob = (that.selectedMonth + '/' + that.selectedDate + '/' +  that.selectedYear);

                  params.dob = updateDob;
                  params.screenName = that.userTeamName;
                  params.state = that.selectedState;
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.UPDATE_TEAMNAME, params, function (response) {
                        if(response.respCode == 100) {
                              $window.location.reload();
                        }else{
                              that.isValidate = false;
                              that.userTeamNameMsg = response.message;
                        }
                  });
            }

       };

  }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:PopupCtrl
 * @description
 * # PopupCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
 .controller('PopupCtrl',['$scope','$rootScope','ngDialog', function ($scope, $rootScope, ngDialog) {
    var that = this;

    that.showCancelBtn = false;
    that.ngDialogId = $scope.ngDialogData.id || $scope.ngDialogId;
    that.type = $scope.ngDialogData.type;
    that.msg = ($scope.ngDialogData.msg !== undefined && $scope.ngDialogData.msg !== null) ? $scope.ngDialogData.msg : "";
    that.title = ($scope.ngDialogData.title !== undefined && $scope.ngDialogData.title !== null) ? $scope.ngDialogData.title : "";
    that.showOkBtn = false;
    that.popupSize = 'medium';
    that.okButtonText = ($scope.ngDialogData.okButtonText !== undefined && $scope.ngDialogData.okButtonText !== null) ?  $scope.ngDialogData.okButtonText : "OK";
    that.closeOnOkBtnClick = ($scope.ngDialogData.closeOnOkBtnClick !== undefined && $scope.ngDialogData.closeOnOkBtnClick !== null) ? $scope.ngDialogData.closeOnOkBtnClick : true;
    that.showCrossBtn = ($scope.ngDialogData.showCrossBtn !== undefined && $scope.ngDialogData.showCrossBtn !== null) ? $scope.ngDialogData.showCrossBtn : true;
    that.cancelButtonText = ($scope.ngDialogData.cancelButtonText !== undefined && $scope.ngDialogData.cancelButtonText !== null) ? $scope.ngDialogData.cancelButtonText : "CANCEL";
    that.closeOnCancelBtnClick = ($scope.ngDialogData.closeOnCancelBtnClick !== undefined && $scope.ngDialogData.closeOnCancelBtnClick !== null) ? $scope.ngDialogData.closeOnCancelBtnClick : true;

    if((that.type.toUpperCase() === "ALERT") || (that.type.toUpperCase() === "INFO")){
        that.title = that.title || that.type;
        that.showOkBtn = true;
        that.showCancelBtn = false;
    } else if(that.type.toUpperCase() === "CONFIRM"){
        that.showOkBtn = true;
        that.showCancelBtn = true;
    }
    else if(that.type.toUpperCase() === "RECONNECTION"){
         that.showOkBtn = false;
    }

    that.onOkClick = function(e){
        e.stopPropagation();
        if($scope.ngDialogData.okBtnCallback){
            $scope.ngDialogData.okBtnCallback();
        }
        if(that.closeOnOkBtnClick){
            ngDialog.close(that.ngDialogId, 0);
        }
    };

    that.onCancelClick = function(e){
        e.stopPropagation();
        if($scope.ngDialogData.cancelBtnCallback){
            $scope.ngDialogData.cancelBtnCallback();
        }
        if(that.closeOnCancelBtnClick){
            ngDialog.close(that.ngDialogId, 0);
        }
    };

    that.closePopup = function(e){
        e.stopPropagation();
        if((that.type.toUpperCase() === "ALERT") || (that.type.toUpperCase() === "INFO")){
            if($scope.ngDialogData.okBtnCallback){
                $scope.ngDialogData.okBtnCallback();
            }
        } else{
            if($scope.ngDialogData.cancelBtnCallback){
                $scope.ngDialogData.cancelBtnCallback();
            }
        }
        ngDialog.close(that.ngDialogId, 0);
    };
}]);

'use strict';
/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:PopuphandlerCtrl
 * @description
 * # PopuphandlerCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
.controller('PopuphandlerCtrl',['$scope','$rootScope','ngDialog','$timeout','devicedetectionservice', function ($scope, $rootScope, ngDialog, $timeout, devicedetectionservice) {
    var that = this;

    that.showPopup = function(data){
        $rootScope.isAnyPopupOpen = true;
        var ngDialogClass = '';
        ngDialog.close();
        switch(data.popupSize){
            case 'large':
                ngDialogClass = 'ngdialog-theme-popup large-popup';
                break;
            default: 
                ngDialogClass = 'ngdialog-theme-popup';
        }

        var isDesktop = devicedetectionservice.isDevice('DESKTOP');
        if(isDesktop)
            ngDialogClass += ' desk-popup-size';

        ngDialog.open({
            preCloseCallback: function(){
                if(data.preCloseCallback){
                    data.preCloseCallback();
                }
            },
            template:data.template,
            controller:"PopupCtrl",
            controllerAs: "popup",
            data: data,
            id: data.id,
            scope : $scope,
            overlay: true,
            closeByDocument: false,
            closeByEscape: false,
            closeByNavigation: true,
            disableAnimation: true,
            appendTo: data.appendTo,
            className: ngDialogClass,
            showClose: data.showClose,
            name: data.id
        }).closePromise.then(function() {
            $rootScope.isAnyPopupOpen = false;
            if(data.closePromise){
                data.closePromise();
            }
        });
    };

    that.closePopup = function (id) {
        if (id === undefined) {
            id = '';
        }
        ngDialog.close(id);
    };

    $scope.$on("showPopup", function(event, data){
        if(data.template === undefined || data.template === null){
            data.template = "/fantasy_sports/app/views/popup.html";
        }
        if(data.appendTo === undefined || data.appendTo === null){
            data.appendTo = false;
        }
        if(data.id === undefined || data.id === null){
            data.id = "";
        }
        if(data.showClose === undefined || data.showClose === null){
            data.showClose = false;
        }
        that.showPopup(data);
    });

    $scope.$on('closePopup', function (event, data) {
        that.closePopup(data.id);
    });


    that.showHideLoader = function(data){
        var loader = document.querySelector('#loaderDom');
        if(loader !== undefined && loader !== null){
            if(data.delay){
                $timeout(function(){
                    loader.style.display = data.display;
                },data.delay);
            }else{
                loader.style.display = data.display;
            }
        }else{
            console.log('could not detect loader DOM');
        }
    };

    $scope.$on('showhideLoader', function (event, data) {
        that.showHideLoader(data);
    });

}]);
'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:prizeInfoCtrl
 * @description
 * # prizeInfoCtrl
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 	.controller('prizeInfoCtrl', function () {
		var that = this;

		console.log('prizeInfoCtrl');
	
 });

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:PromotionCtrl
 * @description
 * # PromotionCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
    .controller('PromotionCtrl', ['$window','$rootScope','$timeout', 'promotionsservice', 'utilityservice', function ($window,$rootScope,$timeout, promotionsservice, utilityservice) {
        var that = this;

        that.init = function(){
            var params ={};
            promotionsservice.getPromotions(params, function(response){
                //console.log("data from service",response);
                that.promotionsData = response;

            });  
        }


        that.showPromtionDetails = function(e, index){
            var promotionImageUrl = that.promotionsData[index].fields.popup_banner_desktop.url;
            var imgDom = document.createElement("img");
            imgDom.src = promotionImageUrl;
            imgDom.className = "w100"; 
            var imgWrapper = document.createElement("div");
            imgDom.appendChild(imgWrapper);
            
            var tmp = document.createElement("div");
            tmp.appendChild(imgDom);


            that.promoDetailsData = {};
            that.promoDetailsData.headerName = that.promotionsData[index].name;
            that.promoDetailsData.content = "<div class='clearfix h100 w100 promotion-details-wrapper'><div class= 'w50 left-float'>"+tmp.innerHTML+"</div><div class='w40 pd-h2_5 right-float'>"+that.promotionsData[index].fields.popup_content.text+"</div>";
            //console.log(that.promoDetailsData.content);
            that.promoDetailsData.pageCss = that.promotionsData[index].fields.popup_content_css.text;
            that.promoDetails = true;
            $timeout(function(){
                that.iframeLoadedCallBack();
            },200);
        };

        that.hidePromtionDetails = function(){
            that.promoDetails = false;
            //var wrapper = angular.element(document.querySelector(".promotions-bg"));
           // wrapper.removeClass("popup-bg");
        };

        that.iframeLoadedCallBack = function () {
            var cssLink = that.promoDetailsData.pageCss;
            var pageData = '';
            pageData = that.promoDetailsData.content;
            var iFrame = angular.element(document.querySelector('iframe.promo-details-popup'))[0];

            var iFrameBody;
            if (iFrame.contentWindow.document){ // FF
                iFrameBody = angular.element(iFrame.contentDocument.getElementsByTagName('body')[0]);
            } else if (iFrame.contentWindow.document){ // IE
                iFrameBody = angular.element(iFrame.contentWindow.document.getElementsByTagName('body')[0]);
            }

            var iFrameHead;
            if (iFrame.contentWindow.document){ // FF
                iFrameHead = angular.element(iFrame.contentDocument.getElementsByTagName('head')[0]);
            } else if (iFrame.contentWindow.document){ // IE
                iFrameHead = angular.element(iFrame.contentWindow.document.getElementsByTagName('head')[0]);
            }
            iFrameHead.append(cssLink);
            iFrameBody.append(pageData);
        };


    }]);
'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:creatTeamCtrl
 * @description
 * # creatTeamCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
    .controller('selectCapVcapCtrl', ['$window','constants', 'createteamservice', 'utilityservice','devicedetectionservice', function ($window, constants, createteamservice , utilityservice, devicedetectionservice) {
    var that = this;
    
   
    
    
    }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:VerifynowCtrl
 * @description
 * # VerifynowCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('VerifynowCtrl',['$rootScope', '$scope', 'constants', '$window', 'config', 'serviceName', 'utilityservice', 'devicedetectionservice', 'session', 'user', 'Upload', 'msgConstants','dateDropdownHandler' , function ($rootScope, $scope, constants, $window, config, serviceName, utilityservice, devicedetectionservice, session, user, Upload, msgConstants, dateDropdownHandler) {
      var that = this;
      that.userStatus = session.getUserStatus();
      that.selectedState = "Select State";
      that.statesArray = constants.STATES_LIST;
      that.isMobileVerified = false;
      that.ismOTPsent = true;
      that.isverifyMobile = false;
      that.isEmailVerified = false;
      that.iseOTPsent = true;
      that.isverifyEmail = false;
      that.mobileOTP = '';
      that.emailOTP = '';
      var isDesktop = devicedetectionservice.isDevice('DESKTOP');
      that.isPanDisabled = true;
      that.isBankDisabled = true;
      that.isPanCardVerified = "NO";
      that.isBankDetailVerified = "NO";

      //Calender Module functionality Starts here---Ashutosh
      that.selectedMonth = "MM";
      that.selectedYear = "YY";
      that.selectedDate = "DD";
      that.dobRange = [];
      that.dobRange.push("YY");
      that.monthName = constants.MONTH_LIST;
      that.dateRange = [];
      dateDropdownHandler.initilizeYears(function(yearRange){
            that.dobRange = yearRange;
      });
      dateDropdownHandler.initilizeDays(function(dayRange){
            that.dateRange = dayRange;
      });

      that.getKey = function(month) {
            return Object.keys(month)[0];
      };
      // On year change function--Ashu.
      that.changeYear = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getMonths(that.selectedYear, function(monthList){
                  that.monthName = monthList;
            });
            that.checkDays();
      };
      // On day change function
      that.dayChange = function(){
            that.dateEmptyvalidator();
      };

      // On month change calucate days to show in dropdown--Ashu
      that.checkDays = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getDays(that.selectedYear, that.selectedMonth, function(dayList){
                  that.dateRange = dayList;
                  if(that.selectedDate > dayList.length-1) {
					  that.selectedDate = "DD";
				  }
            });
      };

      that.dateEmptyvalidator = function(){
            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = "Please fill all date fields.";
            }else {
                that.userDateMsg = '';
            }
      };
      //Calender Module functionality Ends

      that.checkAlphabet = function(event,fieldName){
            that.userMobileMsg='';
            var keyCode = event.keyCode;
            if(keyCode<48 || keyCode>57 ){
               that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
               //event.preventDefault();
            }
      };

      angular.element(document).ready(function () {
            utilityservice.showLoader();
            var params = {};
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.GETUSER_PROFILE, params, function (response) {
                utilityservice.hideLoader();
                if(response.respCode === 100){
                    that.userEmail = response.respData.emailId;
                    if(response.respData.mobileNbr !== null){
                        that.userMobile = response.respData.mobileNbr;
                    }
                    if(response.respData.isPanCardVerified !== null) {
                          that.isPanCardVerified = response.respData.isPanCardVerified;
                    }
                    if(response.respData.isBankDetailVerified !== null){
                          that.isBankDetailVerified = response.respData.isBankDetailVerified;
                    }
                    if(that.isBankDetailVerified == "NO"){
                          that.stateCheck = false;
                    }else{
                          that.stateCheck = true;
                    }

                    if(response.respData.isEmailVerified.toUpperCase() == "NO"  || response.respData.isMobileVerified.toUpperCase() == "NO"){
                          that.isPanDisabled = true;
                          that.isBankDisabled = true;
                    }else{
                          that.isPanDisabled = false;
                          that.isBankDisabled = false;
                    }

                    if(response.respData.isEmailVerified.toUpperCase() == "NO"){
                        that.isEmailVerified = false;
                        that.isverifyEmail = false;
                        that.iseOTPsent = true;
                    }else if(response.respData.isEmailVerified.toUpperCase() == "YES"){
                        that.isverifyEmail = false;
                        that.iseOTPsent = false;
                        that.isEmailVerified = true;
                    }

                    if(response.respData.isMobileVerified.toUpperCase() == "NO"){
                        that.isMobileVerified = false;
                        that.isverifyMobile = false;
                        that.ismOTPsent = true;
                    }else if(response.respData.isMobileVerified.toUpperCase() == "YES"){
                        that.isverifyMobile = false;
                        that.ismOTPsent = false;
                        that.isMobileVerified = true;
                    }

                    if(response.respData.state!= null && response.respData.state!= ''){
                          that.selectedState = response.respData.state;
                    }
                    if(response.respData.dob != null) {
                          that.User_DOB = new Date(response.respData.dob);
                          that.selectedMonth = String(that.User_DOB.getMonth() + 1);
                          if(that.selectedMonth < 9) {
                              that.selectedMonth = '0'+that.selectedMonth;
                          }
                          that.selectedDate = String(that.User_DOB.getDate());
                          if(that.selectedDate < 9) {
                              that.selectedDate = '0'+that.selectedDate;
                          }
                          that.selectedYear = that.User_DOB.getFullYear();
                    }
                }else {
                    $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: response.message,
                        popupSize: 'small',
                        okButtonText: 'OK'
                    });
                }
            });
      });

      that.showLeaguepage = function() {
            if(isDesktop){
                  $window.location.href = utilityservice.getBaseUrl() + "/league";
            } else{
                  $window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
            }
      };

      that.sendOtpMobile = function() {
            var isValidate = true;
            that.userMobileMsg = '';
            user.OnInputFieldValidation('userMobile', that.userMobile,'', function(response){
                if(response.resCode !== 100){
                    isValidate = false;
                    that.userMobileMsg = response.message;
                }
            });

            if(isValidate){
                  var params = {
                        mobileNumber:that.userMobile
                  };
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.VERIFY_MOBILE, params, function (response) {
                        if(response.respCode === 100){
                              that.isMobileVerified = false;
                              that.ismOTPsent = false;
                              that.isverifyMobile = true;
                        }else {
                              that.userMobileMsg = response.message;
                        }
                  });
            }
      };

      that.sendOtpEmail = function() {
            var isValidate = true;
            that.userEmailMsg = '';
            user.OnInputFieldValidation('userEmail', that.userEmail,'', function(response){
                if(response.resCode !== 100){
                    isValidate = false;
                    that.userEmailMsg = response.message;
                }
            });

            if(isValidate){
                  var params = {
                        emailId: that.userEmail
                  };
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.VERIFY_EMAIL, params, function (response) {
                        if(response.respCode === 100){
                              that.isEmailVerified = false;
                              that.iseOTPsent = false;
                              that.isverifyEmail = true;
                        }else {
                              that.userEmailMsg = response.message;
                        }
                  });
            }
      };

      that.mobileVerifyOTP = function() {
            if(that.mobileOTP.length == 6){
                  var params = {
                        otp: that.mobileOTP
                  };
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.OTP_MOBILE, params, function (response) {
                        if(response.respCode === 100){
                              that.ismOTPsent = false;
                              that.isverifyMobile = false;
                              that.isMobileVerified = true;
                              if(that.isEmailVerified == true && that.isMobileVerified == true) {
                                    that.isPanDisabled = false;
                                    that.isBankDisabled = false;
                              }else{
                                    that.isPanDisabled = true;
                                    that.isBankDisabled = true;
                              }
                        }else{
                              that.mobileOTPMsg = response.message;
                        }
                  });
            }else {
                  that.mobileOTPMsg = msgConstants.ERR_OTP;
            }
      };

      that.emailVerifyOTP = function() {
            if(that.emailOTP.length == 6){
                  var params = {
                        otp: that.emailOTP
                  };
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.OTP_EMAIL, params, function (response) {
                        if(response.respCode === 100){
                              that.iseOTPsent = false;
                              that.isverifyEmail = false;
                              that.isEmailVerified = true;
                              if(that.isEmailVerified == true && that.isMobileVerified == true) {
                                    that.isPanDisabled = false;
                                    that.isBankDisabled = false;
                              }else{
                                    that.isPanDisabled = true;
                                    that.isBankDisabled = true;
                              }
                        }else{
                              that.emailOTPMsg = response.message;
                        }
                  });
            }else {
                  that.emailOTPMsg = msgConstants.ERR_OTP;
            }
      };

      that.submitPanDetails = function(){
            var isValidate = true;
            that.errMsg = '';
            if($scope.files != undefined && $scope.files !== null){
                  if($scope.files.length == 0) {
                        isValidate = false;
                        that.errMsg = msgConstants.ERR_PAN_3;
                  }
            }else{
                  isValidate = false;
                  that.errMsg = msgConstants.ERR_PAN_3;
            }

            if(that.isValidDoc === false){
                  isValidate = false;
                  that.errMsg = msgConstants.ERR_PAN_5;
                  that.fileName = '';
                  $scope.files = [];
            }

            if(that.selectedState == "Select State") {
                  isValidate = false;
                  that.errMsg = msgConstants.ERR_STATE;
            }
            user.OnInputFieldValidation('userPAN', that.userPAN,'', function(response) {
                if(response.resCode !== 100){
                    isValidate = false;
                    that.errMsg = response.message;
                }
            });
            if(that.userName !== undefined && that.userName !== null){
                 if(that.userName.length === 0){
                     isValidate = false;
                     that.errMsg = msgConstants.ERR_FULLNAME;
                 }else if(that.userName.length < 2 || that.userName.length > 20){
                     isValidate = false;
                     that.errMsg = msgConstants.ERR_FULLNAME_3;
                 }else if (!user.aplphaNumericWithSpace.test(that.userName)) {
                     isValidate = false;
                     that.errMsg = msgConstants.ERR_FULLNAME_2;
                 }
            }else{
                isValidate = false;
                that.errMsg = msgConstants.ERR_FULLNAME;
            }

            if(isValidate){
                Upload.upload({
                    url: config.websiteNodeHost + serviceName.PAN_UPLOAD,
                    data: {panCardImage: $scope.files[0], 'token': session.getWebsiteToken(),'name':that.userName, 'panNumber':that.userPAN, 'state':that.selectedState}
                }).then(function (resp) {
                    that.fileName = '';
                    $scope.files = [];
                    if(resp.data.respCode == 100){
                          $rootScope.$broadcast("showPopup", {
                              type: "Info",
                              title: msgConstants.HEADER_INFO,
                              appendTo:'#mainWrapper',
                              msg: msgConstants.MSG_PANCARD_SAVED_SUCCESS,
                              popupSize: 'small',
                              okButtonText: 'OK',
                              okBtnCallback: function () {
                                    $window.location.reload();
                              }
                          });
                    }else{
                          that.errMsg = resp.data.message;
                    }
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }/*, function (evt) {
                   var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                   console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                }*/);
            }
      };

      $scope.saveImageFile = function (files, errFiles) {
            that.errMsg='';
            $scope.files = [];
            that.isValidDoc = false;
            that.fileName ='';
            that.errFile = errFiles;
            if (files != '') {
                  $scope.files.push(files[0]);
                  $scope.imageCollection = $scope.files;
                  var fileType = $scope.imageCollection[0].type;
                  var type = fileType.split('/')[1];
                  if(type === 'jpg' || type === 'png' || type === 'jpeg' || type === 'bmp' || type === 'pdf'){
                        that.isValidDoc = true;
                  }
                  that.fileName = $scope.imageCollection[0].name;
            }
            if(errFiles != ''){
                  that.errFile = errFiles && errFiles[0];
                  if(that.errFile !== undefined || that.errFile !== '') {
                        that.errMsg = msgConstants.ERR_LARGE_FILE;
                        //console.log("Error File selection--->",that.errFile);
                  }
            }
      };

      that.submitBankDetails = function() {
            var isValidate = true;
            that.errBankMsg = '';

           if(!that.stateCheck){
                 isValidate = false;
                 that.errBankMsg = msgConstants.ERR_STATE_CHECK;
           }

           if(that.branchName !== undefined && that.branchName !== null){
                if(that.branchName.length === 0){
                    isValidate = false;
                    that.errBankMsg = msgConstants.ERR_BRANCH_NAME;
                }
           }else{
                isValidate = false;
                that.errBankMsg = msgConstants.ERR_BRANCH_NAME;
           }

           if(that.bankName !== undefined && that.bankName !== null){
                if(that.bankName.length === 0){
                    isValidate = false;
                    that.errBankMsg = msgConstants.ERR_BANK_NAME;
                }
           }else{
                isValidate = false;
                that.errBankMsg = msgConstants.ERR_BANK_NAME;
           }

           if(that.ifscCode !== undefined && that.ifscCode !== null){
                if(that.ifscCode.length === 0){
                    isValidate = false;
                    that.errBankMsg = msgConstants.ERR_IFSC_CODE;
                }
           }else{
                isValidate = false;
                that.errBankMsg = msgConstants.ERR_IFSC_CODE;
           }

           if(that.accountNo !== undefined && that.accountNo !== null){
                if(that.accountNo.length === 0){
                    isValidate = false;
                    that.errBankMsg = msgConstants.ERR_BANK_ACCOUNT;
                }
           }else{
                isValidate = false;
                that.errBankMsg = msgConstants.ERR_BANK_ACCOUNT;
           }

           if(isValidate) {
                 var params = {};
                 params.userId = session.getUserID();
                 params.bankName = that.bankName;
                 params.accountNumber = that.accountNo;
                 params.branch = that.branchName;
                 params.ifscCode = that.ifscCode;
                 utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.ADD_BANK_ACCOUNT, params, function (response) {
                        if(response.respCode === 100){
                              $rootScope.$broadcast("showPopup", {
                                    type: "Info",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.MSG_BANK_DETAIL_SAVED_SUCCESS,
                                    popupSize: 'small',
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                          $window.location.reload();
                                    }
                              });
                        }else {
                              that.errBankMsg = response.message;
                        }
                 });
           }

      };

      that.getPanDetail = function(){
            utilityservice.showLoader();
            var params = {};
            params.userId = session.getUserID();
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.PAN_DETAILS, params, function (response) {
                  utilityservice.hideLoader();
                  if(response.respCode === 100){
                        that.userName = response.respData.name;
                        that.userPAN = response.respData.panNumber;
                        that.selectedState = response.respData.state;
                  }else {
                        console.log("PAN DETAILS ERROR--",response.message);
                  }
           });
      };

      that.getBankDetail = function(){
            utilityservice.showLoader();
            var params = {};
            params.userId = session.getUserID();
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.BANK_DETAILS, params, function (response) {
                  utilityservice.hideLoader();
                  if(response.respCode === 100){
                        that.bankName = response.respData.bankName;
                        that.accountNo = response.respData.accountNumber;
                        that.branchName = response.respData.branch;
                        that.ifscCode = response.respData.ifscCode;
                  }else {
                        console.log("BANK DETAILS ERROR--",response.message);
                  }
           });
      };


  }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:WithdrawcashCtrl
 * @description
 * # WithdrawcashCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('WithdrawcashCtrl',['$rootScope','config','msgConstants','utilityservice','serviceName','session','winningAmt','ngDialog','$window','$timeout', function ($rootScope, config, msgConstants, utilityservice, serviceName, session, winningAmt, ngDialog, $window,$timeout) {
        var that = this;
        that.popupTitle = msgConstants.HEADER_WITHDRAW_CASH;
        that.showWithdraw = true;
        that.amount = null;
        that.winningAmt = winningAmt.amt;
        $timeout(function(){
            angular.element('.amt').trigger('focus');
        },100);

        that.withdraw = function() {
            var isSuccess = true;
			if(Number(that.amount) < 200){
				that.amountError = msgConstants.ERR_WITHDRAW_1;
				isSuccess = false;
			}
			if(Number(that.amount) > 200000){
				that.amountError = msgConstants.ERR_WITHDRAW_2;
				isSuccess = false;
			}
            if(Number(that.amount) > Number(that.winningAmt)) {
                that.amountError = msgConstants.ERR_INSUFFICIENT_WINNING;
                isSuccess = false;
            }

			if(isSuccess){
                that.showWithdraw = false;
                that.popupTitle = msgConstants.HEADER_CONFIRMATION;
            }
        };

        that.cancelWithdraw = function() {
            that.showWithdraw = true;
            that.popupTitle = msgConstants.HEADER_WITHDRAW_CASH;
        };

        that.checkAlphabet = function(event,fieldName){
            var keyCode = event.keyCode;
            if(keyCode<48 || keyCode>57 ){
               that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
               //event.preventDefault();
            }
        };

        that.confirmWithdraw = function() {
            var params = {};
            params.userId = session.getUserID();
            params.txnType = "withdrawl";
            params.cancelRemarks = "www";
            params.amount = Number(that.amount);
            var title = "";
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.WITHDRAW, params, function (response) {
                ngDialog.close();
                if(response.respCode == 100) {
                    title = "CONGRATULATIONS";
                }else{
                    title = "FAILURE";
                }
                $rootScope.$broadcast("showPopup", {
                      type: "Info",
                      title: msgConstants.HEADER_INFO,
                      appendTo:'#mainWrapper',
                      msg: response.message,
                      popupSize: 'small',
                      okButtonText: 'OK',
                      okBtnCallback: function () {
                            if(title == "CONGRATULATIONS"){
                                  $window.location.reload();
                            }
                      }
                });
            });
        };
  }]);

'use strict';

/**
 * @ngdoc directive
 * @name fantasyCricketApp.directive:accordionClick
 * @description
 * # accordionClick
 */
angular.module('fantasyCricketApp')
  .directive('accordionClick', [function () {
    return {
      restrict: 'A',
      link: function (scope, element, attr) {
            //element.bind('click', function () {
                // console.log('test click',scope, element, attr);
                // var allAccordions = document.querySelectorAll('.tabbs');
                // for(var i = 0, len = allAccordions.length; i < len; i++){
                //     allAccordions[i].classList.remove('active');
                // }

                // var currentAccordion = document.querySelector('#' + attr.activeId);
                // currentAccordion.classList.add('active');

            //});
        }
    };
  }]);

'use strict';

/**
 * @ngdoc directive
 * @name fantasyCricketApp.directive:activemenu
 * @description
 * # activemenu
 */

/*  This directives functionality need some below attributes in html dom:-
 obj currentrow =====> which gives the data of that row menu.

 if you are not using this currentrow data from html dom then you can send currentrow obj from js code as {id:val}
 By Ashutosh
 */
angular.module('fantasyCricketApp')
  .directive('activemenu', [function () {
    return {
      restrict: 'A',
      link: function (scope, element, attr) {
            element.bind('click', function () {
                var el;
                var arrlength = '';

                function handelActiveClass(el) {
                    arrlength = el.length;
                    for (var j = 0; j < arrlength ; j++) {
                        el[j].removeClassName('active');
                    }
                    if (attr != '' && attr != null) {
                        var val = attr.viewid;
                        if (document.querySelector('[viewid="' + val + '"]')) {
                            document.querySelector('[viewid="' + val + '"]').addClassName('active');
                        }
                    }
                }

                el = document.querySelectorAll('.menu-list li');
                handelActiveClass(el);
            });
        }
    };
  }]);

'use strict';

/**
 * @ngdoc directive
 * @name fantasyCricketApp.directive:clickAnywhereButHere
 * @description
 * # clickAnywhereButHere
 */
 angular.module('fantasyCricketApp')
 	.directive('clickAnywhereButHere', ['$document', function($document){
	return {
		restrict: 'A',
		link: function(scope, elem, attr, ctrl) {
			elem.bind('click', function(e) {
				// this part keeps it from firing the click on the document.
				e.stopPropagation();
			});
			$document.bind('click', function() {
				// magic here.
				scope.$apply(attr.clickAnywhereButHere);
			})
		}
	}
 }]);

'use strict';

/**
 * @ngdoc directive
 * @name fantasyCricketApp.directive:countdown
 * @description
 * # countdown
 */
angular.module('fantasyCricketApp')
	.directive('countdown', ['$interval', '$rootScope', 'utilityservice', '$http', function ($interval, $rootScope, utilityservice, $http) {
		return {
				restrict: 'A',
				scope: false,
				link: function (scope, element, attrs) {
					var future;
					future = new Date(parseInt(attrs.date));

					$rootScope.$on('tickingEvent', function(event, args){
	                	future = new Date(parseInt(attrs.date));
	                	if(isNaN(future)){
							return element.text('Closed');
						}
	                	var diff;
	                	diff = Math.floor((future.getTime() - (new Date().getTime() + $rootScope.serverDiff)) / 1000);
						//diff = Math.floor((future.getTime() - $rootScope.serverTime) / 1000);
						if(diff < 0){
							return element.text('00 : 00 : 00');
						}
						return element.text(dhms(diff));
	                });

					var dhms = function (t) {
	                    var days, hours, minutes, seconds;
	                    // days = Math.floor(t / 86400);
	                    // t -= days * 86400;
	                    hours = Math.floor(t / 3600);
	                    t -= hours * 3600;
	                    minutes = Math.floor(t / 60) % 60;
	                    t -= minutes * 60;
	                    seconds = t % 60;
	                    return formatTime(days, hours, minutes, seconds);
	                }

	                var formatTime = function(days, hours, minutes, seconds){
	                	return [
	                		// days + ' :',
	                        //hours + ' :',    //('0' + hours).slice(-2)
	                        hours >= 10 ? hours : "0" + hours.toString(), ' :',
	                        ('0' + minutes).slice(-2)  + ' :',
	                        ('0' + seconds).slice(-2)
	                    ].join(' ');
	                }
				}
		};
	}]);

'use strict';

/**
 * @ngdoc directive
 * @name fantasyCricketApp.directive:deskpopupclick
 * @description
 * # deskpopupclick
 */
angular.module('fantasyCricketApp')
  .directive('deskpopupclick',['$rootScope' ,function ($rootScope) {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
          element.bind('click', function() {
               var menuId = attrs.menuId;
               var menuType = attrs.menuType;
               if(menuType !== 'popup') {
                  return false;
               }

               var popupOptions = {
                 template: '',
                 controller: '',
                 controllerAs: ''
               };
               switch(menuId) {
                  case 'myprofile':
                      popupOptions = {
                          template: '/fantasy_sports/app/views/desktop/d-myprofile.html',
                          popupSize: 'desk-sm',
                          controller: 'MyprofileCtrl',
		   			      controllerAs: 'myprofile',
                      };
                      break;
                  default:
				     break;
               }
               $rootScope.$broadcast('showDeskPopup', {options: popupOptions});
         });
      }
    };
  }]);

'use strict';

(function (angular) {

  function assignServicesToRootScope($rootScope, devicedetectionservice){
    $rootScope.deviceDetection = devicedetectionservice;
    //$rootScope.session = session;
    //$rootScope.auth = auth;
  }

  // Inject dependencies
  assignServicesToRootScope.$inject = ['$rootScope', 'devicedetectionservice'];

  // Export
  angular
    .module('fantasyCricketApp')
    .run(assignServicesToRootScope);

})(angular);

'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.createteamservice
 * @description
 * # createteamservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('createteamservice', ['utilityservice','config', 'serviceName', function (utilityservice,config, serviceName) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var self = this;

        self.getMatchPlayers = function(params, callback){
            utilityservice._postAjaxCall(config.gameNodeHost+serviceName.FETCH_TOUR_MATCH_PLAYERS, params, function (response) {
                callback(response);
            });
        };

        self.saveMyTeam = function(params, callback){
            utilityservice._postAjaxCall(config.gameNodeHost+serviceName.FETCH_SAVE_TEAM, params, function(response){
                callback(response);
            });
        };

        self.getMySavedPlayers = function(params, callback){
            utilityservice._postAjaxCall(config.gameNodeHost+serviceName.FETCH_TEAM_MATCH_PLAYERS, params, function(response){
                callback(response);
            });
        };
        
        self.updateUserTeam = function(params, callback){
            utilityservice._postAjaxCall(config.gameNodeHost+serviceName.UPDATE_USER_TEAM, params,function(response){
               callback(response); 
            });
        }

    }]);
'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.createteamservice
 * @description
 * # createteamservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('dateDropdownHandler', ['utilityservice','config', 'constants', function (utilityservice,config, constants) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var that = this;
        var now = new Date();
        var startYr = 1920;
        var endYr = now.getFullYear()-18;
        var yearRange = [];
        var dayRange = [];

        that.initilizeYears = function(callback){
            yearRange = [];
            yearRange.push("YY");
            for (var i = startYr; i <= endYr; i++) {
                  yearRange.push(i);
            }
            callback(yearRange);
        };

        that.initilizeDays = function(callback){
            dayRange = [];
            dayRange.push('DD');
            for (var j = 1; j <= 31; j++) {
                if(j<10){
                      j = String('0'+j);
                }
                j = String(j);
                dayRange.push(j);
            }
            callback(dayRange);
        };

        that.getMonths = function(selectedYear, callback){
            var monthList = [];
            var monthArr = constants.MONTH_LIST;

            //Code to show month list upto current month of present year.
            if (selectedYear==now.getFullYear()) {
                for(var i=0; i<13; i++){
                      var key = that.getKey(monthArr[i]);
                      if(key != 'MM') {
                            if(key > now.getMonth()+1) {
                                  monthArr.splice(i,13-i);
                                  monthList = [];
                                  monthList = monthArr;
                                  break;
                            }
                      }
                }
            }else {
                monthList = constants.MONTH_LIST;
            }
            callback(monthList);
        };

        that.getKey = function(month) {
            return Object.keys(month)[0];
        };

        that.getDays = function(selectedYear, selectedMonth, callback){
            var dateRange = [];
            if(selectedYear==now.getFullYear() && selectedMonth ==(now.getMonth()+1)) {
                  dateRange.push('DD');
                  for(var i=1; i<now.getDate()+1; i++){
                        if(i<10) {
                            i = String('0'+i);
                        }
                        i = String(i);
                        dateRange.push(i);
                  }
            }else {
                  var days = 31;
                  var year = selectedYear;
                  var month = String(selectedMonth);
                  if(month==2) {
                        if((year % 4) === 0 && ((year % 100) !== 0 || (year % 400) === 0)) {
                              days = 29;//Leap Year
                        }else {
                              days = 28;
                        }
                  }else if (month==4 || month==6 || month==9 || month==11) {
                        days = 30;
                  }
                  dateRange = [];
                  dateRange.push('DD');
                  for(var j=1; j<days+1; j++){
                        if(j<10) {
                            j = String('0'+j);
                        }
                        j = String(j);
                        dateRange.push(j);
                  }
            }
            callback(dateRange);
        };

}]);
'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.depositeservice
 * @description
 * # depositeservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
  .service('depositeservice', ['$window', '$rootScope', 'session', 'config', 'serviceName', 'utilityservice', '$timeout', 'msgConstants', function ($window, $rootScope, session, config, serviceName, utilityservice, $timeout, msgConstants) {
        var that = this;
        that.openCard = function(token, merchantCode, txnIdentifier, depositAmt) {
            //console.log(token, merchantCode, txnIdentifier);
            if(token==undefined || token=='' || merchantCode==undefined || merchantCode=='' || txnIdentifier==undefined || txnIdentifier==''){
              $rootScope.$broadcast("showPopup", {
                    type: "Info",
                    title: msgConstants.HEADER_INFO,
                    appendTo:'#mainWrapper',
                    msg: msgConstants.MSG_REQUEST_FAILED,
                    popupSize: 'small',
                    okButtonText: 'OK'
              });
              return;
            }

            var configJson = {
                'tarCall':false,
                'features':{
                  'showPGResponseMsg':true
                },
                'consumerData':{
                  'deviceId':'web',
                  'authKey':'123abc123',
                  'token':token,
                  'returnUrl': config.websiteNodeHost + "/myaccount",
                  'responseHandler': that.handleResponse,
                  'paymentMode': 'all',
                  'merchantId': merchantCode,
                  'txnId': txnIdentifier,
                  'items': [{ 'itemId' : 'test', 'amount' : depositAmt, 'comAmt':'0'}]
                }
              };
            new Card(configJson).init();
        };

        that.afterPayment = function() {
            var queryStr = window.location.search;
            if (queryStr.indexOf('txthdntpslmrctcd=') > -1 && queryStr.indexOf('txthdnMsg=') > -1) {
                var token = session.getDepositeData();
                new Card({
                    'tarCall': true,
                    'features': {
                        'showPGResponseMsg': true
                    },
                    'consumerData': {
                        'deviceId': 'web',
                        'authKey': '123abc123',
                        'token': token,
                        'returnUrl': config.websiteNodeHost + "/myaccount",
                        'responseHandler': that.handleResponse,
                        'paymentMode': 'all'
                    }
                }).init();
            }
        };

        that.handleResponse = function(res) {
            if (typeof res != 'undefined' && typeof res.paymentMethod != 'undefined' && typeof res.paymentMethod.paymentTransaction != 'undefined' && typeof res.paymentMethod.paymentTransaction.statusCode != 'undefined' && res.paymentMethod.paymentTransaction.statusCode == '0300') {
                //console.log('merchanttxnIdentifer--->', session.getTransactionID());
                //console.log("uid-->",session.getUserID());
                //console.log("initTxnId--->",session.getTransinitID());
                var params = {};
                params.userId = session.getUserID();
                params.merchanttxnIdentifer = session.getTransactionID();
                params.initTxnId = session.getTransinitID();
                params.txnType = "DEPOSIT";
                utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.DEPOSITE_FINISH, params, function (response) {
                    if(response.respCode == 100){
                        session.removeDepositData();
                        $timeout(function(){
                            $window.location.reload();
                            window.location.search = "";
                        },5000);
                    }else{
                        console.log("Server ERROR:", response.message);
                    }
                });
            }else{
                console.log('Failed response', res);
                //window.location.search = "";
            }

        };
  }]);

'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.devicedetectionservice
 * @description
 * # devicedetectionservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('devicedetectionservice', [function () {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var self = this;

        /**
         *
         * @param {type} deviceName //MOBILE/TABLET/DESKTOP
         * @returns {devicedetectionservice_L11.isDevice.result}
         */

        /* to blur elements used to open keyboard - when click on others #4565*/
        self.blurElement = function(elem){
            if(elem && elem.tagName){
                if (!((elem.tagName.toUpperCase() === 'INPUT' && ( elem.type.toUpperCase() === 'TEXT' || elem.type.toUpperCase() === 'PASSWORD' || elem.type.toUpperCase() === 'FILE' || elem.type.toUpperCase() === 'SEARCH' || elem.type.toUpperCase() === 'EMAIL' || elem.type.toUpperCase() === 'NUMBER' || elem.type.toUpperCase() === 'DATE' || elem.type.toUpperCase() === 'TEL' )) || elem.tagName.toUpperCase() === 'TEXTAREA' || elem.tagName.toUpperCase() === 'SELECT')) {
                    elem.blur();
                    if(document.activeElement){
                        document.activeElement.blur();
                    }
                }
            }
        };

        self.isDevice = function isDevice(deviceName) {
            if (currentDeviceName.toUpperCase() === deviceName.toUpperCase()) {
                return true;
            } else {
                return false;
            }
        };

        self.getPlatform = function () {
            return currentPlatform;
        };

        self.isIphone = function(){
            return !!navigator.userAgent.match(/iPhone/i);
        };

        self.getOrientation = function(){
            var angle = null;
            var orientation = null;
            if(typeof window.orientation !== "undefined")
                angle = window.orientation;
            else if(typeof window.screen.orientation !== "undefined")
                angle = window.screen.orientation.angle;
            else
                return ""; // return empty in case safari desktop where screen.angle is not available

            if (Math.abs(angle) === 90) {
                orientation = "landscape";
            } else {
                orientation = "portrait";
            }

            return orientation;
        };

        self.getOs = function () {
            var ua = navigator.userAgent.toLowerCase();
            var isAndroid = ua.indexOf("android") > -1;
            var isWindows = ua.indexOf("windows") > -1;
            var result = false;
            if (isAndroid && !isWindows) {
                result = 'android';
            } else {
                if (ua.match(/(iphone|ipod|ipad)/) && !isWindows) {
                    result = 'ios';
                }
            }
            if (currentPlatform === "NA") {
                if (navigator.appVersion.indexOf("Win") !== -1) {
                    result = 'winDesk';
                } else if (navigator.appVersion.indexOf("Linux") !== -1) {
                    result = 'linDesk';
                } else if (navigator.appVersion.indexOf("Mac") !== -1) {
                    result = 'macDesk';
                }
            }

            return result;
        };
        self.isIosChrome = function () {
            var is_chrome = navigator.userAgent.match('CriOS');
            if (is_chrome === null) {
                return false;
            } else {
                return true;
            }
        };
        self.getDevice = function () {
            var isiPad = navigator.userAgent.match(/iPad/i);
            if (isiPad != null) {
                result = 'tablet';
                return result;
            }

            var screenDim = {};
            if (screen.width > screen.height) {
                screenDim = {
                    'width': screen.width,
                    'height': screen.height
                };
            } else {
                screenDim = {
                    'width': screen.height,
                    'height': screen.width
                };
            }
            var result = '';
            var isFullScreenEnabled = document.fullscreenEnabled || document.mozFullScreenEnabled || document.documentElement.webkitRequestFullScreen;
            if ((!(isFullScreenEnabled)) && (self.getOs() === 'android')) {
                screenDim.width = screenDim.width / window.devicePixelRatio;
                screenDim.height = screenDim.height / window.devicePixelRatio;
            }
            if (screenDim.width >= 768 && !navigator.userAgent.match(/Windows Phone/i)) {
                result = 'tablet';
            } else {
                result = 'mobile';
            }
            if (getDevicePlatform() === 'NA') {
                result = 'desktop';
            }
            return result;

        };

        var getDevicePlatform = function () {
            var userAgent = navigator.userAgent;
            var platform = 'NA';
            if (userAgent.match(/Android/i)) {
                platform = 'Android';
            } else if (userAgent.match(/BlackBerry/i)) {
                platform = 'BlackBerry';
            } else if (userAgent.match(/iPhone|iPad|iPod/i)) {
                platform = 'iOS';
            } else if (userAgent.match(/Opera Mini/i)) {
                platform = 'Opera';
            } else if (userAgent.match(/IEMobile/i)) {
                platform = 'Windows';
            }
            return platform;
        };

        //function to full screen mobile devices in android small device
        self.launchFullscreen = function(element) {
            if(element.requestFullscreen) {
                element.requestFullscreen();
            } else if(element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if(element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            } else if(element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        };

        //function to full screen iphone devices.
        self.showSwipeToFullScreen = function(){
            var is_chrome = self.isIosChrome();
            if(!is_chrome){
                if((window.innerHeight <= 320)){
                    if(window.innerHeight < 320){
                        var adjustScreen = window.setTimeout(function(){
                            window.scrollTo(0,0);
                            window.clearTimeout(adjustScreen);
                            adjustScreen = null;
                        },500);
                        return true;
                    }
                }
            }
            window.scrollTo(0,0);
            return false;
        };

        var currentDeviceName = self.getDevice();
        var currentPlatform = getDevicePlatform();
        //console.log('currentDeviceName=',currentDeviceName,'=currentPlatform=',currentPlatform);

    }]);
'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.leagueservice
 * @description
 * # leagueservice
 * Service in the fantasyCricketApp.
 */
 angular.module('fantasyCricketApp')
 .service('leagueservice', ['$rootScope', 'utilityservice', 'config', 'serviceName', 'msgConstants', function ($rootScope, utilityservice, config, serviceName, msgConstants) {
	// AngularJS will instantiate a singleton by calling "new" on this function
	var self = this;

	self.getLeagues = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_LEAGUES, params, function (response) {
			callback(response);
		});
	};

	self.getTours = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_TOURS, params, function (response) {
			if(response.respCode === 100){
				var toursArray = response.respData;
				callback(toursArray);
			}else{
				console.log('Error in Fetching tours', response.message);
			}
		});
	};

	self.getMatches = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_MATCH_LIST, params, function (response) {
			if(response.respCode === 100) {
				var  matchesArray = response.respData.matchList;
				callback(matchesArray,response.respData.nextTimeTour);
			}else{

			}

		});
	};

	self.getLeaguePrizeInfo = function(params, callback){
		utilityservice.showLoader();
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_LEAGUE_PRIZE_INFO, params, function (response) {
			if (response.respCode === 100) {
                callback(response.respData);
            }else {
                console.log('Error in fetching League Prize INfo ::::');
            }
            utilityservice.hideLoader();
		});
	};

	self.getJoinedLeagues = function(params, callback){
		utilityservice.showLoader();

		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_USER_JOINED_LEAGUES, params, function (response) {
			if(response.respCode === 100){
				var joinedLeaguesArray = response.respData;
				callback(joinedLeaguesArray);
			}else{
				console.log('Error in Fetching tours', response.message);
			}
			utilityservice.hideLoader();
		});
	};

	self.getLeagueMembers = function(params, callback){
		utilityservice.showLoader();
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_JOINED_LEAGUE_TEAMS, params, function (response) {
			if(response.respCode === 100){
				var leagueMembersArray = response.respData;
				callback(leagueMembersArray);
			}else{
				console.log('Error in Fetching tours', response.message);
			}
			utilityservice.hideLoader();
		});
	};

	self.getTourMatchInfo = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_TOUR_MATCH_INFO, params, function (response) {
			callback(response);
		});
	};

	self.getLeagueInfo = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_LEAGUE_INFO, params, function (response) {
			callback(response);
		});
	};

	self.joinLeague = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_TOUR_MATCH_INFO, params, function (response) {
			callback(response);
		});
	};

	self.getTeamInfo = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_TEAM_INFO, params, function (response) {
			if(response.respCode === 100) {
				callback(response.respData);
			}else{

			}
		});
	};

	self.updateUserJoinedTeam = function(params, callback){
		utilityservice.showLoader();
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.UPDATE_USER_JOINED_TEAM, params, function (response) {
			if(response.respCode === 100) {
				callback(response);
			}else if(response.respCode === 118){
				$rootScope.$broadcast("showPopup", {
					type: "Info",
					title: msgConstants.HEADER_INFO,
					appendTo:'#mainWrapper',
					msg: response.message,
					popupSize: 'small',
					showClose: false,
					okButtonText: 'Ok'
				});
			}
			else{

			}
			utilityservice.hideLoader();
		});
	};

	self.getOpponentTeamInfo = function(params, callback){
		utilityservice._postAjaxCall(config.gameNodeHost + serviceName.FETCH_OPPONENT_TEAM_INFO, params, function (response) {
			if(response.respCode === 100) {
				callback(response.respData);
			}else{

			}
		});
	};

}]);

'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.leagueservice
 * @description
 * # leagueservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('leagueutilservice', ['$window', 'utilityservice', 'config', 'serviceName', '$rootScope','ngDialog', 'msgConstants', function ($window, utilityservice, config, serviceName, $rootScope,ngDialog, msgConstants) {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var self = this;

        var checkLeagueResponse = null;

        self.setToLocalStorage = function(key, value){
            $window.localStorage[key] = angular.toJson(value);
        }

        self.getFromLocalStorage = function(key){
            if($window.localStorage[key] !== undefined && $window.localStorage[key] !== ""){
                return JSON.parse($window.localStorage[key]);    
            }
            else{
                return undefined;
            }            
        }

        self.getActiveMatch = function (matcharray) {
            var activeIndex = 0;
            var matchId = utilityservice.getFromLocalStorage('currentMatchId');
            // Check for matchId difference
            if(!!checkLeagueResponse && (checkLeagueResponse.matchId != matchId)){
                matchId = checkLeagueResponse.matchId;
            }

            var isMatchIdSaved = (matchId !== undefined && matchId !== "") ? true : false;
            
            var arrlen = matcharray.length;

            if(isMatchIdSaved){
                matchId = parseInt(matchId);
                activeIndex = self.getMatchIndexByKeyValue(matcharray, 'matchId', matchId);
                if(matcharray[activeIndex].status !== 'ACTIVE'){
                    activeIndex = self.getMatchIndexByKeyValue(matcharray, 'status', 'ACTIVE');
                }
            }
            else{
                activeIndex = self.getMatchIndexByKeyValue(matcharray, 'status', 'ACTIVE');
            }
            return activeIndex;
        }

        self.getMatchIndexByKeyValue = function(matcharray, key, value){
            var index = 0;
            for(var i = 0, len = matcharray.length; i < len; i++){
                if(matcharray[i][key] === value){
                    index = i;
                    break;
                }
            }
            return index;
        }

        self.getActiveMatchTeamId = function (matchId) {
            return 1;
        }

        self.updateTourTimer = function (tourArray, nextTimeTour) {
            //console.log('tourArray  nextTimeTour :: ', tourArray, nextTimeTour);
            for (var i = 0, length = tourArray.length; i < length; i++) {
                var currentTour = tourArray[i];
                currentTour.nextMatchTime = nextTimeTour[currentTour.tourId];
            }
        }

        self.updatePlayerPositionClass = function (playersArray) {
            var teamPreview = {
                bowlerCount: 0,
                batterCount: 0,
                alrdrCount: 0,
                wkCount: 0
            };

            for (var i = 0, length = playersArray.length; i < length; i++) {
                var player = playersArray[i];
                var playerClass = '';

                if (player.isCaptain === true) {
                    playerClass += ' sel-cap';
                }
                else if (player.isViceCaptain === true) {
                    playerClass += ' sel-v-cap';
                }

                if (player.isBonus === true) {
                    playerClass += ' bonus';
                }

                if (player.batter === true && player.bowler === false && player.wicketKeeper === false) {
                    teamPreview.batterCount = teamPreview.batterCount + 1;
                    playerClass += " bt" + teamPreview.batterCount;
                }
                else if (player.batter === false && player.bowler === true) {
                    teamPreview.bowlerCount = teamPreview.bowlerCount + 1;
                    playerClass += " bw" + teamPreview.bowlerCount;
                }
                else if (player.batter === true && player.bowler === true) {
                    teamPreview.alrdrCount = teamPreview.alrdrCount + 1;
                    playerClass += " alrdr" + teamPreview.alrdrCount;
                }
                else if (player.wicketKeeper === true) {
                    teamPreview.wkCount = teamPreview.wkCount + 1;
                    playerClass += " wk" + teamPreview.wkCount;
                }
                player.className = playerClass;
            }
            return playersArray;
        }

        self.getMatchStatus = function (status) {
            var matchstatus = '';
            switch (status) {
                case 'RUNNING':
                    matchstatus = 'In Progress';
                    break;
                case 'UNDER_REVIEW':
                    matchstatus = 'Under Review';
                    break;
                case 'CLOSED':
                    matchstatus = 'Completed';
                    break;
                case 'ABANDONED':
                    matchstatus = 'Abandoned';
                    break;
                default:
            }
            return matchstatus;
        }

        self.myTeamNamePopoup = function () {
            ngDialog.open({
                template: '/fantasy_sports/app/views/myteampopup.html',
                overlay: true,
                closeByDocument: false,
                closeByEscape: false,
                showClose: false,
                closeByNavigation: false,
                disableAnimation: true,
                controller: 'MyteamnameCtrl',
                controllerAs: 'myteamname',
                appendTo: '#mainWrapper',
                className: 'ngdialog-theme-popup desk-sm '
            });
        };

        self.prizeInfoPopup = function(data){
            ngDialog.open({
                template: '/fantasy_sports/app/views/prizeinfo.html',
                controller: "prizeInfoCtrl",
                controllerAs: "prizeinfo",
                data: data,
                overlay: true,
                closeByDocument: false,
                closeByEscape: false,
                closeByNavigation: true,
                disableAnimation: true,
                appendTo: '#mainWrapper',
                className: 'ngdialog-theme-popup prizeinfo-popup desk-sm',
                showClose: true
            });
        };

        self.checkJoinLeague = function (league, cb) {
            var params = {};
            params.leagueId = league.leagueId;
            utilityservice._postAjaxCall(config.gameNodeHost + '/checkJoinedLeague', params, function (response) {
                checkLeagueResponse = response.respData;
                cb(checkLeagueResponse);
                if (response.respCode == 100) {
                    leagueJoinConfirmPopup(checkLeagueResponse); 
                    return;
                }else {
                    showCheckLeagueValidation(response, checkLeagueResponse);
                }
            });
        }


        self.validateJoinLeague = function (league) {

            var params = {};
            params.leagueId = league.leagueId;
            params.matchId = league.matchId;
            // params.teamId = 1;
            utilityservice._postAjaxCall(config.gameNodeHost + '/checkJoinedLeague', params, function (response) {
                var checkResponse = response.respData;
                if (response.respCode == 100) {
                    leagueJoinConfirmPopup(checkResponse);
                    return;
                }else {
                    showCheckLeagueValidation(response, checkResponse);
                }
            });
        }

        function showCheckLeagueValidation(response, league) {
            if(response.respCode === 117){     //Check for Team not created
                createTeamPopup(league);
            }
            else if(response.respCode === 124){
                $rootScope.$broadcast("showPopup", {
                    type: "Info",
                    title: msgConstants.HEADER_INFO,
                    appendTo: '#mainWrapper',
                    msg: response.message,
                    popupSize: 'small',
                    showClose: false,
                    okButtonText: 'Ok',
                    okBtnCallback: function () {
                        var data = {
                            matchId: response.respData.matchId
                        }
                        $rootScope.$broadcast('fetchLeaguesEvent', data);
                    }
                });
            }
            else if(response.respCode === 129){     //Check for Insufficent balance
                var data = {
                    checkLeagueResponse: response.respData,
                    title: 'Oops! Not Enough Cash',
                    showCurrentBalance: true,
                }
                ngDialog.open({
                    template: '/fantasy_sports/app/views/addcash.html',
                    controller: "addCashCtrl",
                    controllerAs: "addcash",
                    overlay: true,
                    closeByDocument: false,
                    closeByEscape: false,
                    closeByNavigation: true,
                    disableAnimation: true,
                    appendTo: '#mainWrapper',
                    className: 'ngdialog-theme-popup addcash-popup desk-sm',
                    showClose: true,
                    resolve: {
                        leaguedata: function () {
                            return data;
                        }
                    }
                });
            }
            else{
                $rootScope.$broadcast("showPopup", {
                    type: "Info",
                    title: msgConstants.HEADER_INFO,
                    appendTo: '#mainWrapper',
                    msg: response.message,
                    popupSize: 'small',
                    showClose: false,
                    okButtonText: 'Ok',
                    okBtnCallback: function () {
                        var invCode = utilityservice.getUrlParameter("invCode");
                        if(invCode !== "" && invCode !== undefined){
                            // Reload page and removing the query string from url
                            //$window.location.href = utilityservice.getBaseUrl() + '/league';
                            $window.location.href = $window.location.href.split('?')[0];    

                        }
                    }
                });
            }
        }
        function createTeamPopup(league) {
            var msg = '<div>' +
                '<p class="fs18"><span class="clr2">Step 1. Create your Fantasy Team;</span></p>' +
                '<p class="fs18"><span class="clr2">Step 2. Join a league!</span></p>' +
                '</div>';
            var title = 'First things first';
            var okText = 'Create Team';

            $rootScope.$broadcast("showPopup", {
                type: "Info",
                title: title,
                appendTo: '#mainWrapper',
                msg: msg,
                popupSize: 'small',
                showClose: true,
                okButtonText: okText,
                okBtnCallback: function () {
                    window.location.href = utilityservice.getBaseUrl() + "/createteam/" + league.matchId + '/' + (league.totalTeams + 1) + '?invCode=' + league.leagueId;
                }
            });
        }

        function leagueJoinConfirmPopup(league) {

            var leaguedata = {
                league: league
            }

            ngDialog.open({
                template: '/fantasy_sports/app/views/leaguejoinconfirmpopup.html',
                overlay: true,
                closeByDocument: false,
                closeByEscape: false,
                showClose: true,
                closeByNavigation: false,
                disableAnimation: true,
                controller: 'leagueJoinConfirmCtrl',
                controllerAs: 'leaguejoin',
                appendTo: '#mainWrapper',
                className: 'ngdialog-theme-popup desk-sm ',
                resolve: {
                    leaguedata: function () {
                        return leaguedata;
                    }
                }
            });
        }
    }]);

'use strict';
(function (angular) {

  function localStorageServiceFactory($window){
    if($window.localStorage){
      return $window.localStorage;
    }
    throw new Error('Local storage support is needed');
  }

  // Inject dependencies
  localStorageServiceFactory.$inject = ['$window'];

  // Export
  angular
    .module('fantasyCricketApp')
    .factory('localStorage', localStorageServiceFactory);

})(angular);
'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.promotionsservice
 * @description
 * # promotionsservice
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
    .service('promotionsservice', ['utilityservice','config', 'serviceName', function (utilityservice,config, serviceName) {

        var self = this;
        
        self.getPromotions = function(params, callback){
            utilityservice._postAjaxCall(config.websiteNodeHost+serviceName.PROMOTIONS, params, function (response) {
                //console.log(response,"promtions");
                if(response.respCode == 100){
                    callback(response.respData);
                }
                else{
                    // hide the loader
                }
                
            });
        };


            /*var response = {
                "status":true,
                "error":"",
                "data":{
                    "game_promotion":[
                        {
                            "id":"lobby-promo-1",
                            "name":"lobby promo 1",
                            "url":"",
                            "fields":{
                                "small_content":{"text":"zsfasff dfwgsdgfsd sdfafaf"},
                                "page_content":{"text":"This is a Sample Text for Lobby promotion 1"},
                                "image":{"url":"http://localhost:3001/fantasy_sports/app/assets/images/banner.png"},
                                "page_content":{
                                    "text": "<div class ='sample'>sample</div>"
                                },
                                "page_css":{
                                    "text": "<style>.sample{ background: green;}</style>"
                                }
                            }
                        },
                        {
                            "id":"lobby-promo-2",
                            "name":"lobby promo 2",
                            "url":"",
                            "fields":{
                                "small_content":{"text":"asdfsf asfaf swffafas"},
                                "page_content":{"text":"This is a Sample Text for Lobby promotion 1"},
                                "image":{"url":"http://localhost:3001/fantasy_sports/app/assets/images/banner.png"},
                                "page_content":{
                                    "text": "<div class ='sample'>sample</div>"
                                },
                                "page_css":{
                                    "text": "<style>.sample{ background: green;}</style>"
                                }
                            }
                        }
                    ]
                }
            }
            callback(response);
        };*/

    }]);
'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.session
 * @description
 * # session
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
  .service('session', ['localStorage', function (localStorage) {
    this.websiteAccessToken = localStorage.getItem('websiteAccessToken');
    this.depositeData = localStorage.getItem('depositeData');
    this.transactionID = localStorage.getItem('transID');
    this.transInitID = localStorage.getItem('transInitID');
    this._user = {};

    this.setScreenName = function(name){
        this._user.screenName = name;
    };

    this.getScreenName = function(){
        return this._user.screenName;
    };

    this.setTeamName = function(name){
        this._user.teamName = name;
    };

    this.getTeamName = function(){
        return this._user.teamName;
    };

    this.setUserDOB = function(name){
        this._user.userDob = name;
    };

    this.getUserDOB = function(){
        return this._user.userDob;
    };

    this.setUserStatus = function(status){
        this._user.userStatus = status;
    };

    this.getUserStatus = function(){
        return this._user.userStatus;
    };

    this.setUserBalance = function(balance){
        this._user.userBalance = balance;
    };

    this.getUserBalance = function(){
        return this._user.userBalance;
    };

    this.setUserID = function(id){
        this._user.userID = id;
    };

    this.getUserID = function(){
        return this._user.userID;
    };

    this.setUserName = function(name){
        this._user.userName = name;
    };

    this.getUserName = function(){
        return this._user.userName;
    };

    this.setWebsiteToken = function (token) {
        this.websiteAccessToken = token;
        try {
            localStorage.setItem("websiteAccessToken", token);
        } catch (e) {
            console.log(e);
        }
    };

    this.getWebsiteToken = function () {
        return this.websiteAccessToken;
    };


    this.setDepositeData = function(depData) {
        this.depositeData = depData;
        try {
            localStorage.setItem("depositeData", depData);
        } catch (e) {
            console.log(e);
        }
    };

    this.getDepositeData = function () {
        return this.depositeData;
    };

    this.setTransactionID = function(transID) {
        this.transactionID = transID;
        try {
            localStorage.setItem("transID", transID);
        } catch (e) {
            console.log(e);
        }
    };

    this.getTransactionID = function () {
        return this.transactionID;
    };

    this.setTransinitID = function(transID) {
        this.transInitID = transID;
        try {
            localStorage.setItem("transInitID", transID);
        } catch (e) {
            console.log(e);
        }
    };

    this.getTransinitID = function () {
        return this.transInitID;
    };
    /** Destroy session  **/
    this.destroy = function destroy() {
        this._user = {};
        this.setWebsiteToken(null);
        localStorage.removeItem("websiteAccessToken");
        this.removeDepositData();
    };

    this.removeDepositData = function() {
        this.setDepositeData(null);
        this.setTransactionID(null);
        this.setTransinitID(null);
        localStorage.removeItem("depositeData");
        localStorage.removeItem("transID");
        localStorage.removeItem("transInitID");
    };
  }]);

'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.user
 * @description
 * # user
 * Service in the fantasyCricketApp.
 */
angular.module('fantasyCricketApp')
  .service('user', ['msgConstants', function (msgConstants) {
        var that = this;
        that.numericPattern = /^[0-9]+$/;
        that.alphaNumericPattern = /^[a-zA-Z0-9_]+$/;
        that.aplphaNumericWithSpace = /^[a-zA-Z0-9]+[a-zA-Z0-9 ]+$/;
        that.restrictNumeric = /^([^0-9]*)$/;
        that.checkSpecialSymbol = /^([\w&\-]+)$/;
        that.emailPattern = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        that.checkPanCard = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
        that.mobilePattern = /^\d+$/;

        //Validations code---Ashu
        that.OnInputFieldValidation = function (fieldName,fieldValue, additionalInfo , callback) {
                var message = "";
                switch(fieldName){
                   case 'userName':{
                        if(fieldValue !== undefined && fieldValue !== null){
                             if(fieldValue.length === 0){
                                 message = msgConstants.REQUIRED_MESSAGE_1;
                             }else if(fieldValue.length < 2 || fieldValue.length > 20){
                                 message = msgConstants.ERR_USERNAME_1;
                             }else if (!that.aplphaNumericWithSpace.test(fieldValue)) {
                                 message = msgConstants.ERR_USERNAME_2;
                             }
                        }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                        }
                        break;
                   }

                    case 'userTeamName':{
                        if(fieldValue !== undefined && fieldValue !== null){
                             if(fieldValue.length === 0){
                                 message = msgConstants.REQUIRED_MESSAGE_1;
                             }else if(fieldValue.length < 2 || fieldValue.length > 20){
                                 message = msgConstants.ERR_TEAMNAME_1;
                             }else if (!that.alphaNumericPattern.test(fieldValue)) {
                                 message = msgConstants.ERR_TEAMNAME_2;
                             }
                        }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                        }
                        break;
                   }

                   case 'userEmail':
                   case 'loginEmail':
                   case 'userLoginEmail': {
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }else if(fieldValue.length > 40){
                                message = msgConstants.ERR_EMAIL_1;
                            }else if(!this.emailPattern.test(fieldValue)){
                                message = msgConstants.ERR_EMAIL_2;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'userMobile':{
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }else if(fieldValue.length !== 10){
                                message = msgConstants.ERR_MOBILE_1;
                            }else if (!this.mobilePattern.test(fieldValue)) {
                                message = msgConstants.ERR_MOBILE_2;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'userCity':{
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length < 3 || fieldValue.length >30){
                                message = msgConstants.ERR_CITY_1;
                            }else if (!this.restrictNumeric.test(fieldValue)) {
                                message = msgConstants.ERR_CITY_2;
                            }else if(!this.checkSpecialSymbol.test(fieldValue)){
                                message = msgConstants.ERR_SPECIAL_CHAR;
                            }
                       }
                       break;
                   }

                   case 'loginPassword':
                   case 'userLoginPassword':
                   case 'oldPass':  {
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'userPassword':
                   case 'newPass': {
                       if(fieldValue !== undefined && fieldValue !== null){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }else if(fieldValue.length < 5 || fieldValue.length > 15){
                                message = msgConstants.ERR_PASSWORD_1;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'confirmPass':  {
                       if(fieldValue !== undefined){
                            if(fieldValue.length === 0){
                                message = msgConstants.REQUIRED_MESSAGE_1;
                            }else if(fieldValue !== additionalInfo){
                                message = msgConstants.ERR_PASSWORD_2;
                            }
                       }else{
                            message = msgConstants.REQUIRED_MESSAGE_1;
                       }
                       break;
                   }

                   case 'userPAN': {
                       if(fieldValue !== undefined){
                            if(fieldValue.length === 0){
                                message = msgConstants.ERR_PAN_1;
                            }else if(!this.checkPanCard.test(fieldValue)){
                                message = msgConstants.ERR_PAN_2;
                            }
                       }else{
                            message = msgConstants.ERR_PAN_1;
                       }
                       break;
                   }
               }
               if(message !== '') {
                     callback({resCode : 101,message:message});
               }else {
                     callback({resCode : 100});
               }
        };
  }]);

'use strict';

/**
 * @ngdoc service
 * @name fantasyCricketApp.utilityservice
 * @description
 * # utilityservice
 * Service in the fantasyCricketApp.
 */
 angular.module('fantasyCricketApp')
 .service('utilityservice', ['$rootScope','$http', '$timeout', '$location', '$window', 'session', 'msgConstants',  function ($rootScope, $http, $timeout, $location, $window , session, msgConstants) {
	// AngularJS will instantiate a singleton by calling "new" on this function
	var self = this;

	var url = {
		protocol: $location.protocol(),
		host: $location.host(),
		port: $location.port()
	};

	var baseUrl = url.protocol + '://' + url.host + ':' + url.port;

	self._postAjaxCall = function(actionName,param,callback){
             self.hideLoader();
             self.showLoader();
			 var tS = new Date().getTime();
			 if (typeof param === "undefined") {
				param={};
			 }
			 param['ts'] = tS;
             var token = session.getWebsiteToken();
             if(token !== null && token !== undefined){
                   param['token'] = token;
             }

			$http({
				method: 'POST',
				dataType: 'json',
				data: param,
				async : false,
				url: actionName,
                timeout: 15000,
				crossDomain: true
			 }).then(function successCallback(result) {
				// this callback will be called asynchronously
                 self.hideLoader();
                 if(result.data.respCode == 108){
                       $window.location.href = baseUrl + "/";
                 }
                 else if(result.data.respCode == 134){	// Case handling if user send join league request after match has started
                       $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: result.data.message,
                        popupSize: 'small',
                        okButtonText: 'OK',
                        okBtnCallback: function () {
                           $window.location.href = baseUrl + "/league";
                        }
                    });
                 }
                 else{
                       callback(result.data);
                 }
			  }, function errorCallback(e) {
				// called asynchronously if an error occurs
                self.hideLoader();
				console.log(e.message);
			  });
		};


		self._getAjaxCall = function(actionName, param, callback){
             self.hideLoader();
             self.showLoader();
             var token = session.getWebsiteToken();
             if(token !== null && token !== undefined){
                   param['token'] = token;
             }
			 $http({
				method: 'GET',
				dataType: 'json',
				params: param,
				url: actionName,
                timeout: 5000,
				crossDomain: true
			 }).then(function successCallback(result) {
				// this callback will be called asynchronously
                 self.hideLoader();
                 if(result.data.respCode == 108){
                       $window.location.href = baseUrl + "/";
                 }else{
				       callback(result.data);
                 }
			  }, function errorCallback(e) {
				// called asynchronously if an error occurs
                self.hideLoader();
				console.log(e.message);
			  });
		};

		self.showLoader = function(opts){
			var defaults = {
				text: 'Loading...',
				delay: 0
			};

			var options = angular.extend({}, defaults, opts);
			var loader = document.querySelector('#loaderDom');
			if(loader !== undefined && loader !== null){
				document.querySelector('#loaderDom .loading-label').innerHTML = options.text;
				if(options.delay){
					$timeout(function(){
						loader.style.display = 'block';
					},options.delay);
				}else{
					loader.style.display = 'block';
				}
			}else{
				console.log('could not detect loader DOM');
			}
		};

		self.hideLoader = function(){
			var loader = document.querySelector('#loaderDom');

			if(loader !== undefined && loader !== null){
				loader.style.display = 'none';
			}else{
				console.log('could not detect loader DOM');
			}
		};

		self.getBaseUrl = function(){
			return baseUrl;
		};

		self.isEmptyObject = function(arg) {
			for (var item in arg) {
				return false;
			}
			return true;
		};

		self.getUrlParameter = function(param, dummyPath) {
		    var sPageURL = dummyPath || window.location.search.substring(1),
		        sURLVariables = sPageURL.split(/[&||?]/),
		        res;

		    for (var i = 0; i < sURLVariables.length; i += 1) {
		        var paramName = sURLVariables[i],
		            sParameterName = (paramName || '').split('=');

		        if (sParameterName[0] === param) {
		            res = sParameterName[1];
		        }
		    }
		    return res;
		}

		self.showToastError = function(dataObj){
			var defaults = {
				title: '',
				message: ''
			};
			if(dataObj !== undefined && dataObj !== null && !self.isEmptyObject(dataObj)){
				var options = angular.extend({}, defaults, dataObj);
				var elem =  document.querySelector(".mob-toast-error");
				var head =  document.querySelector(".mob-toast-error h3");
				var body =  document.querySelector(".mob-toast-error p");
				head.innerHTML = options.title;
				body.innerHTML = options.message;
				angular.element(elem).hide().show().delay(2000).fadeOut("slow");
			}
		};

        $rootScope.$on("setActiveHeaderMenu", function(){
             var a = window.location.href,
             b = a.lastIndexOf("/");
             self.setActiveMenu(a.substr(b + 1));
        });

        self.setActiveMenu = function(viewID){
                var el;
                var arrlength = '';

                function handelActiveClass(el) {
                    arrlength = el.length;
                    for (var j = 0; j < arrlength ; j++) {
                        el[j].removeClassName('active');
                    }
                    if (viewID != '' && viewID != null) {
                        if (document.querySelector('[viewid="' + viewID + '"]')) {
                            document.querySelector('[viewid="' + viewID + '"]').addClassName('active');
                        }
                    }
                }

                el = document.querySelectorAll('.menu-list li');
                handelActiveClass(el);
        };

        self.isPresentInURL = function(text){
        	var url = window.location.href;
        	if(url.indexOf(text) !== -1){
        		return true;
        	}
        	else{
        		return false;
        	}
        }

        self.updateBalance = function(amount){
        	var selectors = document.querySelectorAll('.userBalance');
        	for(var i = 0, len = selectors.length; i < len; i++){
        		selectors[i].innerHTML = amount;
        	}
        }

        self.setToLocalStorage = function(key, value){
            $window.localStorage[key] = angular.toJson(value);
        }

        self.getFromLocalStorage = function(key){
            if($window.localStorage[key] !== undefined && $window.localStorage[key] !== ""){
                return JSON.parse($window.localStorage[key]);    
            }
            else{
                return undefined;
            }            
        }

        self.removeFromLocalStorage = function(key){
        	localStorage.removeItem("currentMatchId");
        }

	}]);
'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:browseLeagueCtrl
 * @description
 * # browseLeagueCtrl as browseleague
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 	.controller('browseLeagueCtrl', ['$rootScope', '$window', 'leagueservice', 'leagueutilservice', 'utilityservice', function ($rootScope, $window, leagueservice, leagueutilservice, utilityservice) {
		var that = this;

		that.memberDisplayType = '';
		that.showPlayersPreview = false;


		var leagueId = null;
		var winScrollTop = 0;

		var pageSize = 100;

        that.pagination = {
            currentPage: pageSize,
            pageSize: pageSize
        }

		that.init = function(initObj){
			leagueId = initObj.leagueId;
			fetchMatchInfo(initObj.matchId);
			window.scrollTo(0, 0);
		}

		that.getMatchStatus = function(status){
			return leagueutilservice.getMatchStatus(status);
		}

		that.fetchLeaguePrizeInfo = function (league) {
			if(league.chipType === 'skill'){
				return false;
			}
			var params = {};
			params.leagueId = league.leagueId;
			leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
				leagueutilservice.prizeInfoPopup(prizeinfodata);
			});
		}

		that.showTeamPreview = function(item, teamType){
			winScrollTop = document.querySelector('body').scrollTop;
			
			var body = document.querySelector('body');
			body.classList.add('ovf-hdn');
			
			if(teamType === 'MY_TEAM'){
				fetchTeamInfo(item);
			}
			else if(teamType === 'OPPONENT_TEAM'){
				fetchOpponentTeamInfo(item);
			}
		}

		that.closeTeamPreview = function(){
			window.scrollTo(0, winScrollTop);
			var body = document.querySelector('body');
			body.classList.remove('ovf-hdn');
			that.showPlayersPreview = false;
		}

		that.validateJoinLeague = function(league, event){
			return leagueutilservice.validateJoinLeague(league, that.selectedMatch.totalTeams);
		}

		that.onScorecardClick = function(){
			$window.open(utilityservice.getBaseUrl() + "/fantasy-scorecard/" + that.selectedMatch.matchId, '_blank');
			//$window.location.href = that.baseUrl + "/fantasy-scorecard/" + that.selectedMatch.matchId;
		}

		that.updatePagination = function(league, type){
            //var limit = that.pagination.pageSize;
            switch(type){
                case 'SHOW_MORE':
	                if(that.pagination.currentPage >= that.leagueMembersArray.totalTeamCount){
	                	return false;
	                }
                    that.pagination.currentPage = that.pagination.currentPage + that.pagination.pageSize;
                    break;
                case 'SHOW_TOP':
                	if(that.pagination.currentPage <= that.pagination.pageSize){
                		return false;
                	}
                    that.pagination.currentPage = that.pagination.pageSize;
                    break;
                case 'SHOW_PREVIOUS':
                	if(that.pagination.currentPage <= that.pagination.pageSize){
                		return false;
                	}
                    that.pagination.currentPage = that.pagination.currentPage - that.pagination.pageSize;
                    break;
            }
            
            var params = {
                matchId: that.selectedMatch.matchId,
                leagueId: league.leagueId,
                limit: that.pagination.currentPage,
                limitSize: pageSize
            };
            that.leagueMembersArray = [];
            utilityservice.showLoader();
            leagueservice.getLeagueMembers(params, function (leagueMembersArray) {
            	window.scrollTo(0, 0);
               that.leagueMembersArray = leagueMembersArray;
                utilityservice.hideLoader();
            });
        }

		function fetchTeamInfo(data){
 			var params = {};
 			params.matchId = data.matchId;
			params.teamId = data.teamId;
			
			utilityservice.showLoader();

			leagueservice.getTeamInfo(params, function(responseData){
				that.selectedTeamInfo = {};
				that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.currentTeam.players);
				
				that.selectedTeamInfo.teamId = responseData.currentTeam.teamId;
				that.selectedTeamInfo.teamName = responseData.screenName;
				that.selectedTeamInfo.teamPoints = responseData.currentTeam.teamPoints;
				window.scrollTo(0,0);
				that.showPlayersPreview = true;
				utilityservice.hideLoader();
				//that.myTeamsArray = responseData.teamsArr;
			});
		}

		function fetchOpponentTeamInfo(data) {
            var params = {};
            params.matchId = data.matchId;
            params.teamId = data.teamId;
            params.opponentId = data.userId;
            utilityservice.showLoader();
            leagueservice.getOpponentTeamInfo(params, function (responseData) {
                that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.opponentTeam.players);
                that.selectedTeamInfo = {};
                that.selectedTeamInfo.teamId = responseData.opponentTeam.teamId;
                that.selectedTeamInfo.teamName = responseData.screenName;
                that.selectedTeamInfo.teamPoints = responseData.opponentTeam.teamPoints;
                window.scrollTo(0,0);
				that.showPlayersPreview = true;
				utilityservice.hideLoader();
            });
        }

		function fetchLeagueInfo(leagueId){
			var params = {};
			params.leagueId = parseInt(leagueId);
			
			leagueservice.getLeagueInfo(params, function(response){
				
				if(response.respCode === 100){
					that.leagueInfo = response.respData;
					fetchLeagueMembers(leagueId);
				}
				else{

				}
			})
		}

		function fetchMatchInfo(matchId){
			var params = {};
			params.matchId = parseInt(matchId);
			leagueservice.getTourMatchInfo(params, function(response){
				if(response.respCode === 100){
					that.selectedMatch = response.respData;
					setmemberDisplayType(that.selectedMatch.status);
					fetchLeagueInfo(leagueId);
				}
				else{

				}
			})
		}

		function fetchLeagueMembers(leagueId) {
            var params = {
                matchId: that.selectedMatch.matchId,
                leagueId: leagueId,
                limit: pageSize,
                limitSize: pageSize
            };
            utilityservice.showLoader();
            that.leagueMembersArray = [];
            leagueservice.getLeagueMembers(params, function (leagueMembersArray) {
                that.leagueMembersArray = leagueMembersArray;
                utilityservice.hideLoader();
            });

        }

		function setmemberDisplayType(status){
			that.memberDisplayType = '';

			switch(status){
				case 'ACTIVE':
					that.memberDisplayType = 'ACTIVE';
					break;
				case 'RUNNING':
				case 'UNDER_REVIEW':
				case 'CLOSED':
					that.memberDisplayType = 'CLOSED';
					break;
				default:
			}

		}

		$rootScope.$on('fetchLeaguesEvent', function(event, args){
	        //console.log('got the event', args);
	        //$window.location.reload();
	        that.init({matchId: args.matchId, leagueId: args.leagueId});
	    });
	
 }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:joinedLeaguesCtrl
 * @description
 * # joinedLeaguesCtrl as joinedleague
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
	.controller('joinedLeaguesCtrl', ['$rootScope', '$window', 'utilityservice', 'leagueservice', 'session', 'leagueutilservice', function ($rootScope, $window, utilityservice, leagueservice, session, leagueutilservice) {
		var that = this;

		var baseUrl = utilityservice.getBaseUrl();
		that.selectedMatch = null;
		that.leagueDisplayType = null;
		that.showSwitchTeam = false;
		that.teamArr = null;
		that.joinedTeamArr = null;
		that.switchTeamLeagueId = null;
		that.joinedLeaguesArray = [];
		that.noLeaguesError = false;

		var winScrollTop = 0;

		that.init = function(initObj){
			fetchMatchInfo(initObj.matchId);
		};

		that.onBrowseLeague = function(league){
			$window.location.href = baseUrl + '/browse-league/' + that.selectedMatch.matchId + '/' + league.leagueId;
		};

		that.fetchLeaguePrizeInfo = function (league) {
			if(league.chipType === 'skill'){
				return false;
			}
			var params = {};
			params.leagueId = league.leagueId;
			leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
				leagueutilservice.prizeInfoPopup(prizeinfodata);
			});
		};

		that.validateJoinLeague = function(league, event){
			return leagueutilservice.validateJoinLeague(league, that.selectedMatch.totalTeams);
		};

		that.getMatchStatus = function(status){
			return leagueutilservice.getMatchStatus(status);
		};

		that.onSwitchTeamClick = function(league){
			winScrollTop = document.querySelector('body').scrollTop;
			window.scrollTo(0, 0);
			var body = document.querySelector('body');
			body.classList.add('ovf-hdn');

			that.selectedTeamId = league.teamId.toString();
			that.switchTeamLeagueId = league.leagueId;
			that.joinedTeamArr = league.teamArr;
			that.selectedTeamIdArr = [];
			for(var i = 0, len = that.joinedTeamArr.length; i < len; i++){
				var data = {
					teamId: that.joinedTeamArr[i].teamId.toString(),
					leagueJoinedId: that.joinedTeamArr[i].leagueJoinedId};
				that.selectedTeamIdArr.push(data);
			}
			//console.log(that.selectedTeamIdArr);
			that.showSwitchTeam = true;
		};

		that.closeSwitchTeam = function(){
			window.scrollTo(0, winScrollTop);
			var body = document.querySelector('body');
			body.classList.remove('ovf-hdn');
			that.showSwitchTeam = false;
		};

		that.switchMyTeam = function () {

			var receiveCount = 0;
			var callCount = 0;
			for(var i = 0, len = that.joinedTeamArr.length; i < len; i++){
				if(that.joinedTeamArr[i].teamId !== parseInt(that.selectedTeamIdArr[i].teamId)){
					var params = {
		                teamId: parseInt(that.selectedTeamIdArr[i].teamId),
		                leagueId: that.switchTeamLeagueId,
		                matchId: that.selectedMatch.matchId,
		                leagueJoinedId: that.selectedTeamIdArr[i].leagueJoinedId
		            }
		            utilityservice.showLoader();
		            callCount++;
		            leagueservice.updateUserJoinedTeam(params, function (response) {
		                that.showSwitchTeam = false;
		                receiveCount++;
		                if(callCount == receiveCount){
							$window.location.reload();
		                }
		            });
				}
			}
		 	// $window.location.reload();
        };

		function fetchMatchInfo(matchid){
			var params = {};
			params.matchId = parseInt(matchid);
			leagueservice.getTourMatchInfo(params, function(response){
				if(response.respCode === 100){
					that.selectedMatch = response.respData;
					fetchJoinedLeagues();
					setLeagueDisplayType(that.selectedMatch.status);
					var teamArr = [];
					for (var i = 1; i <= that.selectedMatch.totalTeams; i++) {
						teamArr.push({teamId: i});
					}
					that.teamArr = teamArr;
				}
				else{

				}
			});
		}

		function setLeagueDisplayType(status){
			that.leagueDisplayType = '';
			switch(status){
				case 'ACTIVE':
					that.leagueDisplayType = 'ACTIVE';
					break;
				case 'RUNNING':
				case 'CLOSED':
				case 'UNDER_REVIEW':
					that.leagueDisplayType = 'CLOSED';
					break;
				default:
			}

		}

		function fetchJoinedLeagues(){
			that.joinedLeaguesArray = [];
			var params = {
				matchId: that.selectedMatch.matchId
			};
			leagueservice.getJoinedLeagues(params, function(joinedLeaguesArray){
				that.noLeaguesError = true;
				that.joinedLeaguesArray = joinedLeaguesArray;
			});
		}

		$rootScope.$on('fetchLeaguesEvent', function(event, args){
	        that.init({matchId: args.matchId});
	    });
 }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:leagueJoinConfirmCtrl
 * @description
 * # leagueJoinConfirmCtrl as leaguejoin
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
	.controller('leagueJoinConfirmCtrl', ['$rootScope', '$scope', '$window', 'utilityservice', 'leaguedata', 'config', 'serviceName', 'msgConstants', function ($rootScope, $scope, $window, utilityservice, leaguedata, config, serviceName, msgConstants) {
		var that = this;

		that.leagueObj = leaguedata;
		that.selectedTeamId = that.leagueObj.league.teamArr[0].toString();
		that.isJoinBtnDisabled = false;

		var baseUrl = utilityservice.getBaseUrl();

		that.onJoinLeagueClick = function(){
			//console.log('team id :: ', that.selectedTeamId);
			var params = {
				leagueId: that.leagueObj.league.leagueId,
				matchId: that.leagueObj.league.matchId,
				teamId: parseInt(that.selectedTeamId)
			};
			that.isJoinBtnDisabled = true;
			utilityservice.showLoader();
			utilityservice._postAjaxCall(config.gameNodeHost + serviceName.JOIN_LEAGUE, params, function (response) {
				that.isJoinBtnDisabled = false;
				if (response.respCode == 100) {

					var successMsg = '';
					if(that.leagueObj.league.chipType === 'real'){
						successMsg = '<p class="fs18">Join more leagues to challenge other teams to win cash.</p>';
					}
					if(that.leagueObj.league.chipType === 'skill'){
						successMsg = '<p class="fs18">Now try one of our cash leagues to win real money.</p>';
					}
					// Update User Balance in header 
					if(response.respData.balance !== undefined){
						//response.respData.balance = 200;
						utilityservice.updateBalance(response.respData.balance);	
					}
					$rootScope.$broadcast("showPopup", {
						type: "Info",
						title: msgConstants.HEADER_INFO,
						appendTo: '#mainWrapper',
						msg: successMsg,
						popupSize: 'small',
						showClose: false,
						okButtonText: 'Ok',
						okBtnCallback: function () {
							// Updating leagues and league count after league join success
							var data = {
								matchId: that.leagueObj.league.matchId,
								leagueCount: response.respData.leagueCount,
								leagueId: that.leagueObj.league.leagueId
							};
							$rootScope.$broadcast('fetchLeaguesEvent', data);
						}
					});
				}
				else if(response.respCode === 124){
	                $rootScope.$broadcast("showPopup", {
	                    type: "Info",
	                    title: msgConstants.HEADER_INFO,
	                    appendTo: '#mainWrapper',
	                    msg: response.message,
	                    popupSize: 'small',
	                    showClose: false,
	                    okButtonText: 'Ok',
	                    okBtnCallback: function () {
	                        var data = {
	                            matchId: response.respData.matchId
	                        };
	                        $rootScope.$broadcast('fetchLeaguesEvent', data);
	                    }
	                });
	            }
				else{
					$rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo: '#mainWrapper',
                        msg: response.message,
                        popupSize: 'small',
                        showClose: true,
                        okButtonText: 'Ok',
                        okBtnCallback: function () {
                            //console.log('');
                        }
                    });
				}
				utilityservice.hideLoader();
			});
		};
 }]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:matchLeaguesCtrl
 * @description
 * # matchLeaguesCtrl as matchleagues
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('matchLeaguesCtrl', ['$rootScope', 'utilityservice', 'leagueservice', 'ngDialog', 'session', '$window', 'config', 'serviceName', 'leagueutilservice', function ($rootScope, utilityservice, leagueservice, ngDialog, session, $window, config, serviceName, leagueutilservice) {
	var that = this;

	that.userBalance = session.getUserBalance();
	that.baseUrl = utilityservice.getBaseUrl();
	that.selectedMatch = {};
	that.noLeaguesError = false;

	that.init = function(initObj){

		var matchId = initObj.matchId;

		checkAutoLeagueFlow(matchId, function(response){
			fetchMatchInfo(response.matchId);
			//that.fetchLeagues(response.matchId);
		});
	};

	that.fetchLeagues = function(matchid){
		var params = {};
		params.matchId = matchid;
		//utilityservice.showLoader();
		that.leaguesArray = [];
		leagueservice.getLeagues(params, function(response){
			that.noLeaguesError = true;
			if(response.respCode === 100){
				that.leaguesArray = response.respData;
			}
			else{
				console.log('Error in Fetching leagues', response.message);
			}
			//utilityservice.hideLoader();
		});
	};

	that.fetchLeaguePrizeInfo = function (league) {
		var params = {};
		params.leagueId = league.leagueId;
		leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
			leagueutilservice.prizeInfoPopup(prizeinfodata);
		});
	};

	that.onBrowseLeague = function(league){
		$window.location.href = that.baseUrl + '/browse-league/' + 12;
	};

	that.onJoinedLeagues = function(){
		if(that.selectedMatch.leagueCount === 0 || that.selectedMatch.leagueCount === undefined){
			// alert('You havent joined any leagues');
			utilityservice.showToastError({message: 'You havent joined any leagues'});
			return false;
		}
		$window.location.href = that.baseUrl + '/my-joined-league/' + that.selectedMatch.matchId;
	};

	that.onCreateTeamClick = function(){
		$window.location.href = that.baseUrl + '/createteam/' + that.selectedMatch.matchId + '/' + leagueutilservice.getActiveMatchTeamId();
	};

	that.onMyTeamsClick = function(){
		$window.location.href = that.baseUrl + '/my-teams/' + that.selectedMatch.matchId;
	};

	that.validateJoinLeague = function(league, event){
		return leagueutilservice.validateJoinLeague(league, that.selectedMatch.totalTeams);
	};

	that.getMatchStatus = function(status){
		return leagueutilservice.getMatchStatus(status);
	};

	function fetchMatchInfo(matchid){
		var params = {};
		params.matchId = parseInt(matchid);
		leagueservice.getTourMatchInfo(params, function(response){
			if(response.respCode === 100){
				that.selectedMatch = response.respData;
				that.fetchLeagues(matchid);
			}
			else{

			}
		});
	}

	function checkAutoLeagueFlow(matchId, callback){
		var invCode = utilityservice.getUrlParameter('invCode');

            if(invCode !== undefined && invCode !== ""){
                var league = {
                    leagueId: invCode
                };
                leagueutilservice.checkJoinLeague(league, function(checkLeagueResponse){
                	if(!!checkLeagueResponse && (checkLeagueResponse.matchId != matchId)){
		                matchId = checkLeagueResponse.matchId;
		            }
            		callback({matchId: matchId});
                });
            }
            else{
            	callback({matchId: matchId});
            }
	}

	$rootScope.$on('fetchLeaguesEvent', function(event, args){
        // Reload the page with new matchId to show its leagues
        // $window.location.href = that.baseUrl + '/match-leagues/' + args.matchId;
        var invCode = utilityservice.getUrlParameter('invCode');
        if(invCode !== undefined && invCode !== ""){
            $window.location.href = that.baseUrl + '/match-leagues/' + args.matchId;    // Removing query string after successful league join if invCode is present
        }
        else{
            that.init({matchId: args.matchId});
        }
    });

}]);

'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:myTeamsCtrl
 * @description
 * # myTeamsCtrl as myteam
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('myTeamsCtrl', ['$rootScope','$window', 'leagueservice', 'utilityservice', 'leagueutilservice', 'session', 'msgConstants', function ($rootScope, $window, leagueservice, utilityservice, leagueutilservice, session, msgConstants) {
 	var that = this;

 	that.showPlayersPreview = false;
 	that.currentTeamId = null;
 	that.currentMatchId = null;
 	that.teamDisplayType = null;
 	var baseUrl = utilityservice.getBaseUrl();

 	that.init = function(initObj){
 		that.currentMatchId = initObj.matchId;
 		fetchMatchInfo(initObj.matchId);
 	}

 	function fetchMatchInfo(matchid){
 		var params = {};
 		params.matchId = parseInt(matchid);
 		leagueservice.getTourMatchInfo(params, function(response){
 			if(response.respCode === 100){
 				that.selectedMatch = response.respData;
 				setTeamDisplayType(that.selectedMatch.status);
 				that.fetchTeamInfo();
 			}
 			else{
 				$rootScope.$broadcast("showPopup", {
                    type: "Info",
                    title: msgConstants.HEADER_INFO,
                    appendTo: '#mainWrapper',
                    msg: response.message,
                    popupSize: 'small',
                    showClose: false,
                    okButtonText: 'Ok',
                });
 			}
 		});
 	}

 	that.fetchTeamInfo = function(teamId){
 		var params = {};
			params.matchId = that.currentMatchId;
			if(!!teamId){
				params.teamId = teamId;
				that.currentTeamId = teamId;
			}
			utilityservice.showLoader();

			leagueservice.getTeamInfo(params, function(responseData){
				that.selectedTeamInfo = {};
				if(!!teamId){
					that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.currentTeam.players);
					that.selectedTeamInfo.teamId = responseData.currentTeam.teamId;
					that.selectedTeamInfo.teamPoints = responseData.currentTeam.teamPoints;
					that.selectedTeamInfo.leagueCount = responseData.leagueCount;
					that.showTeamPreview(true);
				}

				that.myTeamsArray = responseData.teamsArr;
				that.selectedTeamInfo.teamName = responseData.screenName;
				utilityservice.hideLoader();
			});
		};

		that.getMatchStatus = function(status){
			var matchstatus = '';
			switch(status){
				case 'RUNNING':
					matchstatus = 'In Progress';
					break;
				case 'UNDER_REVIEW':
					matchstatus = 'Under Review';
					break;
				case 'CLOSED':
					matchstatus = 'Completed';
					break;
				case 'ABANDONED':
					matchstatus = 'Abandoned';
					break;
				default:
			}
			return matchstatus;
		};

		that.showTeamPreview = function(bool){
			$window.scrollTo(0,0);
			var body = document.querySelector('body');
			if(bool){
				body.classList.add('ovf-hdn');
			}
			else{
				body.classList.remove('ovf-hdn');	
			}
			that.showPlayersPreview = bool;
		};

		that.onEditTeamClick = function(){
			$window.location.href = baseUrl + "/editteam/" + that.selectedMatch.matchId + '/' + that.currentTeamId;
		};

		that.onShowLeaguesClick = function(){
			$window.location.href = baseUrl + "/match-leagues/" + that.selectedMatch.matchId; // + '/' + that.currentTeamId;
		};

		that.onAddTeamClick = function(){
			$window.location.href = baseUrl + "/createteam/" + that.selectedMatch.matchId + '/' + (that.selectedMatch.totalTeams + 1);
		};

		that.onScorecardClick = function(){
			$window.open(utilityservice.getBaseUrl() + "/fantasy-scorecard/" + that.selectedMatch.matchId, '_blank');
			//$window.location.href = that.baseUrl + "/fantasy-scorecard/" + that.selectedMatch.matchId;
		};

		that.onShowJoinedLeaguesClik = function(){
			$window.location.href = utilityservice.getBaseUrl() + '/my-joined-league/' + that.selectedMatch.matchId;
		};

		function setTeamDisplayType(status){
			that.teamDisplayType = '';
			switch(status){
				case 'ACTIVE':
					that.teamDisplayType = 'ACTIVE';
					break;
				case 'RUNNING':
				case 'UNDER_REVIEW':
				case 'CLOSED':
					that.teamDisplayType = 'CLOSED';
					break;
				default:
			}
		}

	}]);
'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:tourMatchesCtrl
 * @description
 * # tourMatchesCtrl as tourmatches
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
    .controller('tourMatchesCtrl', ['$scope', 'constants', 'leagueservice', 'utilityservice', 'leagueutilservice', '$timeout', '$rootScope', 'devicedetectionservice', 'ngDialog', 'session', 'createteamservice', '$window', 'promotionsservice', function ($scope, constants, leagueservice, utilityservice, leagueutilservice, $timeout, $rootScope, devicedetectionservice, ngDialog, session, createteamservice, $window, promotionsservice) {
        var that = this;

        that.baseUrl = utilityservice.getBaseUrl();

        that.mob = {
            activeMatches: [],
            runningMatches: [],
            closedMatches: []
        }

        that.matchDisplayType = 'LOBBY';
        that.screenName = session.getScreenName();
        that.userBalance = session.getUserBalance();
        that.selectedTour = null;
        that.showNoLeaguesError = false;
        that.selectedMatch = null;
        that.noMatchesError = false;

        // init function called when controller loads
        that.init = function(){
            fetchTours();


            if(session.getTeamName() === '' || session.getTeamName() === null || session.getTeamName() === undefined){
                leagueutilservice.myTeamNamePopoup();
            }


            // Promtions
            var params ={};
            promotionsservice.getPromotions(params, function(response){
                console.log("data from service",response);
                console.log(response.data);
                that.promotionsData = response;
            });  
        };

        that.onTourClick = function(){
            var tourWrapper = document.querySelector('.cstm-tour-select');
            if(tourWrapper.classList.contains('open')){
                tourWrapper.classList.remove('open');
            }
            else{
                tourWrapper.classList.add('open');
            }
        };

        that.onShowLeaguesClick = function(match){
            that.selectedMatch = match;
            $window.location.href = that.baseUrl + '/match-leagues/' + match.matchId;
        }

        that.onTourSelect = function(tour){
            that.selectedTour = tour;
            fetchMatches(tour.tourId);
            var tourWrapper = document.querySelector('.cstm-tour-select');
            tourWrapper.classList.remove('open');
        };

        that.onMatchTypeClick = function(matchtype, event){

            switch(matchtype){
                case 'ACTIVE':
                    that.matchDisplayType = 'LOBBY';
                    break;
                case 'RUNNING':
                    that.matchDisplayType = 'ACTIVE';
                    break;
                case 'CLOSED':
                    that.matchDisplayType = 'RESULTS';
                    break;
            }
        };

        that.onJoinedLeagues = function(match){
            console.log('onBrowseLeague ::::', match);
            // if(match.leagueCount === 0){
            // 	alert('no leagues joined');
            // 	return;
            // }
            $window.location.href = that.baseUrl + '/my-joined-league/' + match.matchId;
        };

        function fetchTours(){
            var params = {};
            leagueservice.getTours(params, function(toursArray){
                var alltour = {shortName: 'All', status: 'ACTIVE', tourId: -1, tourName: 'All Series'};
                toursArray.unshift(alltour);
                that.toursArray = toursArray;
                that.selectedTour = that.toursArray[0];
                fetchMatches(-1);
            });
        }

        // Private Functions
        function fetchMatches(tourid){
            var params = {};
            if(tourid !== -1){
                params.tourId = tourid;
            }
            utilityservice.showLoader();
            leagueservice.getMatches(params, function(matchesArray,nextTimeTour){
                that.matchesArray = matchesArray;
                that.noMatchesError = true;
                filterMatchByStatus(that.matchesArray);
                utilityservice.hideLoader();
            });
        }

        that.onCreateTeamClick = function(match){
            $window.location.href = that.baseUrl + "/createteam/" + match.matchId + '/' + leagueutilservice.getActiveMatchTeamId();
        };

        that.onMyTeamClick = function(match){
            if(match.totalTeams === 0){
                utilityservice.showToastError({title: 'You missed it!', message: 'You have not created a team for this match'});
                return false;
            }
            $window.location.href = that.baseUrl + "/my-teams/" + match.matchId;
        };

        function filterMatchByStatus(matcharray){
            //console.log('filterMatchByStatus');
            that.mob.activeMatches = [];
            that.mob.runningMatches = [];
            that.mob.closedMatches = [];

            var arrlen = matcharray.length;
            for(var i = 0; i < arrlen; i++){
                switch(matcharray[i].status.toUpperCase()){
                    case 'ACTIVE':
                    case 'LOCKED':
                        that.mob.activeMatches.push(matcharray[i]);
                        break;
                    case 'RUNNING': 
                    case 'UNDER_REVIEW':
                        that.mob.runningMatches.push(matcharray[i]);
                        break;
                    case 'CLOSED':
                    case 'ABANDONED':
                        that.mob.closedMatches.push(matcharray[i]);
                        break;
                    default:
                }
            }
            // Reversing matches array by decreasing order time
            that.mob.runningMatches = that.mob.runningMatches.reverse();
            that.mob.closedMatches = that.mob.closedMatches.reverse();
        }

        // function getActiveMatch(matcharray){
        // 	var activematch = matcharray[0];
        // 	var arrlen = matcharray.length;
        // 	for(var i = 0; i< arrlen; i++){
        // 		if(matcharray[i].status === 'ACTIVE'){
        // 			that.selectedMatch = matcharray[i];
        // 			activematch = matcharray[i];
        // 			break;
        // 		}
        // 	}
        // 	return activematch;
        // }
        
        
        that.showPromtionDetails  = function(e, index){
            var promotionImageUrl = that.promotionsData[index].fields.popup_banner_mobile.url;
            var imgDom = document.createElement("img");
            imgDom.src = promotionImageUrl;
            imgDom.className = "w100"; 
            var imgWrapper = document.createElement("div");
            imgDom.appendChild(imgWrapper);
            var tmp = document.createElement("div");
            tmp.appendChild(imgDom);

            that.promoDetailsData = {};
            that.promoDetailsData.headerName = that.promotionsData[index].name;
            that.promoDetailsData.content = tmp.innerHTML+that.promotionsData[index].fields.popup_content.text;
            that.promoDetailsData.pageCss = that.promotionsData[index].fields.popup_content_css.text;
            that.promoDetails = true;
            $timeout(function(){
                that.iframeLoadedCallBack();
            },200);
        };

        that.hidePromtionDetails = function(){
            that.promoDetails = false;
        };

        that.iframeLoadedCallBack = function () {
            var cssLink = that.promoDetailsData.pageCss;
            var pageData = '';
            pageData = that.promoDetailsData.content;
            //console.log(document.querySelector('.promo-details-popup'));
            var iFrame = angular.element(document.querySelector('iframe.promo-details-popup'))[0];

            var iFrameBody;
            if (iFrame.contentWindow.document){ // FF
                iFrameBody = angular.element(iFrame.contentDocument.getElementsByTagName('body')[0]);
            } else if (iFrame.contentWindow.document){ // IE
                iFrameBody = angular.element(iFrame.contentWindow.document.getElementsByTagName('body')[0]);
            }

            var iFrameHead;
            if (iFrame.contentWindow.document){ // FF
                iFrameHead = angular.element(iFrame.contentDocument.getElementsByTagName('head')[0]);
            } else if (iFrame.contentWindow.document){ // IE
                iFrameHead = angular.element(iFrame.contentWindow.document.getElementsByTagName('head')[0]);
            }
            console.log(cssLink.toString());
            iFrameHead.append(cssLink);
            iFrameBody.append(pageData);
        };

    }]);
