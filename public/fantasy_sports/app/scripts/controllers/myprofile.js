'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:MyprofileCtrl
 * @description
 * # MyprofileCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('MyprofileCtrl',['constants','$rootScope','utilityservice','config','devicedetectionservice','user','ngDialog','$window','serviceName','dateDropdownHandler', 'msgConstants', function (constants, $rootScope, utilityservice, config, devicedetectionservice, user, ngDialog, $window, serviceName, dateDropdownHandler, msgConstants) {
      var that = this;
      that.userName = '';
      that.userTeamName = '';
      that.userEmail = '';
      that.userMobile = '';
      that.User_DOB = '';
      that.isUserNameDisable = false;
      that.isTeamNameDisable = false;
      that.isEmailDisable = false;
      that.isMobileDisable = false;
      that.selectedState = "Select State";
      that.statesArray = constants.STATES_LIST;
      that.submitDisable = true;
      that.errMsg = "";
      var isDesktop = devicedetectionservice.isDevice('DESKTOP');

      //Calender Module functionality Starts here---Ashutosh
      that.selectedMonth = "MM";
      that.selectedYear = "YY";
      that.selectedDate = "DD";
      that.dobRange = [];
      that.dobRange.push("YY");
      that.monthName = constants.MONTH_LIST;
      that.dateRange = [];
      dateDropdownHandler.initilizeYears(function(yearRange){
            that.dobRange = yearRange;
      });
      dateDropdownHandler.initilizeDays(function(dayRange){
            that.dateRange = dayRange;
      });

      that.getKey = function(month) {
            return Object.keys(month)[0];
      };
      // On year change function--Ashu.
      that.changeYear = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getMonths(that.selectedYear, function(monthList){
                  that.monthName = monthList;
            });
            that.checkDays();
      };
      // On day change function
      that.dayChange = function(){
            that.dateEmptyvalidator();
      };

      // On month change calucate days to show in dropdown--Ashu
      that.checkDays = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getDays(that.selectedYear, that.selectedMonth, function(dayList){
                  that.dateRange = dayList;
                  if(that.selectedDate > dayList.length-1) {
					  that.selectedDate = "DD";
				  }
            });
      };

      that.dateEmptyvalidator = function(){
            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = "Please fill all date fields.";
            }else {
                that.userDateMsg = '';
            }
      };
      //Calender Module functionality Ends


      angular.element(document).ready(function () {
            var params = {};
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.GETUSER_PROFILE, params, function (response) {
                if(response.respCode === 100){
                    if(response.respData.isEmailVerified == "NO"  || response.respData.isMobileVerified == "NO"){
                          that.submitDisable = true;
                          that.errMsg = msgConstants.VERIFY_EMAIL_MOBILE;
                    }else{
                          that.submitDisable = false;
                          that.errMsg = "";
                    }
                    that.userName = response.respData.firstName;
                    that.userTeamName = response.respData.screenName;
                    if(response.respData.dob != null) {
                          that.User_DOB = new Date(response.respData.dob);
                          that.selectedMonth = String(that.User_DOB.getMonth() + 1);
                          if(that.selectedMonth < 9) {
                              that.selectedMonth = '0'+that.selectedMonth;
                          }
                          that.selectedDate = String(that.User_DOB.getDate());
                          if(that.selectedDate < 9) {
                              that.selectedDate = '0'+that.selectedDate;
                          }
                          that.selectedYear = that.User_DOB.getFullYear();
                    }
                    that.userGender = response.respData.gender;
                    that.userEmail = response.respData.emailId;
                    that.userMobile = response.respData.mobileNbr;
                    that.userAddress = response.respData.address;
                    that.userCity = response.respData.city;
                    that.userPincode = response.respData.pinCode;
                    if(response.respData.state!= null && response.respData.state!= ''){
                          that.selectedState = response.respData.state;
                    }

                    //that.setDisableEnableInput();
                }else {
                    $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: response.message,
                        popupSize: 'small',
                        okButtonText: 'OK'
                    });
                }
            });
      });

      /* Update profile on form submit */
      that.updateProfile = function() {
          that.isValidate = true;
          that.userNameMsg = '';
          that.userTeamNameMsg = '';
          that.userEmailMsg = '';
          that.userMobileMsg = '';

          user.OnInputFieldValidation('userName', that.userName,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userNameMsg = response.message;
                }
          });

          /*user.OnInputFieldValidation('userTeamName', that.userTeamName,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userTeamNameMsg = response.message;
                }
          });

          user.OnInputFieldValidation('userEmail', that.userEmail,'', function(response){
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userEmailMsg = response.message;
                }
          });

          user.OnInputFieldValidation('userMobile', that.userMobile,'', function(response){
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userMobileMsg = response.message;
                }
          });*/


          //Call Api if all validations are pass.
          if(that.isValidate) {
              var params = {};
              var updateDob = '';
              if(that.selectedYear=="YY" || that.selectedYear=="" || that.selectedMonth=="MM" || that.selectedMonth=="" || that.selectedDate=="DD" || that.selectedDate=="") {
                  updateDob = '';
              }else {
                  updateDob = (that.selectedMonth + '/' + that.selectedDate + '/' +  that.selectedYear);
              }
              params.firstName = that.userName;
              //params.screen_name = that.userTeamName;
              //params.emailId = that.userEmail;
              //params.mobileNbr = that.userMobile;
              params.dob = updateDob;
              params.gender = that.userGender;
              params.address = that.userAddress;
              params.city = that.userCity;
              params.pinCode = that.userPincode;
              params.state = that.selectedState;
              params.country = "India";

              utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.UPDATE_PROFILE, params, function (response) {
                  if(response.respCode == 100) {
                        ngDialog.close();
                        $rootScope.$broadcast("showPopup", {
                              type: "Info",
                              title: msgConstants.HEADER_INFO,
                              appendTo:'#mainWrapper',
                              msg: msgConstants.MSG_PROFILE_UPDATED_SUCCESS,
                              popupSize: 'small',
                              okButtonText: 'OK',
                              okBtnCallback: function () {
                                 if(isDesktop){
                                    $window.location.reload();
                                 } else{
                                    $window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
                                 }
			 			      }
                        });
                  }else {
                        $rootScope.$broadcast("showPopup", {
                              type: "Info",
                              title: msgConstants.HEADER_INFO,
                              appendTo:'#mainWrapper',
                              msg: response.message,
                              popupSize: 'small',
                              okButtonText: 'OK'
                        });
                  }
              });
          }
      };

      that.setDisableEnableInput = function() {
            if(that.userName != '' && that.userName != null) {
                that.isUserNameDisable = true;
            }
            if(that.userTeamName != '' && that.userTeamName != null) {
                that.isTeamNameDisable = true;
            }
            if(that.userEmail != '' && that.userEmail != null) {
                that.isEmailDisable = true;
            }
            if(that.userMobile != '' && that.userMobile != null) {
                that.isMobileDisable = true;
            }
      };

      that.checkAlphabet = function(event,fieldName){
            that.userMobileMsg='';
            var keyCode = event.keyCode;
            if(keyCode<48 || keyCode>57 ){
               that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
               //event.preventDefault();
            }
      };

      that.onFieldChange = function(fieldName,fieldValue,additionalInfo){
          user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
            if(response.resCode !== 100){
                that[fieldName+'Msg'] = response.message;
            }else{
                console.log("resCode---->",response.message);
            }
          });
      };

  }]);
