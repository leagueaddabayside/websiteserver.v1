'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:MyteamnameCtrl
 * @description
 * # MyteamnameCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('MyteamnameCtrl',['$rootScope','$window','constants','user','msgConstants','session','config','utilityservice','serviceName','dateDropdownHandler','$timeout', function ($rootScope, $window, constants, user, msgConstants, session, config, utilityservice,  serviceName, dateDropdownHandler,$timeout) {
      var that = this;
      that.userTerms = false;
      that.selectedState = "Select State";
      that.statesArray = constants.STATES_LIST;

      //Calender Module functionality Starts here---Ashutosh
      that.selectedMonth = "MM";
      that.selectedYear = "YY";
      that.selectedDate = "DD";
      that.dobRange = [];
      that.dobRange.push("YY");
      that.monthName = constants.MONTH_LIST;
      that.dateRange = [];
      dateDropdownHandler.initilizeYears(function(yearRange){
            that.dobRange = yearRange;
      });
      dateDropdownHandler.initilizeDays(function(dayRange){
            that.dateRange = dayRange;
      });

      that.getKey = function(month) {
            return Object.keys(month)[0];
      };
      // On year change function--Ashu.
      that.changeYear = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getMonths(that.selectedYear, function(monthList){
                  that.monthName = monthList;
            });
            that.checkDays();
      };
      // On day change function
      that.dayChange = function(){
            that.dateEmptyvalidator();
      };

      // On month change calucate days to show in dropdown--Ashu
      that.checkDays = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getDays(that.selectedYear, that.selectedMonth, function(dayList){
                  that.dateRange = dayList;
                  if(that.selectedDate > dayList.length-1) {
					  that.selectedDate = "DD";
				  }
            });
      };

      that.dateEmptyvalidator = function(){
            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = "Please fill all date fields.";
            }else {
                that.userDateMsg = '';
            }
      };
      //Calender Module functionality Ends

      that.onFieldChange = function(fieldName,fieldValue,additionalInfo){
          user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
            if(response.resCode !== 100){
                that[fieldName+'Msg'] = response.message;
            }else{
                console.log("resCode---->",response.resCode);
            }
          });
      };

       that.init = function() {
             if(session.getUserDOB() != undefined && session.getUserDOB() != null && session.getUserDOB() != '') {
                    that.User_DOB = new Date(session.getUserDOB());
                    that.userTeamName = session.getTeamName();
                    //console.log("that.userDOB---",that.User_DOB+"    that.screenName--",that.userTeamName);
                    that.selectedMonth = String(that.User_DOB.getMonth() + 1);
                    if(that.selectedMonth < 9) {
                        that.selectedMonth = '0'+that.selectedMonth;
                    }
                    that.selectedDate = String(that.User_DOB.getDate());
                    if(that.selectedDate < 9) {
                        that.selectedDate = '0'+that.selectedDate;
                    }
                    that.selectedYear = that.User_DOB.getFullYear();
              }
       };

       that.changeState = function() {
            if(that.selectedState=="Select State" || that.selectedState=="") {
                  that.userStateMsg = msgConstants.REQUIRED_MESSAGE;
            }else {
                  that.userStateMsg = '';
            }
       };

       that.checkTNC = function() {
				if(that.userTerms) {
						that.userTermsMsg = '';
				}else {
						that.userTermsMsg = msgConstants.ERR_TNC;
				}
		};

       that.updateProfile = function() {
            that.isValidate = true;
            that.userTeamNameMsg = '';
            that.userDateMsg = '';
            that.userStateMsg = '';

            user.OnInputFieldValidation('userTeamName', that.userTeamName,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.userTeamNameMsg = response.message;
                }
            });

            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = msgConstants.REQUIRED_MESSAGE;
                that.isValidate = false;
            }else {
                that.userDateMsg = '';
            }

            if(that.selectedYear=="YY" || that.selectedYear==null || that.selectedMonth=="MM" || that.selectedMonth==null || that.selectedDate=="DD" || that.selectedDate==null) {
                that.userDateMsg = msgConstants.REQUIRED_MESSAGE;
                that.isValidate = false;
            }

            if(that.selectedState=="Select State" || that.selectedState=="" ) {
                that.userStateMsg = msgConstants.REQUIRED_MESSAGE;
                that.isValidate = false;
            }

            if(!that.userTerms){
                 that.isValidate = false;
                 that.userTermsMsg = msgConstants.ERR_TNC;
            }
            if(that.isValidate) {
                  var params = {};
                  var updateDob = '';
                  updateDob = (that.selectedMonth + '/' + that.selectedDate + '/' +  that.selectedYear);

                  params.dob = updateDob;
                  params.screenName = that.userTeamName;
                  params.state = that.selectedState;
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.UPDATE_TEAMNAME, params, function (response) {
                        if(response.respCode == 100) {
                              $window.location.reload();
                        }else{
                              that.isValidate = false;
                              that.userTeamNameMsg = response.message;
                        }
                  });
            }

       };

  }]);
