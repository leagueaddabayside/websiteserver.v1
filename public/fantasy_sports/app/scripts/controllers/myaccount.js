'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:MyaccountCtrl
 * @description
 * # MyaccountCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('MyaccountCtrl',['session','$rootScope','$window','utilityservice','msgConstants','config','ngDialog','serviceName','depositeservice','devicedetectionservice', function (session, $rootScope, $window, utilityservice , msgConstants, config, ngDialog, serviceName, depositeservice, devicedetectionservice) {
        var that = this;
        var isDesktop = devicedetectionservice.isDevice('DESKTOP');
        that.userStatus = session.getUserStatus();
        that.downloadTXNhistory = config.websiteNodeHost+serviceName.REQUEST_TXN_HISTORY;
        angular.element(document).ready(function () {
              utilityservice.showLoader();
              var params = {};
              utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.MYACCOUNT_INFO, params, function (response) {
                    utilityservice.hideLoader();
                    if(response.respCode == 100) {
                      var respData = response.respData;
                      that.totalAmt = (respData.depositAmt + respData.bonusAmt + respData.winningAmt).toFixed(2);

                      that.depositeAmt = respData.depositAmt.toFixed(2);
                      that.bonusAmt = respData.bonusAmt.toFixed(2);
                      that.winningAmt = respData.winningAmt.toFixed(2);

                      that.totalWinning = respData.totalWinning.toFixed(2);
                      that.leaguesWin = respData.leaguesWin;
                    }
              });

              var params1 = {userId:session.getUserID(), limit : 50};
              utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.TXN_HISTORY, params1, function (response) {
                    if(response.respCode == 100) {
                        that.TranactionArr = response.respData;
                        for(var i=0; i < that.TranactionArr.length; i++){
                            var date = new Date(that.TranactionArr[i].createdAt);
                            that.TranactionArr[i].createdAt = String(date.getMonth() + 1)+"/"+date.getDate()+"/"+date.getFullYear();
                        }
                    }
              });
              depositeservice.afterPayment();
        });

        that.showLeaguepage = function() {
            if(isDesktop){
                  $window.location.href = utilityservice.getBaseUrl() + "/league";
            } else{
                  $window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
            }
        };
        that.showVerifyNow = function() {
            $window.location.href = utilityservice.getBaseUrl() + "/verify";
        };

        that.downloadHistory = function(){
            $rootScope.$broadcast("showPopup", {
              type: "Confirm",
              title: msgConstants.HEADER_CONFIRMATION,
              appendTo:'#mainWrapper',
              msg: msgConstants.DOWNLOAD_TXN_HISTORY,
              okButtonText: 'YES',
              cancelButtonText: 'NO',
              okBtnCallback: function () {
                    $window.location.href = that.downloadTXNhistory;
              }
            });
        };

        that.addCash = function() {
            var data = {
				balance: 0,
				entryFee: 0,
                title: 'ADD MONEY TO YOUR ACCOUNT',
                showCurrentBalance: false,
			};
			ngDialog.open({
				template: '/fantasy_sports/app/views/addcash.html',
				controller:"addCashCtrl",
				controllerAs: "addcash",
				overlay: true,
				closeByDocument: false,
				closeByEscape: false,
				closeByNavigation: true,
				disableAnimation: true,
				appendTo: '#mainWrapper',
				className: 'ngdialog-theme-popup addcash-popup desk-sm',
				showClose: true,
				resolve: {
			        leaguedata: function() {
			            return data;
			        }
		        }
			});

        };

        that.withdrawCash = function() {
            var data = {
				amt: that.winningAmt,
			};
            ngDialog.open({
				template: '/fantasy_sports/app/views/withdrawcash.html',
                controller:"WithdrawcashCtrl",
				controllerAs: "withdrawcash",
				overlay: true,
				closeByDocument: false,
				closeByEscape: false,
				closeByNavigation: true,
				disableAnimation: true,
				appendTo: '#mainWrapper',
				className: 'ngdialog-theme-popup addcash-popup desk-sm',
				showClose: true,
                resolve: {
			        winningAmt: function() {
			            return data;
			        }
		        }
			});

        };
  }]);
