'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:WithdrawcashCtrl
 * @description
 * # WithdrawcashCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('WithdrawcashCtrl',['$rootScope','config','msgConstants','utilityservice','serviceName','session','winningAmt','ngDialog','$window','$timeout', function ($rootScope, config, msgConstants, utilityservice, serviceName, session, winningAmt, ngDialog, $window,$timeout) {
        var that = this;
        that.popupTitle = msgConstants.HEADER_WITHDRAW_CASH;
        that.showWithdraw = true;
        that.amount = null;
        that.winningAmt = winningAmt.amt;
        $timeout(function(){
            angular.element('.amt').trigger('focus');
        },100);

        that.withdraw = function() {
            var isSuccess = true;
			if(Number(that.amount) < 200){
				that.amountError = msgConstants.ERR_WITHDRAW_1;
				isSuccess = false;
			}
			if(Number(that.amount) > 200000){
				that.amountError = msgConstants.ERR_WITHDRAW_2;
				isSuccess = false;
			}
            if(Number(that.amount) > Number(that.winningAmt)) {
                that.amountError = msgConstants.ERR_INSUFFICIENT_WINNING;
                isSuccess = false;
            }

			if(isSuccess){
                that.showWithdraw = false;
                that.popupTitle = msgConstants.HEADER_CONFIRMATION;
            }
        };

        that.cancelWithdraw = function() {
            that.showWithdraw = true;
            that.popupTitle = msgConstants.HEADER_WITHDRAW_CASH;
        };

        that.checkAlphabet = function(event,fieldName){
            var keyCode = event.keyCode;
            if(keyCode<48 || keyCode>57 ){
               that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
               //event.preventDefault();
            }
        };

        that.confirmWithdraw = function() {
            var params = {};
            params.userId = session.getUserID();
            params.txnType = "withdrawl";
            params.cancelRemarks = "www";
            params.amount = Number(that.amount);
            var title = "";
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.WITHDRAW, params, function (response) {
                ngDialog.close();
                if(response.respCode == 100) {
                    title = "CONGRATULATIONS";
                }else{
                    title = "FAILURE";
                }
                $rootScope.$broadcast("showPopup", {
                      type: "Info",
                      title: msgConstants.HEADER_INFO,
                      appendTo:'#mainWrapper',
                      msg: response.message,
                      popupSize: 'small',
                      okButtonText: 'OK',
                      okBtnCallback: function () {
                            if(title == "CONGRATULATIONS"){
                                  $window.location.reload();
                            }
                      }
                });
            });
        };
  }]);
