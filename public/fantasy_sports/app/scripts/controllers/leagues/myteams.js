'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:myTeamsCtrl
 * @description
 * # myTeamsCtrl as myteam
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('myTeamsCtrl', ['$rootScope','$window', 'leagueservice', 'utilityservice', 'leagueutilservice', 'session', 'msgConstants', function ($rootScope, $window, leagueservice, utilityservice, leagueutilservice, session, msgConstants) {
 	var that = this;

 	that.showPlayersPreview = false;
 	that.currentTeamId = null;
 	that.currentMatchId = null;
 	that.teamDisplayType = null;
 	var baseUrl = utilityservice.getBaseUrl();

 	that.init = function(initObj){
 		that.currentMatchId = initObj.matchId;
 		fetchMatchInfo(initObj.matchId);
 	}

 	function fetchMatchInfo(matchid){
 		var params = {};
 		params.matchId = parseInt(matchid);
 		leagueservice.getTourMatchInfo(params, function(response){
 			if(response.respCode === 100){
 				that.selectedMatch = response.respData;
 				setTeamDisplayType(that.selectedMatch.status);
 				that.fetchTeamInfo();
 			}
 			else{
 				$rootScope.$broadcast("showPopup", {
                    type: "Info",
                    title: msgConstants.HEADER_INFO,
                    appendTo: '#mainWrapper',
                    msg: response.message,
                    popupSize: 'small',
                    showClose: false,
                    okButtonText: 'Ok',
                });
 			}
 		});
 	}

 	that.fetchTeamInfo = function(teamId){
 		var params = {};
			params.matchId = that.currentMatchId;
			if(!!teamId){
				params.teamId = teamId;
				that.currentTeamId = teamId;
			}
			utilityservice.showLoader();

			leagueservice.getTeamInfo(params, function(responseData){
				that.selectedTeamInfo = {};
				if(!!teamId){
					that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.currentTeam.players);
					that.selectedTeamInfo.teamId = responseData.currentTeam.teamId;
					that.selectedTeamInfo.teamPoints = responseData.currentTeam.teamPoints;
					that.selectedTeamInfo.leagueCount = responseData.leagueCount;
					that.showTeamPreview(true);
				}

				that.myTeamsArray = responseData.teamsArr;
				that.selectedTeamInfo.teamName = responseData.screenName;
				utilityservice.hideLoader();
			});
		};

		that.getMatchStatus = function(status){
			var matchstatus = '';
			switch(status){
				case 'RUNNING':
					matchstatus = 'In Progress';
					break;
				case 'UNDER_REVIEW':
					matchstatus = 'Under Review';
					break;
				case 'CLOSED':
					matchstatus = 'Completed';
					break;
				case 'ABANDONED':
					matchstatus = 'Abandoned';
					break;
				default:
			}
			return matchstatus;
		};

		that.showTeamPreview = function(bool){
			$window.scrollTo(0,0);
			var body = document.querySelector('body');
			if(bool){
				body.classList.add('ovf-hdn');
			}
			else{
				body.classList.remove('ovf-hdn');	
			}
			that.showPlayersPreview = bool;
		};

		that.onEditTeamClick = function(){
			$window.location.href = baseUrl + "/editteam/" + that.selectedMatch.matchId + '/' + that.currentTeamId;
		};

		that.onShowLeaguesClick = function(){
			$window.location.href = baseUrl + "/match-leagues/" + that.selectedMatch.matchId; // + '/' + that.currentTeamId;
		};

		that.onAddTeamClick = function(){
			$window.location.href = baseUrl + "/createteam/" + that.selectedMatch.matchId + '/' + (that.selectedMatch.totalTeams + 1);
		};

		that.onScorecardClick = function(){
			$window.open(utilityservice.getBaseUrl() + "/fantasy-scorecard/" + that.selectedMatch.matchId, '_blank');
			//$window.location.href = that.baseUrl + "/fantasy-scorecard/" + that.selectedMatch.matchId;
		};

		that.onShowJoinedLeaguesClik = function(){
			$window.location.href = utilityservice.getBaseUrl() + '/my-joined-league/' + that.selectedMatch.matchId;
		};

		function setTeamDisplayType(status){
			that.teamDisplayType = '';
			switch(status){
				case 'ACTIVE':
					that.teamDisplayType = 'ACTIVE';
					break;
				case 'RUNNING':
				case 'UNDER_REVIEW':
				case 'CLOSED':
					that.teamDisplayType = 'CLOSED';
					break;
				default:
			}
		}

	}]);