'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:matchLeaguesCtrl
 * @description
 * # matchLeaguesCtrl as matchleagues
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('matchLeaguesCtrl', ['$rootScope', 'utilityservice', 'leagueservice', 'ngDialog', 'session', '$window', 'config', 'serviceName', 'leagueutilservice', function ($rootScope, utilityservice, leagueservice, ngDialog, session, $window, config, serviceName, leagueutilservice) {
	var that = this;

	that.userBalance = session.getUserBalance();
	that.baseUrl = utilityservice.getBaseUrl();
	that.selectedMatch = {};
	that.noLeaguesError = false;

	that.init = function(initObj){

		var matchId = initObj.matchId;

		checkAutoLeagueFlow(matchId, function(response){
			fetchMatchInfo(response.matchId);
			//that.fetchLeagues(response.matchId);
		});
	};

	that.fetchLeagues = function(matchid){
		var params = {};
		params.matchId = matchid;
		//utilityservice.showLoader();
		that.leaguesArray = [];
		leagueservice.getLeagues(params, function(response){
			that.noLeaguesError = true;
			if(response.respCode === 100){
				that.leaguesArray = response.respData;
			}
			else{
				console.log('Error in Fetching leagues', response.message);
			}
			//utilityservice.hideLoader();
		});
	};

	that.fetchLeaguePrizeInfo = function (league) {
		var params = {};
		params.leagueId = league.leagueId;
		leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
			leagueutilservice.prizeInfoPopup(prizeinfodata);
		});
	};

	that.onBrowseLeague = function(league){
		$window.location.href = that.baseUrl + '/browse-league/' + 12;
	};

	that.onJoinedLeagues = function(){
		if(that.selectedMatch.leagueCount === 0 || that.selectedMatch.leagueCount === undefined){
			// alert('You havent joined any leagues');
			utilityservice.showToastError({message: 'You havent joined any leagues'});
			return false;
		}
		$window.location.href = that.baseUrl + '/my-joined-league/' + that.selectedMatch.matchId;
	};

	that.onCreateTeamClick = function(){
		$window.location.href = that.baseUrl + '/createteam/' + that.selectedMatch.matchId + '/' + leagueutilservice.getActiveMatchTeamId();
	};

	that.onMyTeamsClick = function(){
		$window.location.href = that.baseUrl + '/my-teams/' + that.selectedMatch.matchId;
	};

	that.validateJoinLeague = function(league, event){
		return leagueutilservice.validateJoinLeague(league, that.selectedMatch.totalTeams);
	};

	that.getMatchStatus = function(status){
		return leagueutilservice.getMatchStatus(status);
	};

	function fetchMatchInfo(matchid){
		var params = {};
		params.matchId = parseInt(matchid);
		leagueservice.getTourMatchInfo(params, function(response){
			if(response.respCode === 100){
				that.selectedMatch = response.respData;
				that.fetchLeagues(matchid);
			}
			else{

			}
		});
	}

	function checkAutoLeagueFlow(matchId, callback){
		var invCode = utilityservice.getUrlParameter('invCode');

            if(invCode !== undefined && invCode !== ""){
                var league = {
                    leagueId: invCode
                };
                leagueutilservice.checkJoinLeague(league, function(checkLeagueResponse){
                	if(!!checkLeagueResponse && (checkLeagueResponse.matchId != matchId)){
		                matchId = checkLeagueResponse.matchId;
		            }
            		callback({matchId: matchId});
                });
            }
            else{
            	callback({matchId: matchId});
            }
	}

	$rootScope.$on('fetchLeaguesEvent', function(event, args){
        // Reload the page with new matchId to show its leagues
        // $window.location.href = that.baseUrl + '/match-leagues/' + args.matchId;
        var invCode = utilityservice.getUrlParameter('invCode');
        if(invCode !== undefined && invCode !== ""){
            $window.location.href = that.baseUrl + '/match-leagues/' + args.matchId;    // Removing query string after successful league join if invCode is present
        }
        else{
            that.init({matchId: args.matchId});
        }
    });

}]);
