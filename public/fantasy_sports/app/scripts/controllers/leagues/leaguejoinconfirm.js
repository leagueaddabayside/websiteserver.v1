'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:leagueJoinConfirmCtrl
 * @description
 * # leagueJoinConfirmCtrl as leaguejoin
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
	.controller('leagueJoinConfirmCtrl', ['$rootScope', '$scope', '$window', 'utilityservice', 'leaguedata', 'config', 'serviceName', 'msgConstants', function ($rootScope, $scope, $window, utilityservice, leaguedata, config, serviceName, msgConstants) {
		var that = this;

		that.leagueObj = leaguedata;
		that.selectedTeamId = that.leagueObj.league.teamArr[0].toString();
		that.isJoinBtnDisabled = false;

		var baseUrl = utilityservice.getBaseUrl();

		that.onJoinLeagueClick = function(){
			//console.log('team id :: ', that.selectedTeamId);
			var params = {
				leagueId: that.leagueObj.league.leagueId,
				matchId: that.leagueObj.league.matchId,
				teamId: parseInt(that.selectedTeamId)
			};
			that.isJoinBtnDisabled = true;
			utilityservice.showLoader();
			utilityservice._postAjaxCall(config.gameNodeHost + serviceName.JOIN_LEAGUE, params, function (response) {
				that.isJoinBtnDisabled = false;
				if (response.respCode == 100) {

					var successMsg = '';
					if(that.leagueObj.league.chipType === 'real'){
						successMsg = '<p class="fs18">Join more leagues to challenge other teams to win cash.</p>';
					}
					if(that.leagueObj.league.chipType === 'skill'){
						successMsg = '<p class="fs18">Now try one of our cash leagues to win real money.</p>';
					}
					// Update User Balance in header 
					if(response.respData.balance !== undefined){
						//response.respData.balance = 200;
						utilityservice.updateBalance(response.respData.balance);	
					}
					$rootScope.$broadcast("showPopup", {
						type: "Info",
						title: msgConstants.HEADER_INFO,
						appendTo: '#mainWrapper',
						msg: successMsg,
						popupSize: 'small',
						showClose: false,
						okButtonText: 'Ok',
						okBtnCallback: function () {
							// Updating leagues and league count after league join success
							var data = {
								matchId: that.leagueObj.league.matchId,
								leagueCount: response.respData.leagueCount,
								leagueId: that.leagueObj.league.leagueId
							};
							$rootScope.$broadcast('fetchLeaguesEvent', data);
						}
					});
				}
				else if(response.respCode === 124){
	                $rootScope.$broadcast("showPopup", {
	                    type: "Info",
	                    title: msgConstants.HEADER_INFO,
	                    appendTo: '#mainWrapper',
	                    msg: response.message,
	                    popupSize: 'small',
	                    showClose: false,
	                    okButtonText: 'Ok',
	                    okBtnCallback: function () {
	                        var data = {
	                            matchId: response.respData.matchId
	                        };
	                        $rootScope.$broadcast('fetchLeaguesEvent', data);
	                    }
	                });
	            }
				else{
					$rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo: '#mainWrapper',
                        msg: response.message,
                        popupSize: 'small',
                        showClose: true,
                        okButtonText: 'Ok',
                        okBtnCallback: function () {
                            //console.log('');
                        }
                    });
				}
				utilityservice.hideLoader();
			});
		};
 }]);
