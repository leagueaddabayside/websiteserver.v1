'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:joinedLeaguesCtrl
 * @description
 * # joinedLeaguesCtrl as joinedleague
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
	.controller('joinedLeaguesCtrl', ['$rootScope', '$window', 'utilityservice', 'leagueservice', 'session', 'leagueutilservice', function ($rootScope, $window, utilityservice, leagueservice, session, leagueutilservice) {
		var that = this;

		var baseUrl = utilityservice.getBaseUrl();
		that.selectedMatch = null;
		that.leagueDisplayType = null;
		that.showSwitchTeam = false;
		that.teamArr = null;
		that.joinedTeamArr = null;
		that.switchTeamLeagueId = null;
		that.joinedLeaguesArray = [];
		that.noLeaguesError = false;

		var winScrollTop = 0;

		that.init = function(initObj){
			fetchMatchInfo(initObj.matchId);
		};

		that.onBrowseLeague = function(league){
			$window.location.href = baseUrl + '/browse-league/' + that.selectedMatch.matchId + '/' + league.leagueId;
		};

		that.fetchLeaguePrizeInfo = function (league) {
			if(league.chipType === 'skill'){
				return false;
			}
			var params = {};
			params.leagueId = league.leagueId;
			leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
				leagueutilservice.prizeInfoPopup(prizeinfodata);
			});
		};

		that.validateJoinLeague = function(league, event){
			return leagueutilservice.validateJoinLeague(league, that.selectedMatch.totalTeams);
		};

		that.getMatchStatus = function(status){
			return leagueutilservice.getMatchStatus(status);
		};

		that.onSwitchTeamClick = function(league){
			winScrollTop = document.querySelector('body').scrollTop;
			window.scrollTo(0, 0);
			var body = document.querySelector('body');
			body.classList.add('ovf-hdn');

			that.selectedTeamId = league.teamId.toString();
			that.switchTeamLeagueId = league.leagueId;
			that.joinedTeamArr = league.teamArr;
			that.selectedTeamIdArr = [];
			for(var i = 0, len = that.joinedTeamArr.length; i < len; i++){
				var data = {
					teamId: that.joinedTeamArr[i].teamId.toString(),
					leagueJoinedId: that.joinedTeamArr[i].leagueJoinedId};
				that.selectedTeamIdArr.push(data);
			}
			//console.log(that.selectedTeamIdArr);
			that.showSwitchTeam = true;
		};

		that.closeSwitchTeam = function(){
			window.scrollTo(0, winScrollTop);
			var body = document.querySelector('body');
			body.classList.remove('ovf-hdn');
			that.showSwitchTeam = false;
		};

		that.switchMyTeam = function () {

			var receiveCount = 0;
			var callCount = 0;
			for(var i = 0, len = that.joinedTeamArr.length; i < len; i++){
				if(that.joinedTeamArr[i].teamId !== parseInt(that.selectedTeamIdArr[i].teamId)){
					var params = {
		                teamId: parseInt(that.selectedTeamIdArr[i].teamId),
		                leagueId: that.switchTeamLeagueId,
		                matchId: that.selectedMatch.matchId,
		                leagueJoinedId: that.selectedTeamIdArr[i].leagueJoinedId
		            }
		            utilityservice.showLoader();
		            callCount++;
		            leagueservice.updateUserJoinedTeam(params, function (response) {
		                that.showSwitchTeam = false;
		                receiveCount++;
		                if(callCount == receiveCount){
							$window.location.reload();
		                }
		            });
				}
			}
		 	// $window.location.reload();
        };

		function fetchMatchInfo(matchid){
			var params = {};
			params.matchId = parseInt(matchid);
			leagueservice.getTourMatchInfo(params, function(response){
				if(response.respCode === 100){
					that.selectedMatch = response.respData;
					fetchJoinedLeagues();
					setLeagueDisplayType(that.selectedMatch.status);
					var teamArr = [];
					for (var i = 1; i <= that.selectedMatch.totalTeams; i++) {
						teamArr.push({teamId: i});
					}
					that.teamArr = teamArr;
				}
				else{

				}
			});
		}

		function setLeagueDisplayType(status){
			that.leagueDisplayType = '';
			switch(status){
				case 'ACTIVE':
					that.leagueDisplayType = 'ACTIVE';
					break;
				case 'RUNNING':
				case 'CLOSED':
				case 'UNDER_REVIEW':
					that.leagueDisplayType = 'CLOSED';
					break;
				default:
			}

		}

		function fetchJoinedLeagues(){
			that.joinedLeaguesArray = [];
			var params = {
				matchId: that.selectedMatch.matchId
			};
			leagueservice.getJoinedLeagues(params, function(joinedLeaguesArray){
				that.noLeaguesError = true;
				that.joinedLeaguesArray = joinedLeaguesArray;
			});
		}

		$rootScope.$on('fetchLeaguesEvent', function(event, args){
	        that.init({matchId: args.matchId});
	    });
 }]);
