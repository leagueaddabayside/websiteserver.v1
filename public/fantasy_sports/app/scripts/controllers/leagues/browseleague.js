'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:browseLeagueCtrl
 * @description
 * # browseLeagueCtrl as browseleague
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 	.controller('browseLeagueCtrl', ['$rootScope', '$window', 'leagueservice', 'leagueutilservice', 'utilityservice', function ($rootScope, $window, leagueservice, leagueutilservice, utilityservice) {
		var that = this;

		that.memberDisplayType = '';
		that.showPlayersPreview = false;


		var leagueId = null;
		var winScrollTop = 0;

		var pageSize = 100;

        that.pagination = {
            currentPage: pageSize,
            pageSize: pageSize
        }

		that.init = function(initObj){
			leagueId = initObj.leagueId;
			fetchMatchInfo(initObj.matchId);
			window.scrollTo(0, 0);
		}

		that.getMatchStatus = function(status){
			return leagueutilservice.getMatchStatus(status);
		}

		that.fetchLeaguePrizeInfo = function (league) {
			if(league.chipType === 'skill'){
				return false;
			}
			var params = {};
			params.leagueId = league.leagueId;
			leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
				leagueutilservice.prizeInfoPopup(prizeinfodata);
			});
		}

		that.showTeamPreview = function(item, teamType){
			winScrollTop = document.querySelector('body').scrollTop;
			
			var body = document.querySelector('body');
			body.classList.add('ovf-hdn');
			
			if(teamType === 'MY_TEAM'){
				fetchTeamInfo(item);
			}
			else if(teamType === 'OPPONENT_TEAM'){
				fetchOpponentTeamInfo(item);
			}
		}

		that.closeTeamPreview = function(){
			window.scrollTo(0, winScrollTop);
			var body = document.querySelector('body');
			body.classList.remove('ovf-hdn');
			that.showPlayersPreview = false;
		}

		that.validateJoinLeague = function(league, event){
			return leagueutilservice.validateJoinLeague(league, that.selectedMatch.totalTeams);
		}

		that.onScorecardClick = function(){
			$window.open(utilityservice.getBaseUrl() + "/fantasy-scorecard/" + that.selectedMatch.matchId, '_blank');
			//$window.location.href = that.baseUrl + "/fantasy-scorecard/" + that.selectedMatch.matchId;
		}

		that.updatePagination = function(league, type){
            //var limit = that.pagination.pageSize;
            switch(type){
                case 'SHOW_MORE':
	                if(that.pagination.currentPage >= that.leagueMembersArray.totalTeamCount){
	                	return false;
	                }
                    that.pagination.currentPage = that.pagination.currentPage + that.pagination.pageSize;
                    break;
                case 'SHOW_TOP':
                	if(that.pagination.currentPage <= that.pagination.pageSize){
                		return false;
                	}
                    that.pagination.currentPage = that.pagination.pageSize;
                    break;
                case 'SHOW_PREVIOUS':
                	if(that.pagination.currentPage <= that.pagination.pageSize){
                		return false;
                	}
                    that.pagination.currentPage = that.pagination.currentPage - that.pagination.pageSize;
                    break;
            }
            
            var params = {
                matchId: that.selectedMatch.matchId,
                leagueId: league.leagueId,
                limit: that.pagination.currentPage,
                limitSize: pageSize
            };
            that.leagueMembersArray = [];
            utilityservice.showLoader();
            leagueservice.getLeagueMembers(params, function (leagueMembersArray) {
            	window.scrollTo(0, 0);
               that.leagueMembersArray = leagueMembersArray;
                utilityservice.hideLoader();
            });
        }

		function fetchTeamInfo(data){
 			var params = {};
 			params.matchId = data.matchId;
			params.teamId = data.teamId;
			
			utilityservice.showLoader();

			leagueservice.getTeamInfo(params, function(responseData){
				that.selectedTeamInfo = {};
				that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.currentTeam.players);
				
				that.selectedTeamInfo.teamId = responseData.currentTeam.teamId;
				that.selectedTeamInfo.teamName = responseData.screenName;
				that.selectedTeamInfo.teamPoints = responseData.currentTeam.teamPoints;
				window.scrollTo(0,0);
				that.showPlayersPreview = true;
				utilityservice.hideLoader();
				//that.myTeamsArray = responseData.teamsArr;
			});
		}

		function fetchOpponentTeamInfo(data) {
            var params = {};
            params.matchId = data.matchId;
            params.teamId = data.teamId;
            params.opponentId = data.userId;
            utilityservice.showLoader();
            leagueservice.getOpponentTeamInfo(params, function (responseData) {
                that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.opponentTeam.players);
                that.selectedTeamInfo = {};
                that.selectedTeamInfo.teamId = responseData.opponentTeam.teamId;
                that.selectedTeamInfo.teamName = responseData.screenName;
                that.selectedTeamInfo.teamPoints = responseData.opponentTeam.teamPoints;
                window.scrollTo(0,0);
				that.showPlayersPreview = true;
				utilityservice.hideLoader();
            });
        }

		function fetchLeagueInfo(leagueId){
			var params = {};
			params.leagueId = parseInt(leagueId);
			
			leagueservice.getLeagueInfo(params, function(response){
				
				if(response.respCode === 100){
					that.leagueInfo = response.respData;
					fetchLeagueMembers(leagueId);
				}
				else{

				}
			})
		}

		function fetchMatchInfo(matchId){
			var params = {};
			params.matchId = parseInt(matchId);
			leagueservice.getTourMatchInfo(params, function(response){
				if(response.respCode === 100){
					that.selectedMatch = response.respData;
					setmemberDisplayType(that.selectedMatch.status);
					fetchLeagueInfo(leagueId);
				}
				else{

				}
			})
		}

		function fetchLeagueMembers(leagueId) {
            var params = {
                matchId: that.selectedMatch.matchId,
                leagueId: leagueId,
                limit: pageSize,
                limitSize: pageSize
            };
            utilityservice.showLoader();
            that.leagueMembersArray = [];
            leagueservice.getLeagueMembers(params, function (leagueMembersArray) {
                that.leagueMembersArray = leagueMembersArray;
                utilityservice.hideLoader();
            });

        }

		function setmemberDisplayType(status){
			that.memberDisplayType = '';

			switch(status){
				case 'ACTIVE':
					that.memberDisplayType = 'ACTIVE';
					break;
				case 'RUNNING':
				case 'UNDER_REVIEW':
				case 'CLOSED':
					that.memberDisplayType = 'CLOSED';
					break;
				default:
			}

		}

		$rootScope.$on('fetchLeaguesEvent', function(event, args){
	        //console.log('got the event', args);
	        //$window.location.reload();
	        that.init({matchId: args.matchId, leagueId: args.leagueId});
	    });
	
 }]);
