'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:tourMatchesCtrl
 * @description
 * # tourMatchesCtrl as tourmatches
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
    .controller('tourMatchesCtrl', ['$scope', 'constants', 'leagueservice', 'utilityservice', 'leagueutilservice', '$timeout', '$rootScope', 'devicedetectionservice', 'ngDialog', 'session', 'createteamservice', '$window', 'promotionsservice', function ($scope, constants, leagueservice, utilityservice, leagueutilservice, $timeout, $rootScope, devicedetectionservice, ngDialog, session, createteamservice, $window, promotionsservice) {
        var that = this;

        that.baseUrl = utilityservice.getBaseUrl();

        that.mob = {
            activeMatches: [],
            runningMatches: [],
            closedMatches: []
        }

        that.matchDisplayType = 'LOBBY';
        that.screenName = session.getScreenName();
        that.userBalance = session.getUserBalance();
        that.selectedTour = null;
        that.showNoLeaguesError = false;
        that.selectedMatch = null;
        that.noMatchesError = false;

        // init function called when controller loads
        that.init = function(){
            fetchTours();


            if(session.getTeamName() === '' || session.getTeamName() === null || session.getTeamName() === undefined){
                leagueutilservice.myTeamNamePopoup();
            }


            // Promtions
            var params ={};
            promotionsservice.getPromotions(params, function(response){
                console.log("data from service",response);
                console.log(response.data);
                that.promotionsData = response;
            });  
        };

        that.onTourClick = function(){
            var tourWrapper = document.querySelector('.cstm-tour-select');
            if(tourWrapper.classList.contains('open')){
                tourWrapper.classList.remove('open');
            }
            else{
                tourWrapper.classList.add('open');
            }
        };

        that.onShowLeaguesClick = function(match){
            that.selectedMatch = match;
            $window.location.href = that.baseUrl + '/match-leagues/' + match.matchId;
        }

        that.onTourSelect = function(tour){
            that.selectedTour = tour;
            fetchMatches(tour.tourId);
            var tourWrapper = document.querySelector('.cstm-tour-select');
            tourWrapper.classList.remove('open');
        };

        that.onMatchTypeClick = function(matchtype, event){

            switch(matchtype){
                case 'ACTIVE':
                    that.matchDisplayType = 'LOBBY';
                    break;
                case 'RUNNING':
                    that.matchDisplayType = 'ACTIVE';
                    break;
                case 'CLOSED':
                    that.matchDisplayType = 'RESULTS';
                    break;
            }
        };

        that.onJoinedLeagues = function(match){
            console.log('onBrowseLeague ::::', match);
            // if(match.leagueCount === 0){
            // 	alert('no leagues joined');
            // 	return;
            // }
            $window.location.href = that.baseUrl + '/my-joined-league/' + match.matchId;
        };

        function fetchTours(){
            var params = {};
            leagueservice.getTours(params, function(toursArray){
                var alltour = {shortName: 'All', status: 'ACTIVE', tourId: -1, tourName: 'All Series'};
                toursArray.unshift(alltour);
                that.toursArray = toursArray;
                that.selectedTour = that.toursArray[0];
                fetchMatches(-1);
            });
        }

        // Private Functions
        function fetchMatches(tourid){
            var params = {};
            if(tourid !== -1){
                params.tourId = tourid;
            }
            utilityservice.showLoader();
            leagueservice.getMatches(params, function(matchesArray,nextTimeTour){
                that.matchesArray = matchesArray;
                that.noMatchesError = true;
                filterMatchByStatus(that.matchesArray);
                utilityservice.hideLoader();
            });
        }

        that.onCreateTeamClick = function(match){
            $window.location.href = that.baseUrl + "/createteam/" + match.matchId + '/' + leagueutilservice.getActiveMatchTeamId();
        };

        that.onMyTeamClick = function(match){
            if(match.totalTeams === 0){
                utilityservice.showToastError({title: 'You missed it!', message: 'You have not created a team for this match'});
                return false;
            }
            $window.location.href = that.baseUrl + "/my-teams/" + match.matchId;
        };

        function filterMatchByStatus(matcharray){
            //console.log('filterMatchByStatus');
            that.mob.activeMatches = [];
            that.mob.runningMatches = [];
            that.mob.closedMatches = [];

            var arrlen = matcharray.length;
            for(var i = 0; i < arrlen; i++){
                switch(matcharray[i].status.toUpperCase()){
                    case 'ACTIVE':
                    case 'LOCKED':
                        that.mob.activeMatches.push(matcharray[i]);
                        break;
                    case 'RUNNING': 
                    case 'UNDER_REVIEW':
                        that.mob.runningMatches.push(matcharray[i]);
                        break;
                    case 'CLOSED':
                    case 'ABANDONED':
                        that.mob.closedMatches.push(matcharray[i]);
                        break;
                    default:
                }
            }
            // Reversing matches array by decreasing order time
            that.mob.runningMatches = that.mob.runningMatches.reverse();
            that.mob.closedMatches = that.mob.closedMatches.reverse();
        }

        // function getActiveMatch(matcharray){
        // 	var activematch = matcharray[0];
        // 	var arrlen = matcharray.length;
        // 	for(var i = 0; i< arrlen; i++){
        // 		if(matcharray[i].status === 'ACTIVE'){
        // 			that.selectedMatch = matcharray[i];
        // 			activematch = matcharray[i];
        // 			break;
        // 		}
        // 	}
        // 	return activematch;
        // }
        
        
        that.showPromtionDetails  = function(e, index){
            var promotionImageUrl = that.promotionsData[index].fields.popup_banner_mobile.url;
            var imgDom = document.createElement("img");
            imgDom.src = promotionImageUrl;
            imgDom.className = "w100"; 
            var imgWrapper = document.createElement("div");
            imgDom.appendChild(imgWrapper);
            var tmp = document.createElement("div");
            tmp.appendChild(imgDom);

            that.promoDetailsData = {};
            that.promoDetailsData.headerName = that.promotionsData[index].name;
            that.promoDetailsData.content = tmp.innerHTML+that.promotionsData[index].fields.popup_content.text;
            that.promoDetailsData.pageCss = that.promotionsData[index].fields.popup_content_css.text;
            that.promoDetails = true;
            $timeout(function(){
                that.iframeLoadedCallBack();
            },200);
        };

        that.hidePromtionDetails = function(){
            that.promoDetails = false;
        };

        that.iframeLoadedCallBack = function () {
            var cssLink = that.promoDetailsData.pageCss;
            var pageData = '';
            pageData = that.promoDetailsData.content;
            //console.log(document.querySelector('.promo-details-popup'));
            var iFrame = angular.element(document.querySelector('iframe.promo-details-popup'))[0];

            var iFrameBody;
            if (iFrame.contentWindow.document){ // FF
                iFrameBody = angular.element(iFrame.contentDocument.getElementsByTagName('body')[0]);
            } else if (iFrame.contentWindow.document){ // IE
                iFrameBody = angular.element(iFrame.contentWindow.document.getElementsByTagName('body')[0]);
            }

            var iFrameHead;
            if (iFrame.contentWindow.document){ // FF
                iFrameHead = angular.element(iFrame.contentDocument.getElementsByTagName('head')[0]);
            } else if (iFrame.contentWindow.document){ // IE
                iFrameHead = angular.element(iFrame.contentWindow.document.getElementsByTagName('head')[0]);
            }
            console.log(cssLink.toString());
            iFrameHead.append(cssLink);
            iFrameBody.append(pageData);
        };

    }]);
