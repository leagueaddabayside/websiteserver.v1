'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:PopupCtrl
 * @description
 * # PopupCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
 .controller('PopupCtrl',['$scope','$rootScope','ngDialog', function ($scope, $rootScope, ngDialog) {
    var that = this;

    that.showCancelBtn = false;
    that.ngDialogId = $scope.ngDialogData.id || $scope.ngDialogId;
    that.type = $scope.ngDialogData.type;
    that.msg = ($scope.ngDialogData.msg !== undefined && $scope.ngDialogData.msg !== null) ? $scope.ngDialogData.msg : "";
    that.title = ($scope.ngDialogData.title !== undefined && $scope.ngDialogData.title !== null) ? $scope.ngDialogData.title : "";
    that.showOkBtn = false;
    that.popupSize = 'medium';
    that.okButtonText = ($scope.ngDialogData.okButtonText !== undefined && $scope.ngDialogData.okButtonText !== null) ?  $scope.ngDialogData.okButtonText : "OK";
    that.closeOnOkBtnClick = ($scope.ngDialogData.closeOnOkBtnClick !== undefined && $scope.ngDialogData.closeOnOkBtnClick !== null) ? $scope.ngDialogData.closeOnOkBtnClick : true;
    that.showCrossBtn = ($scope.ngDialogData.showCrossBtn !== undefined && $scope.ngDialogData.showCrossBtn !== null) ? $scope.ngDialogData.showCrossBtn : true;
    that.cancelButtonText = ($scope.ngDialogData.cancelButtonText !== undefined && $scope.ngDialogData.cancelButtonText !== null) ? $scope.ngDialogData.cancelButtonText : "CANCEL";
    that.closeOnCancelBtnClick = ($scope.ngDialogData.closeOnCancelBtnClick !== undefined && $scope.ngDialogData.closeOnCancelBtnClick !== null) ? $scope.ngDialogData.closeOnCancelBtnClick : true;

    if((that.type.toUpperCase() === "ALERT") || (that.type.toUpperCase() === "INFO")){
        that.title = that.title || that.type;
        that.showOkBtn = true;
        that.showCancelBtn = false;
    } else if(that.type.toUpperCase() === "CONFIRM"){
        that.showOkBtn = true;
        that.showCancelBtn = true;
    }
    else if(that.type.toUpperCase() === "RECONNECTION"){
         that.showOkBtn = false;
    }

    that.onOkClick = function(e){
        e.stopPropagation();
        if($scope.ngDialogData.okBtnCallback){
            $scope.ngDialogData.okBtnCallback();
        }
        if(that.closeOnOkBtnClick){
            ngDialog.close(that.ngDialogId, 0);
        }
    };

    that.onCancelClick = function(e){
        e.stopPropagation();
        if($scope.ngDialogData.cancelBtnCallback){
            $scope.ngDialogData.cancelBtnCallback();
        }
        if(that.closeOnCancelBtnClick){
            ngDialog.close(that.ngDialogId, 0);
        }
    };

    that.closePopup = function(e){
        e.stopPropagation();
        if((that.type.toUpperCase() === "ALERT") || (that.type.toUpperCase() === "INFO")){
            if($scope.ngDialogData.okBtnCallback){
                $scope.ngDialogData.okBtnCallback();
            }
        } else{
            if($scope.ngDialogData.cancelBtnCallback){
                $scope.ngDialogData.cancelBtnCallback();
            }
        }
        ngDialog.close(that.ngDialogId, 0);
    };
}]);
