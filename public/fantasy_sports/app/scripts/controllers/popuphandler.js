'use strict';
/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:PopuphandlerCtrl
 * @description
 * # PopuphandlerCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
.controller('PopuphandlerCtrl',['$scope','$rootScope','ngDialog','$timeout','devicedetectionservice', function ($scope, $rootScope, ngDialog, $timeout, devicedetectionservice) {
    var that = this;

    that.showPopup = function(data){
        $rootScope.isAnyPopupOpen = true;
        var ngDialogClass = '';
        ngDialog.close();
        switch(data.popupSize){
            case 'large':
                ngDialogClass = 'ngdialog-theme-popup large-popup';
                break;
            default: 
                ngDialogClass = 'ngdialog-theme-popup';
        }

        var isDesktop = devicedetectionservice.isDevice('DESKTOP');
        if(isDesktop)
            ngDialogClass += ' desk-popup-size';

        ngDialog.open({
            preCloseCallback: function(){
                if(data.preCloseCallback){
                    data.preCloseCallback();
                }
            },
            template:data.template,
            controller:"PopupCtrl",
            controllerAs: "popup",
            data: data,
            id: data.id,
            scope : $scope,
            overlay: true,
            closeByDocument: false,
            closeByEscape: false,
            closeByNavigation: true,
            disableAnimation: true,
            appendTo: data.appendTo,
            className: ngDialogClass,
            showClose: data.showClose,
            name: data.id
        }).closePromise.then(function() {
            $rootScope.isAnyPopupOpen = false;
            if(data.closePromise){
                data.closePromise();
            }
        });
    };

    that.closePopup = function (id) {
        if (id === undefined) {
            id = '';
        }
        ngDialog.close(id);
    };

    $scope.$on("showPopup", function(event, data){
        if(data.template === undefined || data.template === null){
            data.template = "/fantasy_sports/app/views/popup.html";
        }
        if(data.appendTo === undefined || data.appendTo === null){
            data.appendTo = false;
        }
        if(data.id === undefined || data.id === null){
            data.id = "";
        }
        if(data.showClose === undefined || data.showClose === null){
            data.showClose = false;
        }
        that.showPopup(data);
    });

    $scope.$on('closePopup', function (event, data) {
        that.closePopup(data.id);
    });


    that.showHideLoader = function(data){
        var loader = document.querySelector('#loaderDom');
        if(loader !== undefined && loader !== null){
            if(data.delay){
                $timeout(function(){
                    loader.style.display = data.display;
                },data.delay);
            }else{
                loader.style.display = data.display;
            }
        }else{
            console.log('could not detect loader DOM');
        }
    };

    $scope.$on('showhideLoader', function (event, data) {
        that.showHideLoader(data);
    });

}]);