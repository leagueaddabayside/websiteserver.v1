'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:forgotPasswordCtrl
 * @description
 * # forgotPasswordCtrl
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('forgotPasswordCtrl', ['$rootScope', 'msgConstants', '$interval', 'utilityservice', 'config', 'ngDialog', 'serviceName', 'user', function ($rootScope, msgConstants, $interval, utilityservice, config, ngDialog, serviceName, user) {

 	var that = this;
 	that.isPhase = "PRE_PHASE";
 	that.popupTitle = "Forgot Password";
 	that.isResendDisabled = false;
 	that.userDataType = '';
 	that.userId = '';
 	that.userEmailId = '';
	that.userMobile = '';
 	that.otpChannel = '';

 	that.userInput = '';
 	that.otpInput = '';
 	that.userPassword = '';
 	that.errmsg_userInput = '';
 	that.errmsg_otpInput = '';
 	that.userPasswordMsg = '';

 	that.otpTimer = '';

 	that.checkAlphabet = function(event,fieldName){
		that.userMobileMsg='';
		var keyCode = event.keyCode;
		if(keyCode<48 || keyCode>57 ){
		   that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
		   //event.preventDefault();
		}
    };

	that.validatePrephase = function(){
		var isSuccess = true;
		if(that.userInput == ''){
			that.errmsg_userInput = msgConstants.REQUIRED_MESSAGE;
			isSuccess = false
		}

		if(isSuccess){
			var params = {
				userData: that.userInput
			};
			utilityservice._postAjaxCall(config.websiteNodeHost+ serviceName.FORGOT_PASSWORD_INIT, params, function (response) {
			 	if(response.respCode == 100){
			 		that.userId = response.responseData.userId;
			 		that.userEmailId = response.responseData.emailId;
					that.userMobile = response.responseData.mobileNo;
			 		that.setPlaceholder(response.responseData.userDataType);
			 		that.isPhase = "POST_PHASE";
			 	}else{
			 		that.errmsg_userInput = response.message;
			 	}
			});
		}
	}

	that.validatePostphase = function(){
		var isSuccess = true;
		if(that.otpInput.length !== 6){
			isSuccess = false;
			that.errmsg_otpInput = msgConstants.ERR_OTP;
		}
		user.OnInputFieldValidation('userPassword', that.userPassword,'', function(response) {
			if(response.resCode !== 100){
				isSuccess = false;
				that.userPasswordMsg = response.message;
			}
		});

		if(isSuccess){
			var md5Password  = CryptoJS.MD5(that.userPassword) + "";
			var params = {
				userId: that.userId,
				otp: that.otpInput,
				password: md5Password
			};
			utilityservice._postAjaxCall(config.websiteNodeHost+ serviceName.FORGOT_PASSWORD, params, function (response) {
			 	if(response.respCode == 100){
					ngDialog.close();
			 		$rootScope.$broadcast("showPopup", {
			 			type: "INFO",
			 			title: msgConstants.HEADER_INFO,
			 			appendTo:'#mainWrapper',
			 			msg: msgConstants.PASSWORD_CHANGE_SUCCESSFULLY,
			 			okButtonText: 'OK',
			 			okBtnCallback: function () {
			 			}
			 		});
			 	}else{
			 		that.errmsg_otpInput = response.message;
			 	}
			 });
		}
	};

	that.resendOtp = function(){
		that.isResendDisabled = true;
		var params = {
			userData: that.userInput
		};
		utilityservice._postAjaxCall(config.websiteNodeHost+ serviceName.FORGOT_PASSWORD_INIT, params, function (response) {
			if(response.responseCode == 100){
				//console.log('OTP resend successfully');
			}else{
				console.log("server error : ",response.message);
			}
		});
		setTimeout(function(){
			that.isResendDisabled = false;
		}, 5000);
	};

	that.setPlaceholder = function(type){
		switch(type){
			case 'userName':
			that.userDataType = 'Username';
			that.otpChannel = that.userEmailId;
			break;
			case 'mobileNo':
			that.userDataType = 'Mobile No.';
			that.otpChannel = that.userMobile;
			break;
			case 'emailId':
			that.userDataType = 'Email ID';
			that.otpChannel = that.userEmailId;
			break;
			default: 
			that.userDataType = "";
		}
	};

	that.resetForgotPasswordForm = function(){
		that.isPhase = "PRE_PHASE";
		that.popupTitle = "Forgot Password";
		that.userInput = '';
		that.otpInput = '';
		that.userPassword = '';
		that.errmsg_userInput = '';
		that.errmsg_otpInput = '';
		that.userPasswordMsg = '';
	};

}]);
