'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:creatTeamCtrl
 * @description
 * # creatTeamCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
    .controller('createTeamCtrl', ['$window','$rootScope','$timeout', 'constants','msgConstants',  'createteamservice', 'utilityservice','devicedetectionservice','session','ngDialog', function ($window, $rootScope, $timeout, constants,msgConstants, createteamservice , utilityservice, devicedetectionservice, session, ngDialog) {
        var that = this;
        that.screenName = session.getScreenName();
        that.matchPlayersArray =[];
        that.allRounderArray =[];
        that.bowlerArray = [];
        that.batterArray = [];
        that.wkArray = [];
        that.bonusPlayersArray =[];
        that.selectedSquadArray = [];
        that.selectedCaptain = {};
        that.selectedViceCaptain = {};
        
        that.captainSelected = false;
        that.viceCaptainSelected = false;

        that.selectedWkArray = [];
        that.selectedBowlerArray = [];
        that.selectedBatterArray = [];
        that.selectedAllRounderArray = [];
        that.selectedBonusPlayerArray = [];

        var selectedPlayerCount = 0;
        var selectedAllRounderCount = 0;
        var selectedBatterCount = 0;
        var selectedBowlerCount = 0;
        var selectedWkCount = 0;
        var selectedBonusPlayerCount = 0;

        that.selectedSquadCredits = 0;
        that.selectionErr ="";
        that.teamComplete = false;

        that.matchId = '';
        that.teamId = '';
        that.isProcessing = false;


        that.mobileUI = {
            showTeamPreview : false,
            showPlayerList : true
        };

        that.showSelErr = false;

        var minPlayers = {
            wk: 1,
            batter : 3,
            bowler : 3,
            alrdr : 1,
            bonus : 0,
            total: 8

        };

        var maxPlayers = {
            wk: 1,
            batter : 5,
            bowler : 5,
            alrdr : 3,
            bonus : 1,
            total: 15

        };

        var selectedPlayers = {
            wk: 0,
            batter: 0,
            bowler: 0,
            alrdr:0,
            bonus: 0,
            total: 0
        };

        var teamPlyrCount = {
            team1: 0,
            team1Name: '',
            team2: 0,
            team2Name: ''
        };

        that.matchInfo ={};
        that.autoTeamAdd = false;


        // init function called when controller loads
        that.init = function(obj){
            // obj used to get match id
            //console.log('create team initialized:::',obj);
            that.matchId = obj.matchId;
            that.teamId = obj.teamId;
            that.isNewTeam = obj.isNewTeam;
            fetchMatchPlayers(obj.matchId);

        };


        function fetchMatchPlayers(matchId){
            var params = {};
            params.matchId = matchId;
            utilityservice.showLoader();
            createteamservice.getMatchPlayers(params, function(response){

                if(response.respCode === 100){
                    that.setMatchInfo(response.respData.matchInfo);
                    that.matchPlayersArray = response.respData.playersArr;
                    if(that.isNewTeam == false){
                        that.autoTeamAdd = true;
                        fetchMySavedPlayers(response.respData.playersArr);
                    }

                    else{
                        filterPlayersByRole(response.respData.playersArr);
                    }

                    utilityservice.hideLoader();
                }
            });
        }

        that.setMatchInfo = function(matchInfo){
            that.matchInfo = matchInfo;
            teamPlyrCount.team1Name = (matchInfo.teams.a.key).toUpperCase();
            teamPlyrCount.team2Name = (matchInfo.teams.b.key).toUpperCase();
        };

        function fetchMySavedPlayers(players){

            var params = {};
            params.matchId = that.matchId;
            params.teamId = that.teamId;
            params.token = session.getWebsiteToken();
            //console.log("get saved players", params);
            utilityservice.showLoader();

            createteamservice.getMySavedPlayers(params, function(response){

                if(response.respCode === 100){
                    //console.log("saved players :: ",response.respData);
                    that.matchPlayersArray = response.respData.playersArr;

                    var players = response.respData.playersArr;
                    var len = players.length;
                    for(var i=0; i<len ;i++){
                        if(players[i].isSelected){
                            //console.log("selected before",players[i].isCaptain);
                            if(players[i].isCaptain){
                                that.selectedCaptain = players[i];
                                that.captainSelected = true;
                            }
                            if(players[i].isViceCaptain){
                                that.selectedViceCaptain = players[i];
                                that.viceCaptainSelected = true;
                            }
                            that.addPlayer(players[i],true);
                        }
                    }
                    //if(that.matchId !== undefined || that.matchId !== ""){
                    filterPlayersByRole(response.respData.playersArr);
                    //}
                }

                utilityservice.hideLoader();
            });

        }


        that.getRole = function(player){

            if((player.batter === true) && (player.bowler === true)){
                return "alrdr-icon";
            }

            if((player.batter === true) && (player.bowler === false)){
                return "bt-icon";
            }

            if((player.batter === false) && (player.bowler === true)){
                return "bw-icon";
            }

            if((player.wicketKeeper === true)){
                return "wk-icon";
            } 

        };


        that.getPlayerRole = function(player){

            if((player.batter === true) && (player.bowler === true)){
                return "alrdr";
            }

            if((player.batter === true) && (player.bowler === false)){
                return "batter";
            }

            if((player.batter === false) && (player.bowler === true)){
                return "bowler";
            }

            if((player.wicketKeeper === true)){
                return "wk";
            } 

        };

        function filterPlayersByRole(players){
            //console.log(players);

            var length = players.length;

            for(var i=0;i<length;i++){

                if((players[i].batter === true) && (players[i].bowler === true)){
                    that.allRounderArray.push(players[i]);
                }

                if((players[i].batter === true) && (players[i].bowler === false)){
                    that.batterArray.push(players[i]);
                }

                if((players[i].batter === false) && (players[i].bowler === true)){
                    that.bowlerArray.push(players[i]);
                }

                if((players[i].wicketKeeper === true)){
                    that.wkArray.push(players[i]);
                    //console.log(players[i].isSelected);
                } 

            }
        }


        that.addPlayer = function(player, savedTeam){
            //console.log(that.selectedSquadArray);
            // clearing prev error message
            that.showSelErr = false;
            that.selectionErr = "";
            //console.log("processing :::: ",that.isProcessing);

            if(that.isProcessing == false){
                that.isProcessing = true;

                if(player.isSelected === false || savedTeam == true){

                    if(that.selectedSquadArray.length <11) {

                        updateTeamCount(player,"add");

                        if((teamPlyrCount.team1 >= 8) || (teamPlyrCount.team2 >= 8)){

                            if(teamPlyrCount.team1 >= 8){
                                updateTeamCount(player,"sub");
                            }

                            if(teamPlyrCount.team2 >= 8){
                                updateTeamCount(player,"sub");
                            }
                            //console.log(teamPlyrCount);
                            that.isProcessing = false;
                            that.showSelErr = true;
                            that.selectionErr = msgConstants.ERR_SEL_MAX_PLYR_SAME_TEAM;
                            if(devicedetectionservice.isDevice("MOBILE")){
                                showToast(that.selectionErr);
                            }
                            return false;
                        }   

                        that.selectedSquadCredits = that.selectedSquadCredits + player.playerCredit;

                        if(that.selectedSquadCredits <= 100){

                            if((player.batter === true) && (player.bowler === true)){
                                // check alrdr is grtr than 0 that we are left with min alrdrs we can select
                                if(minPlayers.alrdr > 0){
                                    minPlayers.alrdr--;
                                    minPlayers.total--;
                                    addPlayerInSquad(player,"alrdr");
                                    //console.log("after adding alrdr and less than one :: ",that.selectedSquadCredits);
                                }

                                else{
                                    if(minPlayers.total + selectedPlayers.total < 11){
                                        if(selectedPlayers.alrdr < maxPlayers.alrdr ){
                                            addPlayerInSquad(player,"alrdr");
                                            //console.log("after adding alrdr and greater than one :: ",that.selectedSquadCredits);
                                        }
                                        else{  
                                            if(selectedPlayers.bonus < maxPlayers.bonus){
                                                //checkMinPlayersCreteria(player);
                                                player.isBonus = true;
                                                addPlayerInSquad(player,"alrdr");
                                            }
                                            else{
                                                that.isProcessing = false;
                                                that.showSelErr = true;
                                                that.selectionErr = msgConstants.ERR_SEL_MAX_ALRDR;
                                                if(devicedetectionservice.isDevice("MOBILE")){
                                                    showToast(that.selectionErr);
                                                }
                                                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                                                updateTeamCount(player,"sub");
                                                return false;
                                            }

                                        }
                                    }
                                    else{
                                        updateTeamCount(player,"sub");
                                        checkMinPlayersCreteria(player);
                                    }

                                }

                            }

                            if((player.batter === true) && (player.bowler === false)){
                                //console.log("batter");
                                if(minPlayers.batter > 0){
                                    minPlayers.batter--;
                                    minPlayers.total--;
                                    addPlayerInSquad(player,"batter");
                                    //console.log("after adding batter and less than three :: ",that.selectedSquadCredits);
                                }

                                else{
                                    if(minPlayers.total + selectedPlayers.total < 11){
                                        if(selectedPlayers.batter < maxPlayers.batter ){
                                            addPlayerInSquad(player,"batter");
                                            //console.log("after adding batter and greater than three :: ",that.selectedSquadCredits);

                                        }
                                        else{  

                                            // add 6th as bonus player
                                            //console.log("adding 6 batter");

                                            if(selectedPlayers.bonus < maxPlayers.bonus){
                                                //checkMinPlayersCreteria(player);
                                                player.isBonus = true;
                                                addPlayerInSquad(player,"batter");

                                            }
                                            else{
                                                that.isProcessing = false;
                                                that.showSelErr = true;
                                                that.selectionErr = msgConstants.ERR_SEL_MAX_BATTER;
                                                if(devicedetectionservice.isDevice("MOBILE")){
                                                    showToast(that.selectionErr);
                                                }
                                                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                                                //console.log(teamPlyrCount);
                                                updateTeamCount(player,"sub");
                                                //console.log(teamPlyrCount);
                                                return false;
                                            }
                                        }
                                    }
                                    else{
                                        updateTeamCount(player,"sub");
                                        checkMinPlayersCreteria(player);
                                    }
                                }
                            }


                            if((player.batter === false) && (player.bowler === true)){
                                //console.log("bowler");
                                if(minPlayers.bowler > 0){
                                    minPlayers.bowler--;
                                    minPlayers.total--;
                                    addPlayerInSquad(player,"bowler");
                                    //console.log("after adding bowler and less than three :: ",that.selectedSquadCredits);
                                }

                                else{
                                    if(minPlayers.total + selectedPlayers.total < 11){
                                        if(selectedPlayers.bowler < maxPlayers.bowler ){


                                            addPlayerInSquad(player,"bowler");
                                            //console.log("after adding bowler and greater than three :: ",that.selectedSquadCredits);

                                        }
                                        else{  
                                            if(selectedPlayers.bonus < maxPlayers.bonus){
                                                //checkMinPlayersCreteria(player);
                                                player.isBonus = true;
                                                addPlayerInSquad(player,"bowler");

                                            }
                                            else{
                                                that.isProcessing = false;
                                                that.showSelErr = true;
                                                that.selectionErr = msgConstants.ERR_SEL_MAX_BOWLER;
                                                if(devicedetectionservice.isDevice("MOBILE")){
                                                    showToast(that.selectionErr);
                                                }
                                                updateTeamCount(player,"sub");
                                                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                                                return false;
                                            }

                                        }
                                    }
                                    else{
                                        updateTeamCount(player,"sub");
                                        checkMinPlayersCreteria(player);
                                    }

                                }
                            }

                            if(player.wicketKeeper === true){
                                //console.log("WK");
                                if(minPlayers.wk > 0){
                                    minPlayers.wk--;
                                    minPlayers.total--;
                                    addPlayerInSquad(player,"wk");
                                    //console.log("after adding wk and less than one :: ",that.selectedSquadCredits);
                                }

                                else{
                                    if(minPlayers.total + selectedPlayers.total < 11){
                                        if(selectedPlayers.wk < maxPlayers.wk ){

                                            addPlayerInSquad(player,"wk");
                                            //console.log("after adding wk and greater than one :: ",that.selectedSquadCredits);

                                        }
                                        else{  
                                            if(selectedPlayers.bonus < maxPlayers.bonus){
                                                // checkMinPlayersCreteria(player);
                                                player.isBonus = true;
                                                addPlayerInSquad(player,"wk");

                                            }
                                            else{
                                                that.isProcessing = false;
                                                that.showSelErr = true;
                                                that.selectionErr = msgConstants.ERR_SEL_MAX_WK;
                                                if(devicedetectionservice.isDevice("MOBILE")){
                                                    showToast(that.selectionErr);
                                                }
                                                updateTeamCount(player,"sub");
                                                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                                                return false;
                                            }

                                        }
                                    }
                                    else{
                                        updateTeamCount(player,"sub");
                                        checkMinPlayersCreteria(player);
                                    }

                                }
                            }
                        }

                        else{
                            // if(that.selectedSquadArray.length <11){
                            that.isProcessing = false;
                            that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                            updateTeamCount(player,"sub");
                            that.showSelErr = true;
                            that.selectionErr = msgConstants.ERR_SEL_MAX_CREDITS;
                            if(devicedetectionservice.isDevice("MOBILE")){
                                showToast(that.selectionErr);
                            }
                            //}
                            //else{
                            //alert("Team complete, now select captain and vice-captain");
                            // that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                            //}

                        }
                    }

                    else{
                        that.isProcessing = false;
                        that.showSelErr = true;
                        that.selectionErr = msgConstants.ERR_SEL_TEAM_PLAYER_EXCEED;

                        if(devicedetectionservice.isDevice("MOBILE")){
                            showToast(that.selectionErr);
                        }

                        //alert("Team complete, now select captain and vice-captain");
                        //that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                    }
                }
                else{
                    //console.log("already selected ");
                    deletePlayerInSquad(player);

                    //that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;

                }
            }
            else {
                return false;
            }
        };



        function addPlayerInSquad(player, playerRole){

            if(playerRole === "alrdr"){
                if(player.isBonus === false){
                    selectedPlayers.alrdr++;
                    that.selectedAllRounderArray.push(player);
                }

                else{
                    selectedPlayers.bonus++;
                    that.selectedBonusPlayerArray.push(player);
                }

                selectedPlayers.total++;
                player.isSelected = true;
                that.selectedSquadArray.push(player);
                // var count = selectedPlayers.alrdr + selectedPlayers.bonus;
                addPlayerUI(player,playerRole);
            }

            if(playerRole === "bowler"){
                if(player.isBonus === false){
                    selectedPlayers.bowler++;
                    that.selectedBowlerArray.push(player);
                }

                else{
                    selectedPlayers.bonus++;
                    that.selectedBonusPlayerArray.push(player);
                }

                selectedPlayers.total++;
                player.isSelected = true;
                that.selectedSquadArray.push(player);
                //var count = selectedPlayers.bowler + selectedPlayers.bonus;
                addPlayerUI(player,playerRole);

            }

            if(playerRole === "batter"){ 

                if(player.isBonus === false){
                    selectedPlayers.batter++;
                    that.selectedBatterArray.push(player);
                }

                else{
                    selectedPlayers.bonus++;
                    that.selectedBonusPlayerArray.push(player);
                }

                selectedPlayers.total++;
                player.isSelected = true;
                that.selectedSquadArray.push(player);
                //var count = selectedPlayers.batter + selectedPlayers.bonus;
                addPlayerUI(player,playerRole);

            }

            if(playerRole === "wk"){
                if(player.isBonus === false){
                    selectedPlayers.wk++;
                    that.selectedWkArray.push(player);
                }

                else{
                    selectedPlayers.bonus++;
                    that.selectedBonusPlayerArray.push(player);
                }

                selectedPlayers.total++;
                player.isSelected = true;
                that.selectedSquadArray.push(player);
                //var count = selectedPlayers.wk + selectedPlayers.bonus;
                addPlayerUI(player,playerRole);

            }

        }


        function deletePlayerInSquad(player){
            //console.log("deleting player");
            updateTeamCount(player,"sub");
            that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;

            if((player.batter === true) && (player.bowler === true)){    
                deletePlayerUI(player,"alrdr");
                player.isSelected = false;

                if(minPlayers.alrdr >=0 && selectedPlayers.alrdr <= 1){
                    if(player.isBonus == false){
                        minPlayers.alrdr++;
                        minPlayers.total++;
                    }
                }

                if(player.isBonus === false){
                    selectedPlayers.alrdr--;

                    //  that.selectedAllRounderArray.pop(player);
                    deletePlyerFrmArray(that.selectedAllRounderArray,player);
                }
                else{
                    selectedPlayers.bonus--;
                    player.isBonus = false;
                    // that.selectedBonusPlayerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBonusPlayerArray,player);
                }

                //that.selectedSquadArray.pop(player);
                deletePlyerFrmArray(that.selectedSquadArray, player);
                //console.log(that.selectedSquadArray);
                selectedPlayers.total--;



            }

            if((player.batter === false) && (player.bowler === true)){
                deletePlayerUI(player,"bowler");
                player.isSelected = false;

                if(minPlayers.bowler >=0 && selectedPlayers.bowler <= 3){
                    if(player.isBonus == false){
                        minPlayers.bowler++;
                        minPlayers.total++;
                    }
                }

                if(player.isBonus === false){
                    selectedPlayers.bowler--;
                    //that.selectedBowlerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBowlerArray,player);
                }
                else{
                    selectedPlayers.bonus--;
                    player.isBonus = false;
                    //that.selectedBonusPlayerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBonusPlayerArray,player);
                }

                // that.selectedSquadArray.pop(player);
                deletePlyerFrmArray( that.selectedSquadArray,player);
                selectedPlayers.total--;


            }

            if((player.batter === true) && (player.bowler === false)){

                deletePlayerUI(player,"batter");
                player.isSelected = false;
                if(minPlayers.batter >=0 && selectedPlayers.batter <= 3){
                    if(player.isBonus == false){
                        minPlayers.batter++;
                        minPlayers.total++;
                    }
                }

                if(player.isBonus === false){
                    selectedPlayers.batter--;
                    //that.selectedBatterArray.pop(player);
                    deletePlyerFrmArray(that.selectedBatterArray,player);
                }
                else{
                    selectedPlayers.bonus--;
                    player.isBonus = false;
                    //that.selectedBonusPlayerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBonusPlayerArray,player);
                }

                //  that.selectedSquadArray.pop(player);
                deletePlyerFrmArray(that.selectedSquadArray,player);
                selectedPlayers.total--;



            }

            if(player.wicketKeeper === true){
                deletePlayerUI(player,"wk");
                player.isSelected = false;

                if(minPlayers.wk >=0 && selectedPlayers.wk <= 1){
                    if(player.isBonus == false){
                        minPlayers.wk++;
                        minPlayers.total++;
                    }

                }

                if(player.isBonus === false){
                    selectedPlayers.wk--;
                    // that.selectedWkArray.pop(player);
                    deletePlyerFrmArray(that.selectedWkArray,player);
                }
                else{
                    selectedPlayers.bonus--;
                    player.isBonus = false;
                    // that.selectedBonusPlayerArray.pop(player);
                    deletePlyerFrmArray(that.selectedBonusPlayerArray,player);
                }

                //that.selectedSquadArray.pop(player);
                deletePlyerFrmArray(that.selectedSquadArray,player);

                selectedPlayers.total--;


            }

        }


        function addPlayerUI(player,playerRole){

            var index;
            if(playerRole === "batter"){
                var count = maxPlayers.batter+selectedPlayers.bonus;
                for(var i=0;i< count;i++){
                    var j=i+1;
                    var dummyElem = document.getElementById("bt"+j);
                    if(dummyElem.innerHTML === ""){
                        //console.log("got the blank player");
                        index = i+1;
                        break;
                    }
                }
                //console.log("adding at position bt  ::: ",(index));
                var elem = document.getElementById("bt"+index);
            }

            if(playerRole === "bowler"){
                var count = maxPlayers.bowler+selectedPlayers.bonus;
                for(var i=0;i< count;i++){
                    var j=i+1;
                    var dummyElem = document.getElementById("bw"+j);
                    if(dummyElem.innerHTML === ""){
                        //console.log("got the blank player");
                        index = i+1;
                        break;
                    }
                }
                //console.log("adding at position bw ::: ",(index));
                var elem = document.getElementById("bw"+index);
            }

            if(playerRole === "wk"){
                var count = maxPlayers.wk+selectedPlayers.bonus;
                for(var i=0;i< count;i++){
                    var j=i+1;
                    var dummyElem = document.getElementById("wk"+j);
                    if(dummyElem.innerHTML === ""){
                        //console.log("got the blank player");
                        index = i+1;
                        break;
                    }
                }
                //console.log("adding at position  ::: wk ",(index));
                var elem = document.getElementById("wk"+index);
            }

            if(playerRole === "alrdr"){
                var count = maxPlayers.alrdr+selectedPlayers.bonus;
                for(var i=0;i< count;i++){
                    var j=i+1;
                    var dummyElem = document.getElementById("alrdr"+j);
                    if(dummyElem.innerHTML === ""){
                        //console.log("got the blank player");
                        index = i+1;
                        break;
                    }
                }
                //console.log("adding at position  ::: alrdr ",(index));
                var elem = document.getElementById("alrdr"+index);
            }

            elem.style.backgroundImage = "url('" + $rootScope.cdnUrl + "/assets/images/sports/cricket/jersey/"+player.teamName+".png')";
            if(player.isCaptain)
                elem.classList.add("sel-cap");
            if(player.isViceCaptain)
                elem.classList.add("sel-v-cap");
            elem.title = player.playerName;
            elem.setAttribute("datapid",player.playerId);
            if(devicedetectionservice.isDevice('DESKTOP')){
                if(player.isBonus){
                    elem.innerHTML="<div class='plyr-details bonus'><span class='plyr-name'>(B)"+player.shortName+"</span><span class='plyr-credits'>"+player.playerCredit+"</span></div>";
                }

                else{
                    elem.innerHTML="<div class='plyr-details'><span class='plyr-name'>"+player.shortName+"</span><span class='plyr-credits'>"+player.playerCredit+"</span></div>";
                }

            }

            if(devicedetectionservice.isDevice('MOBILE')){
                if(player.isBonus){
                    elem.innerHTML="<div class='plyr-details bonus'><div class='plyr-name'>(B)"+player.shortName+"</div></div>";
                }

                else{
                    elem.innerHTML="<div class='plyr-details'><div class='plyr-name'>"+player.shortName+"</div></div>";
                }

            }
            //console.log("adding player ui while processing before : ", that.isProcessing);
            that.isProcessing = false;
            //console.log("adding player ui while processing after : ", that.isProcessing);
            // team complete popup
            if(that.selectedSquadArray.length == 11){
                that.showCapVCapPopup();
            }

        }

        function deletePlayerUI(player,playerRole){
            var elem = document.querySelectorAll(".selected-squad .player");
            var elemLength = elem.length;
            for(var i=0;i<elemLength;i++){
                if(elem[i].getAttribute("datapid") !== null){
                    if(elem[i].getAttribute("datapid") == player.playerId){
                        //console.log("got one to delete");
                        var e = elem[i];
                        break;
                    }
                }

            }


            e.style.backgroundImage = "url('" + $rootScope.cdnUrl + "/assets/images/sports/cricket/placeholder.png')";
            //e.classList.remove(player.teamName);
            if(player.isCaptain){
                e.removeClassName("sel-cap"); 
            }
            if(player.isViceCaptain){
                e.removeClassName("sel-v-cap"); 
            }

            e.title = "";
            e.innerHTML="";


            if(player.isCaptain == true){
                player.isCaptain = false;
                that.selectedCaptain ={};
                that.captainSelected = false;
            }
            if(player.isViceCaptain == true){
                player.isViceCaptain = false;
                that.selectedViceCaptain ={};
                that.viceCaptainSelected = false;
            }
            /* if(player.isBonus = true){
                player.isBonus = false;
            }*/

            //console.log("deleting player ui while processing before : ", that.isProcessing);
            that.isProcessing = false;
            //console.log("deleteing player ui while processing after : ", that.isProcessing);


            //console.log(that.selectedSquadArray);

        }


        function deletePlyerFrmArray(players, player) {
            var length = players.length;

            for(var i=0; i< length; i++){
                if(players[i].playerId == player.playerId){
                    //console.log("got the player ::: "+players[i].playerName);
                    players.splice(i,1);
                    break;

                }
            }
            //console.log(players);
        }



        function checkMinPlayersCreteria(player){
            if(selectedPlayers.batter < 3){
                that.isProcessing = false;
                that.showSelErr = true;
                if(that.bonusPlayersArray.length>0){
                    that.selectionErr = msgConstants.ERR_BONUS_SELECTED;
                }
                else{
                    that.selectionErr = msgConstants.ERR_SEL_MIN_BATTER;    
                }


                if(devicedetectionservice.isDevice("MOBILE")){
                    showToast(that.selectionErr);
                }
                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                return false;
            }

            if(selectedPlayers.bowler < 3){
                that.isProcessing = false;
                that.showSelErr = true;
                if(that.bonusPlayersArray.length>0){
                    that.selectionErr = msgConstants.ERR_BONUS_SELECTED;
                }
                else{
                    that.selectionErr = msgConstants.ERR_SEL_MIN_BOWLER;
                }
                if(devicedetectionservice.isDevice("MOBILE")){
                    showToast(that.selectionErr);
                }
                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                return false;
            }

            if(selectedPlayers.wk < 1){
                //console.log("al least one keeperr reqiured");
                that.isProcessing = false;
                that.showSelErr = true;
                if(that.selectedBonusPlayerArray.length>0){
                    that.selectionErr = msgConstants.ERR_BONUS_SELECTED;
                }
                else{
                    that.selectionErr = msgConstants.ERR_SEL_MIN_WK;
                }
                if(devicedetectionservice.isDevice("MOBILE")){
                    showToast(that.selectionErr);
                }
                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;
                return false;
            }

            if(selectedPlayers.alrdr < 1){
                //console.log("3 peice for 1");
                that.isProcessing = false;
                that.showSelErr = true;
                if(that.bonusPlayersArray.length>0){
                    that.selectionErr = msgConstants.ERR_BONUS_SELECTED;
                }
                else{
                    that.selectionErr = msgConstants.ERR_SEL_MIN_ALRDR;
                }
                if(devicedetectionservice.isDevice("MOBILE")){
                    showToast(that.selectionErr);
                }
                that.selectedSquadCredits = that.selectedSquadCredits - player.playerCredit;    
                return false;
            }

        }


        function updateTeamCount(player,op){
            var playerTeamName = player.teamName.toUpperCase();

            if(op =="add"){

                if(playerTeamName === teamPlyrCount.team1Name){
                    teamPlyrCount.team1++;
                }
                else if(playerTeamName === teamPlyrCount.team2Name){
                    teamPlyrCount.team2++;
                }
                //console.log(teamPlyrCount,"adding");


            }

            if(op =="sub"){

                if(playerTeamName === teamPlyrCount.team1Name){
                    teamPlyrCount.team1--;
                }
                else if(playerTeamName === teamPlyrCount.team2Name){
                    teamPlyrCount.team2--;
                }

                //console.log(teamPlyrCount,"deleting");

            }


        }

        that.clearTeam = function(event){
            if(angular.element(event.currentTarget).hasClass("active")){
                $rootScope.$broadcast("showPopup", {
                    type: "Confirm",
                    title: msgConstants.HEADER_CONFIRMATION,
                    appendTo:'#mainWrapper',
                    msg: msgConstants.CLEAR_TEAM,
                    okButtonText: 'YES',
                    cancelButtonText: 'NO',
                    okBtnCallback: function () {
                        var elemArr = document.querySelectorAll('.selected-squad .player');
                        var length = elemArr.length;

                        for(var i=0;i <length; i++){

                            elemArr[i].style.backgroundImage = "";
                            //elemArr[i].classList.remove(player.teamName);
                            elemArr[i].setAttribute("datapid","");
                            elemArr[i].title = "";
                            elemArr[i].innerHTML="";
                            elemArr[i].removeClassName("sel-cap");
                            elemArr[i].removeClassName("sel-v-cap");
                        }


                        var count = that.matchPlayersArray.length;
                        for(var j=0;j<count;j++){
                            that.matchPlayersArray[j].isSelected = false;
                            that.matchPlayersArray[j].isCaptain = false;
                            that.matchPlayersArray[j].isViceCaptain = false;
                            that.matchPlayersArray[j].isBonus = false;
                        }

                        // cleararray

                        that.selectedAllRounderArray =[];
                        that.selectedBatterArray =[];
                        that.selectedBonusPlayerArray =[];
                        that.selectedBowlerArray = [];
                        that.selectedWkArray = [];
                        that.selectedSquadArray = [];
                        that.selectedCaptain = {};
                        that.captainSelected = false;
                        that.selectedViceCaptain = {};
                        that.viceCaptainSelected = false;
                        that.selectedSquadCredits = 0;

                        selectedPlayers = {
                            wk: 0,
                            batter: 0,
                            bowler: 0,
                            alrdr:0,
                            bonus: 0,
                            total: 0
                        };

                        minPlayers = {
                            wk: 1,
                            batter : 3,
                            bowler : 3,
                            alrdr : 1,
                            bonus : 0,
                            total: 8

                        };

                        teamPlyrCount.team1 = 0;
                        teamPlyrCount.team2 = 0;

                    }
                });
            }
            else{
                return false;
            }
        }



        // auto open

        that.showCapVCapPopup = function(event){
            if(that.autoTeamAdd == false){
                that.teamComplete = true;
            }

            that.autoTeamAdd = false;

        }

        that.closeCapVCapPopup = function(){

            that.capVcapErr ="";

            that.teamComplete = false;
            if(that.isNewTeam){
                that.viceCaptainSelected = false;
                that.captainSelected = false;
            }
            else{


            }
        }

        // ui click open
        that.showCapVCapPopupUI = function(event){
            if(angular.element(event.currentTarget).hasClass("active")){
                that.teamComplete = true;
            }

        }


        that.capSelClick = function(){
            var capWrapper = document.querySelector('.cap');
            var vcapWrapper = document.querySelector(' .vcap');
            vcapWrapper.classList.remove('open');
            if(capWrapper.classList.contains('open')){
                capWrapper.classList.remove('open');
            }   
            else{
                capWrapper.classList.add('open');
            }


        }

        that.viceCapSelClick = function(){

            var vcapWrapper = document.querySelector(' .vcap');
            if(vcapWrapper.classList.contains('open')){
                vcapWrapper.classList.remove('open');
            }
            else{
                vcapWrapper.classList.add('open');
            }
        }

        that.onCaptainSelect = function(player){
            
            that.captainSelected = true;
            that.selectedCaptain = player;
            clearCaptain();
            player.isCaptain = true;
            var playerWrapper = document.querySelector('.cap');
            playerWrapper.classList.remove('open');

        }

        that.onViceCaptainSelect = function(player){

            that.viceCaptainSelected = true;
            clearViceCaptain();
            that.selectedViceCaptain = player;
            player.isViceCaptain = true;
            var playerWrapper = document.querySelector('.vcap');
            playerWrapper.classList.remove('open');

        }


        function clearCaptain(){
            var length = that.selectedSquadArray.length;
            for(var i=0;i<length;i++){
                that.selectedSquadArray[i].isCaptain = false;
            }

        }

        function clearViceCaptain(){
            var length = that.selectedSquadArray.length;
            for(var i=0;i<length;i++){
                that.selectedSquadArray[i].isViceCaptain = false;
            }
        }


        function getPlayerFromUI(player){
            var elem = document.querySelectorAll(".selected-squad .player");
            var elemLength = elem.length;
            for(var i=0;i<elemLength;i++){
                if(elem[i].getAttribute("datapid") !== null){
                    if(elem[i].getAttribute("datapid") == player.playerId){
                        return elem[i];
                    }
                }

            }

        }

        that.closeErrorBox = function(){
            that.showSelErr = false;
        }


        // mobile specific funcitons

        that.showMyteam = function(){
            that.mobileUI = {
                showTeamPreview : true,
                showPlayerList : false
            }
        }

        that.hideMyTeam = function(){
            //console.log("hiding");
            that.mobileUI = {
                showTeamPreview : false,
                showPlayerList : true
            }
        }

        that.saveMyTeam =function(event){
            //console.log("called");
            // for desktop 
            if(devicedetectionservice.isDevice('DESKTOP') == true){
                if(angular.element(event.currentTarget).hasClass("active")){


                    if(that.selectedCaptain.playerId == that.selectedViceCaptain.playerId){
                        that.capVcapErr = msgConstants.ERR_SEL_SAME_C_VC;
                        return false;
                    }

                    // icons on head UI
                    addCapVCapUi();
                    // close the dialog
                    var elem = document.querySelectorAll(".cap-vice-cap-sel-container");
                    //elem.();
                    // call api for save team
                    var params = {};
                    params.players = that.selectedSquadArray;
                    params.matchId = that.matchId;
                    params.teamId = that.teamId;

                    //console.log(params);

                    if(that.isNewTeam === true){
                        //console.log("saving first time");
                        createteamservice.saveMyTeam(params,function(response){
                            if(response.respCode == 100){
                                //console.log("team saved");
                                var invCode = utilityservice.getUrlParameter("invCode");
                                console.log(invCode);
                                // show popup with ok button

                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.TEAM_DETAILS_SAVED,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        if(invCode !== undefined && invCode !==  ""){
                                            $window.location.href = utilityservice.getBaseUrl()+"/league?invCode="+invCode;    
                                        }
                                        else{
                                            $window.location.href = utilityservice.getBaseUrl()+"/league";
                                        }

                                    }
                                });
                            }

                            else {
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: response.message,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        ngDialog.close();
                                    }
                                });
                            }

                        });
                    }
                    else{


                        //console.log("saving next time");
                        //console.log(params);
                        createteamservice.updateUserTeam(params,function(response){

                            if(response.respCode == 100){
                                //console.log("upadted team saved");
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.TEAM_DETAILS_SAVED,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        $window.location.href = utilityservice.getBaseUrl()+"/league";
                                    }
                                });
                            }

                            else {
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: response.message,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        ngDialog.close();
                                    }
                                });
                            }

                        });
                    }

                    that.teamComplete =false;
                    that.capVcapErr ="";

                }

            }


            // only in mobile
            if(devicedetectionservice.isDevice('MOBILE') == true){

                if(angular.element(event.currentTarget).hasClass("active")){

                    // call api for save team
                    var params = {};
                    params.players = that.selectedTeam;
                    params.matchId = that.matchInfoLocal.matchId;

                    //console.log(params);

                    if(that.isNewTeamLocal === true){
                        //console.log("saving first time");
                        createteamservice.saveMyTeam(params,function(response){
                            if(response.respCode == 100){
                                //console.log("team saved");
                                var invCode = utilityservice.getUrlParameter("invCode");
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.TEAM_DETAILS_SAVED,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        if(invCode !== undefined && invCode !==  ""){
                                            $window.location.href = utilityservice.getBaseUrl()+"/match-leagues/"+that.matchInfoLocal.matchId+"?invCode="+invCode;    
                                        }
                                        else{
                                            $window.location.href = utilityservice.getBaseUrl()+"/match-leagues/"+that.matchInfoLocal.matchId;
                                        }
                                    }
                                });
                            }

                            else {
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: response.message,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        ngDialog.close();
                                    }
                                });
                            }
                        });
                    }
                    else{
                        params.teamId = that.teamIdLocal;
                        //console.log("saving next time");
                        createteamservice.updateUserTeam(params,function(response){
                            if(response.respCode == 100){
                                //console.log("upadted team saved");
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.TEAM_DETAILS_SAVED,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        $window.location.href = utilityservice.getBaseUrl()+"/match-leagues/"+that.matchInfoLocal.matchId;
                                    }
                                });
                            }

                            else {
                                ngDialog.close();
                                $rootScope.$broadcast("showPopup", {
                                    type: "INFO",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: response.message,
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                        ngDialog.close();
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }


        that.saveMyTeamInLocal = function(event){
            if(angular.element(event.currentTarget).hasClass("active")){

                $window.localStorage['selectedTeam'] = angular.toJson(that.selectedSquadArray);
                //console.log(that.matchInfo);
                $window.localStorage['matchInfo'] = angular.toJson(that.matchInfo);
                $window.localStorage['selectedCaptain'] = angular.toJson(that.selectedCaptain);
                $window.localStorage['selectedViceCaptain'] = angular.toJson(that.selectedViceCaptain);
                $window.localStorage['captainSelected'] = angular.toJson(that.captainSelected);
                $window.localStorage['viceCaptainSelected'] = angular.toJson(that.viceCaptainSelected);
                $window.localStorage['teamId'] = angular.toJson(that.teamId);
                $window.localStorage['isNewTeam'] = angular.toJson(that.isNewTeam);
                var invCode = utilityservice.getUrlParameter("invCode");
                if(invCode !== undefined && invCode !==  ""){
                    $window.location.href = utilityservice.getBaseUrl()+"/sel-cap-vcap?invCode="+invCode;
                }
                else{
                    $window.location.href = utilityservice.getBaseUrl()+"/sel-cap-vcap";
                }



            }
        }

        function addCapVCapUi(){
            //console.log("called fn");

            var elem = document.querySelectorAll(".selected-squad .player");
            var elemLength = elem.length;
            for(var i=0;i<elemLength;i++){
                if(elem[i].getAttribute("datapid") !== null){
                    if(elem[i].getAttribute("datapid") == that.selectedCaptain.playerId){
                        //console.log("captain ::",elem[i].attributes,elem[i]);
                        //elem[i].classList.addClass("sel-cap");
                        elem[i].className += " sel-cap";
                    }
                    else {
                        elem[i].classList.remove("sel-cap");
                    }

                    if(elem[i].getAttribute("datapid") == that.selectedViceCaptain.playerId){
                        //console.log("vice captain ::",elem[i].innerHTML);
                        elem[i].className += " sel-v-cap";
                    }

                    else{
                        elem[i].classList.remove("sel-v-cap");
                    }


                }

            }


        }



        that.selectedTeam = [];
        that.matchInfoLocal = {};
        that.getTeamFromLocalstorage = function(){

            that.selectedTeam =      
                JSON.parse($window.localStorage['selectedTeam']);
            that.matchInfoLocal = JSON.parse($window.localStorage['matchInfo']);
            that.selectedCaptain = JSON.parse($window.localStorage['selectedCaptain']);
            that.selectedViceCaptain = JSON.parse($window.localStorage['selectedViceCaptain']);
            that.captainSelected = JSON.parse($window.localStorage['captainSelected']);
            that.viceCaptainSelected = JSON.parse($window.localStorage['viceCaptainSelected']);
            that.teamIdLocal = JSON.parse($window.localStorage['teamId']);
            that.isNewTeamLocal = JSON.parse($window.localStorage['isNewTeam']);

            //console.log(JSON.parse($window.localStorage['selectedTeam']));
        }


        that.mobileSelCapClick = function(player){

            if(that.selectedViceCaptain.playerId == player.playerId){
                that.selectionErr = msgConstants.ERR_SEL_SAME_C_VC;
                showToast(that.selectionErr);
                return false;
            }

            if(that.selectedCaptain.playerId !== undefined){

                if(that.captainSelected){

                    if(that.selectedCaptain.playerId == player.playerId){
                        player.isCaptain = false;
                        that.selectedCaptain = {};
                        that.captainSelected = false;
                    }

                    else{

                        that.selectionErr = msgConstants.ERR_SEL_CAP_ALRDY_TKN;
                        showToast(that.selectionErr);
                    }

                }

            }

            else{
                player.isCaptain = true;
                that.selectedCaptain = player;
                that.captainSelected = true;
            }

        }

        that.mobileSelVCapClick = function(player){

            if(that.selectedCaptain.playerId == player.playerId){
                that.selectionErr = msgConstants.ERR_SEL_SAME_C_VC;
                showToast(that.selectionErr);
                return false;
            }


            if(that.selectedViceCaptain.playerId !== undefined){

                if(that.viceCaptainSelected){

                    if(that.selectedViceCaptain.playerId == player.playerId){
                        player.isViceCaptain = false;
                        that.selectedViceCaptain = {};
                        that.viceCaptainSelected = false;
                    }

                    else{
                        that.selectionErr = msgConstants.ERR_SEL_VCAP_ALRDY_TKN;
                        showToast(that.selectionErr);
                    }

                }

            }

            else{




                player.isViceCaptain = true;
                that.selectedViceCaptain = player;
                that.viceCaptainSelected = true;
            }

        }



        function showToast(msg){
            var elem =  document.querySelector(".m-selection-err");
            angular.element(elem).hide().show().delay(2000).fadeOut("slow");
        }


        // get save team button class UI

        that.getSaveTeamClass = function(){

            if((that.captainSelected && that.viceCaptainSelected) && (that.selectedSquadArray.length == 11)){
                return 'active';
            }
            else{
                return 'unactive';
            }

        }


        /*   function checkSelectedTeam(players){
            var selectedPlayers = {
                bttr:0,
                bwlr:0,
                alrdr:0,
                wk:0,
                bns:0
            }

            var length  = team.length;
            for(var j=0;j<length;j++){
                if(that.getPlayerRole(players[i]) == "alrdr"){
                    selectedPlayers.alrdr++;
                }

                if(that.getPlayerRole(players[i]) == "batter"){
                    selectedPlayers.bttr++;
                }


                if(that.getPlayerRole(players[i]) == "bowler"){
                    selectedPlayers.bwlr++;
                }

                if(that.getPlayerRole(players[i]) == "wk"){
                    selectedPlayers.wk++;
                }

                if(player[i].isBonus){
                    selectedPlayers.bns++;
                }

            }

            if(selectedPlayers.bns > 1){
                //console.log("max one bonus player allowed");
                return false;
            }


            if(selectedPlayers.bttr<3){
                //console.log("min 3 btr req");
                return false;
            }

             if(selectedPlayers.bwlr<3){
                //console.log("min 3 bwlr req");
                 return false;
            }

             if(selectedPlayers.wk == 0){
                //console.log("min 1 wk req");
                 return false;
            }

             if(selectedPlayers.alrdr == 0){
                //console.log("min 1 alrdr req");
                 return false;
            }



            if(selectedPlayers.bttr>5){
                //console.log("max 5 bttr select");
                return false;
            }

            if(selectedPlayers.bwlr>5){
                //console.log("max 5 bwlr select");
                return false;
            }

             if(selectedPlayers.alrdr>3){
                //console.log("max 3 alrdr req");
                 return false;
            }

             if(selectedPlayers.wk > 1){
                //console.log("max 1 wk sel");
                 return false;
            }

            return true;


        }*/


    }]);
