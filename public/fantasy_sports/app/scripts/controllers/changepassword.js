'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:ChangepasswordCtrl
 * @description
 * # ChangepasswordCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('ChangepasswordCtrl',['$rootScope', 'config', 'user', 'utilityservice', '$window', 'serviceName', 'ngDialog', 'msgConstants', function ($rootScope, config, user, utilityservice, $window, serviceName, ngDialog, msgConstants) {
        var that = this;

        that.onFieldChange = function(fieldName,fieldValue,additionalInfo){
          user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
            if(response.resCode !== 100){
                that[fieldName+'Msg'] = response.message;
            }else{
                console.log("resCode---->",response.resCode);
            }
          });
        };

        /* Change password api call- ashu*/
        that.changePassword = function() {
            that.isValidate = true;
            that.oldPassMsg = '';
            that.newPassMsg = '';
            that.confirmPassMsg = '';
            if(that.oldPass == that.newPass) {
                that.isValidate = false;
                that.newPassMsg = msgConstants.ERR_PASSWORD_MATCH;
            }

            user.OnInputFieldValidation('oldPass', that.oldPass,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.oldPassMsg = response.message;
                }
            });
            user.OnInputFieldValidation('newPass', that.newPass,'', function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.newPassMsg = response.message;
                }
            });
            user.OnInputFieldValidation('confirmPass', that.confirmPass, that.newPass, function(response) {
                if(response.resCode !== 100){
                    that.isValidate = false;
                    that.confirmPassMsg = response.message;
                }
            });

            if(that.isValidate) {
                var params = {};
                params.password = CryptoJS.MD5(that.oldPass)+'';
                params.newPassword = CryptoJS.MD5(that.newPass)+'';
                utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.CHANGE_PASSWORD, params, function (response) {
                    if(response.respCode == 100) {
                        $rootScope.$broadcast("showPopup", {
                              type: "Info",
                              title: msgConstants.HEADER_INFO,
                              appendTo:'#mainWrapper',
                              msg: msgConstants.PASSWORD_CHANGE_SUCCESSFULLY,
                              popupSize: 'small',
                              okButtonText: 'OK',
                              okBtnCallback: function () {
                                 $rootScope.$broadcast("setActiveHeaderMenu");
			 			      }
                        });
                    }else{
                        that.oldPassMsg = response.message;
                        that.isValidate = false;
                    }
                });
            }

        };

        that.close = function(){
            $rootScope.$broadcast("setActiveHeaderMenu");
            ngDialog.close();
        };
  }]);
