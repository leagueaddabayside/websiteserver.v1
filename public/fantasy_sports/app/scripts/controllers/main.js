'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('MainCtrl',['$rootScope', 'constants', 'ngDialog', 'user', 'config', 'utilityservice', 'session', '$window', 'devicedetectionservice', 'serviceName','dateDropdownHandler', 'msgConstants', function ($rootScope, constants, ngDialog, user, config, utilityservice, session, $window, devicedetectionservice, serviceName,dateDropdownHandler, msgConstants) {
		var that = this;
		that.isSignup = true;
		that.testiArray = constants.TESTIMONIAL_ARRAY;
		that.initSlick = true;
		var isDesktop = devicedetectionservice.isDevice('DESKTOP');

  	      //Calender Module functionality Starts here---Ashutosh
      that.selectedMonth = "MM";
      that.selectedYear = "YY";
      that.selectedDate = "DD";
      that.dobRange = [];
      that.dobRange.push("YY");
      that.monthName = constants.MONTH_LIST;
      that.dateRange = [];
      dateDropdownHandler.initilizeYears(function(yearRange){
            that.dobRange = yearRange;
      });
      dateDropdownHandler.initilizeDays(function(dayRange){
            that.dateRange = dayRange;
      });

      that.getKey = function(month) {
            return Object.keys(month)[0];
      };
      // On year change function--Ashu.
      that.changeYear = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getMonths(that.selectedYear, function(monthList){
                  that.monthName = monthList;
            });
            that.checkDays();
      };
      // On day change function
      that.dayChange = function(){
            that.dateEmptyvalidator();
      };

      // On month change calucate days to show in dropdown--Ashu
      that.checkDays = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getDays(that.selectedYear, that.selectedMonth, function(dayList){
                  that.dateRange = dayList;
                  if(that.selectedDate > dayList.length-1) {
					  that.selectedDate = "DD";
				  }
            });
      };

      that.dateEmptyvalidator = function(){
            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = "Please fill all date fields.";
            }else {
                that.userDateMsg = '';
            }
      };
      //Calender Module functionality Ends

     that.showSignupLogin = function(homeTabType, event){
			var homeTabs = document.querySelectorAll('#homeTabs li');
			var currentTab = event.target;
			for(var i = 0; i < homeTabs.length; i++){
				homeTabs[i].classList.remove('active');
			}
			currentTab.classList.add('active');
			switch(homeTabType){
				case 'REGISTER':
					that.isSignup = true;
					that.userLoginEmail = '';
					that.userLoginEmailMsg = '';
					that.userLoginPassword = '';
					that.userLoginPasswordMsg = '';
					break;
				case 'LOGIN':
					that.isSignup = false;
					that.userEmail = '';
					that.userEmailMsg = '';
					that.userPassword = '';
					that.userPasswordMsg = '';
					that.selectedYear = 'YY';
					that.selectedMonth = 'MM';
					that.selectedDate = 'DD';
					that.userDateMsg = '';
					that.userTerms = false;
					that.userTermsMsg = '';
					break;
				default:
			}
		};

		that.onForgotPasswordClick = function(){
				ngDialog.open({
					template: '/fantasy_sports/app/views/forgotpassword.html',
					overlay: true,
					closeByDocument: false,
					closeByEscape: false,
					showClose: true,
					closeByNavigation: false,
					disableAnimation: true,
					controller: 'forgotPasswordCtrl',
					controllerAs: 'forgot',
					appendTo: '#mainWrapper',
					className: 'ngdialog-theme-popup forgot-password-popup'
				});
		};

		that.onFieldChange = function(fieldName,fieldValue,additionalInfo){
		  user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
			if(response.resCode !== 100){
				that[fieldName+'Msg'] = response.message;
			}else{
				//console.log("resCodeInputField---->",response.resCode);
			}
		  });
		};

		that.checkTNC = function() {
				if(that.userTerms) {
						that.userTermsMsg = '';
				}else {
						that.userTermsMsg = msgConstants.ERR_TNC;
				}
		};

		/* Signup Api call & validation--Ashu*/
		that.signup = function(){
				that.isValidate = true;
				that.userEmailMsg = '';
				that.userPasswordMsg = '';

				user.OnInputFieldValidation('userEmail', that.userEmail,'', function(response) {
						if(response.resCode !== 100){
							that.isValidate = false;
							that.userEmailMsg = response.message;
						}
				});

				user.OnInputFieldValidation('userPassword', that.userPassword,'', function(response) {
						if(response.resCode !== 100){
							that.isValidate = false;
							that.userPasswordMsg = response.message;
						}
				});

				if(that.selectedYear=="YY" || that.selectedYear=="" || that.selectedMonth=="MM" || that.selectedMonth=="" || that.selectedDate=="DD" || that.selectedDate=="") {
						that.isValidate = false;
						that.userDateMsg = msgConstants.REQUIRED_MESSAGE_1;
				}

				if(!that.userTerms){
					   that.isValidate = false;
					   that.userTermsMsg = msgConstants.ERR_TNC;
				}

				//Call Api if all validations are pass.
				if(that.isValidate) {
						var params = {};
						params.emailId = that.userEmail;
						params.password = CryptoJS.MD5(that.userPassword)+'';
						params.dob = that.selectedMonth+"/"+that.selectedDate+"/"+that.selectedYear;
						utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.USER_SIGNUP, params, function (response) {
								if(response.respCode === 100) {
										$rootScope.$broadcast("showPopup", {
											  type: "Info",
											  title: msgConstants.HEADER_INFO,
											  appendTo:'#mainWrapper',
											  msg: msgConstants.MSG_SIGNUP_SUCCESS,
											  popupSize: 'small',
											  okButtonText: 'OK',
											  okBtnCallback: function () {
												ngDialog.close();
												var params = {};
												params.userData = that.userEmail;
												params.password = CryptoJS.MD5(that.userPassword)+'';
												that.loginServiceCall(params);
											  }
										});
								}else if (response.respCode == 115){
										that.userEmailMsg = response.message;
								}else{
										console.log("some error occured",response);
								}
						});

				}
		};

		/* Login Api call and validations */
		that.login = function() {
				that.isValidate = true;
				that.userLoginEmailMsg = '';
				that.userLoginPasswordMsg = '';

				user.OnInputFieldValidation('userLoginEmail', that.userLoginEmail,'', function(response) {
						if(response.resCode !== 100){
							that.isValidate = false;
							that.userLoginEmailMsg = response.message;
						}
				});

				user.OnInputFieldValidation('userLoginPassword', that.userLoginPassword,'', function(response) {
						if(response.resCode !== 100){
							that.isValidate = false;
							that.userLoginPasswordMsg = response.message;
						}
				});
				if(that.isValidate) {
						var params = {};
						params.userData = that.userLoginEmail;
						params.password = CryptoJS.MD5(that.userLoginPassword)+'';
						that.loginServiceCall(params);
				}
		   };

		   that.loginServiceCall = function(params) {
				utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.USER_LOGIN, params, function (response) {
						if(response.respCode === 100) {
								session.setWebsiteToken(response.respData.token);
								if(isDesktop){
									$window.location.href = utilityservice.getBaseUrl() + "/league";
								}
								else{
									$window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
								}
						}else{
								console.log("some error occured",response);
								that.userLoginPasswordMsg = response.message;
						}
				});
		   };

		  //  that.onGoogleLoign = function(){
				// console.log('google login');
				// var winOpts = {
				// 	wUrl: "auth/google",
				// 	wHeight: 600,
				// 	wWidth: 450,
				// 	wName: 'Google Login'
				// }
				// var newWindow = window.open(winOpts.wUrl, winOpts.wName, 'height=' + winOpts.wHeight + ',width=' + winOpts.wWidth + ', top=' + ((window.innerHeight - winOpts.wHeight) / 2) + ', left=' + ((window.innerWidth - winOpts.wWidth) / 2));
				// if (window.focus) {
				// 	newWindow.focus();
				// }
		  //  	};

		 //   that.onFbLoign = function(event){
			// 	console.log('ONFB Login');
			// 	if(event.target.href !== ''){
			// 		window.location.href = event.target.href;
			// 		event.target.removeAttribute('href');
			// 	}
				
			// 	event.preventDefault();
			// };

			that.clickAndDisable = function(event) {
				if(event.target.href !== ''){
					window.location.href = event.target.href;
					event.target.removeAttribute('href');
				}
			};
  }]);
