'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:addCashCtrl
 * @description
 * # addCashCtrl
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('addCashCtrl',['$scope', 'ngDialog', 'utilityservice', 'session', 'config', 'serviceName', 'depositeservice', 'leaguedata', 'msgConstants', function ($scope, ngDialog,  utilityservice,session, config, serviceName, depositeservice, leaguedata, msgConstants) {
	var that = this;

	that.isAddCashDisable = false;

	that.title = leaguedata.title;
	that.showCurrBalance = leaguedata.showCurrentBalance;
	that.userID = session.getUserID();

	if(that.showCurrBalance) {		// Case for add cash popup in join league 
		that.userBalance = leaguedata.checkLeagueResponse.validBalance;
		that.entryFee = leaguedata.checkLeagueResponse.leagueCost;
		that.amount = leaguedata.checkLeagueResponse.remainingAmt;
	}else{
		that.amount = leaguedata.balance || '';
	}
	that.amountError = '';

	that.addAmount = function(amount){
		if(that.amount == undefined){
			that.amount = '';
		}
		that.amount =  that.amount === '' ? 0 : that.amount;
		if(parseInt(that.amount) + parseInt(amount) > 25000){
			that.amountError = msgConstants.TRANSACTION_FAILURE_3;
			return false;
		}
		that.amount = parseInt(that.amount) + parseInt(amount);
		that.amountError = '';
	};

	that.onAddCashClick = function(e){
		var isSuccess = true;
		var user = session.getUserName();
		if(session.getUserName() === null || session.getUserName() === undefined){
			that.amountError = msgConstants.TRANSACTION_FAILURE_5;
			isSuccess = false;
		}
		if(parseInt(that.amount) < 1 || that.amount === undefined || that.amount === ""){
			that.amountError = msgConstants.TRANSACTION_FAILURE_4;
			isSuccess = false;
		}

		if(parseInt(that.amount) > 25000){
			that.amountError = msgConstants.TRANSACTION_FAILURE_3;
			isSuccess = false;
		}

		if(isSuccess){
			that.isAddCashDisable = true;
			var paymentMethod = "PAYU";

			if(paymentMethod == "TEKPROCESS"){
				var amt = String(that.amount);
				var params = {};
				params.userId = session.getUserID();
				params.amount = amt;
				utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.DEPOSITE_INIT_TEKPROCESS, params, function (response) {
										//console.log("paymentAPI resp--",response);
					if(response.respCode == 100) {
						session.setDepositeData(response.respData.token);
						session.setTransactionID(response.respData.merchantTransactionIdentifier);
						session.setTransinitID(response.respData.txnInitId);
						depositeservice.openCard(response.respData.token, response.respData.merchantCode, response.respData.merchantTransactionIdentifier,amt);
						ngDialog.close();
					}else{
						that.isAddCashDisable = false;
						that.amountError = response.message;
					}
				});
			}else if(paymentMethod == "PAYU"){
				document.getElementById("addCash").submit();
			}
		}
	};

	that.checkAlphabet = function(event,fieldName){
		var keyCode = event.keyCode;
		if(keyCode<48 || keyCode>57 ){
			that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
						 //event.preventDefault();
		 }
	 };
}]);
