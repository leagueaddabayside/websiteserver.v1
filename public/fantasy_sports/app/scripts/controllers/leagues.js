'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:leaguesCtrl
 * @description
 * # leaguesCtrl as leagues
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
    .controller('leaguesCtrl', ['$window', '$location', '$scope', 'constants', 'leagueservice', 'utilityservice', '$timeout', '$rootScope', 'devicedetectionservice', 'ngDialog', 'session', 'createteamservice', 'config', 'serviceName', 'leagueutilservice', function ($window, $location, $scope, constants, leagueservice, utilityservice, $timeout, $rootScope, devicedetectionservice, ngDialog, session, createteamservice, config, serviceName, leagueutilservice) {
        var that = this;

        that.baseUrl = utilityservice.getBaseUrl();
        that.screenName = session.getScreenName();
        that.selectedTour = null;
        that.initSlick = false;
        that.carouselIndex = 0;
        that.leagueDisplayType = 'ACTIVE';
        that.selectedMatch = null;
        that.nextMatchTime = [];
        that.isJoinBtnDisabled = false;
        that.isShowLeagueTab = true;
        that.currentLeagueTab = "";
        that.myTeamsArray = [];
        that.currentMatchTeamCount = 0;
        that.showTeamDropdown = false;
        that.noLeaguesError = false;
        that.noMyLeaguesError = false;

        var checkLeagueResponse = null;

        var pageSize = 200;

        that.pagination = {
            currentPage: pageSize,
            pageSize: pageSize
        };

        // init function called when controller loads
        that.init = function () {

            var invCode = utilityservice.getUrlParameter('invCode');
            if (session.getTeamName() === '' || session.getTeamName() === null || session.getTeamName() === undefined) {
                leagueutilservice.myTeamNamePopoup();
            }

            if(invCode !== undefined && invCode !== ""){
                var league = {
                    leagueId: invCode
                };
                leagueutilservice.checkJoinLeague(league, function(checkLeagueResponse){
                    fetchTours();
                });
            }
            else{
                fetchTours();
            }    
        };

        that.resetPagination = function(){
            that.pagination = {
                currentPage: pageSize,
                pageSize: pageSize
            }
        };

        that.updatePagination = function(league, type){
            //var limit = that.pagination.pageSize;
            switch(type){
                case 'SHOW_MORE':
                    that.pagination.currentPage = that.pagination.currentPage + that.pagination.pageSize;
                    break;
                case 'SHOW_TOP':
                    that.pagination.currentPage = that.pagination.pageSize;
                    break;
                case 'SHOW_PREVIOUS':
                    that.pagination.currentPage = that.pagination.currentPage - that.pagination.pageSize;
                    break;
            }
            
            var params = {
                matchId: that.selectedMatch.matchId,
                leagueId: league.leagueId,
                limit: that.pagination.currentPage,
                limitSize: pageSize
            };
            that.leagueMembersArray = [];
            utilityservice.showLoader();
            leagueservice.getLeagueMembers(params, function (leagueMembersArray) {
               that.leagueMembersArray = leagueMembersArray;
                utilityservice.hideLoader();
            });
        };

        that.leagueTabHandler = function (tabType) {
            that.currentLeagueTab = tabType;
            switch (tabType) {
                case 'PUBLIC_LEAGUE':
                    that.fetchLeagues(that.selectedMatch.matchId);
                    break;
                case 'MY_LEAGUE':
                    fetchJoinedLeagues();
                    break;
                default:
            }
        };

        that.teamTabHandler = function (currentTeam, index) {
            that.currentTeamId = currentTeam.teamId;
            that.fetchTeamInfo(currentTeam.teamId);
            resetLeagueAccordions();
        };

        that.browseLeagueHandler = function (joinedLeague, index) {
            if(joinedLeague.isOpen){
                return false;
            }
            that.resetPagination(pageSize);
            fetchLeagueMembers(joinedLeague.leagueId, index);
        };

        that.onTourClick = function () {
            var tourWrapper = document.querySelector('.cstm-tour-select');
            if (tourWrapper.classList.contains('open')) {
                tourWrapper.classList.remove('open');
            }
            else {
                tourWrapper.classList.add('open');
            }
        };

        that.closeTourDropdown = function(){
            var tourWrapper = document.querySelector('.cstm-tour-select');
            tourWrapper.classList.remove('open');
        };

        that.validateJoinLeague = function (league) {
            return leagueutilservice.validateJoinLeague(league);
        };

        that.reJoinLeague = function (league, event) {
            event.preventDefault();
            event.stopPropagation();
            return leagueutilservice.validateJoinLeague(league);
        };

        that.onTourSelect = function (tour) {
            utilityservice.removeFromLocalStorage('currentMatchId');
            that.selectedTour = tour;
            fetchMatches(tour.tourId);
            var tourWrapper = document.querySelector('.cstm-tour-select');
            tourWrapper.classList.remove('open');
        };

        that.onMatchClick = function (match, index) {
            that.selectedMatch.isSelectedMatch = false;
            getActiveMatchData(index);
        };

        that.fetchLeaguePrizeInfo = function (league) {
            var params = {};
            params.leagueId = league.leagueId;
            leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
                leagueutilservice.prizeInfoPopup(prizeinfodata);
            });
        };

        // get league prize info popup from joined league title click
        that.getLeaguePrizeInfo = function (league, event) {
            if(league.chipType.toUpperCase() == 'SKILL'){
                return false;
            }
            event.preventDefault();
            event.stopPropagation();
            var params = {};
            params.leagueId = league.leagueId;
            leagueservice.getLeaguePrizeInfo(params, function (prizeinfodata) {
                leagueutilservice.prizeInfoPopup(prizeinfodata);
            });
        };

        that.fetchLeagues = function (matchid) {
            var params = {};
            params.matchId = matchid;
            utilityservice.showLoader();
            that.noLeaguesError = false;
            leagueservice.getLeagues(params, function (response) {
                that.noLeaguesError = true;
                if (response.respCode === 100) {
                    that.leaguesArray = response.respData;
                }
                else {
                    console.log('Error in Fetching leagues', response.message);
                }
                utilityservice.hideLoader();
            });
        };


        that.onCreateTeamClick = function () {
            if(that.selectedMatch.totalTeams === undefined || that.selectedMatch.totalTeams === 0){
                that.selectedMatch.totalTeams = 0;
            }
            $window.location.href = that.baseUrl + "/createteam/" + that.selectedMatch.matchId + '/' + (that.selectedMatch.totalTeams + 1);
        };

        that.onEditTeamClick = function () {
            $window.location.href = that.baseUrl + "/editteam/" + that.selectedMatch.matchId + '/' + that.currentTeamId;
        };

        that.fetchTeamInfo = function (teamId, event) {
            if(event !== undefined){
                var currentLi = event.currentTarget;
            }
            var params = {};
            params.matchId = that.selectedMatch.matchId;
            params.teamId = teamId;
            utilityservice.showLoader();

            leagueservice.getTeamInfo(params, function (responseData) {

                that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.currentTeam.players);
                that.myTeamInfo = responseData;
                that.myTeamsArray = responseData.teamsArr;
                that.currentTeamInfo = responseData.currentTeam;	//to be reviewed
                that.currentTeamInfo.teamName = responseData.screenName;
                that.currentTeamId = teamId;
                utilityservice.hideLoader();
                if(event !== undefined){
                    highlightList('joined-leagues-list', currentLi);
                }
            });
        };

        that.fetchOpponentTeamInfo = function (data, event) {
            if(event !== undefined){
                var currentLi = event.currentTarget;
            }
            var params = {};
            params.matchId = that.selectedMatch.matchId;
            params.teamId = data.teamId;
            params.opponentId = data.userId;
            utilityservice.showLoader();
            leagueservice.getOpponentTeamInfo(params, function (responseData) {
                that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(responseData.opponentTeam.players);
                that.currentTeamInfo = responseData.opponentTeam;
                that.currentTeamInfo.teamName = responseData.screenName;
                that.currentTeamId = -1;    // resetting teamId to remove highlight from team tabs
                if(event !== undefined){
                    highlightList('joined-leagues-list', currentLi);
                }
                utilityservice.hideLoader();
            });
        };

        that.onTeamUpdateClick = function (data) {
            data.showTeamDropdown = true;
            data.newTeamId = data.teamId.toString();
            //console.log('teams array ::', that.myTeamsArray);
        };

        that.updateMyTeam = function (data) {
            var params = {
                teamId: data.newTeamId,
                leagueJoinedId: data.leagueJoinedId,
                leagueId: data.leagueId,
                matchId: that.selectedMatch.matchId
            };

            leagueservice.updateUserJoinedTeam(params, function (response) {
                data.showTeamDropdown = false;
                data.teamId = data.newTeamId;
            });
        };

        that.getMatchStatus = function (status) {
            return leagueutilservice.getMatchStatus(status);
        };

        that.onScorecardClick = function(){
            $window.open(that.baseUrl + "/fantasy-scorecard/" + that.selectedMatch.matchId, '_blank');
        };

        that.onRefreshClick = function(){
            fetchJoinedLeagues();
            that.fetchTeamInfo(that.currentTeamId);
        };

        // Private Functions
        function fetchTours() {
            var params = {};
            leagueservice.getTours(params, function (toursArray) {
                var alltour = {shortName: 'All', status: 'ACTIVE', tourId: -1, tourName: 'All Series'};
                toursArray.unshift(alltour);
                that.toursArray = toursArray;
                that.selectedTour = that.toursArray[0];
                fetchMatches(-1);
            });
        }

        function fetchMatches(tourid) {
            var params = {};
            if (tourid !== -1) {
                params.tourId = tourid;
            }
            that.initSlick = false;
            leagueservice.getMatches(params, function (matchesArray, nextTimeTour) {
                that.matchesArray = matchesArray;
                if(that.matchesArray.length == 0){
                    return false;
                }
                var activeMatchIndex = leagueutilservice.getActiveMatch(that.matchesArray);
                getActiveMatchData(activeMatchIndex);
                // Timeout to show matches carousel
                $timeout(function () {
                    that.initSlick = true;
                    // Timeout to slide matches carousel
                    that.carouselIndex = 0;
                    $timeout(function () {
                        that.carouselIndex = activeMatchIndex;
                    }, 400);
                },100);

                
                
                if (tourid === -1) {
                    leagueutilservice.updateTourTimer(that.toursArray, nextTimeTour);
                }
            });
        }

        function getActiveMatchData(matchIndex) {
            var currentMatch = that.matchesArray[matchIndex];
            that.selectedMatch = currentMatch;
            that.matchesArray[matchIndex].isSelectedMatch = true;
            that.leaguesArray = [];
            that.matchPlayersArray = [];
            that.joinedLeaguesArray = [];
            that.myTeamsArray = [];
            that.currentTeamInfo = {};
            that.myTeamInfo = {};
            that.currentMatchTeamCount = that.selectedMatch.totalTeams;

            var teamId = leagueutilservice.getActiveMatchTeamId(that.selectedMatch.matchId);
            that.currentTeamId = teamId;

            //Save matchId to local storage
            utilityservice.setToLocalStorage('currentMatchId', that.selectedMatch.matchId);

            if (currentMatch.status === 'LOCKED') {
                that.currentLeagueTab = '';
                that.leagueDisplayType = 'LOCKED';
                return false;
            } else if (currentMatch.status === 'ACTIVE') {
                that.leagueDisplayType = 'ACTIVE';
                that.currentLeagueTab = 'PUBLIC_LEAGUE';
                if (that.currentMatchTeamCount !== 0) {
                    that.fetchTeamInfo(teamId);
                }
                that.fetchLeagues(that.selectedMatch.matchId);
            } else {
                that.leagueDisplayType = 'CLOSED';
                that.currentLeagueTab = 'MY_LEAGUE';
                if (that.currentMatchTeamCount !== 0) {
                    that.fetchTeamInfo(teamId);
                }
                fetchJoinedLeagues();
            }
        }
        
        function fetchJoinedLeagues() {
            that.joinedLeaguesArray = [];
            var params = {
                matchId: that.selectedMatch.matchId
            };
            that.noMyLeaguesError = false;
            leagueservice.getJoinedLeagues(params, function (joinedLeaguesArray) {
                for(var i = 0, len = joinedLeaguesArray.length; i < len; i++){
                    joinedLeaguesArray[i].isOpen = false;
                }
                that.noMyLeaguesError = true;
                that.joinedLeaguesArray = joinedLeaguesArray;
                
            });
        }

        function fetchLeagueMembers(leagueid, index) {
            var params = {
                matchId: that.selectedMatch.matchId,
                leagueId: leagueid,
                limit: pageSize,
                limitSize: pageSize
            };
            that.leagueMembersArray = [];
            leagueservice.getLeagueMembers(params, function (leagueMembersArray) {
                that.leagueMembersArray = leagueMembersArray;

                var userTeam = leagueMembersArray.userTeam;
                that.currentTeamId = userTeam.currentTeam.teamId;
                // data to show in team status in ground section
                that.matchPlayersArray = leagueutilservice.updatePlayerPositionClass(userTeam.currentTeam.players);
                that.currentTeamInfo.teamId = userTeam.currentTeam.teamId;
                that.currentTeamInfo.teamName = userTeam.screenName;
                that.currentTeamInfo.teamPoints = userTeam.currentTeam.teamPoints;

                that.myTeamsArray = userTeam.teamsArr;
                var currentTeamCount = that.currentMatchTeamCount;
                var teamArr = [];
                for (var i = 1; i <= currentTeamCount; i++) {
                    teamArr.push({teamId: i});
                }
                that.currentTeamArr = teamArr;
                // Updating toprank and teamid on opening of my leagues accordion
                that.joinedLeaguesArray[index].rank = that.leagueMembersArray.leagueUsers[0].rank;
                that.joinedLeaguesArray[index].teamId = that.leagueMembersArray.leagueUsers[0].teamId;

            });
        }

        function checkAutoLeagueJoinFlow(){
            var invCode = utilityservice.getUrlParameter('invCode');
            var isAutoJoinFlow = (invCode !== undefined && invCode !== "");
            if(isAutoJoinFlow){
                var league = {
                    matchId: 1,
                    leagueId: invCode
                }
                leagueutilservice.validateJoinLeague(league);
            }
        }

        function highlightList(ulSelector, currentLi){
            if(currentLi === undefined){
                currentLi = document.querySelectorAll('.' + ulSelector + ' li:eq(0)');
            }
            var liList = document.querySelectorAll('.' + ulSelector + ' li');
            if(liList === undefined){
                return false;
            }
            for(var i = 0, len = liList.length; i < len; i++){
                liList[i].classList.remove('active');
            }
            currentLi.classList.add('active');
        }

        function resetLeagueAccordions(){
            for(var i = 0, len = that.joinedLeaguesArray.length; i < len; i++){
                that.joinedLeaguesArray[i].isOpen = false;
            }
        }

        $rootScope.$on('fetchLeaguesEvent', function(event, args){
            // Update league count for the current Match
            if(args.leagueCount !== undefined){
                that.selectedMatch.leagueCount = args.leagueCount;    
            }

            var invCode = utilityservice.getUrlParameter('invCode');
            if(invCode !== undefined && invCode !== ""){
                $window.location.href = $window.location.href.split('?')[0];    // Removing query string after successful league join if invCode is present
            }
            else{
                that.fetchLeagues(args.matchId);
                fetchJoinedLeagues();
            }
        });
}]);