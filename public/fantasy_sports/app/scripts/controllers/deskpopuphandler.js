'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:deskPopupHandlerCtrl
 * @description
 * # deskPopupHandlerCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('deskPopupHandlerCtrl',['$scope','$rootScope','ngDialog', function ($scope, $rootScope, ngDialog) {
    var that = this;
    that.showDeskPopup = function(options){

        var className = getClassName(options.popupSize)+" "+options.className;

        // Return false if template is not given
        if(options.template === '' || options.template === null){
            return false;
        }
        ngDialog.open({
            template: options.template,
            controller: options.controller,
            controllerAs: options.controllerAs,
            scope: options.scope,
            data: options.data,
            showClose: true/*options.showClose*/,
            closeByDocument: options.closeByDocument,
            closeByEscape: options.closeByEscape,
            overlay: options.overlay,
            preCloseCallback: options.preCloseCallback,
            /*appendTo: options.appendTo,*/
            className: className
        });
    };

    $scope.$on("showDeskPopup", function(event, data){
        var defaultOptions = {
            template: null,
            className: '',
            controller: null,
            controllerAs: null,
            scope: null,
            data: null,
            showClose: false,
            closeByDocument: false,
            closeByEscape: false,
            overlay: true,
            preCloseCallback: null,
            closeByNavigation: true,
            disableAnimation: true,
            appendTo: '#lobbycontainer',
            id: '',
            name: '',
            popupSize: 'desk-sm'
        };

        // Overriding default options with the given options
        var options = angular.extend({}, defaultOptions, data.options);
        ngDialog.close();

        that.showDeskPopup(options);
    });

    var getClassName = function(size){
        var ngDialogClass = '';
       switch(size){
            case 'desk-lg':
               ngDialogClass = 'ngdialog-theme-popup desk-lg';
                break;
            case 'desk-md':
                ngDialogClass = 'ngdialog-theme-popup desk-md';
                break;
            case 'desk-sm':
                ngDialogClass = 'ngdialog-theme-popup desk-sm';
                break;
            default:
                ngDialogClass = 'ngdialog-theme-popup';
        }
        return ngDialogClass;
    };

  }]);
