'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:headerCtrl
 * @description
 * # headerCtrl
 * Controller of the fantasyCricketApp
 */
 angular.module('fantasyCricketApp')
 .controller('headerCtrl', ['$rootScope','$scope', 'user','ngDialog', 'msgConstants', 'devicedetectionservice', 'utilityservice', 'config', 'session', '$window', '$location', 'serviceName','$http','$timeout', function ($rootScope,$scope, user, ngDialog, msgConstants, devicedetectionservice, utilityservice, config, session, $window, $location, serviceName,$http,$timeout) {
	var that = this;
    var isDesktop = devicedetectionservice.isDevice('DESKTOP');
	if(window.opener){
        // const url = '/private';
        // window.opener.open(url, '_self');
        // window.opener.focus();
        // window.close();
	}
	if(window.location.hash && window.location.hash){
		window.location.hash=''
	}

	 that.init = function (loginData) {
	 	if(loginData && loginData.token){
	 		session.setWebsiteToken(loginData.token);
	 		session.setScreenName(loginData.myName);
	 		session.setUserBalance(loginData.balance);
			session.setUserDOB(loginData.dob);
            session.setUserID(loginData.userId);
            session.setUserStatus(loginData.userStatus);
            if(loginData.userName !== undefined){
                session.setUserName(loginData.userName);
            }
			if(loginData.screenName !== undefined) {
				session.setTeamName(loginData.screenName);
			}
			if(loginData.isProduction){
				disableDevTools();	//Disabling right click and developer tools in browser
			}
	 	}
         /* Hightight mobile menu item ---Ashutosh */
         var a = window.location.href,
         b = a.lastIndexOf("/");
         utilityservice.setActiveMenu(a.substr(b + 1));

         //Add cash success failure response handling---Ashutosh
         $timeout(function(){
             var txnStatus = utilityservice.getUrlParameter("status");
             var txnID = utilityservice.getUrlParameter("txnid");
             var txnAmt = utilityservice.getUrlParameter("amount");
             var errCode = utilityservice.getUrlParameter("code");
             if(txnStatus !== undefined){
                 if(txnStatus == "success"){
                     var successMsg = "Cash amount of Rs "+txnAmt+" has been added to your LeagueAdda account, Now its time to win some real cash !!";
                     $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: successMsg,
                        popupSize: 'small',
                        okButtonText: 'OK',
                        okBtnCallback: function () {
                           $window.location.href = utilityservice.getBaseUrl() + "/myaccount";
                        }
                    });
                 }else {
                     var failureMsg = "";
                     if(errCode == 129){
                         failureMsg = msgConstants.TRANSACTION_FAILURE_1;
                     }else if(errCode == 131){
                         failureMsg = msgConstants.TRANSACTION_FAILURE_3;
                     }else if(errCode == 136){
                         failureMsg = msgConstants.ERR_FB_REGISTRATION;
                     }else {
                         failureMsg = msgConstants.TRANSACTION_FAILURE_2;
                     }
                     $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: failureMsg,
                        popupSize: 'small',
                        okButtonText: 'OK',
                        okBtnCallback: function () {
                           $window.location.href = utilityservice.getBaseUrl() + "/myaccount";
                        }
                    });
                 }
             }
         },100);

		$rootScope.serverTime = loginData.serverTime;
		$rootScope.serverDiff = loginData.serverTime - new Date().getTime();
	 }


	that.loginEmail = '';
	that.loginPassword = '';
	that.loginEmailError = '';
	that.loginPasswordError = '';

	that.onLoginClick = function(){

		var isSuccess = true;

		user.OnInputFieldValidation('loginEmail', that.loginEmail,'', function(response){
			if(response.resCode !== 100){
				isSuccess = false;
				that.loginEmailError = response.message;
			}
		});

		user.OnInputFieldValidation('loginPassword', that.loginPassword,'', function(response){
			if(response.resCode !== 100){
				isSuccess = false;
				that.loginPasswordError = response.message;
			}
		});

		if(isSuccess){
			var params = {};
			params.userData = that.loginEmail;
			params.password = CryptoJS.MD5(that.loginPassword)+'';
			utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.USER_LOGIN, params, function (response) {
				if(response.respCode === 100) {
					session.setWebsiteToken(response.respData.token);
					 if(isDesktop){
                        $window.location.href = utilityservice.getBaseUrl() + "/league";
                     } else{
                        $window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
                     }
				}else{
					that.loginEmailError = response.message;
				}
			});
		}
	}

	that.onLogoutClick = function(){
		var params = {};
		utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.USER_LOGOUT, params, function (response) {
				if(response.respCode === 100) {
					utilityservice.removeFromLocalStorage('currentMatchId');
					session.destroy();
				 	$window.location.href = utilityservice.getBaseUrl() + "/";
				}else{
					session.destroy();
					$window.location.href = utilityservice.getBaseUrl() + "/";
				}
			});
	}

	that.onForgotPasswordClick = function(){
		ngDialog.open({
			template: '/fantasy_sports/app/views/forgotpassword.html',
			overlay: true,
			closeByDocument: false,
			closeByEscape: false,
			showClose: true,
			closeByNavigation: false,
			disableAnimation: true,
			controller: 'forgotPasswordCtrl',
			controllerAs: 'forgot',
			appendTo: '#mainWrapper',
			className: 'ngdialog-theme-popup forgot-password-popup desk-sm'
		});
	}

	that.onFieldChange = function(fieldName, fieldValue, additionalInfo){
		user.OnInputFieldValidation(fieldName,fieldValue,additionalInfo, function(response) {
			if(response.resCode !== 100){
				that[fieldName+'Error'] = response.message;
			}else{
				console.log("resCode---->",response.resCode);
			}
		});
	};

	that.toggleMenu = function(){
		var mobMenuWrapper = document.querySelector('#mobMenuWrapper');
		if(mobMenuWrapper.classList.contains('open')){
			mobMenuWrapper.classList.remove('open');
		}
		else{
			mobMenuWrapper.classList.add('open');
		}
	}

	that.closeMenu = function(){
		if(document.querySelector('#mobMenuWrapper') != null) {
			var mobMenuWrapper = document.querySelector('#mobMenuWrapper');
			mobMenuWrapper.classList.remove('open');
		}
	}

	that.onChangePassword = function(){
		that.closeMenu();
		ngDialog.open({
			template: '/fantasy_sports/app/views/changepassword.html',
			overlay: true,
			closeByDocument: false,
			closeByEscape: false,
			showClose: false,
			closeByNavigation: false,
			disableAnimation: true,
			controller: 'ChangepasswordCtrl',
			controllerAs: 'changePassword',
			appendTo: '#mainWrapper',
			className: 'ngdialog-theme-popup forgot-password-popup desk-sm'
		});
	};

	that.addCash = function() {
		var data = {
			balance: 0,
			entryFee: 0,
			title: 'ADD MONEY TO YOUR ACCOUNT',
			showCurrentBalance: false,
		};
		ngDialog.open({
			template: '/fantasy_sports/app/views/addcash.html',
			controller:"addCashCtrl",
			controllerAs: "addcash",
			overlay: true,
			closeByDocument: false,
			closeByEscape: false,
			closeByNavigation: true,
			disableAnimation: true,
			appendTo: '#mainWrapper',
			className: 'ngdialog-theme-popup addcash-popup desk-sm',
			showClose: true,
			resolve: {
				leaguedata: function() {
					return data;
				}
			}
		});
	};

	that.onLogoClick = function(){
		if(utilityservice.isPresentInURL('createteam') || utilityservice.isPresentInURL('editteam')){
			return false;
		}
		$window.location.href = utilityservice.getBaseUrl() + "/";
	}

	function disableDevTools(){
		//disabling right click
		document.oncontextmenu = function(event) {
	        event.preventDefault();
	    }

	    window.addEventListener("keydown", function(event){
	    	if(event.keyCode == 123){
	    		event.preventDefault();
			    return false;
			   }
			else if(event.ctrlKey && event.shiftKey && event.keyCode == 73){        
			      event.preventDefault();
			      return false;  //Prevent from ctrl+shift+i
			   }
	    });
	}

 }]);
