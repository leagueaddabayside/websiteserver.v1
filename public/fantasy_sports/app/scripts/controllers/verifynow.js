'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:VerifynowCtrl
 * @description
 * # VerifynowCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
  .controller('VerifynowCtrl',['$rootScope', '$scope', 'constants', '$window', 'config', 'serviceName', 'utilityservice', 'devicedetectionservice', 'session', 'user', 'Upload', 'msgConstants','dateDropdownHandler' , function ($rootScope, $scope, constants, $window, config, serviceName, utilityservice, devicedetectionservice, session, user, Upload, msgConstants, dateDropdownHandler) {
      var that = this;
      that.userStatus = session.getUserStatus();
      that.selectedState = "Select State";
      that.statesArray = constants.STATES_LIST;
      that.isMobileVerified = false;
      that.ismOTPsent = true;
      that.isverifyMobile = false;
      that.isEmailVerified = false;
      that.iseOTPsent = true;
      that.isverifyEmail = false;
      that.mobileOTP = '';
      that.emailOTP = '';
      var isDesktop = devicedetectionservice.isDevice('DESKTOP');
      that.isPanDisabled = true;
      that.isBankDisabled = true;
      that.isPanCardVerified = "NO";
      that.isBankDetailVerified = "NO";

      //Calender Module functionality Starts here---Ashutosh
      that.selectedMonth = "MM";
      that.selectedYear = "YY";
      that.selectedDate = "DD";
      that.dobRange = [];
      that.dobRange.push("YY");
      that.monthName = constants.MONTH_LIST;
      that.dateRange = [];
      dateDropdownHandler.initilizeYears(function(yearRange){
            that.dobRange = yearRange;
      });
      dateDropdownHandler.initilizeDays(function(dayRange){
            that.dateRange = dayRange;
      });

      that.getKey = function(month) {
            return Object.keys(month)[0];
      };
      // On year change function--Ashu.
      that.changeYear = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getMonths(that.selectedYear, function(monthList){
                  that.monthName = monthList;
            });
            that.checkDays();
      };
      // On day change function
      that.dayChange = function(){
            that.dateEmptyvalidator();
      };

      // On month change calucate days to show in dropdown--Ashu
      that.checkDays = function() {
            that.dateEmptyvalidator();
            dateDropdownHandler.getDays(that.selectedYear, that.selectedMonth, function(dayList){
                  that.dateRange = dayList;
                  if(that.selectedDate > dayList.length-1) {
					  that.selectedDate = "DD";
				  }
            });
      };

      that.dateEmptyvalidator = function(){
            if(that.selectedYear=="YY" || that.selectedMonth=="MM" || that.selectedDate=="DD") {
                that.userDateMsg = "Please fill all date fields.";
            }else {
                that.userDateMsg = '';
            }
      };
      //Calender Module functionality Ends

      that.checkAlphabet = function(event,fieldName){
            that.userMobileMsg='';
            var keyCode = event.keyCode;
            if(keyCode<48 || keyCode>57 ){
               that[fieldName] = that[fieldName].replace(/[^0-9]/g, '');
               //event.preventDefault();
            }
      };

      angular.element(document).ready(function () {
            utilityservice.showLoader();
            var params = {};
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.GETUSER_PROFILE, params, function (response) {
                utilityservice.hideLoader();
                if(response.respCode === 100){
                    that.userEmail = response.respData.emailId;
                    if(response.respData.mobileNbr !== null){
                        that.userMobile = response.respData.mobileNbr;
                    }
                    if(response.respData.isPanCardVerified !== null) {
                          that.isPanCardVerified = response.respData.isPanCardVerified;
                    }
                    if(response.respData.isBankDetailVerified !== null){
                          that.isBankDetailVerified = response.respData.isBankDetailVerified;
                    }
                    if(that.isBankDetailVerified == "NO"){
                          that.stateCheck = false;
                    }else{
                          that.stateCheck = true;
                    }

                    if(response.respData.isEmailVerified.toUpperCase() == "NO"  || response.respData.isMobileVerified.toUpperCase() == "NO"){
                          that.isPanDisabled = true;
                          that.isBankDisabled = true;
                    }else{
                          that.isPanDisabled = false;
                          that.isBankDisabled = false;
                    }

                    if(response.respData.isEmailVerified.toUpperCase() == "NO"){
                        that.isEmailVerified = false;
                        that.isverifyEmail = false;
                        that.iseOTPsent = true;
                    }else if(response.respData.isEmailVerified.toUpperCase() == "YES"){
                        that.isverifyEmail = false;
                        that.iseOTPsent = false;
                        that.isEmailVerified = true;
                    }

                    if(response.respData.isMobileVerified.toUpperCase() == "NO"){
                        that.isMobileVerified = false;
                        that.isverifyMobile = false;
                        that.ismOTPsent = true;
                    }else if(response.respData.isMobileVerified.toUpperCase() == "YES"){
                        that.isverifyMobile = false;
                        that.ismOTPsent = false;
                        that.isMobileVerified = true;
                    }

                    if(response.respData.state!= null && response.respData.state!= ''){
                          that.selectedState = response.respData.state;
                    }
                    if(response.respData.dob != null) {
                          that.User_DOB = new Date(response.respData.dob);
                          that.selectedMonth = String(that.User_DOB.getMonth() + 1);
                          if(that.selectedMonth < 9) {
                              that.selectedMonth = '0'+that.selectedMonth;
                          }
                          that.selectedDate = String(that.User_DOB.getDate());
                          if(that.selectedDate < 9) {
                              that.selectedDate = '0'+that.selectedDate;
                          }
                          that.selectedYear = that.User_DOB.getFullYear();
                    }
                }else {
                    $rootScope.$broadcast("showPopup", {
                        type: "Info",
                        title: msgConstants.HEADER_INFO,
                        appendTo:'#mainWrapper',
                        msg: response.message,
                        popupSize: 'small',
                        okButtonText: 'OK'
                    });
                }
            });
      });

      that.showLeaguepage = function() {
            if(isDesktop){
                  $window.location.href = utilityservice.getBaseUrl() + "/league";
            } else{
                  $window.location.href = utilityservice.getBaseUrl() + "/match-leagues";
            }
      };

      that.sendOtpMobile = function() {
            var isValidate = true;
            that.userMobileMsg = '';
            user.OnInputFieldValidation('userMobile', that.userMobile,'', function(response){
                if(response.resCode !== 100){
                    isValidate = false;
                    that.userMobileMsg = response.message;
                }
            });

            if(isValidate){
                  var params = {
                        mobileNumber:that.userMobile
                  };
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.VERIFY_MOBILE, params, function (response) {
                        if(response.respCode === 100){
                              that.isMobileVerified = false;
                              that.ismOTPsent = false;
                              that.isverifyMobile = true;
                        }else {
                              that.userMobileMsg = response.message;
                        }
                  });
            }
      };

      that.sendOtpEmail = function() {
            var isValidate = true;
            that.userEmailMsg = '';
            user.OnInputFieldValidation('userEmail', that.userEmail,'', function(response){
                if(response.resCode !== 100){
                    isValidate = false;
                    that.userEmailMsg = response.message;
                }
            });

            if(isValidate){
                  var params = {
                        emailId: that.userEmail
                  };
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.VERIFY_EMAIL, params, function (response) {
                        if(response.respCode === 100){
                              that.isEmailVerified = false;
                              that.iseOTPsent = false;
                              that.isverifyEmail = true;
                        }else {
                              that.userEmailMsg = response.message;
                        }
                  });
            }
      };

      that.mobileVerifyOTP = function() {
            if(that.mobileOTP.length == 6){
                  var params = {
                        otp: that.mobileOTP
                  };
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.OTP_MOBILE, params, function (response) {
                        if(response.respCode === 100){
                              that.ismOTPsent = false;
                              that.isverifyMobile = false;
                              that.isMobileVerified = true;
                              if(that.isEmailVerified == true && that.isMobileVerified == true) {
                                    that.isPanDisabled = false;
                                    that.isBankDisabled = false;
                              }else{
                                    that.isPanDisabled = true;
                                    that.isBankDisabled = true;
                              }
                        }else{
                              that.mobileOTPMsg = response.message;
                        }
                  });
            }else {
                  that.mobileOTPMsg = msgConstants.ERR_OTP;
            }
      };

      that.emailVerifyOTP = function() {
            if(that.emailOTP.length == 6){
                  var params = {
                        otp: that.emailOTP
                  };
                  utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.OTP_EMAIL, params, function (response) {
                        if(response.respCode === 100){
                              that.iseOTPsent = false;
                              that.isverifyEmail = false;
                              that.isEmailVerified = true;
                              if(that.isEmailVerified == true && that.isMobileVerified == true) {
                                    that.isPanDisabled = false;
                                    that.isBankDisabled = false;
                              }else{
                                    that.isPanDisabled = true;
                                    that.isBankDisabled = true;
                              }
                        }else{
                              that.emailOTPMsg = response.message;
                        }
                  });
            }else {
                  that.emailOTPMsg = msgConstants.ERR_OTP;
            }
      };

      that.submitPanDetails = function(){
            var isValidate = true;
            that.errMsg = '';
            if($scope.files != undefined && $scope.files !== null){
                  if($scope.files.length == 0) {
                        isValidate = false;
                        that.errMsg = msgConstants.ERR_PAN_3;
                  }
            }else{
                  isValidate = false;
                  that.errMsg = msgConstants.ERR_PAN_3;
            }

            if(that.isValidDoc === false){
                  isValidate = false;
                  that.errMsg = msgConstants.ERR_PAN_5;
                  that.fileName = '';
                  $scope.files = [];
            }

            if(that.selectedState == "Select State") {
                  isValidate = false;
                  that.errMsg = msgConstants.ERR_STATE;
            }
            user.OnInputFieldValidation('userPAN', that.userPAN,'', function(response) {
                if(response.resCode !== 100){
                    isValidate = false;
                    that.errMsg = response.message;
                }
            });
            if(that.userName !== undefined && that.userName !== null){
                 if(that.userName.length === 0){
                     isValidate = false;
                     that.errMsg = msgConstants.ERR_FULLNAME;
                 }else if(that.userName.length < 2 || that.userName.length > 20){
                     isValidate = false;
                     that.errMsg = msgConstants.ERR_FULLNAME_3;
                 }else if (!user.aplphaNumericWithSpace.test(that.userName)) {
                     isValidate = false;
                     that.errMsg = msgConstants.ERR_FULLNAME_2;
                 }
            }else{
                isValidate = false;
                that.errMsg = msgConstants.ERR_FULLNAME;
            }

            if(isValidate){
                Upload.upload({
                    url: config.websiteNodeHost + serviceName.PAN_UPLOAD,
                    data: {panCardImage: $scope.files[0], 'token': session.getWebsiteToken(),'name':that.userName, 'panNumber':that.userPAN, 'state':that.selectedState}
                }).then(function (resp) {
                    that.fileName = '';
                    $scope.files = [];
                    if(resp.data.respCode == 100){
                          $rootScope.$broadcast("showPopup", {
                              type: "Info",
                              title: msgConstants.HEADER_INFO,
                              appendTo:'#mainWrapper',
                              msg: msgConstants.MSG_PANCARD_SAVED_SUCCESS,
                              popupSize: 'small',
                              okButtonText: 'OK',
                              okBtnCallback: function () {
                                    $window.location.reload();
                              }
                          });
                    }else{
                          that.errMsg = resp.data.message;
                    }
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }/*, function (evt) {
                   var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                   console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                }*/);
            }
      };

      $scope.saveImageFile = function (files, errFiles) {
            that.errMsg='';
            $scope.files = [];
            that.isValidDoc = false;
            that.fileName ='';
            that.errFile = errFiles;
            if (files != '') {
                  $scope.files.push(files[0]);
                  $scope.imageCollection = $scope.files;
                  var fileType = $scope.imageCollection[0].type;
                  var type = fileType.split('/')[1];
                  if(type === 'jpg' || type === 'png' || type === 'jpeg' || type === 'bmp' || type === 'pdf'){
                        that.isValidDoc = true;
                  }
                  that.fileName = $scope.imageCollection[0].name;
            }
            if(errFiles != ''){
                  that.errFile = errFiles && errFiles[0];
                  if(that.errFile !== undefined || that.errFile !== '') {
                        that.errMsg = msgConstants.ERR_LARGE_FILE;
                        //console.log("Error File selection--->",that.errFile);
                  }
            }
      };

      that.submitBankDetails = function() {
            var isValidate = true;
            that.errBankMsg = '';

           if(!that.stateCheck){
                 isValidate = false;
                 that.errBankMsg = msgConstants.ERR_STATE_CHECK;
           }

           if(that.branchName !== undefined && that.branchName !== null){
                if(that.branchName.length === 0){
                    isValidate = false;
                    that.errBankMsg = msgConstants.ERR_BRANCH_NAME;
                }
           }else{
                isValidate = false;
                that.errBankMsg = msgConstants.ERR_BRANCH_NAME;
           }

           if(that.bankName !== undefined && that.bankName !== null){
                if(that.bankName.length === 0){
                    isValidate = false;
                    that.errBankMsg = msgConstants.ERR_BANK_NAME;
                }
           }else{
                isValidate = false;
                that.errBankMsg = msgConstants.ERR_BANK_NAME;
           }

           if(that.ifscCode !== undefined && that.ifscCode !== null){
                if(that.ifscCode.length === 0){
                    isValidate = false;
                    that.errBankMsg = msgConstants.ERR_IFSC_CODE;
                }
           }else{
                isValidate = false;
                that.errBankMsg = msgConstants.ERR_IFSC_CODE;
           }

           if(that.accountNo !== undefined && that.accountNo !== null){
                if(that.accountNo.length === 0){
                    isValidate = false;
                    that.errBankMsg = msgConstants.ERR_BANK_ACCOUNT;
                }
           }else{
                isValidate = false;
                that.errBankMsg = msgConstants.ERR_BANK_ACCOUNT;
           }

           if(isValidate) {
                 var params = {};
                 params.userId = session.getUserID();
                 params.bankName = that.bankName;
                 params.accountNumber = that.accountNo;
                 params.branch = that.branchName;
                 params.ifscCode = that.ifscCode;
                 utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.ADD_BANK_ACCOUNT, params, function (response) {
                        if(response.respCode === 100){
                              $rootScope.$broadcast("showPopup", {
                                    type: "Info",
                                    title: msgConstants.HEADER_INFO,
                                    appendTo:'#mainWrapper',
                                    msg: msgConstants.MSG_BANK_DETAIL_SAVED_SUCCESS,
                                    popupSize: 'small',
                                    okButtonText: 'OK',
                                    okBtnCallback: function () {
                                          $window.location.reload();
                                    }
                              });
                        }else {
                              that.errBankMsg = response.message;
                        }
                 });
           }

      };

      that.getPanDetail = function(){
            utilityservice.showLoader();
            var params = {};
            params.userId = session.getUserID();
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.PAN_DETAILS, params, function (response) {
                  utilityservice.hideLoader();
                  if(response.respCode === 100){
                        that.userName = response.respData.name;
                        that.userPAN = response.respData.panNumber;
                        that.selectedState = response.respData.state;
                  }else {
                        console.log("PAN DETAILS ERROR--",response.message);
                  }
           });
      };

      that.getBankDetail = function(){
            utilityservice.showLoader();
            var params = {};
            params.userId = session.getUserID();
            utilityservice._postAjaxCall(config.websiteNodeHost + serviceName.BANK_DETAILS, params, function (response) {
                  utilityservice.hideLoader();
                  if(response.respCode === 100){
                        that.bankName = response.respData.bankName;
                        that.accountNo = response.respData.accountNumber;
                        that.branchName = response.respData.branch;
                        that.ifscCode = response.respData.ifscCode;
                  }else {
                        console.log("BANK DETAILS ERROR--",response.message);
                  }
           });
      };


  }]);
