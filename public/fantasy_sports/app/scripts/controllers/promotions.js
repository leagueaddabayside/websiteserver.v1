'use strict';

/**
 * @ngdoc function
 * @name fantasyCricketApp.controller:PromotionCtrl
 * @description
 * # PromotionCtrl
 * Controller of the fantasyCricketApp
 */
angular.module('fantasyCricketApp')
    .controller('PromotionCtrl', ['$window','$rootScope','$timeout', 'promotionsservice', 'utilityservice', function ($window,$rootScope,$timeout, promotionsservice, utilityservice) {
        var that = this;

        that.init = function(){
            var params ={};
            promotionsservice.getPromotions(params, function(response){
                //console.log("data from service",response);
                that.promotionsData = response;

            });  
        }


        that.showPromtionDetails = function(e, index){
            var promotionImageUrl = that.promotionsData[index].fields.popup_banner_desktop.url;
            var imgDom = document.createElement("img");
            imgDom.src = promotionImageUrl;
            imgDom.className = "w100"; 
            var imgWrapper = document.createElement("div");
            imgDom.appendChild(imgWrapper);
            
            var tmp = document.createElement("div");
            tmp.appendChild(imgDom);


            that.promoDetailsData = {};
            that.promoDetailsData.headerName = that.promotionsData[index].name;
            that.promoDetailsData.content = "<div class='clearfix h100 w100 promotion-details-wrapper'><div class= 'w50 left-float'>"+tmp.innerHTML+"</div><div class='w40 pd-h2_5 right-float'>"+that.promotionsData[index].fields.popup_content.text+"</div>";
            //console.log(that.promoDetailsData.content);
            that.promoDetailsData.pageCss = that.promotionsData[index].fields.popup_content_css.text;
            that.promoDetails = true;
            $timeout(function(){
                that.iframeLoadedCallBack();
            },200);
        };

        that.hidePromtionDetails = function(){
            that.promoDetails = false;
            //var wrapper = angular.element(document.querySelector(".promotions-bg"));
           // wrapper.removeClass("popup-bg");
        };

        that.iframeLoadedCallBack = function () {
            var cssLink = that.promoDetailsData.pageCss;
            var pageData = '';
            pageData = that.promoDetailsData.content;
            var iFrame = angular.element(document.querySelector('iframe.promo-details-popup'))[0];

            var iFrameBody;
            if (iFrame.contentWindow.document){ // FF
                iFrameBody = angular.element(iFrame.contentDocument.getElementsByTagName('body')[0]);
            } else if (iFrame.contentWindow.document){ // IE
                iFrameBody = angular.element(iFrame.contentWindow.document.getElementsByTagName('body')[0]);
            }

            var iFrameHead;
            if (iFrame.contentWindow.document){ // FF
                iFrameHead = angular.element(iFrame.contentDocument.getElementsByTagName('head')[0]);
            } else if (iFrame.contentWindow.document){ // IE
                iFrameHead = angular.element(iFrame.contentWindow.document.getElementsByTagName('head')[0]);
            }
            iFrameHead.append(cssLink);
            iFrameBody.append(pageData);
        };


    }]);