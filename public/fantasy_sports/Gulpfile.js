

var gulp = require('gulp'),
    //cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    cssmin = require('gulp-cssmin'),
    uglify = require('gulp-uglify');
    //imagemin = require('gulp-imagemin'),
    // notify = require('gulp-notify'),
    // cache = require('gulp-cache'),
    // del = require('del')
    //;

    var browserSync = require('browser-sync').create(); // create a browser sync instance.


var config = {
    BASE_DIR: '../..',
    SERVER_URL: 'localhost:3000'
}

var paths = {
  styles: {
    src: 'app/styles/**/*.scss',
    dest: 'app/styles',
    watch: 'app/styles/**/*.scss'
  },
  scripts: {
    src: ['app/scripts/**/*.js', '!app/scripts/scripts.js', '!app/scripts/scripts.min.js'],     // ignore srcipts.js and scripts.min.js 
    dest: 'app/scripts'
  }
};

gulp.task('browser-sync', function() {
    browserSync.init({
        // server: {
        //     baseDir: "app"
        // }
        proxy: config.SERVER_URL // makes a proxy for localhost:8080
    });
});

/* Reload task */
gulp.task('bs-reload', function () {
    browserSync.reload();
});

// JS hint task
gulp.task('jshint', function() {
  gulp.src(paths.scripts.src)
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// SASS task to complile scss files and add vendor prefixes
gulp.task('sass', function(){
  return gulp.src(paths.styles.src)
    .pipe(sourcemaps.init())
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe( autoprefixer() )     // Add Vendor Prefixes with gulp-autoprefixer
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.reload({stream: true})); // prompts a reload after compilation
});

/* Watch scss, js and html files, doing different things with each. */
gulp.task('serve', ['sass', 'browser-sync'], function () {
    /* Watch scss, run the sass task on change. */
    gulp.watch('app/styles/**/*.scss',['sass']);
    /* Watch .js files, run the bs-reload task on change. */
    gulp.watch('app/scripts/**/*.js',['bs-reload'])
    /* Watch .html files, run the bs-reload task on change. */
    gulp.watch([config.BASE_DIR + '/**/*.html'], ['bs-reload']);
});


 // JS minify task

gulp.task('minifyjs', ['concat-js', 'minify-js']);

gulp.task('concat-js', function() {  
    return gulp.src(paths.scripts.src)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(paths.scripts.dest));
});

gulp.task('minify-js', function() {  
    return gulp.src(paths.scripts.src)
        .pipe(concat('scripts1.js'))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts.dest));
});

 // CSS minify task
gulp.task('minifycss', function() {  
    return gulp.src(paths.styles.src)
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe( autoprefixer() )     // Add Vendor Prefixes with gulp-autoprefixer
        .pipe(cssmin({ sourceMap: false })) // Minify Css files
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(paths.styles.dest))
});